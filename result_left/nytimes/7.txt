Senator Bernie Sanders, the Vermont independent, announced Thursday that he was running for president as a Democrat, injecting a progressive voice into the contest and providing Hillary Rodham Clinton with her first official rival for the party’s nomination.

Avoiding the fanfare that several Republicans have chosen so far when announcing their candidacies, Mr. Sanders issued a statement to supporters that laid out his goals for reducing income inequality, addressing climate change and scaling back the influence of money in politics.

“After a year of travel, discussion and dialogue, I have decided to be a candidate for the Democratic nomination for president,” Mr. Sanders said in an email early Thursday.

Mr. Sanders’s bid is considered a long shot, but his unflinching commitment to stances popular with the left — such as opposing foreign military interventions and reining in big banks — could force Mrs. Clinton to address these issues more deeply.

On a patch of grass known as the Swamp outside the Capitol, Mr. Sanders later articulated before a horde of journalists and a few curious onlookers why he was running. He acknowledged that he faced big financial challenges but said that, as a politician with the “most unusual political history of anyone in Congress,” he was optimistic about his chances.

“We’re in this race to win,” he said.

Mr. Sanders plans to hold a formal campaign kickoff on May 26 in Burlington, Vt., where he was once mayor, but he will hit the campaign trail this weekend, holding an event in Manchester, N.H., where the nation’s first primary is being held in February. And he is expected to make accessibility a hallmark of his campaign.

“This is not going to be, ‘I’ll give you two questions and see you later,’ ” an adviser said, in an apparent reference to Mrs. Clinton’s campaign style.

Mr. Sanders, 73, has said that he will not run a negative campaign and that he has never run an attack ad in his life. A self-described “Democratic socialist” and grumpy grandfather-type, Mr. Sanders has promised to steer the Democratic Party toward a mature debate about the issues he is passionate about.

While traveling the country considering a run, Mr. Sanders has acknowledged that going up against Mrs. Clinton will be a daunting financial challenge. He has $4.6 million available for his 2018 Senate re-election campaign that he can use for a presidential run, and Mr. Sanders said he hoped to galvanize a movement of small donors to give himself a fighting chance.

“We’re not going to raise $2 billion, and we’re not going to raise $1 billion,” said Mr. Sanders, who added that he did not intend to use the help of a “super PAC.” “I do not have millionaire or billionaire friends.”

In a speech at the National Press Club in Washington on March 9, Mr. Sanders said he fantasized about getting 3 million supporters to each donate $100 to his campaign. The total, he joked, would be about a third of what the billionaire Koch brothers planned to spend to elect a Republican president.

Born in Brooklyn, Mr. Sanders served for 16 years as Vermont’s sole congressman before he was elected to the Senate in 2006. He often brags about being the longest-serving independent in congressional history, but he plans to run for president as a Democrat to avoid the obstacles of getting on state ballots and participating in debates.

Democrats who have been hoping for more liberal candidates to enter the race, such as Senator Elizabeth Warren of Massachusetts or former Gov. Martin O’Malley of Maryland, cheered Mr. Sanders’s decision.

“Having Bernie Sanders in the race, calling for populism, will help open the political space for people like Hillary Clinton and others to take bold stands,” said Adam Green, a co-founder of the Progressive Change Campaign Committee.

If anything, Mr. Sanders, who embraces his reputation for being gruff, abrupt and honest, promises to be bold. Recalling that he has defeated Democrats and Republicans with far greater financial resources in his long career, Mr. Sanders suggested that his campaign should not be taken lightly.

“I think people should be a little bit careful underestimating me,” he said.