Six weeks after learning that investigators had focused on his role in the years-old disappearance of his wife, Robert A. Durst married Debrah Lee Charatan, a driven New York real estate executive who has been a bulwark in his life for nearly three decades.

Eight days later, on Dec. 19, 2000, Mr. Durst, the eccentric and estranged son of one of New York’s most prominent real estate families, was on a plane to California, where Los Angeles authorities now believe he executed a close friend and confidante, Susan Berman, who had served as his shield against demanding reporters.

Ms. Charatan was his first phone call the following year, after he was arrested in Galveston, Tex., for the murder and dismemberment of a neighbor. She served as his de facto banker with access to his financial accounts and visited him in prison in Texas.

But if Ms. Charatan relied on Mr. Durst during her own divorce and custody battle and supported him when he was accused of a gruesome killing, their relationship has taken a sharp turn in recent years. Whether she will stand by him again amid another murder charge remains an intriguing question.

She used $20 million — her share of Mr. Durst’s $65 million settlement of his claims against a family trust — to start a new real estate company with her son. Today, she lives not with Mr. Durst, but with one of his many lawyers, Steven I. Holm.

She continues to invest Mr. Durst’s money in real estate, but she has put distance between herself and Mr. Durst — no more trips with him to Amsterdam, Japan or a tennis camp in Florida.

After Mr. Durst adopted an alias and drove to New Orleans several weeks ago in what the authorities believe was a nascent plan to flee the country, perhaps to Cuba, his first phone call was to another woman in New York. That woman sent him a piece of luggage containing, the authorities said, shoes and $117,000 in cash. By then, Mr. Durst was in police custody.

Still, Ms. Charatan remains, at least on paper, married to Mr. Durst.

Mr. Durst, who is 71 and is worth an estimated $100 million, faces gun possession and drug charges in New Orleans and is being held in a psychiatric correctional center outside the city because of concerns about his mental health.

He was arrested in a New Orleans hotel room on March 14 and is wanted in Los Angeles, where he could face the death penalty in the killing of Ms. Berman.

Ms. Charatan, like Mr. Durst’s lawyer, had advised him against participating in an HBO documentary, “The Jinx: The Life and Deaths of Robert Durst,” which concluded with what seemed to be Mr. Durst’s whispered confession, caught on a recording. “What the hell did I do?” he said. “Killed them all, of course.”

In January, before the series was broadcast, Mr. Durst said in an interview, “My wife, Debrah Charatan, is extremely formidable and always has a strong opinion about what I do.”

Investigators in Los Angeles want to talk to Ms. Charatan about Mr. Durst’s whereabouts and actions before and after they were married. They believe that Ms. Charatan, like Ms. Berman in Los Angeles, knew some of Mr. Durst’s secrets. But Ms. Charatan is protected by law from being compelled to testify against her husband.

Ms. Charatan could waive the spousal privilege to discuss what she learned about Mr. Durst. But he could legally block her from disclosing any confidential communications, Dan Bookin, a criminal defense lawyer in San Francisco, said.

The Los Angeles district attorney declined to comment about Ms. Charatan.

Ms. Charatan, 58, who is described as a keen-eyed investor with very sharp elbows by those who know her, has long refused to talk about Mr. Durst, and she declined to be interviewed for this article. She has her admirers among developers like Larry Silverstein and the parking magnate Jerry Gottesman.

“I always found dealing with Debbie to be very straightforward,” said Robert A. Knakal, the chairman of real estate investment sales at Cushman & Wakefield in New York. “She seems to be doing very well as an investor now.”

But there are other developers, including Leonard Stern and Steven C. Witkoff, whom she has offended. There is also a legion of female real estate brokers who say they were cheated out of commissions when they worked for her.

At one time in the late 1980s and early 1990s, there were hundreds of thousands of dollars in court judgments against Ms. Charatan. Most of those brokers, however, were left empty-handed after her company filed for bankruptcy.

Mr. Durst and Ms. Charatan were introduced by a mutual friend in 1988 at a real estate dinner. There was an immediate attraction. He was a member of a real estate dynasty who admired that she had built her own company in a male-dominated industry after emerging from a working-class background in Howard Beach, Queens.

“I knew if I couldn’t be a star,” she said in the 1980s, “I wouldn’t be happy.”

During that time, a Manhattan Inc. magazine article, “Money Dearest,” chronicled her rise and the less appealing side of her relentless drive to be rich.

The couple shared childhoods marked by tragedy and being alienated from their families.

Mr. Durst’s mother committed suicide in 1950, jumping from the roof of their home in Scarsdale, a wealthy suburb in Westchester County. In 1982, his first wife, Kathleen Durst, vanished. He said he was as baffled as her family and friends, who have always suspected Mr. Durst had something to do with it.

Ms. Charatan’s parents survived the Nazis in Poland during World War II. Her father, a kosher butcher, lost his foot to a land mine. Her mother was an orphan hidden by a Christian family. Ms. Charatan’s personality was infused with their survivalist mentality, friends and relatives said.

But if Ms. Charatan strove to be wealthy and accepted among the elite, Mr. Durst enjoyed watching people’s reactions as he flouted convention by burping or smoking marijuana at social functions.

Ms. Charatan was going through a difficult divorce and custody battle when the two met. Mr. Durst helped her financially, providing a stack of car vouchers from the family business and using family funds to pay some of her legal bills, according to Durst family members and people who worked for Ms. Charatan.

She eventually got back on her feet, but lost custody of her son, Bennat Charatan Berger, then 5, to whom she did not speak for the next 15 years.

Mr. Durst broke with his family late in 1994, after his father, Seymour, picked his younger brother Douglas to take the reins of the Durst Organization.

A brokenhearted Seymour Durst tried to make contact with Robert Durst through Ms. Charatan. But on the day in 1995 she arrived at his townhouse on the Upper East Side of Manhattan, he had a stroke. A short time later, he died. Robert did not attend the funeral.

After the break with his family, Mr. Durst moved restlessly between homes in California, Connecticut, New York and Texas. His one constant: Ms. Charatan, though the only time the couple ever lived together was a short stint before they were married.

Mr. Durst, by his own account, panicked in late October 2000, after learning that investigators had reopened the investigation into his first wife’s disappearance. In rapid order, he proposed to Ms. Charatan and gave her power of attorney over his bank accounts and business affairs.

“I wanted Debbie to be able to receive my inheritance, and I intended to kill myself,” he said in a 2005 deposition related to his suit against his family.

If he died, Mr. Durst did not want his share of the family fortune going back into the family coffers.

“It was our understanding that unless I was married to him, I couldn’t benefit from any of the trusts,” Ms. Charatan said in her own deposition.

She picked a rabbi out of the phone book and the couple wed in an office building in New York City, across from one of his brother Douglas’s great triumphs, a 50-story skyscraper at 4 Times Square.

Mr. Durst, wanting to disappear, arranged to rent a $300-a-month apartment in a rooming house in Galveston, posing as a mute woman.

Less than a week after the wedding, Mr. Durst flew from New York to California.

In a videotape of a 2005 deposition that was broadcast in “The Jinx,” Mr. Durst said that Ms. Berman had called him before her death, saying: “The Los Angeles police contacted me. They wanted to talk about Kathie Durst’s disappearance.”

Ms. Berman was killed early in the morning on Dec. 23, 2000, shot in the back of the head. Mr. Durst arrived back in New York on Dec. 24. Mr. Durst and Ms. Charatan spent Christmas in the Hamptons.

Mr. Durst was arrested in Galveston in October 2001 for the murder and dismemberment of his neighbor, Morris Black. Ms. Charatan wired him $300,000 for bail. He went on the run. She then tried to withdraw more than $1.8 million from his accounts but was thwarted by the police.

After Mr. Durst was captured, Ms. Charatan flew frequently to Galveston, before he was acquitted of the murder charges after claiming self-defense.

During a recorded telephone call while Mr. Durst was in jail, Ms. Charatan, who seems to have a special appreciation of Mr. Durst’s psychic pain, is heard railing against his brother Douglas and a lawyer hired by the Durst family.

“He screwed you out of everything, your birthright, the entire Durst Organization,” she said.

“He took over the family business,” Mr. Durst responded, sounding put upon. “No doubt about it.”

“He took it from you,” she said. “He could have done it with you. But no, he took it from you.”

This time, as Mr. Durst is in another prison, Ms. Charatan is busy expanding her new investment office in Union Square in Manhattan. She has sold the Fifth Avenue co-op that Mr. Durst had helped her buy in 1997, as well as their house in Bridgehampton.

A friend of Ms. Charatan’s told CNN that she had not spoken to Mr. Durst since the first episode of the “The Jinx” was broadcast on Feb. 8.