OMAHA —  Mayor Bill de Blasio, dismayed by a Democratic Party that he believes has moved too slowly to embrace a populist platform, arrived in the Midwest on Wednesday with an audacious mission: leading the nation leftward.

On a two-day tour of Nebraska and Iowa — more than 1,200 miles from the New York City Hall where he has presided for 15 months — Mr. de Blasio is seeking to transcend his relative obscurity and jump-start a countrywide movement to promote liberal policies like raising taxes on the rich.

Already, the mayor’s effort is drawing scrutiny. His refusal this week to endorse the presidential candidacy of Hillary Rodham Clinton, his former boss, spurred criticism from more centrist Democrats, who questioned whether the mayor had earned the credibility to drive an insurgency within his party.

Mr. de Blasio’s aides did not anticipate the fierceness of the backlash to his comments about Mrs. Clinton, according to several people familiar with their thinking, but the episode underscored the notion that he could be positioned as the standard-bearer for the American left.

That is an image the mayor is keen to cultivate. A profile in Rolling Stone magazine is in the works. A forum for presidential candidates is being planned. The mayor will travel to Milwaukee and Washington this spring, and his aides are eyeing a West Coast trip to confer with liberal leaders in California.

First, however, the cerebral Mr. de Blasio will have to overcome a less lofty problem: basic name recognition. In Iowa, where he will appear on Thursday with former Senator Tom Harkin, political strategists said he remained unknown.

“Honestly, I think most people think Bloomberg is mayor of New York,” said Jeff Link, a Democratic consultant based in Des Moines, who helped arrange the mayor’s visit with Mr. Harkin.

“He’s a blank slate,” Mr. Link added, “which gives him the opportunity to fill that in.”

Mr. de Blasio began that work inside a University of Nebraska lecture hall here on Wednesday evening, telling the audience that “the rich and powerful are dominating the political discourse, which means they rig the game in their favor.”

The mayor called for higher wages, a national mandate for sick workers to receive paid time off and the abolition of a loophole used by hedge fund managers to pay a lower tax rate on their income. He dismissed the notion that his ideas might be seen as a form of class warfare, saying that President Franklin D. Roosevelt faced a similar critique.

“It’s never politically safe, or easy, to ask the most privileged Americans to pay more in taxes,” Mr. de Blasio said.

Still, some prominent Democrats say they are concerned that Mr. de Blasio, who has a short track record as mayor, is more focused on symbolism than substance.

The mayor “wants to be a big shot,” said William M. Daley, President Obama’s former chief of staff. If Mr. de Blasio wants a national profile, Mr. Daley said, he should “lay out specifics for urban America that are doable, not just, ‘We want the wealthy to be taxed more so we can spend their money somehow.’”

“Something specific that’s urban related,” Mr. Daley added. “Not just something amorphous, like ‘the progressive agenda.’”

Friends and colleagues of Mr. de Blasio’s say that his idea for a liberal coalition — it has not yet been given a formal name — is fueled by genuine moral dismay at the rise of economic inequality and that he wants to take advantage of his bully pulpit while it lasts.

“We’re talking about the state of the United States of America; we’re talking about the direction of the country at a time of crisis,” Mr. de Blasio told reporters in the Bronx on Tuesday, before his trip. “This is something that I’m going to speak about every chance I get, everywhere I go,” the mayor said.

Some of those opportunities are not exactly by chance. When Mr. Harkin, visiting Manhattan in September, suggested an Iowa visit to Mr. de Blasio, the mayor was intrigued, and he aggressively followed up. He enlisted one of his political consultants to work with contacts in Iowa to ensure that the trip would happen.

And his appearance on NBC’s “Meet the Press,” during which he made his comments about Mrs. Clinton, was solicited by Mr. de Blasio’s advisers this month — before it was clear when Mrs. Clinton would announce her bid — to help bring attention to his new coalition.

Mr. de Blasio has tweaked his fellow Democrats before. In November, some complained after he published an op-ed article in The Huffington Post suggesting that the party’s midterm losses could have been avoided if candidates had followed a liberal policy playbook similar to his own.

His decision to hold off endorsing Mrs. Clinton — he said he wanted to hear her “vision” first — led to accusations that the mayor was grandstanding; a New York congressman, Sean Patrick Maloney, said he “should have his head examined.” (“I appreciate his concern for my health,” Mr. de Blasio later replied.)

Others in the extended Clinton universe were even blunter. John Morgan, a prominent Florida lawyer and longtime Clinton campaign fund-raiser, called Mr. de Blasio’s broadside “a B-team move in a prime-time venue.”

“You ran her campaign; was she not progressive enough when you did that?” Mr. Morgan asked, referring to Mr. de Blasio’s management of Mrs. Clinton’s first Senate race.

Those on the Democratic left, however, welcomed Mr. de Blasio’s comments, with union leaders and activist groups praising the mayor for wanting to nudge the party left. A gathering hosted by the mayor this month at Gracie Mansion attracted Senator Sherrod Brown of Ohio, Gov. Dannel P. Malloy of Connecticut and Katrina vanden Heuvel, editor of The Nation magazine, among other notable liberals.

“He’s going to be a real spokesman in the Democratic Party for what we stand for as Democrats,” Mr. Harkin, a liberal lion of the party, said in an interview. Mr. Harkin said he did not “make anything of” the mayor’s decision not to endorse.

And is Mr. Harkin behind Mrs. Clinton’s bid?

“As a matter of fact,” the former senator said, “both my wife and I are big supporters.”