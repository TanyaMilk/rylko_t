Nearly every week, it seems, we get a new survey or study correlating people’s ideological beliefs with their views on science. Thus, we know that conservatives are more likely to doubt climate change and evolution, liberals are more likely to distrust nuclear power, and that both political camps include anti-vaccine minorities. Research also tells us that left-wing sociologists distrust attempts to explain many aspects of human behavior by invoking our evolutionary history.

It’s a fun game to play, this correlating — but it lends itself more to partisan finger pointing than deeper explanations. Moreover, it doesn’t necessarily get at what political antagonists on either side of the aisle feel about science itself, as opposed to how they feel about a particular scientific issue that may rub lefties (or righties) the wrong way for some reason.

A new paper, published in the journal Social Forces by sociologist Gordon Gauchat of the University of Wisconsin-Milwaukee, goes much further in this regard. And what did it find? That to simply claim that conservatives distrust science, or that liberals love it, doesn’t really explain much at all.

In the study, Gauchat first gathered data from the General Social Survey on two key questions: How Americans feel about the use of scientific information to determine government policy, and how they feel about the extent to which it should be funded by the federal government. He then examined how people’s views on these issues relate both to their left-right ideological dispositions, but also to various other beliefs, psychological factors, and aspects of identity that may underlie political beliefs.

And — in sum — it looks like now we’re really getting somewhere when it comes to understanding the politics of science.

To see why, let’s look first at the survey questions that lie at the center of this study. In 2006, 2008, 2010, and 2012, the General Social Survey asked Americans two key questions about science that were based on the National Science Foundation’s Survey of Public Attitudes Toward and Understanding of Science and Technology. In each case, Americans were asked how much they agreed or disagreed with the following statements:

Most Americans tend to respond to these questions by saying that science should support policy decisions, and that it should get federal funding. But there’s considerable variation, and some people out there do “strongly agree” that science is indeed too airy and speculative to drive decision-making, even as others “strongly disagree” that it should get federal funding.

So why do people’s responses to these questions vary? That’s where it gets interesting.

On a first look at the data, Gauchat found left-right differences in the responses — conservatives were somewhat less likely to favor the use of scientific information in shaping public policy, and also less likely to support science receiving federal funding. But when Gauchat used more sophisticated statistical techniques (called regressions) to try to pull apart the root causes of why people vary in how they feel about these topics, political ideology decreased in significance (in the case of the first item, becoming statistically insignificant entirely). Instead, other underlying factors jumped out.

“The ‘direct effect’ of liberal-conservative orientation is spurious once the distinct belief systems that underlie those identifications are accounted for,” wrote Gauchat.

Which belief systems? In particular, being a biblical literalist — endorsing the statement, “The Bible is the actual word of God and is to be taken literally, word for word” — was a much bigger factor than liberalism or conservatism in explaining why some people disagreed with the use of science in “concrete government policy decisions,” and also why they were against federal science funding.

Meanwhile, several other factors also leaped out as being more important than simple left-right orientation. A politico-psychological trait called authoritarianism — often described as a tendency to see issues in sharply black-and white terms — was also tied to distrusting the use of science for policy. Meanwhile, distrust of government itself was (not surprisingly) linked to not wanting science to receive government funding.

“Overall, these results show that perceptions of science are polarized, but this political discord reflects deeper cultural belief systems that cohere on the political right,” wrote Gauchat.

Gauchat also found something else striking: Political ideology became more significant in driving people’s views about science as they became more scientifically literate. Thus, being a liberal or a conservative alone didn’t matter much to how the questions above were answered – outside of the cases where political beliefs were combined with scientific knowledge.

“Only for scientifically sophisticated respondents, those 1.5 standard units above the mean, is conservative political ideology associated with less favorable views towards science’s authority,” wrote Gauchat. That’s not surprising: More scientifically literate conservatives are surely more literate and informed in all aspects of life, including politics. And thus, they’re more likely to be aware that the scientific community is a very politically liberal place, overall — far more liberal than the American public.

And knowing this, in turn, they’re inclined to distrust it.

The upshot of the analysis is that while science is definitely politicized, you have to be nuanced in how you describe why that’s the case. After all, fundamentalist religion — not ideology — emerged as the biggest factor in explaining why some people don’t think science should influence policy or receive federal funding.

When you think about it, that makes a lot of sense. Biblical literalists tend to strongly distrust the theory of evolution, one of the centerpieces of modern science. Knowing how strongly the scientific community backs evolution, these individuals are naturally inclined to distrust that community — and to not want to see it getting lots of taxpayer money for (still more) ideologically challenging research.

This doesn’t deny the fact that science is a very political topic; Gauchat presumably also wouldn’t deny that based on his data, there’s somewhat more science tension on the right than on the left (though it also exists on the left). But it does show that merely calling it a left-right conflict, without deeper analysis, misses much of what’s actually going on.

“The unidimensional model of left-right conflict oversimplifies the cultural sources of science’s politicization in the United States,” wrote Gauchat.

Also in Energy & Environment:

“Why you shouldn’t freak out about those mysterious Siberian craters”

“We could keep a huge amount of carbon out of the atmosphere just by changing people’s behavior”

“The global warming slowdown is real — but that’s no reason to question climate science“