More than four decades after an accident at a sawmill sent a piece of wood into Cecil Clayton’s skull, costing him a portion of his brain, the state of Missouri executed him Tuesday night for shooting and killing a police officer.

Clayton, 74, had petitioned the U.S. Supreme Court to delay his lethal injection. His attorneys argued that because of the sawmill accident, he had an intellectual disability and therefore cannot be put to death. However, the Supreme Court denied his stay requests on Tuesday evening, rejecting all of his petitions for the justices to intervene and prevent his execution.

Clayton was sentenced to death for shooting and killing Christopher Castetter, a sheriff’s deputy in Purdy, Mo., in 1996. Attorneys for Clayton are not arguing that he is innocent, but instead point to his brain injury in asking the justices to act.

The execution was scheduled to take place shortly after 6 p.m. local time, but it was delayed for more than three hours as the Missouri Department of Corrections awaited word from the Supreme Court. Shortly after 8:30 p.m., the justices released three orders declining the requests. None of the orders included any explanations, and only one of these orders mentioned that any justices said they would have granted the stay.

The Department of Corrections said his lethal injection began at 9:13 p.m. local time at a prison in Bonne Terre, Mo. He was pronounced dead at 9:21 p.m.

“As one who has carried a badge most of my adult life, I share the outrage of every Missourian at the murder of law enforcement officer, Deputy Christopher Castetter,” Missouri Attorney General Chris Koster said in a statement. “Cecil Clayton tonight has paid the ultimate price for his terrible crime.”

Clayton had “severe mental illness and dementia related to his age and multiple brain injuries,” Elizabeth Unger Carlyle, one of Clayton’s attorneys, said in a statement after the execution. “The world will not be a safer place because Mr. Clayton has been executed.”

One of Clayton’s filings specifically pointed to the fact that the Supreme Court will consider lethal injection this spring in urging them to stay the execution. The court’s four liberal justices said they would have granted this request, mirroring their similar responses to such requests that have been made by other death-row inmates in recent weeks.

Clayton’s attorneys had filed additional requests for a reprieve Tuesday afternoon and evening, arguing in one of them that his brain damage creates an “immediate and grave” danger that he would suffer “a substantial likelihood of unlawful pain and suffering” if the lethal injection was carried out. They pointed to recent problems with lethal injections in asking the justices to delay the execution, specifically arguing that the court should have granted the stay because it is going to hear this very issue soon.

In another of their filings to the Supreme Court, Clayton’s attorneys outlined the crime that resulted in his death sentence. They wrote that the police were called about Clayton trespassing around the home of his girlfriend’s mother. When Castetter was dispatched, they say, Clayton shot him and later said that Castetter “probably should have just stayed home” and “shouldn’t have smarted off to me.” But they note that Clayton also said he “wasn’t out there,” before going on to outline issues involving his intelligence and memory.

This argument has its origins in an accident that occurred in 1972. Clayton, who was a logger and sawmill operator, was working at his sawmill when a piece of wood broke off and stabbed into his skull. He was taken to a hospital, where he stayed for nine days, and he ultimately lost 7.7 percent of his brain and 20 percent of his frontal lobe, according to his attorneys.

As his attorneys describe it, Clayton “changed drastically and immediately” after the injury. Before the injury, they write that he was a married father who stopped drinking, became a preacher and traveled the country with his family, singing gospel and playing his guitar; afterward, he began drinking again, became depressed and violent, suffered memory loss and experienced hallucinations.

The Missouri Supreme Court declined over the weekend to halt Clayton’s execution, declaring in a 4 to 3 decision that he was competent to be executed.

In part, the court noted that Missouri’s law states that for a person to be deemed “intellectually disabled” — and, therefore, someone who could be spared execution under a Supreme Court ruling from 2002 — the condition had to emerge before the person turned 18. However, Clayton’s injury occurred more than a decade after he turned 18.

The U.S. Supreme Court ruled in 2002 that executing the mentally disabled violates the Constitutional ban on cruel and unusual punishment. Last year, for the first time since that ruling, the court again considered the issue, this time determining that state laws relying heavily on IQ test results to determine this are unconstitutional.

“Intellectual disability is a condition, not a number,” Justice Anthony Kennedy wrote in the majority opinion last year. He later added that the law requires that inmates “have the opportunity to present evidence of [their] intellectual disability.”

Clayton’s attorneys argued that he should have had a hearing where his mental competence could be determined, but the Missouri Supreme Court did not agree. In a dissenting opinion, Judge Laura Denvir Stith of the Missouri Supreme Court wrote that the evidence presented by Clayton’s attorneys and an expert medical opinion should have been enough for him to receive this hearing.

“The denial of such a hearing deprives Mr. Clayton of a fair opportunity to show that the Constitution prohibits his execution,” she wrote.

Attorneys for Clayton turned to the U.S. Supreme Court, asking that the execution be delayed so that he could have a hearing to prove his intellectual impairment.

Executing a person who has a mental disability “does not serve any legitimate goals of capital punishment,” Clayton’s attorneys wrote in one of their filings. “The impairments are what make the intellectually disabled undeserving of death.”

For his last meal, Clayton had fried chicken, mashed potatoes, corn, green beans and a soda.

“They brought me up here to execute me,” were his final words, according to the Department of corrections.

Clayton’s execution is the 10th so far this year nationwide. It is the second so far this year in Missouri, which executed Walter Storey last month. Storey had asked the U.S. Supreme Court to delay his execution until after the justices hear a case on lethal injection later in the spring, but the justices rejected his request.

While the death penalty is generally on the decline in the country, multiple states — Missouri among them — are still trying to carry out executions, but they are struggling to obtain the necessary drugs amid an ongoing shortage. This shortage played a major role in why the Supreme Court is hearing an argument over lethal injections next month. As states have had problems finding the drugs, they have experimented with different drugs and combinations. The justices have agreed to hear a case involving lethal injections in Oklahoma, which turned to the controversial drug midazolam because of the shortage.

However, the Supreme Court has been unwilling thus far to halt all executions until after it considers this case, which speaks to the divided nature of the court and a quirk of how it operates.

While it takes only four justices to accept a case, it takes five of them to stay an execution. The same four justices who would have halted Clayton’s execution and would have stopped an execution in Oklahoma earlier this year also would have ordered Missouri to stop the execution of Storey last month.

There have been five executions since the court decided to consider lethal injection. The justices did delay three scheduled executions in Oklahoma, while the Florida Supreme Court stayed an execution there because that state uses the same drug protocol as Oklahoma.

Executions in Missouri have typically occurred after midnight, but last month the state said it would begin scheduling executions after 6 p.m. local time. The death warrants authorizing executions remain in effect for a 24-hour period.

[This post has been updated. First published: 2:10 p.m.]

RELATED: Georgia postpones executions indefinitely so it can examine lethal injection drugs