The Rev. Theodore M. Hesburgh, a transformative figure in Catholic higher education who led the University of Notre Dame for 35 years and wielded influence with U.S. presidents on civil rights and other charged issues of his era, died Feb. 26 on the university campus. He was 97. 

A Notre Dame spokesman confirmed the death and said the cause was not immediately known.

Father Hesburgh, president of Notre Dame from 1952 to 1987, was considered one of the most important university leaders of the century. On his watch, the Midwestern school once known mainly for Fighting Irish football became an academic powerhouse that was the pride of Catholic America.

Notre Dame went on a building spree in that time, including adding a 14-story library now named for Father Hesburgh. The university’s enrollment doubled, and its endowment soared. Top scholars joined the faculty, lured by his vision of creating “a Catholic Princeton” in northern Indiana.

Beyond the South Bend campus, Father Hesburgh spearheaded a movement among Catholic universities in the 1960s to assert autonomy from the church hierarchy and put governing power in the hands of the laity. Some clerics thought it was folly to cede control. But Father Hesburgh called it a practical reform that brought more expertise into the leadership of increasingly complex institutions.

And in 1972, Notre Dame admitted female students for the first time. “Our women students brought a good measure of gentility” to what had been “a kind of rough, raunchy, macho place,” ­Father Hesburgh later wrote.

Father Hesburgh, who made Time magazine’s cover in 1962, became a moral leader with clout in the White House and the Vatican on matters from human rights to nuclear proliferation.

Derek C. Bok, then president of Harvard University, said in 1987 that Father Hesburgh “succeeded not only in strengthening Notre Dame academically but in teaching audiences everywhere about the values that matter in our society.”

He traveled so often on presidential and papal business that a joke circulated on campus: “What’s the difference between God and Father Hesburgh? God is everywhere. Father Hesburgh is everywhere but Notre Dame.”

President Dwight D. Eisenhower named Father Hesburgh to the Civil Rights Commission at its inception in 1957. He held the position for 15 years, immersing him in confrontations over racial discrimination.

In one of its first actions, the commission held hearings in Southern states to investigate suppression of the black vote. When it came time to write a report to Congress in 1959, Father Hesburgh brought the commission to Notre Dame’s Land O’Lakes retreat in Wisconsin for a day of fishing, steaks and martinis — and votes on recommendations that later influenced civil rights legislation.

Eleven proposals won unanimous support from the six commissioners, and a 12th won approval from five.

The degree of consensus shocked Eisenhower. “I told Ike that he had not appointed just Republicans and Democrats or Northerners and Southerners, he had appointed six fishermen,” ­Father Hesburgh recounted in “God, Country, Notre Dame,” a 1999 memoir written with Jerry Reedy.

Eisenhower replied that more federal commissions should be sent to Land O’Lakes to resolve disputes.

In 1964, Father Hesburgh joined hands with the Rev. Martin Luther King Jr. at a rally at Soldier Field in Chicago as a landmark civil rights bill was on the cusp of enactment. In 1970, he was chairman of the Civil Rights Commission when it released a report a few weeks before the fall elections that found a “major breakdown” in enforcement of federal laws and executive orders against racial discrimination. 

The critique drew the ire of President Richard M. Nixon. In 1972, Father Hesburgh resigned from the commission at the demand of White House aides.

Nixon’s successor, President Gerald R. Ford, named Father Hesburgh to a board that reviewed clemency petitions for draft dodgers and military deserters during the Vietnam War. A few years later, President Jimmy Carter asked him to lead a commission to propose changes to immigration laws. Such was his pull that Father Hesburgh, an aviation buff, wangled permission from Carter in 1979 to fly aboard an Air Force supersonic jet, the SR-71 Blackbird.

Father Hesburgh, a member of the Congregation of Holy Cross, also held multiple papal appointments. From 1956 to 1970, he was the Vatican’s representative to the International Atomic Energy Agency, which made him at times a broker between the United States and the Soviet Union during the Cold War.

At an agency gathering in Vienna, he met former Soviet foreign minister Vyacheslav Molotov. The two struck up such a cordial relationship that Molotov telephoned Father Hesburgh one day to express regrets at not being able to attend Mass and brunch with the priest.

“It tickled my fancy that a top Russian Communist, Molotov no less, called me up to apologize for not coming to a Catholic Mass,” Father Hesburgh later wrote.

Theodore Martin Hesburgh was born May 25, 1917, in Syracuse, N.Y., the second of five children of a plate-glass company executive. Aspiring to the priesthood from a young age, he enrolled in 1934 in a seminary at Notre Dame.

“I liked dancing. I liked everything. But I thought there was something more to life,” he told Time in describing what drew him to the priesthood. “By really belonging to nobody except God, you belong to everybody.”

He graduated from the Pontifical Gregorian University in Rome in 1939 and was ordained four years later. He earned a doctorate in sacred theology in 1945 from Catholic University in Washington, then returned to Notre Dame to teach religion.

Recognized for rapport with students and administrative acumen, Father Hesburgh became Notre Dame’s 15th president in 1952, without any apparent competition.

At the time, Notre Dame was known for little more than a football program that had generated numerous national championships under coaches Knute Rockne and Frank Leahy.

Father Hesburgh quickly asserted his priorities with coaches. “I would insist that we wanted only student-athletes ,” he wrote. “We expected all the players to be students, and we expected all of them to graduate — not 50 percent of them, not 30 percent of them. All of them.”

Under his tenure, Fighting Irish football had ups and downs. Coach Ara Parseghian’s 1966 and 1973 teams won national championships. So did Coach Dan Devine’s 1977 squad. But the team also had a handful of losing seasons.

Father Hesburgh had grander aims than gridiron glory. He recruited first-rank academic deans and raised huge sums of money to endow professorships and expand the campus. When he retired in 1987, Notre Dame’s endowment of $456 million ranked 23rd in the country.

A centerpiece project, the massive main library, opened in 1963. Its famed “Word of Life” mural, nicknamed “Touchdown Jesus,” is visible from inside the university’s stadium.

Father Hesburgh sought to shield the university from church intrusion on academic freedom. In 1954, he rebuffed a Vatican request to withdraw from circulation a book Notre Dame had published that contained views on religious freedom opposed by some conservative theologians. 

“Notre Dame would lose all its credibility in the United States, and so would I, if an official in Rome could abrogate our academic freedom with a snap of his fingers,” Father Hesburgh wrote.

In 1967, following the liberalizing reforms of the Second Vatican Council, he helped coordinate a “Statement on the Nature of the Contemporary Catholic University” that was signed by leading Catholic educators.

“To perform its teaching and research functions effectively, the Catholic university must have a true autonomy and academic freedom in the face of authority of whatever kind, lay or clerical, external to the academic community itself,” the group said in a manifesto known as the Land O’Lakes Statement.

Critics said the statement went too far, accelerating the secularization of Catholic institutions. Proponents said it laid the foundation for a productive relationship between the church and Catholic academia.

“You would be hard-pressed to find a more seminal moment in the development of Catholic higher education in the last century,” said John J. DeGioia, current president of Georgetown, the nation’s oldest Catholic university. “The basic idea was, we joined American higher education as an authentic partner in developing the contemporary American academy. Up until then, there was a question as to whether Catholic universities were really part of the mainstream.”

Also in 1967, Father Hesburgh obtained Vatican approval for an overhaul of governance at the university. Since its founding in 1842, Notre Dame had been run exclusively by the Congregation of Holy Cross. The revision — a pioneering shift in the world of Catholic universities — gave the laity a dominant majority on the board of trustees but ensured that future presidents would be Holy Cross priests.

Known to many as Father Ted, the Notre Dame president kept night-owl hours and was often amenable to chatting with students who climbed a fire escape after dark and tapped on his office window.

But during the Vietnam War, Father Hesburgh laid down a hard line against disruption of the university. He told students in February 1969, “Anyone or any group that substitutes force for rational persuasion, be it violent or nonviolent, will be given 15 minutes of meditation to cease and desist.” Students who did not heed the warning would be suspended and, if they persisted, expelled. Outsiders would be subject to arrest for trespassing.

Student leaders condemned his statement, but Nixon sent a congratulatory telegram and asked for advice on federal legislation to address turmoil on campuses. Father Hesburgh urged the government not to intervene. “Things will be messy from time to time,” he wrote Vice President Spiro Agnew, “but we will make it as universities if we determine strongly to maintain our freedoms and our values.”

Father Hesburgh was awarded the Presidential Medal of Freedom in 1964 and the Congressional Gold Medal in 2000. 

Survivors include a brother, the university spokesman said.

In 2001, Father Hesburgh lamented that university presidents had become distant from public affairs.

“Once upon a time chief executives in higher education talked to the press about military policy in the same breath as the Constitutional amendment for the 18-year-old vote, but I wonder whether we’d hear them taking stands on similar topics now,” he wrote in the Chronicle of Higher Education.

“Where we once had a fellowship of public intellectuals,” Father Hesburgh asked, “do we now have insulated chief executives intent on keeping the complicated machinery of American higher education running smoothly?”