CINCINNATI — Jim Obergefell cannot fathom hearing his name mentioned alongside Brown or Roe.

For one thing, he knows people will mispronounce it. (It’s Oh-ber-guh-fell.) And he never wanted to be the face of a movement or a legal groundbreaker.

“I’m just Jim,” he said. “I just stood up for our marriage.”

It was not a long marriage, just three months and 11 days — the time it took his husband, John Arthur, to struggle to say, “I thee wed,” and then die from ALS. Now their union, and the 20-year relationship that preceded it, is at the center of Obergefell v. Hodges, the title case of four consolidated appeals the Supreme Court will hear this month to decide whether gay couples have a constitutional right to marry.

For Obergefell, the case is simply about that tricky-to-pronounce name: He wants it on Arthur’s death certificate as the surviving spouse, an idea the state of Ohio, where same-sex marriage is illegal, opposes. Should Obergefell win, history books will probably take a more expansive view of his quest.

“If the Supreme Court decides in favor of full marriage equality, it will be the largest conferral of rights on LGBT people in the history of our country,” said Fred Sainz, vice president of communications at the Human Rights Campaign. “There is absolutely no doubt in my mind that Jim will become a historic figure.”

 [Supreme Court agrees to hear gay marriage issue] 

How Obergefell, a soft-spoken real estate broker with little previous interest in political activism, wound up in this spot is a story of judicial chance, but it’s also about resolve, fate and heartbreak. The court will hear arguments on April 28 concerning the four cases, but because Obergefell’s suit has the lowest case number, the court, per its tradition, has lumped everything under his legal citation.

That means Oh-ber-guh-fell could become as famous as Brown (the lead plaintiff in the historic school desegregation decision) or Roe (the pseudonym for Norma McCorvey in the case that legalized abortion nationwide) simply because his case number is 14-556, not 14-562 or 14-571 or 14-574.

“On one level, I get it,” he said, sitting next to his attorney in a downtown office tower. “But it hasn’t sunk in on an emotional level. I get it. But I don’t.”

Walking around Cincinnati, Obergefell is sometimes greeted with spontaneous hugs. People thank him. They wish him good luck. And as the hearing approaches, Obergefell finds himself emerging as a national public figure.

The Human Rights Campaign put him on the cover of its magazine. It is flying him around the country for speaking engagements — not just because his name is on a landmark case but also because his story, advocates say, perfectly symbolizes what the fight has always been about.

Arthur was essentially on his deathbed when the couple decided to marry. He could no longer walk. Speaking was difficult. Amyotrophic lateral sclerosis, which attacks the body’s motor neurons, was rapidly destroying his body. Obergefell stayed at Arthur’s bedside. Their friends had to beg him to leave the house to take a break.

In their two decades together, the couple talked about marriage but never considered it seriously. They didn’t want it to be symbolic. They wanted it to have legal weight, just like the marriages of their heterosexual friends. But in 2013, the Supreme Court struck down a key portion of the Defense of Marriage Act, giving same-sex married couples federal benefits in the states where such unions were legal.

 [Supreme Court strikes down key part of Defense of Marriage Act] 

Obergefell saw the news online. He leaned over, kissed Arthur on the head and said, “Let’s get married.”

“Okay,” Arthur said.

Their wedding was a production. Obviously, they needed to travel to a different state, choosing Maryland on a friend’s suggestion. But how to get there? A car trip was out. A medical flight was their only option, but how could they arrange one, much less come up with the $13,000 to pay for it? Obergefell sought advice on Facebook. Their friends and family offered more than guidance — they offered cash, sending them money via PayPal, funding the entire trip.

And so on the morning of July 11, 2013, an ambulance transported them to the airport, where they boarded a medical jet with a nurse and Arthur’s aunt, Paulette Roberts, who became ordained online to perform weddings. They flew to Baltimore-Washington International Marshall Airport. Roberts began the ceremony on the tarmac, in the plane, shortly after landing. The couple held hands, Obergefell’s thumb rubbing Arthur’s. They stared into each other’s eyes.

“With this ring,” said Obergefell, slipping a ring on Arthur’s hand, “I thee wed.” Then he gently helped Arthur guide a ring onto his own hand. 

“With this ring,” said Arthur, his speech distorted by ALS, “I thee wed.”

And then they kissed, husband and husband.

“If marriage vows mean anything,” Roberts later said, “then those two were more married than anyone I have ever known.”

It was, they often joked, love at third sight.

The early 1990s. A bar called Uncle Woody’s, near the University of Cincinnati. They met there once — no love connection. They met there again — no love connection. They met the third time at a mutual friend’s party.

“And then I never left,” Obergefell said.

Arthur was Obergefell’s first serious boyfriend. Obergefell grew up in northern Ohio, the youngest of six in a Catholic family. By the time he came out in his mid-20s, Obergefell’s mother was deceased. He told his oldest sister first. He called her one evening and said he wanted to take her to dinner. There was something important to discuss.

“My kids said, ‘He’s gonna tell you he’s gay,’ ” Ann Hippler remembered.

They sat down at dinner. He took her hand and said, “I need to tell you something.”

“What?” his sister asked. “That you’re gay?”

Obergefell was stunned.

“It doesn’t bother me,” his sister told him. “It’s who you are.”

The rest of his family was similarly unsurprised and unperturbed.

Arthur had a more difficult time. While his mother and brother accepted him, Arthur’s father, with whom he had a strained relationship, didn’t find out until several years later.

“Once my dad was able to put his mind to it, I think it was okay,” Arthur’s brother Curtis said.

Jim and John. John and Jim. They were inseparable. They moved in together not long after they began dating. They worked together in IT consulting and client relations management at a series of companies, their desks sometimes only inches apart. They rehabbed old houses. They bought paintings by local artists. Obergefell was the more serious, reasonable one. Arthur was, as his brother put it, “highly, highly charismatic,” with a biting sense of humor.

One day in the winter of 2011, Obergefell noticed Arthur’s left foot slapping the ground as he walked. Then he started falling. It became difficult for him to get into their small Volkswagen. Arthur went to their family doctor, who referred them to a neurologist. The diagnosis: ALS. They got a second opinion: ALS.

“Learning what it meant,” Obergefell said, choking up, “was devastating.”

By summer, Arthur was walking with a cane. The cane turned into a walker. The walker turned into a manual wheelchair. The manual wheelchair turned into a motorized wheelchair. The couple could no longer stay in their two-level condo. They moved into a one-level condo in Cincinnati’s hip Over-the-Rhine neighborhood.

By early 2013, Arthur was confined to bed. Hospice care started. Arthur was never angry. He never asked, “Why me?” Obergefell did those things.

“But it was hard to be angry,” Obergefell said, tears streaming down his face, “when I saw him being so good about it.”

And then they got married.

There was no honeymoon, of course. And a few days after they wed, an old neighbor mentioned their situation to Al Gerhardstein, a local civil rights attorney.

“I knew right away they had a problem,” Gerhardstein said. “And I knew they probably weren’t thinking about it. Who thinks about a death certificate after getting married?”

Obergefell met with Gerhardstein, who told him they needed an injunction saying he should be listed as the surviving spouse on the death certificate. They filed a lawsuit a few days later. A federal judge ruled in their favor, a decision so controversial that a Republican state lawmaker called for the judge’s impeachment.

Arthur died a few months later. Their victory and a subsequent ruling in court were not theoretical: Obergefell’s name was listed as the surviving spouse. But Ohio appealed to a higher court, arguing that, if successful, officials should be able to reissue a death certificate without Obergefell’s name.

The state won. Obergefell appealed to the Supreme Court. If the justices don’t rule in his favor, it’s likely that new death certificate will be issued.

“We need to protect their interest for posterity, for the end of time,” Gerhardstein said. “For many people, marriage is some kind of theoretical thing. There are so many ways that this case is about marriage, about that enduring commitment.”

Today, Obergefell lives alone in the couple’s condo, among dozens of paintings hanging on nearly every inch of wall space. Arthur is there, too, his ashes in a box tucked away in a safe place. In the TV room, a large painting of the couple hangs above the couch. They are young and in love and smiling at a beautiful old cemetery where they used to take long walks.

In the corner of the room are several stacked volumes of bound legal documents — copies of the “People’s Brief” that Obergefell delivered to the Supreme Court in support of his case. Several volumes list supporters who signed the brief, which calls for full marriage equality. Obergefell has marked pages where his friends’ names appear.

The apartment is quiet, even though Arthur wanted Obergefell to date again after he was gone, to find love with someone else.

“I can’t picture that,” Obergefell said. “I just can’t.”

He got their wedding rings fused, and now two rings are one on his left hand. At some point, Obergefell will spread Arthur’s ashes in the water off the Outer Banks in North Carolina, where they have a house. He is not emotionally ready yet.

He keeps in touch with his husband at night, before he falls asleep. “I tell John about my day,” he said. Those days are becoming busier with awards and public appearances and more recountings of their love story.

“Getting ready for my first time speaking at a university!” he posted on Facebook the other day.

Obergefell finds the idea of becoming a public figure both foreign and exhilarating. What it’s all really about, he said, is “continuing this fight for John, for the promises I made. It keeps John alive. It’s my way of honoring and protecting him.”

And he keeps reminding himself about what his fight is really about — the death certificate, the title of spouse — even as he feels the growing pressure of a monumental civil rights moment.

“It’s hard to put into words,” Obergefell said, “how to grasp that our decision to stand up and say, ‘This isn’t right,’ is going to affect so many people.”

It’s Oh-ber-guh-fell.