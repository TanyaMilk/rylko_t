Today, the U.S. Supreme Court will hear arguments in a case that could open the door for gay couples to marry nationwide. Though historic, a decision in favor of gay rights would primarily affect just 13 states — those that have gay-marriage bans on the books, including my home state of Ohio. I traveled to Washington with hundreds of other LGBT Ohioans to rally for marriage equality outside the Supreme Court today — a trip that took us through three states where we can get married. Tomorrow, we’ll head home to one of the handful of states where we still can’t.

More than a third of gay Americans still live in states with gay-marriage bans. Our friends in New York, Chicago and Los Angeles tell us to move. And many have. Ohio’s former attorney general, Republican Jim Petro, said his own daughter, a lesbian, “was not about to come back” to the state after a same-sex marriage ban was added to its constitution in 2004, with the support of 62 percent of voters. And I know many gay Ohioans who moved to Boston, Washington, San Francisco and other cities where the higher financial costs of living aren’t as burdensome as the emotional cost of living in a state that classifies them as second-class citizens.

Even corporations have noted the exodus. In an amicus brief filed with the Supreme Court in support of marriage equality, corporations such as AT&T, Bank of America, Delta Airlines, Google, Marriott and Microsoft argue that they’re disadvantaged when looking to hire or relocate people – gay, bi or straight – in states that don’t treat people equally. “Marriage discrimination drives talented individuals away” from states where these companies do business, they wrote.

Other gay couples who live in states with gay-marriage bans have decided to cross state borders to get married rather than wait for the right to do it at home: Thousands have wed in Canada, California, Iowa and other places that recognize our right to do so. It’s less than a two-hour drive from Cleveland to Erie, Pa., and less than 40 minutes from Cincinnati to Lawrenceburg, Ind., the nearest county courthouses in marriage-equality states.

But thousands more are waiting — together, but still legally single — for the opportunity to drive just a few minutes to their local courthouses in Toledo, Lima, Marietta, Ashtabula, Youngstown and other Ohio hometowns. They’re not driven by the convenience. When six Cincinnati couples filed a lawsuit in April 2014 to overturn Ohio’s marriage ban, they said they wanted to get married before family and friends, not in a government office across a state line. We want the right to marry in our home states for a very simple reason: They’re our home states.

We are fighting for the right to marry at home even though home hasn’t been the happiest place for the last 11 years. In 2004, as George W. Bush and Karl Rove mapped out a presidential-election strategy that required a win in Ohio (it always does), they capitalized on the amendment to ban same-sex marriage to boost turnout among their socially conservative supporters. As I gathered with my family in Toledo that Thanksgiving, I wondered to myself how my relatives cast their ballots once they were standing alone in the voting booth.

Two years later, as Ken Blackwell, Ohio’s secretary of state at the time, ran for governor, he compared gays to kleptomaniacs and arsonists, describing them as people who make “bad choices” and “can be changed.”

Today, the rhetoric is softer, but the actions are just as harsh. A city-run swimming pool in Galion, Ohio last year refused to sell a family pass to a lesbian couple and their children. Legislators who oversee operations at the Ohio Statehouse require that couples show a state-issued marriage license if they want to host their weddings or receptions in the Civil War-era building — a policy that effectively shuts out same-sex couples from a place legislators like to call “the People’s House.”

In taking the gay marriage fight all the way to the U.S. Supreme Court, Ohio Attorney General Mike DeWine is battling against gay widowers who simply want their late husbands’ death certificates to record them as married. He’s trying to keep Ohio-born infants from having two moms or two dads listed on their state-issued birth certificates.

While Ohio bans other people from marrying, too — first cousins, for instance — the state will honor the marriages of cousins who move to Ohio from California or Tennessee or other states where their unions are legal. It also will recognize the marriages of Ohio cousins who cross state lines to wed. Same-sex couples are the only ones whose legal, out-of-state marriages are null and void as soon as they enter Ohio.

But we stay, and we continue to come back, because it’s home.

Increasingly, our neighbors are supporting us. Last August, more than 10,000 athletes from more than 60 countries came to Cleveland and Akron for the ninth Gay Games. It’s a quadrennial event that moves from gay capital to gay capital — San Francisco, Amsterdam, Cologne, Sydney — but organizers in 2014 decided that going to Ohio had the potential to change hearts and minds in the Heartland. At the opening ceremony, Sen. Sherrod Brown quoted his wife, columnist Connie Schultz, who wrote soon after the gay-marriage ban passed: “The only threat I feel from gays is that so many of these kind and talented members of our community will now leave us. Who am I to ask them to stay?”

During the week of the Gay Games, businesses with no particular ties to the LGBT community flew rainbow flags, and Cleveland’s iconic Terminal Tower was lit up in all of its colors. When athletes boarded one city bus with their medals around their necks, passengers burst into applause. Ohioans have progressed further and faster on the issue of marriage equality than have the politicians they elected. The state’s attorney general is sending his lawyers to the U.S. Supreme Court to fight for a status quo based in a bygone era.

More from PostEverything:

I’m a senior GOP spokesman, and I’m gay. Let me get married.

How Alabama’s chief justice helped the gay marriage movement he was trying to block

I’m an evangelical minister. I now support the LGBT community — and the church should, too.