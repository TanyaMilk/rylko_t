Your birth control pill is affecting more than just your body.

Flushed down toilets, poured down sinks and excreted in urine, a chemical component in the pill wafts into sewage systems and ends up in various waterways where it collects in fairly heavy doses. That's where fish soak it up.

A recent survey by the U.S. Geological Survey found that fish exposed to a synthetic hormone called 17a-ethinylestradiol, or EE2, produced offspring that struggled to fertilize eggs. The grandchildren of the originally exposed fish suffered a 30 percent decrease in their fertilization rate. The authors mulled the impact of what they discovered and decided it wasn't good.

"If those trends continued, the potential for declines in overall population numbers might be expected in future generations," said Ramji Bhandari, a University of Missouri assistant research professor and a visiting scientist at USGS. "These adverse outcomes, if shown in natural populations, could have negative impacts on fish inhabiting contaminated aquatic environments."

[Three million people drink from the Potomac River, and something in it causes male fish to switch sex]

The study, with Bhandari as lead author, also determined that the chemical BPA, used widely in plastics, had a similar effect on the small Japanese medaka fish used for the research. The medaka was chosen because it reproduces quickly so that scientists can see results of subsequent generations faster than slow reproducing species such as smallmouth bass.

BPA and EE2 are both endocrine disruptors that interfere with hormones and cause developmental disorders. Over the past 12 years, male smallmouth and largemouth bass throughout the country, including the Potomac River basin in the Chesapeake Bay region, have switched sex, developing ovaries where their testes should be, and the two disruptors are prime suspects.

These particular chemicals were employed in the study for good reason. EE2 is a major ingredient in oral contraceptives for women, and up to 68 percent of each dose is released in the latrine through urine and excrement. A full dose is released when some women simply pour unused pills down the drain.

BPA is a chemical used primarily for polycarbonate plastics and epoxy resins. Environmental waste from products containing it "has been a serious concern and potential threat to public and wildlife health," according to a USGS statement about the study. It was published last week in the journal Scientific Reports. 

[Do farm pesticides cause fish to switch sex? A Maryland bill seeking answers was fought fiercely by chemical companies]

Gender-bending is happening at several locations on the Potomac River, showing up in between 50 and 100 percent of bass caught and dissected. EE2 survives even after wastewater is treated, so it's a safe bet that large doses arrive when sewage facilities dump millions of gallons of untreated wastewater into the Potomac and other waterways during overflow events when pipes are overwhelmed by rain. 

Farmers spray enormous amounts of chemicals in pesticides that run into water during every storm. In 2013, Maryland lawmakers sought to make farmers report the amount of pesticides they used, but the rule was fought by the farm lobby and chemical companies.

Vicky Blazer, a USGS researcher who wasn't involved in the study about impacts across generations, said during the fight in the Maryland legislature that chemical reporting was needed because the number of intersex fish keep spreading. “The fish is an indicator that something else is really wrong,” she said. There's a good chance the chemicals "are also affecting people," she said.

No one has found a strong link that sticks, but a growing body of science has found that the sex glands of fish exposed to these and other chemicals in labs change dramatically. Synthetic estrogen such as EE2 is more persistent than natural steroids and present in higher concentrations in treated wastewater, according to a 2002 study, Coastal and Estuarine Risk Assessment.

"We know intersex is occurring, we don’t understand exactly how that’s occurring," said the recent study's co-author, Don Tillitt, research toxicologist at USGS's Columbia Environmental Research Center in Missouri. "We know that certain endocrine disrupting chemicals can cause intersex from exposure during development or the birth cycle."

[Scientists fishing for intersex bass found a bumper crop near a D.C. wastewater treatment plant ]

But that's not what this study was for, Tillitt said. It was started two years ago as a transgenerational investigation. "We were looking at things that can be important in reproduction and survival, the fertilization rate and the survival of embryos," he said.

"What we know now is it can occur in fish," Tillit said. "Now we know that fish exposed can pass the effects to offspring several generations down."

Tillitt said this is just the beginning of USGS investigations into the impact of synthetic hormones and chemicals on fish. "We do studies with bass and we’re trying to understand what’s causing intersex," he said.

"We have a Fish and Wildlife Service hatchery where we get our fish. We’re starting with fish that have not been exposed. Those studies are happening now, and we’re getting ready to report some of those."