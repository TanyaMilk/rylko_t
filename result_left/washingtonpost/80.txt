A Loudoun County parent has sued state officials to force the release of evaluation data for thousands of teachers across Virginia, making it the latest in a series of states to grapple with whether such information should be made public.

Brian Davison has pressed for the data’s release because he thinks parents have a right to know how their children’s teachers are performing, information about public employees that exists but has so far been hidden. He also wants to expose what he says is Virginia’s broken promise to begin using the data to evaluate how effective the state’s teachers are.

“I do think the teacher data should be out there,” Davison said. “If you know that you have a teacher that’s not effective . . . is it fair to ask a parent to put their student in that teacher’s classroom?”

Davison’s crusade for the teacher data offers a window into a national debate about what parents and taxpayers have a right to know about teachers and how to balance transparency with privacy. A Richmond judge ruled in his favor in January, but that ruling has been challenged by state officials and the Virginia Education Association, which represents teachers across the state. The next hearing in the case is scheduled for Monday.

The debate over the value of making teacher evaluation data public first exploded into view in 2010, when the Los Angeles Times published the names and value-added ratings of thousands of elementary school teachers in the Los Angeles Unified School District. The ratings attempt to gauge the impact a teacher has on a student’s academic progress, using English and math test scores and a student’s expected growth to judge teaching performance. The move to expose the results drew immediate backlash from teachers and unions, who argued that the scores offered an incomplete and often misleading picture of teacher effectiveness.

Even many of the most ardent proponents of value-added scores came to agree that the information should not be broadly disseminated and could be misleading without additional context.

“It’s a real invasion of privacy,” said Kate Walsh of the National Council on Teacher Quality, a group that advocates for using value-added scores in teacher evaluations. “We know that value-added is a very useful measure of what a teacher can do in the classroom, but it’s not 100 percent accurate,” she said.

Legislators in many states have recently passed or tightened laws to shield teacher evaluations from public disclosure. Others — including Arkansas and Indiana — require schools or school districts to report on the average performance of teachers in such a way that individual teachers cannot be identified.

Virginia began collecting “student growth percentiles” in 2011, data meant to capture how much a student grows academically from one year to another based on standardized test results. The state was one of many that promised to start using such data to evaluate teachers in exchange for a federal waiver from the No Child Left Behind education law.

But Virginia school districts have not been using the data, calling them flawed and unreliable measures of a teacher’s effectiveness. Virginia passed a law in 2013 that makes “teacher performance indicators” confidential.

Since Davison prevailed in January, the state has released millions of student records — with names and other information redacted — and teacher-level data, though teacher names are not listed. He and John Butcher, of Richmond, have posted the data to their respective blogs.

Butcher, a retired attorney and frequent critic of Richmond city schools, said he found a teacher whose students on average ranked in the bottom 1 percent in student growth percentiles.

“Any kid who is stuck with one of those teachers is going to have a problem,” he said. “Why would you want to hide that information from parents?”

Davison has used the data to argue that the high test scores in Loudoun, one of the most affluent counties in the United States, might be more reflective of student wealth than of good teachers.

“We have great SOL scores,” Davison said, referring to Virginia’s standardized tests, known as Standards of Learning exams. “But are teachers teaching that, or do we just have a lot of smart kids and rich kids who are going to score well?”

Only in a few states do parents have a right to more information. In Florida, a state appeals court ruled in 2013 that value-added model scores should be made public after the Florida Times-Union newspaper sued to obtain the data. New York requires school districts to give parents access to the overall ratings of their children’s teachers, and Michigan requires parents to be notified, starting in the 2015-2016 school year, if their children have a teacher rated ineffective for two years running.

“This is a conversation that is happening across the country and I think is going to continue to happen as we have richer and better information” about teacher performance, said Aimee Rogstad Guidera of the Data Quality Campaign, a nonprofit organization that advocates using data to boost student achievement.

U.S. Education Secretary Arne Duncan acknowledged the tension between teachers’ privacy and transparency for parents, and said the most important thing is for teachers and principals to have access to data about their students’ growth.

“You can have a conversation about whether that teacher’s students’ parents maybe should or shouldn’t — you know, that’s complicated,” Duncan said recently at a meeting with reporters. “I think that’s best decided locally.”

In Virginia, the debate has been shaped in part by criticisms about student growth percentiles, which rank students on a scale of 1 through 99, according to how much progress they made on math and reading tests relative to other students who performed similarly in previous years.

Despite pledging to use student growth percentiles in their application for a waiver from No Child Left Behind, state education officials now say that the approach is problematic and have moved to scrap it.

Growth percentiles cannot accurately measure growth among the highest- and lowest-performing children, officials say, and they warn that in some cases student scores might be erroneously assigned to teachers who never actually taught them. In addition, they rely on consecutive years of test data that might not be widely available in schools serving transient populations.

And unlike value-added models used by other states, Virginia’s model does not attempt to control for the effects of poverty or other demographic characteristics. Critics of the growth percentiles say that disadvantages teachers who work with the neediest children.

Charles Pyle, spokesman for the Virginia Department of Education, said the state has not monitored which districts use student growth percentile data. But he said many districts have opted to use other ways to measure a student’s progress for the purpose of teacher evaluations.

“I don’t think anyone expects for a school district or a state to persist in doing something that doesn’t make a whole lot of sense on the ground just because it’s written somewhere,” Pyle said.

Davison’s lawsuit has created a quandary for Loudoun County, where board members describe themselves as fiscally conservative and often emphasize the importance of accountability and transparency. The board voted unanimously to join state officials in opposing Davison’s lawsuit.

School Board member Debbie Rose (Algonkian) said she thinks transparency is important, but she worries that the public could misinterpret the data.

“I feel that the parents might . . . get the wrong impression of a teacher if they don’t completely understand the data,” she said. “But at the same time, some of those risks are worth putting the data out there.”

Not only does Loudoun not use the data for teacher evaluations, but several Loudoun teachers say that they’ve never seen their own student-growth percentile data. Davison said his public-records requests have yielded evidence that school system administrators are not accessing the database that holds the information.

Loudoun County Schools Superintendent Eric Williams said he might support releasing teacher evaluation data if there were better measures available.

“Conceptually, I think it makes sense for parents to have lots of information,” Williams said. But he added that student growth percentiles are a poor way to measure a student’s academic growth. “I’m definitely open to having that conversation when there’s a different way for measuring it.”

Education associations and teachers unions warn that releasing teacher evaluation data is unfair and could devastate morale.

“Releasing this student growth data — just one part of teacher evaluations — is nothing short of public shaming and is particularly wrong when the scores are based on an unstable, disproven and misleading statistical model like the value-added model,” said Randi Weingarten, president of the American Federation of Teachers. “Teachers are so much more than a score.”

And while some teachers said they would pay little mind if the scores were released, others echoed those concerns. It could also influence teachers to focus even more on standardized tests and test preparation, which already consume several school days and loom large over curricula.

“I feel like it would demean me,” said Jeffrey Pool, who teaches language arts at Seneca Ridge Middle School. “I would be reduced to a number.”