Charity Bryant only intended to stay a few days in Weybridge, Vt., a tiny rural town with little to hold her attention. But then she met Sylvia Drake.

Drake was 22 — a talented, literary-minded woman in search of a kindred spirit. Bryant, seven years her senior, was brilliant, charismatic and exactly the kind of partner Drake had been looking for. The two fell swiftly, madly in love. Within months, Bryant rented a one-room apartment and asked Drake to become her roommate and wife.

It may sound like something from a 21st-century vows column, but this romance predates most newspapers’ style sections — by about two centuries.

“Our popular narrative of same-sex marriage says it’s this brand new thing,” said Rachel Hope Cleves, an associate professor of history at the University of Victoria and the author of a new study in the latest issue of the Journal of American History chronicling 500 years of same-sex unions in the United States. “But the reality is that it came over with human migration” — contrary, for example, to Justice Samuel A. Alito Jr.’s comment during oral arguments on California’s Proposition 8 case that it’s an “institution which is newer than cellphones or the Internet.”

Long before United States vs. Windsor — before the Defense of Marriage Act, even before the Stonewall Riots — gays and lesbians in North America found ways to live as married couples, in practice if not in law, according to Cleves’s research. In the mid-16th century, Spanish conquistador Alvar Nunez Cabeza de Vaca wrote about a custom of “one man married to another,” which he saw in several Gulf Coast communities. Newspaper accounts from the 18th and 19th centuries tell sensationalized stories of “female husbands,” women who passed as men and married other women for love or money. California miners Jason Chamberlain and John Chaffee lived together for more than 50 years and were thought of as “wedded bachelors” by those who knew them.

But Bryant and Drake’s 44-year marriage is by far the best and most explicitly documented example of an early same-sex union, said Cleves, who has also written a book about the relationship, “Charity and Sylvia: A Same-Sex Marriage in Early America.”

It began in 1807, when Bryant was visiting Drake’s older sister in Weybridge. Drake was something of an enigma to her family, who couldn’t understand why the 22-year-old — practically an “old maid” by the standards of the early 19th century — continually rejected her male suitors. Bryant, on the other hand, had a reputation: By 27, she had spent several years traveling around Massachusetts as an itinerant teacher and had a number of relationships with other women. She was visiting Vermont, in part, to escape the gossip that dogged her, Cleves said.

But Bryant was also worldly, fascinating and a talented seamstress — whatever her reputation, townspeople seemed happy to have her stay in Weybridge to make their clothes. Shortly after meeting Drake, she hired the younger woman as her assistant. When their friendship turned into a romance and Bryant asked Drake to move in, they were able to use their tailoring business as an excuse for the unusual arrangement.

“But from the beginning, their choice to live together was about their shared relationship,” Cleves said in a phone interview with The Washington Post. “They worked together in order to live together, not the other way around.”

Weybridge was a conservative, provincial town — not quite the stereotypical home of an early American lesbian couple. But townspeople tolerated Bryant and Drake’s marriage as an “open secret.”

“What made their relationship work was how public it was,” Cleves said. “Charity and Sylvia were enormously valued in the community. They did everything from leading the local charitable organizations to contributing money to the church … and people just chose not to know what was inconvenient to know.”

Though they turned a blind eye to the romantic aspect of Bryant and Drake living together, the couple’s families and neighbors widely referred to them as close to or nearly “married.” In his 1850 collection “Letters of a Traveller,” Charity’s nephew William Cullen Bryant, the famed poet and journalist, wrote: “In their youthful days, they took each other as companions for life, and … this union, no less sacred to them than the tie of marriage, has subsisted, in uninterrupted harmony, for forty years, during which they have shared each other’s occupations and pleasures and works of charity while in health, and watched over each other tenderly in sickness.”

Another local named Hiram Harvey Hurlburt recounted meeting the couple in his diary: “I heard it mentioned as if Miss Bryant and Miss Drake were married to each other,” he wrote. “I always heard they got along pleasantly together … Miss Bryant was the man, this I thought was perfectly proper.”

“All acknowledged Bryant and Drake’s marriage while denying its possibility,” Cleves wrote in her study. Comparing their relationship to a marriage without explicitly calling it one allowed the traditional residents of Weybridge to recognize Bryant and Drake as a couple without confronting the fact that they were lesbians, she said.

For their part, Bryant and Drake considered themselves married, according to Cleves. They celebrated the day they moved in together as their anniversary, and Bryant referred to Drake as her “helpmeet,” a common 19th-century synonym for “spouse.” Drake’s archives at the Henry Sheldon Museum of Vermont History in Middlebury also include a scrap on which she had written her and Bryant’s name over and over again, like every teen movie stereotype of a young woman in love. It reads: “Bryant, Bryant Charity, Bryant Sylvia, Bryant Sylvia, Bryant Charity, Bryant Sylvia.”

Their relationship was like that of most other 19th-century couples, Cleves said. Each took on a socially designated gender role — Bryant was the “husband,” who ran the business, and Drake the “wife” — and they shared a “purse,” a home and a bed. And their marriage’s lack of official recognition meant a lot less to them than it means to some gay couples today.

“The legal rights that are being struggled over now would have been less significant to Charity and Sylvia — they didn’t pay federal income taxes or have private health insurance,” Cleves said.

Cleves is a proponent of legalizing same-sex marriage and thinks that Bryant and Drake’s unofficial marriage ought to help modern-day gay couples gain legal recognition. In her study, she said that opposition to gay marriage has long relied on the argument that marriage is a traditional institution, something that has always involved just one man and one woman — a viewpoint that ignores the infrequent but documented occurrences of relationships like Bryant and Drake’s.

“Marriage has always been a changeable and plastic tradition,” she said. “This current movement, which I hope is going to result in a Supreme Court decision finding a federal right to same sex marriage, is about making this practice work for the present day, just as previous generations found ways to make it work in the past.”

For evidence of how well Bryant and Drake made it work, Cleves points to the couple’s tombstone, which stands in the Weybridge cemetery. Though the white granite is worn from weather and age, the couple’s names are still visible on its mottled surface.

That Bryant and Drake are buried together, under an expensive headstone with raised lettering, “is a testimony to the regard that the people who knew them held them in,” Cleves said. “The people of the town and the family chose to remember them as a married couple, and they spent extra money to make it beautiful.”