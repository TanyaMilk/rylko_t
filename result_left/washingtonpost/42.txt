Louisiana Gov. Bobby Jindal (R), a possible 2016 contender, is a relative unknown among voters; only 1 percent of Republicans and Republican-leaning independents prefer him, according to the most recent Washington Post-ABC News poll.

That could soon change.

Following on the heels of contentious religious freedom bills in Indiana and Arkansas, Jindal said he plans to support his state's own bill. Judging from how Indiana's bill catapulted Gov. Mike Pence (R) to the national spotlight, Jindal could soon see the same thing happen for him — and not necessarily in a good way. But Louisiana's debate could be different in one significant way.

Whereas Indiana and Arkansas had versions of the Religious Freedom Restoration Act, or RFRA, which included broad language that critics have said could have unintended consequences, Louisiana's Marriage and Conscience Act is more focused and deals specifically with religious beliefs in relation to same-sex marriage.

Polling suggests that could -- emphasis on could -- be more popular and more difficult for opponents to beat back. Critics can't as easily point to the possibility of the vague language leading to unintended discrimination, and polling shows half or more of voters support exempting religious businesses from serving gay weddings. A March 2014 Washington Post-ABC News poll found only 28 percent believe businesses should be able to refuse service to gay and lesbian people in general because of religious belief, but a January AP-GfK poll found 57 percent believe that wedding-related businesses should be able to refuse service. (A later Pew poll put it at 47 percent.)

Of course, that doesn't mean the bill won't have its critics. HRC legal director Sarah Warbelow said in a statement that Louisiana's bill is actually worse than other states'.

"This bill is worse than any RFRA in that it explicitly allows discrimination based on an individual's religious beliefs about marriage," she said. "Nobody gets to go into court for a balancing test, there's no interpretation by a state judicial system. It flat-out gives individuals a right to discriminate, period."

The bill would allow private businesses to refuse to recognize same-sex marriage and not provide the same benefits to same-sex married couples, and the bill's sponsor, state Rep. Mike Johnson (R), said he is considering changes to the bill, according to the Times-Picayune. So the final product could be different than the bill we see today.

Backing the bill is a calculated risk for Jindal, who already stood up for the original bill in Indiana that Pence later pushed to clarify. Although it isn't like other states', it looks as if the public reaction could be similar, and Pence had a very difficult time beating back the public pressure.

On the flip side, though, is that in a crowded GOP presidential field, it could also give Jindal a national profile more quickly and effectively than anything else he has done, and could turn him into something of a hero in socially conservative circles. Where other Republicans declined to be bold, he will argue, he stood up for religious liberty — a cause he is no stranger to.

It's a gamble, and one he looks prepared to make. And at least this time, the debate over religious freedom will be less in the abstract.