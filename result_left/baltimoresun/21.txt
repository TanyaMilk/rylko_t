With the creation of two panels devoted to combating heroin use, Gov. Larry Hogan has waded into a worsening crisis — one that has defied solutions for decades.

It once looked as if Maryland had brought some measure of control to its long-standing battle against the drug, driving down fatal overdose rates for years. In Baltimore, for example, overdose deaths plunged from more than 300 in 1999 to around 100 in 2010.

Then came a collective relapse.

Statewide deaths have climbed each year since 2010 and could top 500 when statistics for 2014 are finalized. The problem has also appeared to take on troubling new dimensions, with growing numbers of addicts in suburban and rural areas.

But identifying the factors behind those trends, along with developing an effective response, has not been easy. For years, officials in Maryland and across the nation have struggled to balance the sometimes conflicting approaches of law enforcement and public health.

The rising tide of deaths has been blamed on an explosion in the rates at which doctors prescribed opiate painkillers in the past decade followed by a federal law enforcement crackdown that drove addicts to seek out heroin instead. At the same time, the incredibly potent drug fentanyl has been mixed into some of the state's heroin supply and was been blamed for scores of deaths last year.

Lt. Gov. Boyd Rutherford, the governor's point man on the heroin issue, said previous approaches have been reactions to an increase in overdose deaths and have focused too much on driving down those numbers.

"When you're taking that approach, you're really at the end of the line," he said.

The task force that Hogan announced last week will have a more ambitious agenda, Rutherford said. It will explore prevention and treatment efforts, as well as ways to reduce the pipeline of illegal drugs throughout Maryland. One of the main goals is reducing the number of people addicted to opiates and heroin.

"We're not just reacting to the sudden surge of overdoses and overdoses deaths," he said. "We're taking a holistic approach."

In addition to the task force, which is scheduled to report back by Dec. 1, Hogan created a coordinating council of the agencies involved in tackling the heroin problem — a group that mirrors one set up by former Gov. Martin O'Malley in June. Hogan also announced a donation of equipment for the rapid treatment of heroin overdoses, and a $500,000 federal grant to increase treatment programs in the state's jails and prisons.

One of those Hogan appointed to the task force is Tracey Myers-Preston, executive director of the Maryland Addictions Directors Council.

Lynn Albizo, a spokeswoman for the council, credited O'Malley for working to reduce addiction but said the new task force will bring a broader range of professionals to the table. Under O'Malley, she said, only workers from state agencies were involved in the work to decrease overdoses.

"What O'Malley did was the first step," she said. "This task force will take a different approach and get more input from the community. It's a treatment approach instead of a criminal justice approach."

Beth McGinty, a professor at the Johns Hopkins Bloomberg School of Public Health, said the image of the crisis as one that affects suburbanites as well as city residents might have opened a route for that switch.

"We've got this whole group of heroin users who started out using prescription opioids," she said. "Many of these folks started out using these drugs legitimately."

McGinty said the journey of the addict who travels from pain patient to heroin user is easier to understand and sympathize with than that of an urban addict whose typical route to drug use is less clear and easier to blame on personal moral failings.

"The rhetoric among policymakers and among medical experts, the opinion leaders, I think, is very different than it has been in the past," McGinty said. Still, she said, her polling statistics show that respondents look down at abusers of prescription pills, as well as urban heroin addicts.

The fight to stop heroin addiction has raged for years. In the 1970s, police seized the initiative with the launch of the war on drugs. But health advocates have long had a voice in the debate —and it has become louder in recent years.

A public health approach is the best way to fight substance abuse, and Baltimore has already made good progress, city Health Commissioner Dr. Leana Wen said, pointing out that heroin was a problem in Baltimore before it hit many other cities.

With indicators showing that the enforcement of drug laws has fallen heavily on African-American communities, and with states across the country looking to cut their corrections budgets, an increased emphasis on treatment could be an appealing option.

In Baltimore, officials convened their own task force in October; it was given nine months to develop strategies to better coordinate treatment options. The quasi-public Behavioral Health System Baltimore oversees the city task force, which has about 50 members, including representatives from law enforcement, faith groups and the mayor's office.

Studies have estimated that it costs from about $26,000 to $38,000 annually to incarcerate a drug offender in Maryland, according to Behavioral Health System Baltimore.

Costs for treatment vary by the level of care and the length of a program, but the estimate is about $5,300, the group said.

Wen said one of her priorities is to re-energize and fight substance abuse in Baltimore. Because substance abuse is a chronic medical condition, a multi-pronged approach is needed, she said.

To reduce addiction, Wen said, it is crucial to screen individuals for the problem when they are seen by medical professionals or social workers. There should be a single phone number that people can call for "treatment on demand," so they can get help when they need it.

Meanwhile, there has also been some progress in addressing a pathway to heroin addiction.

Under a new program, the state can closely monitor how opiate medicines are being dispensed and monitor anyone who seems to be filling out multiple prescriptions. McGinty said that's worthwhile, but there still needs to be more research on how to stop people switching from pills to heroin.

There is also some indication that doctors themselves are recognizing the risks associated with the powerful pain pills they prescribe. A survey published this month in the American Medical Association's internal medicine journal found that doctors regularly saw problems with patients to whom they prescribed opioids, even those who follow the directions.