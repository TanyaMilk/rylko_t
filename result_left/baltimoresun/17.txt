Rep. Donna F. Edwards announced Tuesday that she will run for Senate, jumping into a still undefined race to replace retiring Sen. Barbara A. Mikulski.

The Prince George's County Democrat, first elected to the House in 2008, would be the first African American to represent the state in the Senate if elected -- and would be only the second black woman to serve in the body.

Edwards announced her plans in a two-minute video that went live Tuesday morning.

"I'm announcing my candidacy for the United States Senate," Edwards said in the video filmed at National Harbor. "When I step into Barbara Mikulski's shoes as your next senator, you will always know where I stand: With you."

Edwards, 56, has enjoyed a level of independence from state Democrats that may prove to be a benefit as she moves forward with a campaign. She won in 2008 by challenging 15-year incumbent Al Wynn in the primary, and was boosted by an aggressive advertisement campaign paid for by national liberal groups and unions.

Some of those same groups are already backing Edwards for Senate, even though the field is not yet defined.

"Donna Edwards has proven time and again that she's a bold progressive," Stephanie Taylor, co-founder of the Progressive Change Campaign Committee, said in a statement. "In the Senate, she'll fight alongside Elizabeth Warren for an economic populist agenda that includes expanding Social Security benefits, making college more affordable, and holding Wall Street accountable."

Edwards will face Rep. Chris Van Hollen of Montgomery County, the top Democrat on the House Budget Committee who is close with House Democratic leader Nancy Pelosi. Van Hollen is also a reliable vote for Democratic causes, and has already secured several high-profile endorsements, including from Senate Minority Leader Harry Reid.

Edwards takes a swipe at Van Hollen in the video on Social Security, an issue he faced some criticism for. Some progressive groups are upset over generally supportive statements Van Hollen has made supporting a deficit reduction plan. Van Hollen has fought against many of those reductions as well.

In the video, Edwards said she will support Social Security with "no ifs, buts or willing to considers."

Many more are considering the April 2016 primary, including most of the state's congressional delegation, Baltimore Mayor Stephanie Rawlings-Blake and several out-of-office candidates such as former NAACP director Ben Jealous.

A founding executive director of the National Network to End Domestic Violence, Edwards helped to push passage of the Violence Against Women Act of 1994. She also served as executive director of the Arca Foundation, a Washington-based that issues grants to promote labor and human rights, an end to the death penalty, environmental protection and other causes.

She holds degrees from Wake Forest University and a law degree from the University of New Hampshire.

Former Lt. Gov. Anthony Brown issued a statement following Edwards' announcement, saying, in part: “Time and again, Donna has proven that she puts working families ahead of Wall Street special interests, that's why she's emerged as a leading voice of opposition to the extreme agenda of the Republicans in Congress. I am encouraged that such a broad and diverse field of Democratic candidates are rising to carry on the tradition of effective leadership that Senator Mikulski embodies.”