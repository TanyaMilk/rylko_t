Polls consistently showed broad support for a 2012 ballot initiative in Massachusetts to permit doctors to prescribe a lethal dose of medicine to terminally ill patients.

Then, just weeks before the vote, Catholic institutions and donors stepped in, spending tens of thousands of dollars on a sophisticated TV ad campaign aimed at persuading voters that "physician-assisted suicide" represents a dangerous devaluation of life that could harm society's most vulnerable.

The question was defeated by a scant 2 percentage points.

The political arm of the Roman Catholic Church in Connecticut is determined to quash the issue in this state, as well.

The Connecticut Catholic Public Affairs Conference is part of a coalition lobbying heavily against House Bill 7015, which would allow patients with less than six months to live to seek a doctor's help in ending their life. Similar proposals have come up in each of the past two years, and each time fizzled.

"The concept of physician-assisted suicide is a major affront to the teachings of the church,'' said Michael C. Culhane, executive director of the Catholic conference. "It was defeated twice and I believe it is going to be defeated this year, as well."

For the Catholic hierarchy, the stakes could not be higher. Though it remains a player in policy debates at the state Capitol, the church has seen a sharp erosion of its political clout in recent years, due to declining numbers, internal scandals and a flock increasingly reluctant to embrace church doctrine, particularly on matters relating to sex and gender.

In heavily Catholic blue states such as Massachusetts and Connecticut, the bishops have found themselves on the losing side of key cultural battles, from abortion to transgender civil rights.

"This is an important fight for them,'' said Maurice Cunningham, a professor of political science at the University of Massachusetts Boston, who has written about the church's role in the aid-in-dying debate. "On issues of sexuality, the teachings of the church, even among Catholics, are rejected quite often. ... This is different.''

A Quinnipiac University poll of Connecticut voters released earlier this month found nearly 2-to-1 support for aid-in-dying. (The survey did not break down that support along religious lines.)

The church isn't the only organization mobilizing against the measure: Groups advocating for people with disabilities, hospice providers and the Connecticut State Medical Society all testified at a recent public hearing against the aid-in-dying bill.

"To label this as a Catholic issue and put the church out there as the only one opposed to it is false,'' Culhane said. In Massachusetts, the Roman Catholic Archdiocese of Boston was part of a large and diverse coalition against Ballot Question 2.

Template For Connecticut

"Massachusetts is a fascinating case study and a template for Connecticut,'' said Peter Wolfgang, executive director of the Family Institute of Connecticut and an outspoken opponent of aid in dying.

"Our opponents think there are people out there, like the big bad Catholic church, who want to keep people alive against their will and want them to suffer,'' Wolfgang said. "What we're really talking about is licensing doctors to prescribe a death cocktail and receive legal immunity for it.''

But supporters of the proposal have homed in on the church's high-profile role. In their view, efforts by Catholic leaders to prevent dying patients from exercising a profoundly intimate choice at the end of life echoes earlier struggles.

"This is the same organization that spent money to fight gay rights, the morning-after pill for rape victims, insurance coverage for contraception, civil unions and same-sex marriage,'' said Betty Gallo, a lobbyist for Compassion & Choices, a national group promoting aid-in-dying legislation in statehouses around the nation.

"Connecticut citizens overwhelmingly supported every one of those rights becoming law and they support aid in dying too. The Catholic Church has been on the wrong side of every social issue of our time and they are on the wrong side of this one, too," said Gallo, a veteran Capitol lobbyist for progressive causes such as gay rights and gun control.

The church is deeply invested in efforts to stop the legislation. It is sponsoring a multimedia public relations campaign that includes print and radio spots, as well as ads on buses. Letters were inserted into church bulletins and some parishes have sponsored forums on the topic. And the Catholic conference has retained two outside lobbying firms — Reynolds Strategy Group and Updike, Kelly and Spellacy — to advocate against the bill.

Tim Appleton, Connecticut campaign manager for Compassion & Choices, said the church's focus on aid in dying is an example of "misplaced priorities."

"Nonprofits that assist the poor, the disabled and the mentally ill are under great stress right now,'' Appleton said. "It would be great if the money spent by the church on this campaign was spent helping those groups, instead."

Sanctity Of Life

Culhane said the church's investment in efforts to defeat the bill is dwarfed by sums spent by Compassion & Choices. But he made no apologies for the church's involvement in the debate, which is deeply embedded in a core tenet of Catholic doctrine: the sanctity of life.

"This issue fits in perfectly with the teaching of the church as it relates to life issues,'' Culhane said. "We believe life is sacred from natural conception to natural death. That's why we supported the repeal of the death penalty ... and that's why we oppose assisted suicide.''

On a snowy Sunday in February, more than 50 people gathered in a community room at the Church of St. Ann in Avon to learn more about what a panel of speakers referred to as "physician-assisted suicide." On each chair was a piece of paper with the names and numbers of members of the legislature's judiciary committee, which is considering the aid-in-dying bill.

Three states — Oregon, Washington and Vermont — now have laws allowing doctors to prescribe lethal medications to the terminally ill. The bill under consideration in Connecticut would apply only to patients deemed mentally competent who have less than six months to live.

Supporters say such laws provide a sense of control to dying patients, even those who never wind up seeking a doctor's help to end their life.