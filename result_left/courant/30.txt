The course of true love never did run smooth, especially when one is an infamous mass murderer serving a life sentence.

Charles Manson, 80, and 27-year-old fan Afton Elaine Burton have yet to wed despite time running out on their marriage license, said Terry Thornton, spokeswoman with the California Department of Corrections and Rehabilitation.

The couple’s license, drafted in November, expires Thursday. Inmate weddings take place on the weekends, during visiting hours.

“A Manson wedding did not take place,” she said. Manson is incarcerated in Corcoran State Prison.

Initially sentenced to death for his role in the 1969 slayings of pregnant actress Sharon Tate and six others in Los Angeles County, Manson had his sentence changed to life in prison when California's death penalty was abolished for a time in the 1970s.

He has been denied parole a dozen times, and his next parole hearing is scheduled for 2027, when he will be 92 years old. He has been in prison since 1971.

It is unclear why a wedding has not been held. Thornton said she couldn't speculate or speak on the couple's behalf.

According to records with the Kings County Clerk-Recorder, Manson and Burton applied for the marriage license on Nov. 7.

Manson is allowed to have "contact visits," so there would have been no glass between the couple during the wedding ceremony, Thornton said. Inmates also are allowed to wear wedding rings, she said.

But Manson does not qualify for overnight family visits -- better known as conjugal visits.

Burton, who also goes by the name Star, had said what she wanted out of the marriage: As Manson's wife, she would have been better able to work on his case. The expiring license indicated that Burton intended to take Manson's last name if they had married.

They could still apply for another marriage license.

James McGrath, a New York City photo agency editor, said he maintained contact with Burton and that she intended to obtain another 90-day license and go ahead with the marriage.

Manson wouldn't be the first of his murderous family to marry while in prison. Susan Atkins, a Manson follower who was convicted of eight murders, married twice while she was serving 38 years of a life sentence before dying at the age of 61.

Manson follower Charles "Tex" Watson, 68, married, fathered four children and divorced while in prison before prison officials in the mid-1990s banned conjugal visits for inmates with life sentences.

The slayings for which the Manson family were convicted all occurred in the summer of 1969. In July, Gary Hinman, 34, a musician, was stabbed to death after refusing to turn over his money and property to Manson.

About a week later in early August, four Manson followers -- Susan Atkins, Patricia Krenwinkel, Watson and Linda Kasabian -- made their way through the Hollywood Hills to the Benedict Canyon estate rented by Tate and her husband, director Roman Polanski.

Steven Parent, 18, a friend of the estate's caretaker, was the first to die. Before being stabbed to death, Tate, 26, begged for her unborn child to be spared. Also killed were hairstylist Jay Sebring, 35; Voytek Frykowski, 32, a friend of Polanski's; and Abigail Folger, 25, a coffee heiress and Frykowski's girlfriend.

Later, Manson himself entered the Los Feliz home of Leno LaBianca, 44, owner of a small supermarket chain, and tied up LaBianca and his wife, Rosemary, 38. He left them to die at the hands of Watson, Krenwinkel and Leslie Van Houten.

Donald “Shorty” Shea, a hand at the Spahn Ranch in Chatsworth, where the Manson family lived for a time, would be killed later and his body concealed on the ranch for years. He was the last victim whose killing yielded a conviction.

Times staff writers Christine Mai-Duc and Richard Winton contributed to this report. The Associated Press was used in compiling the report.

Follow Ryan Parker on Twitter, Facebook and Instagram