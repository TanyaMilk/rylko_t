The second phase of the trial of Boston Marathon bomber Dzhokhar Tsarnaev will not begin until April 21, almost two weeks after his conviction in the 2013 deadly bombing.

A judge granted a request Friday from Tsarnaev's lawyers to have a recess to give the defense time to resolve logistical issues with potential witnesses.

During the second stage — known as the penalty phase — the same jury that convicted Tsarnaev will be asked to decide whether he receives the death penalty or is sentenced to life in prison.

The delay means the trial won't be held on two sensitive dates: April 15, the second anniversary of the attack; and April 20, this year's running of the marathon.

The bombings killed three people and wounded more than 260.

Associated Press