Several weeks ago, before I even thought about seeing "American Sniper," I was on a plane on a runway in Philly awaiting takeoff to New Haven. It was the last leg of several delayed and canceled flights that had seen me sleeping one night in Chicago's O'Hare Airport and spending half the day and night in Philly's. Now I had made it aboard after being No. 9 on standby. I sat back in my window seat, feeling lucky.

Then I noticed the guy in the seat in front of me staring through a sniper scope.

Mesmerized, I watched as he calmly lined up his target and pulled the trigger. There was no bang, but I saw the splattering of blood and saw the man fall dead.

I sat there as he repeated the process, changing and assembling a new weapon before shooting man after man after man.

When he finally put away his smartphone, I thought about this sniper game app and the business of playing it aboard a plane. Should he be allowed to do this on a plane?

This flashed through my mind as I sat in the North Haven theater last week watching "American Sniper," the movie about Chris Kyle, the Navy SEAL credited with 160 kills in Iraq.

The movie is a box office hit, though discussion of late has centered on whether he was a hero as depicted, or a coward as posited by Michael Moore.

For one, I find it difficult to call anyone a coward who goes off to war facing the risk of being killed. Certainly, I understand the notion of Kyle's saving American lives by shooting those about to kill Americans. It is similar to American fighter pilots serving as bomber escorts during World War II, whose job was to shoot down enemy planes before they could shoot down Americans.

But I had just seen "Selma" and with the bomb blast from that church echoing in my mind, it troubled me that this idea of protecting Americans can be extrapolated in so many ways to support one's actions. It can be said those white Southerners who bombed the 16th Street Baptist church killing four black girls did so to protect their way of life. The same can be said for all those who beat and killed American protesters during the civil rights movement — including the sniper who killed the Rev. Martin Luther King Jr..

And having recently interviewed a 100-year-old black woman who recounted the things done to blacks by whites, it makes me cringe to hear a white American calling others "savages," as Kyle's character did in the movie.

Yes. No one can deny that the things that occur in war can be gruesome and border on savagery. The scene of an American soldier hanging dead and of body parts in a room certainly depicts savagery. But such savagery also occurred in America.

The thing is, it is hard to fully board the Chris Kyle-as-hero bandwagon when knowing he bragged — though it has been seriously questioned as to whether he actually did it — about going to New Orleans during Hurricane Katrina, setting up shop as an American sniper atop the Superdome, and shooting down 30 looters. For one, it can be presumed the people he allegedly shot weren't white "looters." (There remains that wire service caption to a picture that referred to a pair of whites as "finding" food while a black was described as "looting." )

Whether it occurred or not, the fact remains that he thought about it, which suggests that Kyle would be just as comfortable and remorseless at killing African-Americans in America as he was at killing Muslim "savages" in Iraq.

Something about this just does not sit easy with me.

Frank Harris III of Hamden is a professor of journalism at Southern Connecticut State University in New Haven. His email address is frankharristhree@gmail.com. Follow him on Twitter at fh3franktalk.