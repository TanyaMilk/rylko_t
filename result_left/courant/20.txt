The state Supreme Court this week helped to clean up some of the wreckage left by the ill-conceived war on drugs — a war that filled prisons and cost billions but has failed so far in its objective of stomping out drug use.

In a welcome decision Monday, the state high court ruled that in 2011 the General Assembly in effect legalized the possession of less than a half-ounce of marijuana, thus clearing the way for thousands of people to have pre-2011 convictions erased.

The unanimous ruling came in the case of former Manchester and Bolton resident Nicholas Menditto, who wanted the state to erase his two 2009 marijuana possession convictions.

"The legislature has determined that such violations are to be handled in the same manner as civil infractions such as parking violations," Justice Carmen Espinosa wrote. "The state has failed to suggest any plausible reason why erasure should be denied in such cases."

This is the way a second-chance society works.

The court has made it possible to lift from the shoulders of thousands the stigma of a criminal conviction that follows them for life and in many cases shuts the door to jobs and social acceptance.

Possession of small amounts of marijuana should be treated as a public health problem, not a criminal justice problem, and that's what Connecticut is doing.

Education and taxation are more effective and less expensive means of addressing the problem, as they were with tobacco.

This state is one of 23 that allows the use of marijuana for medical purposes. It is one of 18 that have decriminalized possession of varying amounts of marijuana.

The 2011 legislation that became law changed possession of less than a half-ounce of marijuana from a misdemeanor punishable by a possible prison sentence to a civil violation with a $150 fine for a first offense and fines of $200 to $500 for subsequent offenses.

And now the court's decision. Connecticut is on the right track.