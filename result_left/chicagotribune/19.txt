Indiana Gov. Mike Pence said he would support legislation to "clarify the intent" of a new state law that has attracted widespread criticism over concerns it could allow discrimination against gay people.

In an interview Saturday with the Indianapolis Star (http://indy.st/1MhuY1d), the Republican governor said he's been in discussions with legislative leaders this weekend. He expects that a clarification bill will be introduced this coming week to the religious objections law he signed Thursday. Pence declined to provide details but told the newspaper that making gay and lesbian Indiana residents a protected legal class is "not on my agenda."

Pence disputes that the law allows state-sanctioned anti-gay discrimination, as some Indiana businesses, convention organizers and others have argued. He says he didn't anticipate "the hostility that's been directed at our state."

Since Republican Gov. Mike Pence signed the bill into law Thursday, Indiana has been widely criticized by businesses and organizations around the nation, as well as on social media with the hashtag #boycottindiana. Local officials and business groups around the state hope to stem the fallout, although consumer review service Angie's List said Saturday that it is suspending a planned expansion in Indianapolis because of the new law.

Meanwhile, hundreds of people, some carrying signs reading "no hate in our state," gathered Saturday outside the Indiana Statehouse for a boisterous rally against a new state law that opponents say could sanction discrimination against gay people.

Pence and other supporters of the law contend discrimination claims are overblown and insist it will keep the government from compelling people to provide services they find objectionable on religious grounds. They also maintain that courts haven't allowed discrimination under similar laws covering the federal government and 19 other states.

But state Rep. Ed DeLaney, an Indianapolis Democrat, said Indiana's law goes further than those laws and opens the door to discrimination.

"This law does not openly allow discrimination, no, but what it does is create a road map, a path to discrimination," he told the crowd, which stretched across the south steps and lawn of the Statehouse. "Indiana's version of this law is not the same as that in other states. It adds all kinds of new stuff and it moves us further down the road to discrimination."

The measure, which takes effect in July, prohibits state laws that "substantially burden" a person's ability to follow his or her religious beliefs. The definition of "person" includes religious institutions, businesses and associations.

Angie's List had sought an $18.5 million incentive package from Indianapolis' City-County Council to add 1,000 jobs over five years. But founder and CEO Bill Oseterle said in a statement Saturday that the expansion was on hold "until we fully understand the implications of the freedom restoration act on our employees."

Saturday's crowd, for which police didn't have an exact estimate, chanted "Pence must go!" several times and many people held signs like "I'm pretty sure God doesn't hate anyone" and "No hate in our state."

In the newspaper interview, Pence said he didn't expect the reaction the law has generated.

"I just can't account for the hostility that's been directed at our state," he said. "I've been taken aback by the mischaracterizations from outside the state of Indiana about what is in this bill."

Zach Adamson, a Democrat on Indianapolis' City-County Council, said to cheers that the law has nothing to do with religious freedom but everything to do with discrimination.

"This isn't 1950 Alabama; it's 2015 Indiana," he told the crowd, adding that the law has brought embarrassment on the state.

Among those who attended the rally was Jennifer Fox, a 40-year-old from Indianapolis who was joined by her wife, Erin Fox, and their two boys, ages 5 and 8, and other relatives.

Fox said they married last June on the first day that same-sex marriage became legal in Indiana under a federal court ruling. She believes the religious objections law is a sort of reward to Republican lawmakers and their Conservative Christian constituents who strongly opposed allowing the legalization of gay marriage in the state.

"I believe that's where this is coming from — to find ways to push their own agenda, which is not a religious agenda; it's aimed at a specific section of people," Fox said.

Although many Indianapolis businesses have expressed opposition to the law and support for gays and lesbians, Fox worries her family could be turned away from a restaurant or other business and that her sons would suffer emotionally.

"I certainly would not want them to think that there's something wrong with our family because we're a loving family," she said.

Indianapolis Mayor Greg Ballard, a Republican who opposed the law, said he and other city officials would be talking to many businesses and convention planners to counter the uproar the law has caused. "I'm more concerned about making sure that everyone knows they can come in here and feel welcome," Ballard said.

The Indianapolis-based NCAA has expressed concerns about the law and has suggested it could move future events elsewhere; the men's Final Four will be held in the city next weekend.

On Saturday, the founder of Angie's List said the consumer review service is suspending its plans for a $40 million expansion in Indianapolis because of Indiana's new religious objections law.

Founder and CEO Bill Oesterle said in a statement Saturday the expansion that called for adding 1,000 jobs over five years is now on hold "until we fully understand the implications of the freedom restoration act on our employees."

Oesterle says the Indianapolis-based company is now "reviewing alternatives for the expansion of its headquarters."

Angie's List was seeking an $18.5 million incentive package from the City-County Council. The council had delayed a decision on that proposal.

Associated Press