In the wake of Mitt Romney’s announcement that he would not be seeking the Republican nomination for President, a new poll out of Iowa shows Wisconsin Governor Scott Walker surging to a narrow lead ahead of other potential 2016 Republican in the state that will hold the first contest in the nation:

Presidential stage newcomer Scott Walker, the conservative reform pit bull who inspired death threats from the left, has become the one to watch in the race for the Republican nomination a year out from the Iowa caucuses.

At 15 percentage points, he leads a big, tightly packed field of potential contenders in a new Des Moines Register/Bloomberg Politics Iowa Poll of likely Republican caucusgoers. The caucuses are scheduled for Feb. 1, 2016.

The Wisconsin governor is also the No. 2 most popular choice for likely caucusgoers who want an establishment candidate, and he’s the No. 2 for those who want an anti-establishment candidate, the poll shows.

“He’s in a sweet spot,” pollster J. Ann Selzer said. “People who don’t want an ultra-conservative think he’s OK. People who don’t want a moderate think he’s OK.”

Just one point behind is Rand Paul, a U.S. senator from Kentucky and the son of three-time presidential candidate Ron Paul, a hero to dissidents who want to shake up government. Paul draws support from the same anti-establishment well.

Rounding out the top tier are Mitt Romney, the GOP’s 2012 presidential nominee; Mike Huckabee, the 2008 winner of the Iowa caucuses; Ben Carson, a best-selling author and famed brain surgeon; and Jeb Bush, a relative to two past presidents.

The day after polling wrapped up, Romney announced he’s out of the competition. When the numbers in this poll are shuffled — by giving Romney’s votes to the contenders his supporters named as their second-choice pick — the five others in the top tier gain support.

Huckabee, a former TV commentator and two-term Arkansas governor, benefits the most, picking up 3 percentage points. The pecking order doesn’t shift, though.

For the bottom tier, the horse race ranking shifts slightly without Romney in the mix. New Jersey Gov. Chris Christie moves up a notch to tie with Texas U.S. Sen. Ted Cruz for sixth place. Former U.S. Sen. Rick Santorum of Pennsylvania stays in eighth. Florida U.S. Sen. Marco Rubio climbs one spot into ninth, followed by former Texas Gov. Rick Perry.

Louisiana Gov. Bobby Jindal is next, followed by a three-way tie among TV star and real estate developer Donald Trump, former computer company CEO Carly Fiorina and Ohio Gov. John Kasich. Indiana Gov. Mike Pence doesn’t register on poll respondents’ radar.

Sophisticated campaign operatives will now decide which candidate they have to topple for their candidate to rise — and begin targeting them with negative information, said Katie Packer Gage, a Washington, D.C.-based strategist who was deputy campaign manager for Romney in 2012.

“This is where campaigns start to matter,” Gage said. “Huckabee will go hard after Santorum. Jeb and Christie will go to war. Cruz and Paul will figure out that they have to take Carson down, then each other.”

The poll of 402 likely Republican caucusgoers was conducted Jan. 26-29 by Selzer & Co. of Des Moines. The margin of error is plus or minus 4.9 percentage points.

Here are the numbers both with and without Romney factored in:

Walker’s apparent surge comes just about a week after his appearance at a conservative forum that received widely positive reviews on the right. In addition, of course, Walker generally has wide approval from conservatives going back several years now due largely to the fact that he has managed to survive a fairly strong onslaught from nationwide Democrats and activists in the wake of his efforts to scale back the powers of public employee unions. The additional fact that he has managed to win three statewide elections over the course of four years, his initial election in 2010, a recall election in 2012, and then re-election last November, by increasing margins in a state that has gone Democratic in every Presidential election since 1988, while also managing to maintaining a Republican majority in the state legislature, has but him at the top of the list of mid-western Governors that many have pointed to as potential 2016 candidates should the “establishment” trio of Romney, Bush, or Christie end up failing. Indeed, one of the advantages that Walker seems to have at this point is that he seems to be attracting support from establishment figures and from conservatives. If he manages to survive the inevitable onslaught and things play out in just the right way, Walker could find himself well positioned to pull off a win in Iowa that could propel him into the top tier of 2016 contenders.

That being said, it’s worth noting something about the Iowa Caucuses when it comes to the Republican Party. Unlike the Democrats, where the winner in Iowa has frequently gone on to win the party nomination and, in three cases the Presidency itself, the Hawkeye State has not been a very good predictor for Republicans. Only three of the candidates who won the caucuses in years where they were contested went on to win the party’s nomination, and only one of those three George W. Bush in 2000 went on to win the Presidency. If you’re looking for states that are predictors of how the race for the Republican nomination might turn out, it’s better to look to New Hampshire and South Carolina, both of which have done a fairly good job over the years of tracking the ultimately winner of the Republican nomination. Given that, it’s arguably not very important how Walker does in Iowa unless he’s able to translate that into support in the states that follow it on the calendar. Assuming that he enters the race, which seems more and more likely every day, his ability to carry this momentum forward will be the biggest question surrounding a Walker campaign. In the end, he will either be a George W. Bush who manages to win in Iowa and then translate that into support across the country, or he’ll be another Huckabee or Santorum who finds himself unable to translate a Hawkeye State win into anything of substance.