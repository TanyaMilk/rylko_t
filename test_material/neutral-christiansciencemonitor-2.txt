Mountain View, Calif. — The latest version of Google's self-driving car — a pod-like two-seater that needs no gas pedal or steering wheel — will make its debut on public roads this summer, a significant step in the technology giant's mission to have driverless cars available to consumers in the next five years.

This prototype is the first vehicle built from scratch for the purpose of self-driving, Google says. It looks like a Smart car with a shiny black bowler hat to hide its sensors, and it can drive, brake and recognize road hazards without human intervention. It has more capabilities than the prototype Google introduced last May, which was so rudimentary it had fake headlights.

The new pod isn't designed for a long trip, or a joyride. It lacks air bags and other federally required safety features, so it can't go more than 25 miles per hour. It's electric, and has to be recharged after 80 miles. And the pod can only drive in areas that have been thoroughly mapped by Google.

At first, it will likely even have a steering wheel and gas pedal — current California regulations require them. Those regulations also require a driver to be able to take back control of the car at any time. But Google is lobbying for more flexible regulations.

Google will initially build and test 25 pods, mostly in neighborhoods surrounding its Mountain View headquarters. It will eventually build between 50 and 100, and will broaden testing to sites that are hillier and rainier.

The ultimate goal, says Google co-founder Sergey Brin, is computer-controlled cars that can eliminate human error, which is a factor in an estimated 90 percent of the 1.2 million road deaths that occur worldwide each year. Self-driving cars could also improve traffic congestion and transport the elderly and disabled.

Google shocked the auto industry in 2010 with its announcement that it was working on a driverless car. Brin insists Google doesn't aspire to be a car company, but wants its technology to be adopted by automakers.

"We want to partner to bring self-driving to all the vehicles in the world," Brin told a group of journalists and community members gathered earlier this week to take rides in the prototype.

For now the traditional automakers are pursuing their own self-driving technology, but with less ambitious timeline of 10 to 15 years for a truly driverless car.

Chris Urmson, who directs Google's self-driving car project, says the slow-moving, friendly looking prototype — his young son thinks it looks like a koala because of the nose-like black laser on the front — is a good bridge between the company's current test fleet of 20 specially outfitted Lexus SUVs and the more advanced, higher-speed driverless cars of its future, which might not even look like anything on the road today.

"This vehicle is really all about us learning. This vehicle could go on a freeway, but when we think about introducing the technology, we want to do that very thoughtfully and very safely," Urmson says.

Convincing drivers that driverless technology is safe is one of the hurdles the company must overcome. Earlier this week, in response to questions from The Associated Press, Google acknowledged 11 minor accidents in the six years it has been testing autonomous cars. Urmson says the company is proud of that record, and notes that Google's vehicles have completed more than 1.7 million miles of testing. He says all but one of the accidents were caused by drivers in other cars; in the only incident caused by a Google car, a staffer was driving in manual mode.

Consumers question whether they can trust self-driving cars to work all the time, who will be liable if there's an accident and how self-driving cars will interact with regular cars, says the consulting firm J.D. Power and Associates. In a 2013 survey of U.S. drivers, J.D. Power found only one in five was interested in a fully autonomous car.

Urmson says Google needs to do a better job of educating people about self-driving technology and updating them on Google's progress. It's building a Web site to teach people about the technology, and the site will feature a monthly report that will include details of any accidents involving Google cars. The site will also have a section where people can send feedback when they interact with the cars.

The prototype cars — assembled in suburban Detroit by Roush Industries — have the same array of radars, lasers and cameras as Google's fleet of Lexus SUVs, which allows them to share data. If one car's camera spots orange cones and construction signs, for example, it will alert all the others to slow down in that area or reroute around a lane closure.

Dmitri Dolgov, the head of software for the self-driving car project, says Google's software has gotten much better over the last year at classifying objects, like trees and mailboxes, and predicting behavior of pedestrians and other cars. For example, Google's cars will slow down if they sense that a car in the next lane is speeding up to cut in front of them. And in one recent test, a Google car paused when a cyclist ran a red light. Another car, driven by a human, went ahead and nearly hit the cyclist.

The system isn't perfect. On a test drive, one of Google's Lexus SUVs seemed momentarily confused when a mail truck partially blocked its path. Later, during a demonstration drive in Google's parking lot, the prototype — without a wheel or pedal — braked when it spotted a row of folding chairs. It had to figure out that the chairs wouldn't move before it proceeded.

Dolgov says it's impossible to predict everything its test cars might see, so they're programmed to act in the most conservative way when they confront something unusual, like the time a Google SUV stopped and waited while a woman in a wheelchair chased a duck with a broom.

Google isn't alone in developing self-driving cars. Mercedes-Benz, Infiniti and other brands already have advanced driver assistance systems, like lane keeping and adaptive cruise control, that can pilot the car on the highway with minimal input from the driver. Unlike Google, automakers think self-driving cars will arrive feature-by-feature instead of all at once, giving people plenty of time to adapt to autonomous driving.

But Urmson says that approach is "fundamentally wrong."

"We believe that's like saying, 'If I work really hard at jumping, one day I'll just be able to fly,'" he said.

Egil Juliussen, the principal analyst of infotainment and advanced driver assist systems for the consulting firm IHS Automotive, says Google's "moon shot" strategy is difficult and riskier than just adding features to existing cars. But he thinks it could ultimately be successful. Google could make self-driving urban pods for universities or urban centers, for example, or sell its technology to automakers.

Brin says the company is still refining its plans for self-driving cars, but he's excited about their potential.

"Our goal is to create something safer than human drivers," he said.