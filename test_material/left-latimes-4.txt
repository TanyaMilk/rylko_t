New Jersey Gov. Chris Christie's response in a recent radio interview to a question about legal marijuana was in keeping with his tough-on-crime persona.

A former prosecutor and potential presidential candidate, Christie has long been a staunch opponent of pot, at one time lambasting tax revenue generated from the sale of legal recreational marijuana as "blood money."

"I will crack down and not permit it," he told radio host Hugh Hewitt, who had asked whether legal marijuana sales in Colorado and Washington state should be allowed. "Marijuana is an illegal drug under federal law. And the states should not be permitted to sell it and profit from it."

Christie's comments put him on the conservative end of the divide over marijuana among both declared and likely Republican candidates for president, one that goes far beyond whether they've ever inhaled. Each of the current and prospective members of the GOP field opposes full legalization of marijuana, although they differ somewhat on medicinal use.

But if the candidates are hewing to the views of Republican voters on the question of legalization, they are running against the tide of opinion in the country overall, a conundrum the party faces on a host of social issues, including same-sex marriage.

In a recent Pew Research Center survey, 53% of Americans polled said they supported legalizing marijuana, compared with 44% who were opposed. The political divide was stark. Only 39% of Republicans favored legalization, compared with 59% of Democrats and 58% of independents.

Still, there was some nuance among Republicans: When asked whether the federal government should enforce its anti-pot laws in states that allowed marijuana use, 54% said it should not, while 43% said the government should enforce federal marijuana laws.

That result points to one of the central dilemmas confronting the party's voters and candidates on the issue of marijuana: They favor a weaker federal government and giving more power to the states in general, but when it comes to pot, a substantial bloc of the party wants the federal government to rein in the states.

"This whole idea of legalized marijuana is twofold for Republicans," said David Kopel, an associate policy analyst at the Cato Institute who has researched drug policy. "Opposition to marijuana use plays well with conservatives, which is the core voter base in the primary. Yet that stance is not popular with the larger electorate."

And, he said, the idea of "state's rights and limiting the reach of the federal government" is crucial to Republican voters — and thus to candidates seeking support.

In the range of Republican views, Christie's opposition to state decision-making and medicinal use of marijuana are the most conservative.

Former Florida Gov. Jeb Bush and Sens. Marco Rubio of Florida and Ted Cruz of Texas have said they oppose marijuana legalization — either medicinal or recreational — but agree that it's up to the states to decide.

Wisconsin Gov. Scott Walker shares the same view on marijuana, though last year he signed a bill that allows cannabinoid oil to be used to treat children who suffer from seizures.

Cruz has cited as a cautionary example the swing state of Colorado, which legalized recreational marijuana sales in 2012.

"If we see in Colorado teen drug use skyrocketing dramatically, I suspect the citizens of other states are going to be a lot slower to make that change if we see in the laboratory of democracy, gosh, this policy is really hurting people," Cruz said in an interview with a local reporter in March. Several surveys taken since legal marijuana sales took effect in Colorado more than a year ago have shown no increase in use by teens.

Sen. Rand Paul of Kentucky, who has tapped himself as "a different kind of Republican," is on the permissive end of the spectrum among the GOP field.

Paul has sought to ease penalties for drug convictions and supports medicinal marijuana, though he has not supported legalizing marijuana for recreational use. (His father, Republican former Rep. Ron Paul of Texas, has been a vocal supporter of legalizing marijuana and has worked in Congress to end federal regulations that make it illegal.)

In February, Sen. Paul, who has gone on record as having smoked marijuana in the past, called Bush a "hypocrite" for opposing medicinal marijuana use despite having smoked pot himself. (Cruz also acknowledges having smoked pot.)

"If you got MS in Florida, Jeb Bush voted to put you in jail if you go to a local drug store and get medical marijuana," Paul said in an interview with Yahoo. "Yet he was doing it for recreational purposes and it's a different standard for him, because he was from a very wealthy family going to a wealthy school and he got off scot-free."

Hillary Rodham Clinton, the Democratic front-runner, has not commented recently on marijuana legalization. In a July radio interview, she said more studies were needed to "see what kind of results we get, both from medical marijuana and from recreational marijuana, before we make any far-reaching conclusions." During her first White House run in 2008, Clinton was opposed to legalization.

Marijuana legality remains a potent issue in key electoral states, which guarantees that the candidates will be drawn into the debate.

Colorado is at the center of a lawsuit filed with the Supreme Court by Nebraska and Oklahoma — two conservative heartland states — which alleges legal marijuana is flowing across state borders and burdening their communities. The Supreme Court has not decided whether it will hear the case.

Laura Carno, a Republican strategist in Colorado, said the argument to keep the federal government from infringing on state's rights resonates with Republicans and unaffiliated voters, which are a key voting bloc in her presidential swing state.

"There's an independent spirit in the West, and in Colorado this is a new experiment," Carno said. "But it doesn't matter if the candidates are for or against marijuana. The bigger, more important question is we don't want Washington telling us what to do. If that's your platform as a candidate, it's not going to play well. So on marijuana, and issues like healthcare, it's a safe bet for these candidates to leave these decisions up to the states."

Floyd Ciruli, a Denver-based nonpartisan pollster who has surveyed voters on the issue for several years, said the idea of medicinal marijuana is not up for debate in Colorado, even among Republicans.

"It's been around since the early 2000s and is here with little or no dispute," he said. "Even with legal marijuana, the recreational sorts, the idea among Democrats and certainly many Republicans is this 'live and let live' attitude."