Damion (World) Hardy, a larger-than-life drug dealer from Brooklyn whose alter ego dated rapper Lil' Kim and entertained a fantasy of being a hip-hop music impresario, was sentenced Monday to life in prison for orchestrating six murders.


	Hardy's enforcer, Aaron (E-Bay) Granton was also slammed with a life sentence for being the triggerman in five killings carried out for the Cash Money Brothers drug crew, which terrorized the Lafayette Gardens project for more than a decade.


	Federal Judge Frederic Block said it is not an easy chore to send someone away so they will die in prison.


	“In this particular case, I can't think of a case where it's more justified,” Block said, adding that it is highly possible they would have gotten the death penalty had the U.S. Attorney General sought the ultimate punishment against them.


	Hardy and Granton, both 40, showed no remorse and looked bored as seven family members of their victims stood in the courtroom and delivered heart-wrenching impact statements.


	Prosecutors had argued the killings were carried out in connection with drug turf wars and as retribution for the murder of Hardy’s brother, Myron (Wise) Hardy, who co-founded Cash Money Brothers.


	The crew got the name from the gang in the Wesley Snipes film “New Jack City.”


	“They (the victims) weren't angels in the streets, but they were angels in our lives,” said Epiphany Baum, 27, the daughter of murder victim Tyrone Baum.


	Three members of the anonymous jury panel who found the defendants guilty of all charges on April 29 returned to Brooklyn Federal Court to observe the sentencing.


	“We feel a connection to the case and it's closure for us too,” Juror No. 2 said outside court.


	Hardy was busted more than a decade ago, but he suffers from schizophrenia and was not deemed competent to stand trial until the feds obtained a court order to forcibly medicate him. Defense lawyer Jean Barrett insisted in court papers that Hardy is still not mentally competent.


	The vicious thug, who lorded over a gang known for their ultra-violence and wearing two diamond earrings, stood up and told Block: “I am 100% completely innocent.”


	Granton mocked the victims' family members, reminding them of the times when they were his friends and "You all stood around me and ate candy out of my pocket."


	Zakia Baum, the sister of victims Tyrone and Darryl Baum, said Hardy's bloodlust was about one thing and one thing only — his desire for power. “The only person you care about was your alter ego 'World,'” she said.