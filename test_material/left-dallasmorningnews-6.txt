NEW ORLEANS — Lawyers clashed Friday over Texas’ and two nearby states’ marriage laws and whether they encourage childbirth or just discriminate against gays and lesbians.

In wide-ranging arguments about sex, fertility, free lunches and judges’ proper roles, the squads of state lawyers and plaintiffs’ lawyers made their best case before a three-judge panel of the 5th U.S. Circuit Court of Appeals.

The session was part of a nationwide rush to place the constitutionality of states’ gay marriage bans before the Supreme Court. Although it met privately Friday, the high court hasn’t tipped its hand on whether it will hear one or more cases from five states or just wait. An announcement could come any day – or not at all.

The Supreme Court’s refusal to get involved in October helped bring to 36 the number of states where same-sex couples can marry.

Texas is among the 14 hold out states. Its top appeals court lawyer, state Solicitor General Jonathan Mitchell, said it’s not illegal discrimination to let only opposite-sex couples marry.

“Everyone acknowledges that same sex couples are biologically incapable of producing children,” he said. “This is just a withholding of government recognition and subsidies.”

Appeals court Judge Patrick Higginbotham of Dallas interrupted to question whether ending Texas’ gay marriage ban would affect how many children are born out of wedlock.

Mitchell said he wasn’t suggesting that, only arguing it’s proper for a state to confer such benefits only on heterosexuals. They’re the people most likely to advance the state’s goals of having plenty of children be born, and ensuring they’re well cared for, he said.

Mitchell compared the policy to one where government subsidizes school lunches only for poor children, not those from middle- or upper-income families.

Neel Lane, lawyer for the plaintiffs, blasted that logic. He noted Mitchell didn’t specify a financial benefit Texas grants to heterosexual couples to induce them to have children. Barring same-sex marriages won’t encourage heterosexuals to reproduce, Lane said.

“There’s a disconnect” in the state’s argument, he said. “… They’re really trying to twist themselves into a pretzel to justify why this thing should be upheld.”

Judge Jerry Smith of Houston repeatedly hinted he supports Texas’ argument that its gay marriage prohibition clashes with neither past Supreme Court rulings nor the U.S. Constitution’s language.

“The [Supreme] Court will let us know when it’s changed its mind on this,” he said.

Judge James Graves Jr. of Mississippi, an appointee of President Barack Obama, raised a tantalizing possibility: The panel might force the three states to recognize same-sex marriages performed beyond their borders while continuing to ban them.
“Is it your claim that everybody wins or nobody wins,” he asked Lane.

“Everybody wins, your honor,” replied Lane, a partner with the Akin Gump firm that is representing two men from Plano who want to get married and two women from Austin who were married in Massachussetts.

The three-hour argument retraced familiar ground about social stigma and possible damage to children of same-sex couples, and whether a 1972 Supreme Court decision in a Minnesota case still applies.

Debate grew livelier, though, on the question of judicial activism.

Texas Attorney General Greg Abbott and others have said the issue should be left to state legislatures and citizens to decide – as was the case in a series of moves in Texas in 1973, 2003 and 2005.

But Neel noted an impressive string of court victories by gay and lesbian couples in federal courts over the past year or so.

“You can be blinded by the age that you’re in,” he said, citing state bans on interracial marriage that the Supreme Court struck down less than 50 years ago.

Higginbotham, widely seen as the panel’s swing vote, interrupted a discussion of whether Mississippi’s voters might soon change their minds on gay marriage.

Higginbotham referred to a line of famous 5th Circuit rulings on racial desegregation and civil rights.

“Those words ‘will Mississippi change its mind’ have resonated in these halls before,” he said.