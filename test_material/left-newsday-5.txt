Should Bill and Denise Richard, who lost their 8-year-old son Martin in the Boston Marathon bombing, have a special say in whether Dzhokhar Tsarnaev gets the death penalty?

The question goes beyond the family's tragedy or even the problem of terrorism. It gets to the heart of why we punish criminals -- and why we sometimes execute murderers. The answer we give will tell us something important about how we want the law itself to work.

Begin with a striking fact: In many traditional legal systems, the decision whether to execute a murderer lay exclusively with the victim's family. The idea, which could be found from old England to the Middle East and beyond, was that the harm of the murder was suffered by the victim's family. The family could therefore choose between accepting some form of compensation or demanding the killer's death.

In a legal system operating without a police force or even without a very developed state, the logic of focusing on the family made practical sense. Only family members could be counted on to avenge the death of a relative. If they did not, then they would be opening themselves up to further attacks.

The introduction of an independent adjudicator to decide guilt or innocence was, of course, a vast improvement on simple self-help. A judge not only would help distinguish between intentional and accidental killings, but also could reduce the likelihood of a cycle of revenge killings, by having the authority to say who deserved to die.

The mere presence of the judge didn't eliminate the old idea that family members have a special say in the act of punishment. As the prime sufferers, the family could choose to exercise mercy. In such traditional systems, the notion of "blood money" didn't have the taint that the phrase later acquired in the West. "Wergild," literally "man-money" in Old English, was a Germanic concept that played an important role in establishing social peace. To this day, systems operating under the norms of classical Islamic law include the possibility of "diyya," compensation, in capital cases.

In the contemporary U.S., we still believe in monetary compensation for victims' families. But the compensation side has been separated out of the criminal trial and put into a distinct civil proceeding. As in the case of O.J. Simpson after he'd been acquitted for the murder of Nicole Simpson, the civil trial can still touch directly on the question of guilt or innocence. Yet we believe -- or like to tell ourselves we believe -- that we've bifurcated criminal justice from civil payment.

The theory behind the separation is supposed to be that a criminal act violates not simply the rights of the victim and the victim's family, but the rights of the public at large. At one time, English law charged criminals with violating the king's peace. Today it's the public peace that has been violated -- and so the public has the authority to vindicate the crime.

The transfer of injury corresponds to a shift in decision- making from the family to the public prosecutor. When the U.S. attorney or the attorney general or the president decides to seek the death penalty in a federal case, the decision-maker is acting on behalf of the public. The U.S. attorney may decide to speak to the victims' families. But as U.S. Attorney Carmen Ortiz has made clear in the aftermath of the Richards' public comments against seeking the death penalty for Tsarnaev, the decision is up to her -- and she feels no obligation to listen to the Richards. Tsarnaev's sentencing hearing begins Tuesday.

Given the official theory that the wrong belongs to the American people, not the Richard family, it would seem that the family's opinion shouldn't get special weight. Terrorism aims to affect the whole public. The Boston Marathon bombing achieved that goal, both by attacking a major public event and through the citywide shutdown that followed the search for the bombers.

Yet it's worth noting that the U.S. capital punishment system as it exists today backslides on the theory of public harm through victim impact statements, in which affected family members get to address the jury that's deciding on the capital penalty.

When victim impact statements first became popular, they met opposition not only from death penalty abolitionists who worried that they would make execution more commonplace, but also from some supporters of capital punishment who thought that listening to the victims took us backward into a theory of murder as a private harm.

You could say, of course, that we listen to the victims' families to ascertain the depth of the public harm. But that can't be quite right. The public harm is the murder. What victims' families bring to the penalty phase of a capital trial is precisely their private anguish. In short, we allow victim impact statements because we haven't been successful in separating the idea of private harm from public injury.

The upshot is that we're confused about whether to listen to the Richards because we're confused about whether the harm of crime is public or private. If we want focus on public solutions to public harms, we should let government officials make this decision. If we want to acknowledge that even public tragedy has a private face, we should listen to the Richards' call for mercy, just as we would take seriously another family's desire for the fullest punishment allowed by law.


	        						
	        
						
						
								   Noah Feldman, a Bloomberg View columnist, is a professor of constitutional and international law at Harvard and the author of six books, most recently "Cool War: The Future of Global Competition."