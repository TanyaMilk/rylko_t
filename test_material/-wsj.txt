Baltimore 

The game unlike any other looked, in some respects, like every other.

As the Baltimore Orioles and the Chicago White Sox lined up in front of their dugouts Wednesday afternoon, the Camden Yards public-address announcer asked “ladies and gentlemen” to stand for the national anthem. There was even a seventh-inning stretch, complete with a playing of “Take Me Out to the Ball Game.”

It was all in keeping with baseball tradition, except for one small detail: There was no one to stand, no one to sing and no one to stretch.

Two days after riots shook Baltimore, the Orioles returned to the field in front of more than 45,000 empty green seats. In a game closed to the public for safety reasons, they beat the Chicago White Sox, 8-2. It was the first Major League Baseball game ever played with an attendance of zero.

About two dozen fans peered through a gate beyond left-center field, cheering loudly at times. Others watched from balconies at a hotel overlooking the ballpark. But inside Camden Yards, only music and the sounds of the game pierced an eerie silence.

“It was a weird experience,” Orioles pitcher Zach Britton said.

As hitters stepped to the plate, walk-up songs blared from stadium speakers, just as they normally would. Music also played between innings. But what made the game unique was how audible everything else was in between.

From the sidewalk on West Camden Street, fans could hear the thwack of a bat striking the ball. From the infield, players could hear outfielders making small talk. From the bullpen, relievers could hear home-plate umpire Jerry Layne bellowing, “Stri-eeeke!” And from the mound in the ninth inning, Britton could hear Orioles broadcasters calling the game from the press box.

Sounds normally imperceptible to most people in a stadium were suddenly amplified: the pop of a fastball into a catcher’s mitt, players whistling from the dugout, White Sox first baseman Jose Abreu shouting “I-got-it-I-got-it-I-got-it-I-got-it!” on an infield fly.

“You usually can’t hear the guy sitting next to you, let alone an umpire standing 400 feet away,” Orioles pitcher Tommy Hunter said.

The largest group in attendance was the media. An Orioles spokesman said the game drew nearly 200 reporters, broadcasters, cameramen, photographers and the like, more than the team attracted on opening day. Three scouts sat a few rows back in the stands behind home plate. And a lone stadium employee walked around picking up foul balls, which clanked off empty seats with no fans to give chase.

As a historical oddity, the game drew widespread intrigue. “I didn’t want to miss out on a once-in-a-lifetime experience,” said Ben Bloom, a 24-year-old medical student who was among the fans watching through the gate. But that sentiment was largely dwarfed by the somber reason that the game was closed to the public.

The Orioles’ previous two games had been postponed, following the unrest that stemmed from Monday’s funeral for Freddie Gray, a 25-year-old black man who died in police custody earlier this month.

Before the game, Orioles outfielder Adam Jones said protesters’ frustration was warranted. “This is their cry,” he said. “It’s not a cry that’s acceptable, but this is their cry. They need love. They need hugs. They need support.”

During the game, one of the fans watching from outside the gate held a sign that read “Don’t forget Freddie Gray.” Another fan, 32-year-old Liina Sarapik, said she came because “it’s a beautiful way to show there is still so much good in Baltimore.”

MLB moved the Orioles’ series this weekend against the Tampa Bay Rays from Baltimore to St. Petersburg, Fla. The league will provide at least some financial support to the team to help offset revenues lost this week, according to a person briefed on MLB’s plans. The amount of money remains to be determined.

Also unclear is whether stadium employees such as ushers and concession workers will be paid for Wednesday’s game, for which they weren’t needed. Orioles officials declined to answer that question, though Jones said he was confident team owner Peter Angelos would do the right thing. “I think having the owner that we have, they will be compensated,” Jones said.

The previous record low attendance for a major-league game was 6, for a Sept. 28, 1882 National League game between Troy and Worcester. In the modern era, games occasionally draw crowds that are significantly smaller than the official attendance, which is based on tickets sold. But no other game matched the complete emptiness of Camden Yards.

The downtown ballpark was an oasis of calm. In the hours before the game, the surrounding streets were silent, save for birds chirping and the hum of a few TV satellite trucks. About an hour before the game, five National Guard trucks drove by.

On the other side of Washington Boulevard, two sports bars that typically attract fans had doors and windows boarded up. One of them, Sliders Bar and Grille, had its windows covered in orange boards but its front door open. Lettering on one of the boards near the door read, “We are open.”

Forty-five minutes before the game, there were seven customers inside the bar. Typically, at that time, there would be easily more than 100 people there, one of the bar’s managers said.

“I was thinking of all the businesses that depend on us playing games here,” Orioles manager Buck Showalter said. “There are so many people impacted by it.”

 Write to Brian Costa at brian.costa@wsj.com