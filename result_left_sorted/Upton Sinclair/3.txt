THE MONOPOLISTS: Obsession, Fury, and the Scandal Behind the World’s Favorite Board Game

By Mary Pilon

Bloomsbury. 313 pp. $27

Nations, politicians, athletes, icons — they all need their foundation myths. But George Washington did not chop down the cherry tree. Michael Jordan was not really cut from his high school basketball team.

And Charles Darrow did not invent “Monopoly.”

Yet for decades game maker Parker Brothers peddled the story: how Darrow, an out-of-work salesman, was struggling to get by in Depression-era Philadelphia; how, sitting in his basement, he devised a board game with properties, fake money and cute tokens; how Parker Brothers bought it in 1935; how its astonishing popularity made the salesman wealthy and the company prosperous. “There was only one problem,” writes Mary Pilon, in this legal, corporate and intellectual whodunit. “The story wasn’t exactly true.”

In fact, it was exactly false, but it took decades of court battles, resentments and long memories for the truth to emerge. In “The Monopolists,” Pilon sorts out the innumerable players behind the creation of this beloved pastime. The tale, like the game, becomes a parable for American capitalism, with powerful players stamping out competitors and fortunes being made or destroyed at the roll of the dice.

But the answer to the simple question “Who created Monopoly?” will never be simple. As Pilon shows, creativity seldom appears in a light-blub flash. It can happen slowly, from the incremental efforts of many. With Monopoly, we can only conclude that one person started it, a few people made money off it, and a whole lot more people made it the game we’ve come to know.

The prime mover was Lizzie Magie, who in the early 1900s survived in Washington as a stenographer by day and a poet, actor and inventor by night. Magie was devoted to the theories of Henry George, a 19th-century economist who believed that only land should be taxed — because it belonged to all — while all other goods should remain untaxed. In 1902, Magie sketched out the Landlord’s Game, mainly as a way to disseminate George’s theories. The game included play money and properties. It had nine rectangular spaces, including a railroad stop, along each side of the board. It also had a Go to Jail square and a space that yielded $100 whenever someone crossed. And the person with the most wealth won. “It might well have been called the ‘Game of Life,’ as it contains all the elements of success and failure in the real world,” Magie wrote in 1902. She obtained a patent for the game at a time, Pilon emphasizes, when less than 1 percent of patents awarded in the United States went to women.

“Lizzie created two sets of rules,” Pilon explains: “an anti-monopolist set in which all were rewarded when wealth was created, and a monopolist set in which the goal was to create monopolies and crush opponents.” Though Magie’s sympathies were with the former rules, the latter set became popular, whether in the single-tax community of Arden, Del. — where Upton Sinclair was an early aficionado — or in elite Northeastern universities, where it became known as “the monopoly game.”

So how did a version of this game fall into Darrow’s hands three decades later? Pilon, a New York Times reporter, traces the connections. Deep breath. . . . Daniel Layman, who played the game at Williams College and whose friends added the miniature houses, later sold a version of it dubbed “Finance” and taught it to some Quaker friends in Indianapolis. Reaching the Quaker community in Atlantic City, the game morphed further. “The streets on the board mirrored the Quaker social network,” Pilon explains. The families lived on roads such as Pennsylvania and Ventnor avenues, as well as Park Place, while servants lived on Baltic Avenue, in a lower-income neighborhood. “The placement of these poorer properties on the monopoly board reflected the harsh reality of the city,” the author explains. The Quakers also added hotels, and Jesse Raiford, a real estate agent, assigned each trio of streets a color.

Raiford introduced the game to his brother and sister-in-law, visiting from Philadelphia. Once back home, they in turn shared it with their friends Charles and Olive Todd. In 1932, Charles ran into a childhood friend, Esther Jones, who was married to one Mr. Charles Darrow. “The Darrows were so taken with the game that Charles Todd made them a monopoly set of their own,” Pilon writes, “and began teaching them some of the more advanced rules.”

Let’s just say Darrow learned to pass “Go” and collect his $200.

Struggling financially — that part is true — and needing cash for medical treatment for a disabled child, Darrow got an artist friend to illustrate a new board and sought to copyright it before pitching the game to Parker Brothers. (The original documents relating to his claim disappeared from the U.S. Copyright Office, enhancing the story’s conspiratorial vibe.)

Even after purchasing the game from Darrow for $7,000 plus residuals, Parker Brothers executives didn’t entirely trust him, asking Darrow to detail its origins so they could have the information for “publicity purposes” and in case “any patent questions do come up.” In an astonishing letter, Darrow explained how “my brain child was born,” referred to “my original drawings,” and said he wanted to feel “some degree of security from theft of my idea.”

To make such claims when the game “had essentially existed in the public domain for thirty years was somewhere between a stretch of the facts and a lie,” Pilon writes, in a rare accusatory passage. No matter. Parker Brothers methodically bought off any potential challengers, including Lizzie Magie herself.

Founder George Parker visited Magie in November 1935 and bought the original patent to the Landlord’s Game for $500. Magie was thrilled, thinking her game would finally become a success. So she was devastated when she saw a different version, with a different name, lionizing a different creator and celebrating American-style capitalism. Magie died in 1948, “neither her obituary nor her headstone making any mention of her game invention.”

Little of this would be known but for Ralph Anspach, a Bay Area economics professor who in the 1970s developed an “Anti-Monopoly” game only to feel the wrath of Parker Brothers’ lawyers. Pilon devotes much of the book — too much, really — to Anspach’s nearly decade-long fight, all the way to the Supreme Court, and to his excavation of the facts; Anspach and his lawyers dug up the game’s history and tracked down some still-indignant Quakers.

Perhaps Pilon feels indebted: “Had Ralph never pursued his Anti-Monopoly case until the end or taken a settlement of some kind,” she acknowledges, “it’s extremely likely that Monopoly’s true history would never have been unearthed.” Yet once again Magie is sidelined. In 1989, when Anspach arranged for a plaque “honoring the truth about Monopoly” at the former site of the Atlantic City Friends School, Magie went unmentioned. “An incomplete story,” Pilon writes, with considerable understatement.

But, of course, the game is not entirely Magie’s, either. “Does Monopoly belong to Lizzie?” Pilon asks. “The Quakers? The Darrows? Parker Brothers? Or all of the above and anyone and everyone who plays it?”  She hopes the book will be “a jumping-off point . . . in the conversation about the ownership of ideas [and] the evolution of innovation.” Then Pilon concludes, a bit perplexingly: “But, after all, it is just a game.”

That is precisely the appeal. Even in a “Clash of Clans” world, anyone who grew up playing Monopoly will have a hard time resisting “The Monopolists.” Reading it, I desperately hoped for vindication for Magie and a Go to Jail card for some Parker Brothers execs. But I knew how the story would end: that is, a lot like the game Magie developed more than a century ago. The player with the most wealth wins.

Read more from Book Party:

Most writers love words. This one loves letters.

David Carr, book critic

How Stuart Scott came up with ‘boo-yah!’