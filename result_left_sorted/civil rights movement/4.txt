As our van in the presidential motorcade reached the crest of the Edmund Pettus Bridge in Selma, Ala., and began the descent toward the thousands of waiting faces and waving arms of those who had come to commemorate the 50th anniversary of “Bloody Sunday,” the gravity of that place seized me, pushing out the breath and rousing the wonder.

The mind imagines the horror of that distant day: the scrum of bodies and the cloud of gas, the coughing and trampling, the screaming and wailing, the batons colliding with bones, the opening of flesh, the running down of blood.

In that moment I understood what was necessary in President Obama’s address: to balance celebration and solemnity, to honor the heroes of the past but also to motivate the activists of the moment, to acknowledge how much work had been done but to remind the nation that that work was not complete.

(I, along with a small group of other journalists, had been invited by the White House to accompany the president to Selma and have a discussion with him during the flight there.)

About an hour north of where the president spoke was Shelby County, whose suit against the Department of Justice the Supreme Court had used to gut the same Voting Rights Act that Bloody Sunday helped to pass.

His speech also came after several shootings of unarmed black men, whose deaths caused national protests and racial soul-searching.

It came on the heels of the Justice Department’s report on Ferguson, Mo., which found pervasive racial bias and an oppressive use of fines primarily against African-Americans.

It came as a CNN/ORC poll found that four out of 10 Americans thought race relations during the Obama presidency had gotten worse, while only 15 percent thought they had gotten better.

The president had to bend the past around so it pointed toward the future. To a large degree, he accomplished that goal. The speech was emotional and evocative. People cheered. Some cried.

And yet there seemed to me something else in the air: a lingering — or gathering — sense of sadness, a frustration born out of perpetual incompletion, an anger engendered by the threat of regression, a pessimism about a present and future riven by worsening racial understanding and interplay.

To truly understand the Bloody Sunday inflection point — and the civil rights movement as a whole — one must appreciate the preceding century.

After the Civil War, blacks were incredibly populous in Southern states. They were close to, or exceeded, half the population in Alabama, Florida, Georgia, Virginia, Mississippi, Louisiana and South Carolina.

During Reconstruction, the 13th, 14th and 15th Amendments were ratified, abolishing slavery, granting citizenship and equal protection to former slaves and extending the vote to black men. As a result, “some 2,000 African-Americans held public office, from the local level all the way up to the U.S. Senate,” according to the television channel History.

This was an assault on the traditional holders of power in the South, who responded aggressively. The structure of Jim Crow began to form. The Ku Klux Klan was born, whose tactics would put the current Islamic State to shame.

Then in the early 20th century came the first wave of the Great Migration, in which millions of Southern blacks would decamp for the North, East and West.

This left a smaller black population in Southern states that had developed and perfected a system to keep those who remained suppressed and separate.

Here, the civil rights movement and Bloody Sunday played out.

The movement was about justice and equality, but in a way it was also about power — the renewed fear of diminished power, the threat of expanded power, the longing for denied power.

Now, we must look at the hundred years following the movement to understand that another inflection point is coming, one that again threatens traditional power: the browning of America.

According to the Census Bureau, “The U.S. is projected to become a majority-minority nation for the first time in 2043,” with minorities projected to be 57 percent of the population in 2060.

In response, fear and restrictive laws are creeping back into our culture and our politics — not always explicitly or violently, but in ways whose effects are similarly racially arrayed. Structural inequities — economic, educational — are becoming more rigid, and systemic biases harder to eradicate. But this time the threat isn’t regional and racially binary but national and multifaceted.

So, we must fight our fights anew.

As the president told a crowd in South Carolina on Friday, “Selma is not just about commemorating the past.” He continued, “Selma is now.”