Maria Praeli, from New Milford, is one of several United We Dream leaders who have been invited to the White House Wednesday to meet President Obama and discuss immigrant youth, their families, and the ongoing political debate about immigrants in Washington. 

Praeli, a Quinnipiac student, was born in Ica, Peru and came to the United States when she was five. 

According to United We Dream and Quinnipiac officials, she grew up in New Milford and graduated from New Milford High School in 2012. She then graduated in 2014 from Gateway Community College in New Haven with an Associate’s Degree.

At Gateway, she was the school’s first undocumented Student Body President.

Praeli is now a junior political science major working towards her bachelor's degree at Quinnipiac University.

"Maria is an exceptional leader both in the state of Connecticut and at the national level with United We Dream, where she has worked tirelessly to win relief for the undocumented community," the prepared release from United We Dream and Quinnipaic said. United We Dream is an immigrant youth activist group. 

A White House spokesman said President Obama is meeting with Praeli and other young immigrants Wednesday because they have benefited from Deferred Action for Childhood Arrivals (DACA). The context for the meeting is an effort by Congressional Republicans to block funding for programs including the Department of Homeland Security as part of their efforts to reverse President Obama's executive actions relating to immigration, he said. 

According to the White House spokesman, the other immigrants Obama is scheduled to meet with today include: 

Steven Arteaga - Houston, TX

"Steven was born in Mexico City and he is a DACA recipient. After learning about the Civil Rights Movement in high school and reading about the marches, protests, and rallies people organized in hopes of advancing justice and dignity for all, a desire to do something similar for the immigrant community sparked in him. After obtaining DACA status in 2013, he began working at Mi Familia Vota (MFV), which has allowed him to empower and engage members of his community to continue to bring about positive social change. On January 17th, 2014, Steven was honored for as a Champion of Change for his commitment and dedication to public service as an immigrant activist."

Jean Yannick Diouf - Rockville, MD

"Jean was 8 years old when his father, a diplomat, brought his family over from Senegal to live in the U.S. He and his family lost their diplomatic status once his father no longer lived with them. Now at 22, Jean lives with his mother, his older sister, and two younger U.S. citizen siblings. After becoming a DACA recipient in December of 2012, Jean is now able to provide for himself and help support his family. He graduated from Montgomery Community College with his Associate’s degree, and is now a junior at the University of Maryland-College Park where he is pursuing a degree in Business Management. Jean has been an active member of the Maryland Dream Youth Committee, where he supports fellow undocumented students."

Blanca Gamez  - Las Vegas, NV

"Blanca arrived to the United States when she was seven months old. For her, home means Nevada because it is the only place she has lived for the past 25 years. She is a proud graduate of the University of Nevada at Las Vegas with degrees in Political Science and English Literature. She is currently applying for law school and hopes to attend next fall."

Rishi Singh - South Ozone Park, NY

"Rishi is Trinidadian immigrant and graduate from Hunter College of the City University of New York, where he obtained a degree in Psychology & Accounting.  Rishi has been organizing for immigrant rights since 2003 when he joined DRUM- South Asian Organizing Center. In DRUM, Rishi has served as a Youth Organizer from 2004-2007 and Steering Committee Member from 2009-2012 and is currently an Educational Justice Organizer. In 2009, Rishi was honored with the SAALT (South Asian Americans Leading Together) National Leadership Award. He was the NYC Peoples Global Action on Migration Development & Human Rights Coordinator in 2013 and is a recent DACA recipient. Rishi is now the lead organizer for DRUM to the United We Dream network."

Bati-amgalan Tsoftsaikhan - Arlington, VA

"Bati moved to the United States with his parents from Mongolia at the age of ten. He graduated from Yorktown High School in 2011 and received his associate’s degree in business administration from Northern Virginia Community College in 2013. Bati is a DACA recipient who is interning at NAKASEC focusing on in-state tuition in Virginia and assistance with DACA renewals.  Currently, he is pursuing a Bachelor of Science degree in Finance at George Mason University."