EVERYONE in my family has an Auntie Jean story.

There was the time my big brother and cousins thought they were calling her bluff about making them walk home from the rec center because they wouldn’t stop clowning in the back seat. “Keep talkin’,” she said, as she casually pulled into a gas station, reached back and opened the door, then flashed the peace sign as we sped off without them. I could see them in the rearview mirror, jaws almost touching the concrete.

Another cousin, Alaric, becomes animated when he recalls announcing his plans to have a girl over for the evening while his mom was out of town. “No you ain’t!” Auntie Jean said, dashing his teenage dreams. “Not until you clean this house!” She stood over him for the next hour, puffing on a Newport and watching closely as he scrubbed each tile, fixture and faucet until the girl would be able to check her lipstick in the doorknob.

She is our fierce Auntie Jean who doesn’t care what others think: the one who will send her bacon back three times if it isn’t crispy enough, the one who once hopped out of the car at a red light to dance in the street to Maze’s “Happy Feelings.”

But the most famous story about her isn’t cute or funny at all. One day, sometime in the 1960s, she, my mom, their siblings and cousins and a bunch of other kids went down to Airport Road in their hometown, LaGrange, Ga., as part of an organized effort to integrate the local roller rink. They were teenagers and preteens, led by my grandfather, Frank Cox, the Rev. Elijah Jackson and others — and they refused to move when the white owner demanded that they leave.

The man seemed so big to them, and he grew angry at their show of defiance. Some say he loomed over them like something that emerged from the backwoods of the county. Others say he was foaming at the mouth with rage.

Auntie Jean stood right in his face, staring him down, transfixed by the level of hatred she had seen in his eyes. Her knees were buckling and her hands were shaking, yet she stood and stared. As the story goes, he reached into a drawer and pulled out a revolver, letting off a series of shots into the ceiling above his head, sending everyone running for safety.

It took a long time for me to keep a dry eye whenever I heard that story. The civil rights movement wasn’t just some historic event or fodder for my middle-school field trip. It was real and part of our immediate past, something that could have killed my mom, my auntie and their siblings. Something as benign and innocent as roller-skating had to be fought for.

I traveled home to Houston a couple of months ago when my relatives were gathered there for a holiday. While lounging around my mom’s bedroom, I happened upon a scrapbook she made during her senior year of high school. She swore I’d seen it before, but I hadn’t. I had never seen pictures of Willie, her high school boyfriend, who looked like one of the Jackson 5. There were also pictures of her with a homecoming sash across her chest, an obituary for the “Ain’t No Mountain High Enough” singer Tammi Terrell and a five-year plan written in pink ink.

And there was a newspaper clipping with the headline, “Skating Rink Closed; Negroes Turned Away.”

My heart stopped.

I had definitely never seen this before. A piece of my family history was right there for me to touch. That story, one that I’d heard countless times, had helped shape me: I was the daughter and niece of civil-rights heroes who fought for something as seemingly mundane as the right to roller-skate. Now it was canonized in some obscure newspaper article that my mother had thought to save all those years ago at the age of 18.

Excited for a further glimpse into my history, I began to read.

“The skating rink on the Airport Road was closed Tuesday night after the owner refused to allow a group of Negroes to skate,” it began. “Deputy Mac Smith said between 50 and 60 young Negroes were at the rink when he arrived to answer a call from the owner, O. L. (Tot) Underwood.”

In bland newspaperese, the article explained that the officer gave Mr. Underwood two options — allow them to skate, or close down for the day. The owner, the article stated, chose the latter: “They were denied admittance and left.”

It said there was no further disturbance and described everyone as orderly. And that was it. No mention of gunshots. No mention of terrified teenagers. No mention of Auntie Jean in a standoff with the roller rink owner.

I was shocked.

“Y’all?” I shouted. “This ain’t right!”

I read it out loud to my mom, my Auntie Caroline, Great-Aunt Betty Ruth and Auntie Jean, who were all sitting around my mother’s room with me.

“That’s a lie,” Auntie Caroline said, barely roused. “They sanitized it.”

Auntie Jean was quiet. She just kept shaking her head.

I was shattered.

I wanted to believe the article. I write articles just like that for a living. Journalists deal in truth, right? Had my entire family conjured a tall tale and kept it alive my whole life? Was this moment of bravery a gross exaggeration? Somebody had lied.

Then I noticed, in that same pink ink my mom had used to write down her teenage thoughts and aspirations, the words “He shot at us!” above the headline.

Of course, I thought. History is written by the victors.

We don’t just share memories because they’re funny and make us feel nostalgic. We tell those Auntie Jean stories and Uncle Bus stories and Granddaddy Frank stories because if we don’t, someone else surely will. By filling my childhood with those anecdotes, my mom and her siblings had entrusted me with our family record.

And that record is foundational, letting me know that I came from people who owned businesses and organized protests, folks who were passionate about their beliefs and courageous enough to not back down even in the face of life-threatening adversity. That family history — told through sometimes hilarious anecdotes, sometimes muddled memories — gave me a sense of pride. If they could accomplish such things, then I could, too.

I sat on the bed, staring at the scrapbook in my lap as my mom and aunts went downstairs for poundcake and coffee, talking about something else entirely. They were far more nonchalant about what I had just read than I was. I guess they were used to this kind of thing.

Not me. I stayed behind and took a picture of the article, with my mom’s notation written above, and uploaded it to Instagram.

The story would live on. And it would be the Auntie Jean version.