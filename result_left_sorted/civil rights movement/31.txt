America's racial history "still casts its long shadow upon us," President Barack Obama said Saturday as he stood in solidarity and remembrance with civil rights activists whose beatings by police a half-century ago galvanized much of the nation against racial oppression and hastened passage of historic voting rights for minorities. Tens of thousands of people joined to commemorate the "Bloody Sunday" march of 1965 and take stock of the struggle for equality.

Under a bright sun, the first black U.S. president praised the figures of a civil rights era that he was too young to know but that helped him break the ultimate racial barrier in political history with his ascension to the highest office. He called them "warriors of justice" who pushed America closer to a more perfect union.

"So much of our turbulent history — the stain of slavery and anguish of civil war, the yoke of segregation and tyranny of Jim Crow, the death of four little girls in Birmingham, and the dream of a Baptist preacher — met on this bridge," Obama told the crowd before taking a symbolic walk across part of the Edmund Pettus Bridge, where the 1965 march erupted into police violence.

"It was not a clash of armies, but a clash of wills, a contest to determine the meaning of America," Obama said. He was 3 years old at the time of the march.

A veteran of that clash, Rep. John Lewis, who was brought down by police truncheons that day in 1965 and suffered a skull fracture, exhorted the crowd to press on with the work of racial justice.

"Get out there and push and pull until we redeem the soul of America," Lewis said. He was the youngest and is the last survivor of the Big Six civil rights activists, a group led by the Rev. Martin Luther King Jr. that had the greatest impact on the movement.

In the crowd stood Madeline McCloud of Gainesville, Florida, who traveled overnight with a group of NAACP members from central Florida and marched in Georgia for civil rights back in the day. "For me this could be the end of the journey since I'm 72," she said. "I'm stepping back into the history we made." Also in attendance was Peggy Wallace Kennedy, a daughter of the late George Wallace, the Alabama governor who once vowed "segregation forever."

Selma's fire department estimated the crowd reached 40,000. Former President George W. Bush shared the platform. Republican congressional leaders were mostly absent but one, House Majority Leader Kevin McCarthy, joined the walk.

The walk progressed under the bold letters on an arch, identifying the bridge named after Pettus, a Confederate general, senator and reputed Ku Klux Klan leader.

Obama, his wife, Michelle, and their two daughters walked about a third of the way across, accompanied by Lewis, who has given fellow lawmakers countless tours of this scene. Bush, his wife, Laura, and scores of others came with them before a larger crowd followed.

Two years after King's historic "I have a dream" speech in Washington, the Bloody Sunday march became the first of three aiming to reach Montgomery, Alabama, to demand an end to discrimination against black voters and all such victims of segregation. Scenes of troopers beating marchers on the bridge shocked the nation, emboldening leaders in Washington to pass the Voting Rights Act five months later.

On his way to Selma, Obama signed a law awarding the Congressional Gold Medal to participants of the trio of marches, the last of which brought protesters all the way to Montgomery.

The shadow of enduring discrimination touched the event as Obama addressed his government's investigation of the Ferguson, Missouri, police department. The investigation, he said, "evoked the kind of abuse and disregard for citizens that spawned the civil rights movement."

"What happened in Ferguson may not be unique, " he said, "but it's no longer endemic, or sanctioned by law and custom. And before the civil rights movement, it most surely was."

The Justice Department concluded this past week that Ferguson had engaged in practices that discriminated against the city's largely black population. The department also declined to prosecute the white police officer who shot and killed an unarmed black 18-year-old last year, sparking days of violent protests and marches.

"We just need to open our eyes, and ears, and hearts, to know that this nation's racial history still casts its long shadow upon us," Obama said.

Yet, he said, "if you think nothing's changed in the past 50 years, ask somebody who lived through the Selma or Chicago or L.A. of the '50s. Ask the female CEO who once might have been assigned to the secretarial pool if nothing's changed. Ask your gay friend if it's easier to be out and proud in America now than it was 30 years ago. To deny this progress - our progress - would be to rob us of our own agency, our responsibility to do what we can to make America better."

In New York, a multigenerational and racially mixed crowd of about 250 people crossed the Brooklyn Bridge in a "Selma is Everywhere" march.

"I'm not sure how many of us would have been willing to walk across that bridge in Selma, getting beat on every step of the way," said David Dinkins, 87, who in the early 1990s was New York's first black mayor. "We think it's important that people not forget Bloody Sunday," he said. "You'd be surprised how many young people don't know."

Obama says Selma a living history lesson for his daughters 

In four minutes, President Barack Obama gave his daughters a living history lesson on the civil rights movement.

"I want to say what an extraordinary honor this has been, especially to have Sasha and Malia," Obama said of his 13-year-old and 16-year-old.

Obama had said he was taking his daughters to Selma to "remind them of their own obligations."

"There are going to be marches for them to march, and struggles for them to fight. And if we've done our job, then that next generation is going to be picking up the torch, as well," Obama said at a Black History Month observance at the White House last month.

Earlier this week, in a radio interview with host Tom Joyner, Obama said he thinks his daughters appreciate that people made sacrifices so that life would be easier for them. He noted that they live in the White House with their grandmother, who he said remembers what it was like living in a segregated setting on the South Side of Chicago.

"Part of what I want Malia and Sasha to understand is that this is an unfinished project," he said, referring to simmering racial tensions that flared up following the police-involved killings last year of black men in Ferguson, Missouri, and Staten Island, New York. Neither of the officers involved was charged with committing any crimes.