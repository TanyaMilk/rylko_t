1. It’s a night of Washington elegance at its finest.

You’ve seen the photos: the tuxedoed commander in chief sharing a laugh with a late-night comedy superstar; the media mogul greeting a glittering starlet. But pull back the camera, and the glamour quotient drops sharply — they’re in a hotel ballroom the size of an airplane hangar, and 2,600 other people are there, too. Some Hollywood guests have been surprised to learn that their invitations do not actually entail dinner at the White House.

A few unaffiliated parties during the weekend boast an A-list scene, such as the after-party co-hosted by Vanity Fair and Bloomberg. But the sheer number of people at the correspondents’ dinner itself can feel like the L’Enfant Plaza Metro station at rush hour — even before you factor in their behavior. The chattering classes, it turns out, can be really loud: The dinner is notorious for the constant oblivious babble that drowns out the speeches and awards presentations (before the president and the paid entertainer take the podium, anyway). 

And the room’s mix of headline-seizing celebrities and headline-chasing reporters can be volatile. A mob swarmed George Clooney as he inched through the Washington Hilton’s corridors in 2006, no one daring to give him breathing room for fear they might miss — I don’t know, what did they think he was going to do? Far uglier was the crowd that pressed around Lindsay Lohan’s table in 2012, pointing and gawking and laughing from just feet away.

On the flip side, you can’t accuse this crowd of being uptight: Some guests get too drunk — just like at a regular party. In the later hours, there is more PDA than you’d ever see at a state dinner. 

2. It’s the “Nerd Prom.”

The writer Ana Marie Cox first applied this warmly mocking phrase to the WHCA dinner several years ago, and it quickly caught on. It has never been perfectly apt — the party has always drawn the glossiest broadcast news stars — but it does capture a certain dynamic: a Beltway clan of ink-stained wretches and their counterparts from government and politics, just a little too excited about a rare night of dress-up and drink-up.

But you know what happens when the nerds start to have too much fun: The cool kids muscle in on the action. The presidential comedy routines started getting buzz, the trickle of novelty celebrity guests became a steady stream, and suddenly the dinner turned into a bona fide happening — a coveted invitation for out-of-towners who have little or nothing to do with journalism. Many of the media organizations that buy tables are reserving more of their seats for their corporate bosses or crucial advertisers — which means fewer seats for authentic nerds. At the pre-parties that now extend two or three days before the main event, you’ll run into plenty of Washingtonians muttering into their drinks about how they’re “not doing the dinner this year.” Translation: They’re not invited anymore.

3. It’s a great place to meet celebrities.

Dinner old-timers are dying to tell you about the time they chatted up Barbra Streisand in the ladies’ room or gave “West Wing” script notes to Rob Lowe at the bar. Back in the day, it was much easier to strike up a conversation with visiting stars, almost as a favor to those who seemed clearly out of their element. But now there are so many celebrities that most are quite at ease. With all their old pals from the festival circuit or the awards galas to greet, they can’t be bothered with meeting Washington strangers.

Media organizations have started inviting the entire casts of some shows popular among Washingtonians — “House of Cards,” “Scandal,” “Modern Family” — who then return the following year as guests of some other covetous news outlet. Go ahead, try to talk to them; you’ll just feel like you’re crashing a family reunion. In 2011, I watched as some of our era’s top comedy stars mingled at a swanky after-party — with one another. At one end of the terrace, Paul Rudd and Amy Poehler formed a jolly huddle with Andy Samberg and Rashida Jones. At the other end, Bradley Cooper and his “Hangover” co-stars pulled their chaises longues into a circle with Seth Meyers and his “Saturday Night Live” crew. They had formed a VIP room with their own bodies.

4. It’s all for a good cause.

To the critics who grouse that the dinner has become too bloated, celebrity-obsessed and overhyped, the correspondents’ association has had a standard response: Hey, all that hype is selling tickets, and all those tickets are funding scholarships!

But charity galas are rarely the most efficient form of fundraising, and this one is no exception. The dinner was founded as a strictly social affair; only since 1991 have proceeds underwritten scholarships for journalism students (a total of $86,550 last year). A Washington Post review of tax records shows that while the dinner reliably grosses more than $700,000 in revenue these days and always covers its expenses, operating costs in the past few years have taken a bite out of what’s supposed to be the purely charitable portion of ticket sales, presumably leaving less for scholarships. 

WHCA President Christi Parsons acknowledges that the $142,000 salary for the group’s executive director has exceeded the scholarship amount in a couple of recent years, but she says that’s because the scholarship program is just a sideline to the organization’s larger mission of fighting for “openness and transparency at the White House,” as well as coordinating the press pool arrangements. And while most galas find big-name sponsors, Parsons points out that “our members would [not] be comfortable with the idea of fellow journalists going hat-in-hand to corporations, trying to raise money.”

5. It’s a beloved Beltway tradition.

It’s old, yes, but Washington journalists have a deeply conflicted relationship with this event. Sure, it’s a kick to get a selfie with a Disney Channel star for your kid — but yuck, all the fawning over celebrities! Wow, the president’s speech was hilarious — but should we really be laughing and applauding him? It’s so useful to get face time with valuable political sources — but ugh, now there’s a photo on Twitter of me yukking it up with the Cabinet secretary I cover! Never mind that reporters routinely treat government officials to lunch or drinks in hopes of turning them into sources; the perception that it’s a cozy relationship, true or not, can be embarrassing.

That’s just one of the many optical perils of the dinner. In 2006, Stephen Colbert delivered an after-dinner routine that skewered the Bush administration but drew a tepid response from the distracted room, suggesting to many TV viewers that the journalists were too deferential to the president. The next year, a VIP scuffle broke out when Sheryl Crow tried to buttonhole an unwilling Karl Rove for a chat about global warming; the New York Times, which had hosted Rove, announced that it would skip the dinner in the future. “It sends the wrong signal to our readers and viewers, like we are all in it together and it is all a game,” Dean Baquet, now the paper’s executive editor, told the New York Observer in 2011. (The Post still attends.) 

Still, his words suggested that the problem wasn’t the dinner so much as how it looked to outsiders. Probably the only way Washington could relax and enjoy the event is if no one wrote about it. But then it wouldn’t be the White House correspondents’ dinner, would it?

 

Five myths is a weekly feature challenging everything you think you know. You can check out previous myths, read more from Outlook or follow our updates on Facebook and Twitter.