It was near Green Bank, W.Va., in 1960 that a young radio astronomer named Frank Drake conducted the first extensive search for alien civilizations in deep space. He aimed the 85-foot dish of a radio telescope at two nearby, sun-like stars, tuning to a frequency he thought an alien civilization might use for interstellar communication.

But the stars had nothing to say.

So began SETI, the Search for Extraterrestrial Intelligence, a form of astronomical inquiry that has captured the imaginations of people around the planet but has so far failed to detect a single “hello.” Pick your explanation: They’re not there; they’re too far away; they’re insular and aloof; they’re zoned out on computer games; they’re watching us in mild bemusement and wondering when we’ll grow up.

Now some SETI researchers are pushing a more aggressive agenda: Instead of just listening, we would transmit messages, targeting newly discovered planets orbiting distant stars. Through “active SETI,” we’d boldly announce our presence and try to get the conversation started.

Naturally, this is controversial, because of . . . well, the Klingons. The bad aliens.

“ETI’s reaction to a message from Earth cannot presently be known,” states a petition signed by 28 scientists, researchers and thought leaders, among them SpaceX founder Elon Musk. “We know nothing of ETI’s intentions and capabilities, and it is impossible to predict whether ETI will be benign or hostile.”

This objection is moot, however, according to the proponents of active SETI. They argue that even if there are unfriendlies out there, they already know about us. That’s because “I Love Lucy” and other TV and radio broadcasts are radiating from Earth at the speed of light. Aliens with advanced instruments could also detect our navigational radar beacons and would see that we’ve illuminated our cities.

“We have already sent signals into space that will alert the aliens to our presence with the transmissions and street lighting of the last 70 years,” Seth Shostak, an astronomer at the SETI Institute in California and a supporter of the more aggressive approach, has written. “These emissions cannot be recalled.”

That’s true only to a point, say the critics of active SETI. They argue that unintentional planetary leakage, such as “I Love Lucy,” is omnidirectional and faint, and much harder to detect than an intentional, narrowly focused signal transmitted at a known planet.

These critics add that it’s bad form for scientists to attempt such interstellar communication without getting permission from the rest of humanity. Plus there’s the question of what, exactly, a message to the stars ought to say.

Thus one of the greatest scientific mysteries — Are we alone in the universe? — leads to a thorny political and cultural question: Who speaks for Earth?

This discussion about the proper protocols of communicating with aliens is not the most mainstream scientific debate ever concocted. But it got a lot of attention here in San Jose at the annual meeting of the ultra-mainstream American Association for the Advancement of Science.

Astronomer Jill Tarter, a pioneer of SETI who is neutral about the more active approach, organized a symposium on the topic. Before the symposium, two advocates of the idea, Shostak and Douglas Vakoch, appeared at a press briefing alongside science-fiction writer David Brin and planetary scientist David Grinspoon.

“Active SETI is a reflection of SETI growing up as a discipline,” said Vakoch, a clinical psychologist who is the SETI Institute’s director of Interstellar Message Composition. “It may just be the approach that lets us make contact with life beyond Earth.”

But Brin, a signer of the petition protesting the campaign for active SETI, said we don’t know what’s out there and shouldn’t presume that aliens are benign. He said there are roughly 100 scenarios to explain why we haven’t heard from the aliens so far. About a dozen of those scenarios are unpleasant, he said.

Vakoch countered that Brin was being inconsistent, because he collaborated on a message that will be carried into space by NASA’s New Horizons spacecraft after its fly-by of Pluto later this year.

“No one is going to get it!” Brin interjected. (The spacecraft is very slow in the galactic scheme of things and will journey for eons into the void of interstellar space.)

As the scientists debated one another, a white-haired, bespectacled man in the back of the room listened quietly: Frank Drake.

He is 84 years old, the beloved dean of the SETI field. He is the Drake of the famous Drake Equation, the formula he scribbled down in 1961 in advance of a meeting in Green Bank. His equation offers a technique for estimating the abundance of communicative civilizations.

He parked himself on a bench in a corridor and, bracketed by a clutch of reporters, held forth for 30 minutes. He said he thinks it’s too soon to engage in active SETI. We don’t know enough.

“I think it’s a waste of time at the present. It’s like somebody trying to send an e-mail to somebody whose e-mail address they don’t know, and whose name they don’t know.”

When Drake plugs his estimates into the Drake Equation (and who is more entitled to do so?), he comes up with 10,000 alien civilizations that we could detect if we looked in the right places with the right techniques.

“It’s 10,000 that we can detect. There are a lot more,” Drake clarifies. “A lot more young ones that can’t be detected because they don’t have the technology, and there are older ones that have technology that is so good that they don’t waste any energy.”

The Drake Equation has endured despite being rather ungainly at first glance:

N=R*· f p  · n e  · f l  · f i  · f c  · L

It’s not as complicated as it looks. The number (N) of detectable civilizations is the product of seven factors: the rate of star formation (R*), the fraction of stars with planetary systems (f p ), the average number of habitable planets per planetary system (n e ), the fraction that actually have life (f l ), the fraction that have intelligent life (f i ), the fraction with communicative civilizations (f c ) and the average longevity of the communicating phase of such civilizations (L).

Exoplanets — outside our solar system — were first discovered in 1995. NASA’s Kepler Space Telescope and other observatories in space and on the ground have found more than 1,000 planets in the years since. Astronomers say it’s likely that our galaxy has tens of billions of “habitable zone” planets. And of course (channeling Carl Sagan) our galaxy is just one of billions and billions of galaxies.

But after the first three factors in the Drake Equation, we enter the murk. How many of those potentially habitable planets out there actually have life? No one knows, because we don’t yet know how life began on Earth. How likely is it that simple, microbial life will evolve into complex, multicellular organisms and eventually into creatures with large brains? We don’t know, because we have only the one data point of life on Earth.

Do intelligent creatures tend to be communicative and potentially detectable? No idea. And finally, there’s that ominous “L” at the end of the equation: Do technological civilizations tend to survive a long time?

“Those factors are just completely unknown. It’s a great way to organize our ignorance,” Tarter says.

Why, a reporter asked Tarter, should we try to pick up signals from an alien civilization?

“We’re curious how many different ways there are to do this thing called life,” she said. “And we’re curious if it’s possible for us to have a long future.”

That’s because we’d most likely find a very old civilization, not a young one. It’s a matter of statistical probabilities. The universe is 13.8 billion years old. If we pick up a signal, it is unlikely to be from a civilization that has only recently become communicative.

Tarter isn’t discouraged by SETI’s null result to date. She says our ability to detect signals, though much improved since 1960, remains limited.

“We’ve explored one eight-ounce glass of water out of the ocean,” she says.

But you hear something different from Geoff Marcy, an astronomer who has found many of those exoplanets, and who also came to San Jose to discuss results from the Kepler mission. Marcy — who, like David Brin and Elon Musk, signed the petition to protest efforts in active SETI — said it is striking that we have found all these distant planets but no evidence at all of intelligent civilizations.

“The absence of strong radio beacons, television broadcasts, robotic spacecraft, obelisks on the moon — all of those absences add up to give us the suggestion that our galaxy is not teeming with technological life,” Marcy said.

After the active SETI symposium at the AAAS convention in San Jose, the interested parties reconvened for a Valentine’s Day workshop at the SETI Institute up the road in Mountain View. Bottom line: No one’s going to be beaming signals to the aliens anytime soon.

“We need tools to enable true global deliberation and then action,” Tarter said in an e-mail summarizing the workshop. She pointed out that this active SETI issue echoes another debate that got a lot of attention at the AAAS meeting: whether to inject aerosols into the upper atmosphere to reflect sunlight and combat global warming. No one’s going to do that, either, in the near future, but suddenly people are discussing these basic issues of planetary management and global decision-making.

Rogue alien-hunters can always go it alone, of course — and they have. For example, a Russian astronomer, Alexander Zaitsev, has repeatedly beamed messages to nearby stars. Even NASA has gotten into the act, beaming the Beatles song “Across the Universe” toward the star Polaris in 2008 (“I see that this is the beginning of the new age in which we will communicate with billions of planets across the universe,” Yoko Ono said, according to the NASA news release).

Frank Drake has dabbled in active SETI himself. It was just a stunt, a proof-of-concept, on April 16, 1974, at the dedication ceremony of the rebuilt Arecibo Observatory in Puerto Rico. He transmitted an encoded message — one that described the elements that make up DNA, the planets in our solar system, the size of a human being, etc. — toward a star cluster in the constellation Hercules. The star cluster is about 25,000 light-years away.

The odds that anyone will get that message are vanishingly small, but Drake did catch grief from Britain’s Astronomer Royal at the time, Sir Martin Ryle, who thought it was reckless. Drake shrugs and says, “Anyone who’s even 100 years ahead of us [in technology] could detect our run-of-the-mill transmission.”

Drake said he doesn’t worry, as some do, that we would become depressed by contact with a superior civilization. Children aren’t depressed by the company of adults, he says. He compared SETI to doing research on ancient civilizations on Earth, such as the Greeks and the Romans.

“We’re going to do the archaeology of the future,” Drake says. “We’re going to find out what we’re going to become.”