For years, a Dutch farm headed by Mark van Rijsselberghe has experimented with fruits and vegetables that can grow in saltwater. The hope is to address a pressing problem that will become even worse in the coming years: Nearly 20 percent of the world's irrigated land that could be used for agricultural purposes is made unusable by saltwater. The problem is getting worse – the U.N. Institute for Water, Environment and Health estimates that at least 2,000 more hectares are lost each year.

Desalinating the land would be too expensive and impossible in many of the nations affected, which includes Syria, Iraq and Pakistan. So Van Rijsselberghe's team is  trying to create food that can grow in saltwater. For 10  years, the farmer and researcher has observed various types of potato plants and their ability to survive in salty water.

Now, the work has yielded some results.

This video, with English subtitles, shows how the researchers work. "We're discovering varieties and crops that thrive in saline conditions ... even in seawater," lead scientist Arjen de Vos says in the video. Apart from potatoes, the scientists have also begun to look into the resilience of onions, carrots and strawberries.

Last year, the Dutch project won a grand challenge of the U.S. Agency for International Development, asserting themselves against 560 international competitors. “It’s a game-changer," de Vos told the Guardian at that time. "We don’t see salination as a problem; we see it as an opportunity."

“Up until now, everyone has been concentrating on how to turn the saltwater into fresh water; we are looking at what nature has already provided us with," his colleague Van Rijsselberghe told the British paper. Other projects have primarily focused on trying to desalinate affected areas, but critics say it is a battle not worth fighting because global warming will continue to raise sea water levels.

What further distinguishes the Dutch Salt Water Farm is its refusal to experiment with genetically modified food or plants produced in laboratories. "Salt tolerant agriculture is not only about the selection of crops, but it involves much more -- such as a totally new crop cultivation strategy," farm spokesperson Esther Koorn told The Washington Post.

Potatoes are the preferred plant of the Dutch researchers because of the crop's global spread, which has produced more than 5,000 varieties over the last centuries. Potato plants that were common in the past used to grow with seawater, and the Dutch researchers believe that those genes were never completely lost. "A potato needs very little water to grow. Per kilogram of potatoes you need almost 10 times less water than for one kilogram of rice," Koorn said.

"It could be a hundred, it could be 1,000 years ago, they still are capable of coping with saline surroundings," Van Rijsselberghe said, according to AFP.

Interest for the salt potatoes is growing, according to the researchers. In the coming months, they want to expand their project to the south of France to experiment with different climatic conditions.

Van Rijsselberghe and his team want to increase the output of potatoes per hectare and lower their price. So far, one kilogram of salt potatoes costs about five times the price of normal potatoes produced in Europe. But at least one concern has already been refuted: The potatoes do not taste salty. In fact, Van Rijsselberghe describes their taste as rather sweet.