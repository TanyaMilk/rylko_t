I first heard about Mohamed Nasheed in 2008, when, immediately after taking office as the first-ever democratically elected leader of the Maldives, he decided not just to repair his country’s broken economy and nurture its fragile democracy but also to take up the formidable battle against climate change.

The Maldives is considered among the nations most vulnerable to the rising sea levels and aberrant weather that are a result of increased carbon dioxide in the atmosphere. It is composed of 1,200 tiny islands in the Indian Ocean, where the average elevation is about five feet and where there are no hills. It is likely that the sea will eventually overtake the islands.

After he was inaugurated, Mr. Nasheed declared that the Maldives was looking for a new homeland for its doomed population, and he later pledged that the country would be the first to become carbon neutral. A former journalist and longtime human rights activist, he viewed the struggle against climate change itself as a fight for human rights.

He had been imprisoned and tortured under the regime of the Maldives’s longtime dictator, Maumoon Abdul Gayoom, spending months in solitary confinement. Mr. Nasheed went into exile in 2003, and his return and subsequent election as president was nothing short of a miracle.

In 2009, I traveled to the Maldives to pitch Mr. Nasheed on my idea for a documentary film that would cover his dual challenges of despotism and global warming and culminate in Copenhagen in December 2009, where world leaders were to gather for a United Nations meeting about climate change.

I realized I was asking for a lot: free rein to film cabinet meetings, intimate family meals and tense bilateral exchanges with world leaders. But Mr. Nasheed thought for only a moment before saying: “Yes. Let’s do it.” Throughout our one and a half years of filming, my team was struck by his candor, tirelessness, humanity and deep commitment to transparency.

On Tuesday, we were stunned to learn that Mr. Nasheed was forced to resign his presidency under duress. Mr. Gayoom’s supporters had taken violently to the streets and put Mr. Nasheed in an impossible position: attack your own countrymen or resign. He once again followed his conscience and stepped down.

This Op-Doc includes excerpts from our feature documentary about him, “The Island President.”

This video was produced by independent filmmakers supported in part by the nonprofit Sundance Institute and the Ford Foundation.