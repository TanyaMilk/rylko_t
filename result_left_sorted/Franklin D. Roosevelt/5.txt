WASHINGTON —  Less than three months into President Obama’s first term, a top White House national security official told reporters that the new commander in chief’s diplomacy would be different from the regular-guy friendships with foreign leaders sought by President George W. Bush.

No longer would the goal be “to establish some, you know, buddy-buddy relationship,” said the official, Michael A. McFaul.

The lack of such a relationship was starkly evident in Washington last week, when Prime Minister Benjamin Netanyahu of Israel took the extraordinary step of de nouncing Mr. Obama’s Iran policy in a speech to a joint meeting of Congress. The president responded icily hours later from the Oval Office. The two did not meet face to face.

Mr. Obama’s strained association with Mr. Netanyahu, who has clashed with other American presidents as well, has been difficult from the start. But the absence of any real connection between them underscores the rule, not the exception, for Mr. Obama, who has only occasionally invested time in cultivating foreign leaders.

It is a cool, businesslike approach, similar to the way Mr. Obama deals with members of Congress, donors and activists at home. But historians and some of the president’s former foreign policy advisers say the distance the president keeps from foreign leaders leaves him without the durable relationships that previous presidents forged to help smooth disagreements and secure reluctant cooperation.

“Personal relationships are not his style,” said Martin S. Indyk, a former special envoy for Middle East peace in the Obama administration who is now vice president of the Brookings Institution. Mr. Indyk said Mr. Bush and President Bill Clinton “yukked it up with everybody.”

“With Obama, some he invested in, some he clicked with,” Mr. Indyk said. “But you could count them on one hand.”

White House officials say that while warm relationships may be desirable, they do not necessarily lead to success in American foreign policy, and that Mr. Obama has made headway without them.

“The president is driven by an assessment of our interests in dealing with world leaders,” said Benjamin J. Rhodes, a deputy national security adviser to Mr. Obama. “It’s more about how do you get off the talking points with somebody, see where they are coming from.”

Mr. Rhodes pointed out that Mr. Obama has methodically assembled support from like-minded leaders for sanctions against Russia for its actions in Ukraine, for the fight against the Islamic State, and in some cases to confront climate change.

Robert Dallek, a historian who has written extensively on the American presidency, called Mr. Obama a “cool customer” and said he appears not to exude the kind of warmth that characterized the relationships between Ronald Reagan and Margaret Thatcher or between Franklin D. Roosevelt and Winston Churchill.

“If a foreign leader connects with another head of government, it can be salutary in helping them work through difficulties or problems that may exist,” Mr. Dallek said. “If they have a lot of animus toward each other, it impedes the diplomatic give-and-take.”

Churchill spent several weeks at the White House bonding with Roosevelt after the attack on Pearl Harbor, a time that Mr. Dallek said helped them form “a kind of comfort zone” that deepened the already strong relationship between the United States and Britain during World War II.

Aides say it is unlikely that a friendship between Mr. Obama and Mr. Netanyahu could ease the fundamental disagreement the two have over how to stop Iran from developing a nuclear weapon. But former advisers say the hard feelings have made the rift worse.

“There is a tone that’s set from the top,” said Tamara Cofman Wittes, who was deputy assistant secretary of state for Near East affairs under Mr. Obama. “Right now, unfortunately, sadly, the tone between the United States and Israel is one of bitterness.”

In the past six years, administration officials have sometimes strayed from their early no-friends strategy. Aides say that Mr. Obama and Prime Minister Dmitri A. Medvedev of Russia, both lawyers from the same generation, established a rapport after their first meeting. They also say that Mr. Obama developed warm feelings for Lee Myung-bak when he was the president of South Korea, and — for a time — Recep Tayyip Erdogan, the president of Turkey.

The president has built workmanlike relationships with allies like Prime Minister David Cameron of Britain and President François Hollande of France, but nothing close. Aides say he had a genuine bond with Chancellor Angela Merkel of Germany, but that has been strained by revelations that the United States tapped her private cellphone.

“He’s invested a lot of time in his relationship with Angela Merkel that has made it resilient through a lot of ups and downs,” Mr. Rhodes said.

For much of the rest of the world, aides say the president’s strategy is one of reality: He must forge working relationships based on mutual interests with leaders who are in many cases reserved personalities who keep their own distance.

Mr. Obama never got along with Nuri Kamal al-Maliki when he was prime minister of Iraq or with Hamid Karzai when he was president of Afghanistan, both of whom suspected — with justification — that Mr. Obama wanted to see them leave office. Soon after becoming president, Mr. Obama stopped holding the regular video conferences with Mr. Maliki that the Iraqi prime minister had with Mr. Bush. Mr. Obama is also not very close to other leaders in the Middle East, like King Abdullah II of Jordan.

Mr. Obama has largely outsourced the relationship building to Secretary of State John Kerry, who has tried to develop close bonds with some of the world’s most challenging characters. Mr. Kerry has spent hundreds of hours courting Sergey V. Lavrov, the Russian foreign minister, and Mohammad Javad Zarif, the Iranian minister of foreign affairs. Mr. Kerry also spent countless hours unsuccessfully wooing Mr. Netanyahu during failed talks on Middle East peace.

Mr. Obama’s own efforts to get closer to prime ministers and his fellow presidents have ended with largely disappointing results.

In June 2013, Mr. Obama and President Xi Jinping of China spent two days at the Sunnylands estate in Southern California, hoping to build a more comfortable relationship. Aides later conceded that the effort at friendship had largely failed.

“Xi Jinping wasn’t exactly interested in having a warm and sunny relationship,” Mr. Indyk said.

From the beginning of his presidency, Mr. Obama made little effort with President Vladimir V. Putin of Russia, a relationship that has since turned hostile over Russia’s military incursions into Ukraine and the anti-American wave of nationalism that Mr. Putin has overseen at home. In 2013, Mr. Obama infuriated Mr. Putin by publicly describing him as “looking like the bored kid in the back of the classroom.”

But Mr. McFaul, a former American ambassador to Russia, said there was little Mr. Obama could have done to right the relationship with Mr. Putin. “No amount of golfing, banya-sharing or hunting with Putin would have changed that,” he said, using a Russian word for sauna.

White House officials say Mr. Obama has established a personal connection with Prime Minister Narendra Modi of India, and point out that

Ashraf Ghani, the president of Afghanistan, and Prime Minister Haider al-Abadi of Iraq are expected at the White House in the coming weeks.

But few think Mr. Obama is making a fundamental change in his approach to his fellow leaders.

“It’s not where he’s chosen to put his time,” Ms. Wittes said.