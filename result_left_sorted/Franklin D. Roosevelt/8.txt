To the Editor:

Your April 12 editorial “A New Phase in Anti-Obama Attacks” is right on the mark. From Day 1 of his presidency, Barack Obama has been subjected to a nonstop drumbeat of character assassination, often laced with blatantly racist venom. In their desire to destroy his presidency, his attackers have fought every one of his policies, paying scant heed to the fact that a failed presidency can mean a failed country. Fortunately, this has not happened.

America has shown its best nature (in twice electing Mr. Obama) and also its darkest demons (in trying to destroy him).

DANIEL R. MARTIN

Hartsdale, N.Y.

To the Editor:

Yes, the vitriolic attacks on President Obama are disturbing and hurtful to the presidency and to the American people. But having been forced to listen for years to Republican commentators who highly disturbed me, owing to a strong difference in opinion inside my own home and marriage, here is what I have to say:

There are many among us who strongly supported President Obama not once, but twice, and saw our hopes for change fall to the ground. It is not that we don’t love diplomacy and desire peace. Simply, we regard Mr. Obama’s diplomatic efforts and foreign policy as weak and misguided, and some of his decisions as dangerous, to the United States and the Western world.

We pray that it is not the case, that we are just mistaken, deceived by appearances and the theatrical moves that largely characterize politics nowadays.

NOGA SKLAR

Greenville, S.C.

To the Editor:

Your editorial says, with respect to the G.O.P.’s opposition to President Obama, “If this insurrection is driven by something other than a blend of ideological extremism and personal animosity, it is not clear what that might be.” The basis of this behavior is perfectly clear; it works.

For 34 years, and counting, our G.O.P. has steadily impoverished much of its own base with tax cuts for the wealthy, uncontrolled military spending and an erosion of help for low- and middle-income Americans while feeding that base a steady narrative of welfare slackers, unrestrained criminality, affirmative action beneficiaries and illegal-alien predators. Attacks on the president fit seamlessly into this process, generating fear that translates into political support. It is notable that all attempts by the G.O.P. to turn to responsible governance have been quickly thrown overboard by its base.

ERIC R. CAREY

Arlington, Va.

To the Editor:

The destructive, hate-driven personal attacks against President Obama by the Republicans that your editorial accurately describes should be contrasted with the rational, calm and statesmanlike manner in which President Obama conducts himself and manages the presidency. His professionalism brings great credit to the presidency, and Americans should be proud to have a person of this caliber as their leader.

His conduct in response to this Republican rage reminds me of the dignity and grace demonstrated by Jackie Robinson when he broke the color barrier of Major League Baseball.

HAROLD J. SMITH

White Plains

To the Editor:

In denouncing the criticism directed at President Obama, have you forgotten the insulting and demeaning opprobrium dumped on Presidents George W. Bush and Harry S. Truman — and even Franklin D. Roosevelt?

When, for instance, two former secretaries of state, George Shultz and Henry Kissinger, point out the dangers to global security arising from Mr. Obama’s Iran “framework,” are you suggesting that such criticism is based on race or a desire to deny Mr. Obama “any policy victory”? That would be difficult to accept.

As President Truman said, “If you can’t take the heat ...”

JOEL J. SPRAYREGEN

Glencoe, Ill.