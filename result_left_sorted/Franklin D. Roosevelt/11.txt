Maps are meant to guide and explain, but not all do it in the same way.

The following maps are "fake" because they represent, either intentionally or not, a fictive reality. Even so, they tell us some intriguing things about the world where we live and the one we want to inhabit.

(See: 40 maps that explain the world)

The world in "Civilization" 

"In the beginning the Earth was without form and void," reads the grainy introduction to Sid Meier's 1991 Civilization computer game. It then tells about the fiery formation of the planet, the shaping of the continents, and the advent of humanity. This is all a prelude to the game, a classic of the genre, where players go about building a civilization through centuries of rise and fall, war and progress.

On a fan forum, one Civilization enthusiast posted the map above, modeled on the Robinson Projection, an attempt to represent the globe without the distortions of earlier maps. It was a hit with gamers who downloaded it for their own campaigns. Maps, after all, are made for exploration.

The Great Flood

This map, digitally archived by cartographer and map collector David Rumsey, shows the known world (at least according to the Bible) from creation to the time of the Great Flood. You can zoom into it in full detail here — and glean the historical curiosities of biblical archaeologists at the time. It identifies Mt. Ararat, the spot in modern-day Turkey where some believe Noah's ark can still be found.

The date attributed to this moment, 2343 B.C., is bogus. There are records of cities and civilizations across the Mediterranean world and Asia; few were wholly wiped out by a cataclysm. Myths of the flood, though, exist in a whole range of cultures, including a book more ancient than the Bible: the Epic of Gilgamesh.

(See: 40 *more* maps that explain the world)

The Shield of Achilles

This is an 18th century rendering in a British publication of the mythic Shield of Achilles, the escutcheon borne by the great Achilles in his clash with Hector, the doomed Trojan hero, as told in Homer's "The Illiad." It was crafted by the god Hephasteus (also known as Vulcan), who fashions upon it images of the Earth, the sea, and the celestial bodies above. In its circles there are cities, fields for pasture and cultivation, grazing cattle and dancing youths "crowned with garlands." It's a map of humanity in a much wider world — one that the ancients hoped would not be swallowed up wholly by war.

The beings in the heavens

This is one of six gorgeous star maps created by Ignace Gaston Pardies in the 17th century. For eons before this French mapmaker's time, people across the world looked to the heavens and saw all sorts of creatures and gods staring back from the firmament.

(See: These maps show how Ukraine became Ukraine)

The world according to Ptolemy

Printed in Venice not long after the first expedition of Christopher Columbus, this map is a remarkably accurate depiction of the world's continents and climes built on principles first laid down by the ancient Alexandrian Ptolemy. What's "fake" about it, though, are the flights of fancy found all over the map and others (see below) in the early modern period. These include the notation of "cannibalus romon" in the northern rump of what's supposed to South America, which, according to the Osher Map Library in Maine, "reflects the prevalent but erroneous impression that cannibalism was widely practiced in the New World."

The world from the North Pole

German astronomer Franz Ritter produced this curious vision of the world, which is a projection from the North Pole. The result appears to heavily distort the world's landmasses, even though it's technically accurate, and reflects cartographers' increasing reckoning with the curvature of the Earth. The map is not without its own cast of strange monsters, believed to populate some of the more unknown reaches of the planet.

The history of the Roman Empire, if it flowed like the Amazon

Here's the story of the birth and rise of the Roman empire, as told in a 19th century book of ancient geography, digitized at the David Rumsey Map Collection. It is illustrated in the shape of South America's Amazon River; the little tributaries feeding into it were the many lesser tribes, kingdoms and regions that the Romans captured over eight centuries of conquest and expansion, culminating in its apogee under the Emperor Trajan.

A fool's map

Cited by Frank Jacobs's excellent Strange Maps blog, this "Fool's Cap Map of the World" is believed to have been produced in the late 16th century. What the map reveals is far less important than where it's placed — as the face of a jester in full garb. Fools were important, subversive figures in the courts of kings: their afflictions, physical or mental, and comic attire enabled them to poke at the false piety and tremendous of those in command. The map, writes Jacobs, is itself a statement "that the world is a somber, irrational and dangerous place, and that life on it is nasty, brutish and short." (You can see a better resolution image here.)

[40 more maps that explain the world.]

Nations as comics, #1

Politics is about people or, at least in this popular 19th century European tradition, people who represent pat stereotypes of whole nations. The image above is from a moment where Europe's fragile alliance system was delicately poised.

Nations as comics, #2

And this is when things blew up. A patriotic British cartoon map from 1914 shows the advent of World War I, as the British (with their colonial followers emerging from the seas near Ireland) take on the "eagle" of Germany with the support of the Russian "bear," aided by galloping Cossacks. The feisty hound of Serbia nips at the sides of Austro-Hungary; a fez-wearing Ottoman doesn't know what hit him.

Nations as comics, #3

It's not just Israeli Prime Minister Benjamin Netanyahu who's wary of "tentacles of terror." The land octopus is a decades-old trope for the grim intentions of some hegemonic power, usually Russia. Here, in a map posted on the Beautiful Maps Tumblr, Russia ensnares its neighbors. It keeps a firm hold of Crimea, too.

An alternate vision for the Middle East

In 1919, U.S. President Woodrow Wilson dispatched his own diplomatic mission to the lands of the defeated Ottoman Empire to evaluate how best to divide and govern the complex multi-ethnic and multi-confessional region. The above map, made by Karl Sturm and Nick Danforth and featured in the Atlantic, is a rendering of what the Middle East could have been had the findings of the King-Crane Commission been followed. You can see a fully interactive version of it here.

Some striking conclusions: Greeks and Turks didn't need their own separate countries; Muslims should remain custodians of Jerusalem (not Jews); the Kurds would be already guaranteed a degree of autonomy, though not necessarily be granted statehood.

None of this came to pass, though, because the British and French weren't interested in the American proposals.

The Sykes-Picot Agreement

In 1916, British and French diplomats Sir Mark Sykes and Francois Georges-Picot hatched a secret deal to carve up Ottoman lands into parallel spheres of interest. The vision drafted here was not fully realized, as WorldViews discussed, but "Sykes-Picot" has become short-hand for the long history of Western meddling and intervention in the Middle East.

Hitler's "plot" to capture South America

This is a map apparently cited by U.S. President Franklin D. Roosevelt in 1941 as a sign of Hitler's intentions to extend its reach into the Western hemisphere — it showed, said Roosevelt, "the Nazi design not only against South America but against the United States itself." But it's a fake, explains Mental Floss, likely forged by British intelligence to win further sympathy from the then neutral Americans. When Hitler justified Germany's declaration of war on the United States a few months later, he pointed to how Roosevelt "incites war, then falsifies the causes."

If Africa had never been colonized

Africa was carved up in the 19th century by rapacious European imperial powers; its modern-day borders are often a reflection of arbitrary colonial lines in the sand. But that obscures hundreds of years of rich, political history. Swedish artist Nikolaj Cyon attempted this map of Africa sans European influence, depicting various famed kingdoms and sultanates, as well as key urban and trading centers, through the centuries. It's upside-down orientation restores a sense of primacy to the African continent, leaving Europe on the fringes. You can see a larger version here.

When the world thought California was an island

For centuries, Europeans aware of the existence of what's now California thought it was an island. A collection at Stanford University, featured by Wired, has the maps to prove it.

If Los Angeles went underwater

If the world's continents were placed in the Pacific Ocean

The Pacific Ocean is rather large.

The lost city of Atlantis

The legendary continent of Atlantis, believed to have been lost following some great natural disaster in antiquity, has excited the Western imagination for centuries. Above is a 17th century rendering of one of its rumored positions, somewhere between Europe and the Americas.

The world, if countries reflected the size of their populations

Originally made in 2005 by cartographer Paul Breding, this map was updated by a Reddit user a decade later. It scales the size of countries according to their respective populations. Some nations — Australia, Canada, for example — virtually disappear. Others, such as India, are far bigger than in other physical maps.

If the United States was literally (or cartographically) an eagle

This map appeared in an atlas in 1833 aimed at educating American youth on the breadth of their still fledgling republic. The eagle — whose feathers, talons and wings extend over most of the American states and territories at the time (though curiously not Maine) — represents a national emblem against forces that would sunder the Union. Not long after the map was published, it became obsolete.

In the jarring prose of mapmaker Joseph Churchman, it was supposed to inculcate a new patriotism:

A cryptozoological map of the U.S.

A state-by-state guide to American monsters, including Bigfoot, the dreaded skunk ape and the Pukwudgies, mythical little people known to Algonquin tribes in New England. You can purchase the full map as a poster here.

If Hobbits journeyed across North America

The protagonists of J.R.R. Tolkien's beloved "Lord of the Rings" trilogy embark on the archetype of an epic quest — from an innocent, pastoral land into darker realms, full of corruption, danger and death. According to one Reddit user, who superimposed maps of the Hobbits' journeys onto North America and Europe, that journey is like going from Kansas to the southern fringes of Georgia. Go figure.

If the Hobbits journeyed across Europe

Tolkien would probably understand this rendition a bit better. For the British scholar and writer, conjuring Middle Earth was an attempt at creating a mythic past for Britain, borrowing from other traditions in northern Europe, including the "Kalevala," an old Finnish epic.

[If 'Game of Thrones' were in the Middle East.]

If "Game of Thrones" took place in Europe and Asia

Tolkien's fantasy universe isn't the only one that has its fans. Here's a Redditor's take on how the continents of Westeros and Essos in the hit HBO series "Game of Thrones" — based on the novels of George R.R. Martin — would line up over the Eurasian landmass. Westeros appears to be a blown-up version of Britain — which is fitting, since Martin drew his medieval saga from the historical intrigues and clashes of the War of the Roses.

In the Americas

If Brazil was part of Game of Thrones

The intrigues between the feuding houses of Westeros are rich with metaphors applicable to the present; WorldViews is guilty of indulging in its own fit of speculative analysis. This image, created by Brazilian Web site Aheadmkt.com, imagines the country's complex, fractious politics arrayed in Westerosi fashion. You can peruse the full interactive here.

If Europe was defined by its terrible cuisine

The world, as seen by a Thatcherite

The "Tory Atlas of the World"— certain pejorative terms replace country names; the Falkands archipelago is conspicuously large; Ireland disappears. (Note this map was clearly made by someone opposed to Britain's conservatives.)

The lands of sobriety

A "temperance map," dated to 1838, shows the dangers of alcohol and the merits of sobriety. In the land of Self-Denial, to the north, you have virtuous provinces such as Industry, Improvement and Plenty. In the land of Inebriation, in the south, you have such dreary places as Total Indifference, False Hopes and False Comforts. The waters in between are particularly perilous, including the Gulf of Wretchedness and the Sea of Anguish.

You can pore over the map in full detail at the Library of Congress Web site and view the Library's digital collections here.

Related on WorldViews:

If 'Game of Thrones' were in the Middle East

How ancient Mesopotamia birthed Western civilization

Was Moses real?

Sykes-Picot and the collapse of the Middle East's borders

Why Crimea is Russia's Jerusalem