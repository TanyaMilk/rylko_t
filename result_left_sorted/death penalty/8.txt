BOSTON —  This city, for so long huddled inward, is slowly breaking out of its winter shell.

After Boston’s snowiest winter on record, the last of the ice-encrusted black mounds are finally receding. The Public Garden is still shrouded in earth tones, but tiny buds are just visible on the swaying branches of the weeping willows. Ice on the Charles River has given way to billowing white sails. On Boylston Street, in one of the surest harbingers of spring, the scaffolding and bleachers are going up at the finish line for Monday’s 119th running of the Boston Marathon, the world’s oldest annual road race.

Like opening day at Fenway Park, which was a sun-splashed ode to joy this week, the marathon is a rite of spring. And this year, after a winter horribilis, Bostonians are more desperate than ever to celebrate its arrival. But since the bombings on April 15, 2013, which killed three spectators and wounded 264 others, some of them grievously, the marathon is now a two-tiered occasion.

It is a time for Boston to rejoice in the race on Marathon Monday with one million spectators along the 26.2-mile route, but it also honors the victims and survivors of the bombings, and the many emergency workers and bystanders who rushed to their aid and saved countless lives.

“April 15 is a date that has come to stand for our city’s deepest values,” Mayor Martin J. Walsh said as he announced a new annual observance, called One Boston Day. At a solemn ceremony Wednesday morning, with mournful bagpipes playing, Mr. Walsh joined survivors, victims’ families, law enforcement officers and runners in unveiling banners at the site of the two bombings.

At 2:49 p.m., the time the first bomb went off, people near the finish line and across Boston stopped and stood for a few minutes of silence. Shortly afterward, church bells tolled across the city.

Survivors talk about moving forward, but the marathon is cyclical, returning annually, bringing them back in a public way to events that changed their lives forever. Some have embraced the public remembrances; others have tried to avoid them.

The Richard family, devastated by the death of 8-year-old Martin, hesitated at first to be public with their grief. They all suffered: Martin’s little sister, Jane, then 7, lost her left leg; his brother, Henry, then 9, witnessed unfathomable carnage; their mother, Denise, is blind in her right eye; their father, Bill, caught burning shrapnel in his legs, and his eardrums were blown out.

Bill Richard said last year that if Martin had to die, he wished it had happened privately, in a car accident, on a random night.

“Martin died at the Boston Marathon,” he told The Boston Globe. “The marathon is going to happen every year, and it’s going to be public, whether we like it or not.”

But by now, on the second anniversary of the bombings, the Richard family seems to have embraced its role, perhaps perceiving that other people derive strength and inspiration from them. On Wednesday, they took part in the remembrance ceremonies and later attended a blood drive.

Gov. Charlie Baker may have had them in mind when he said in a statement, “In many respects, those most affected by the events of two years ago have shown us all the way back with their courage, grace and determination.”

But the remembrance arrived this year with an added layer of complexity — in the midst of the trial of Dzhokhar Tsarnaev, 21, a failing college student and dislocated Russian immigrant, who was convicted last week on all 30 counts against him in connection with the bombings.

The trial is on pause right now, suspended between the guilt phase and the sentencing phase. It will resume Tuesday, the day after the marathon. And so, even as winter has ebbed and the bats are cracking again at Fenway, full relief may not come until Mr. Tsarnaev’s fate is decided.

For now, the trial has plunged the region into a debate over whether Mr. Tsarnaev should be sentenced to death or to life in prison. Polls show that Bostonians favor life in prison for Mr. Tsarnaev, 2-1, while the country as a whole favors the death penalty by the same amount.

From the finish line to talk radio, from social media to everyday conversations in bars and offices, Boston is brimming with opinions of anger, vengeance and mercy. Schools here are using the occasion as a teachable moment.

Even visitors are caught up in the debate.

“I thought I was immune to a lot of stuff, but being here just leaves a pit in your stomach,” said Lorrie Pfaff, 46, a state parole officer from LaCrosse, Wis., as she took pictures recently of the finish line. She said she favored the death penalty in this case.

But her friend, Jean Ryan, 46, of nearby Bedford, said that as loathsome as she found Mr. Tsarnaev, she opposed putting him to death. “Much as I’d like to see him fried, I’m Catholic, and I just couldn’t do it,” she said.

Pope Francis and the archbishop of Boston, Cardinal Sean O’Malley, have said that they want his life spared. So does the sister of Sean Collier, the M.I.T. police officer whom Mr. Tsarnaev killed; she wrote on Facebook that she “can’t imagine that killing in response to killing would ever bring me peace or justice.”

But Governor Baker, a Republican, wants Mr. Tsarnaev sentenced to death. And Boston’s police commissioner, William Evans, says he is torn between his Catholic faith and his views as a law enforcement officer. “He deliberately, maliciously blew up that family,” Mr. Evans told WGBH radio, speaking of the Richard family, which has been thrust to prominence with the trial.

In an attempt to inflame the jury’s passions, the government, which is seeking the death penalty, showcased the horrific details of Martin Richard’s death. Bill Richard testified, and prosecutors led the medical examiner through a graphic account of what the explosion did to the boy’s body. Several jurors hung their heads in their hands; some wept openly.

The Richards, like many survivors, have not said publicly what punishment they would like to see, instead focusing on the positive. Mr. Richard testified that although he had lost most of his hearing, he could “still hear the beautiful voices of my family.”

He got that chance on Monday, when the St. Ann’s Parish children’s choir sang the national anthem during opening day ceremonies at Fenway. Front and center in her Red Sox jersey was a beaming Jane Richard, standing on her prosthesis, her hand on her heart.