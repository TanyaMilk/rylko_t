A union plumber, a school teacher and a survivor of the 1999 Columbine High School massacre were among the 19 women and five men chosen to serve as jurors in the death penalty trial of Colorado theater shooter James Holmes.

The 12 jurors and 12 alternates were chosen Tuesday after a selection process that began Jan. 20, and which experts said was among the largest and most complicated in U.S. history.

Holmes is charged with killing 12 people and wounding 70 others in the July 2012 attack in suburban Denver. His attorneys don't dispute that he pulled the trigger but say he was in the grips of a psychotic episode when he slipped into the packed movie theater and opened fire.

Jurors will decide whether he was legally insane at the time. If they find him guilty, they must also decide whether he should be put to death or sentenced to life in prison without parole.

The jurors

All 24 jurors will sit through the entire trial. Neither the group nor the public will know who is a primary juror and who is an alternate until the case is handed over for deliberations.

Also on the jury are a Denver Public Schools employee, a person with depression and a businesswoman who cares for her elderly parents.

One juror said during questioning, before she was chosen, that she was nervous what her community would think of her verdict.

The case is "big and serious, and it's going to have a huge impact on me and everyone else, the defendant and people in the community," she said. "They're not going to be directly involved, but they're going to be watching."

Line of questioning

Attorneys on Tuesday questioned the 93 remaining jurors about their interpretations of the law, how they would gauge witnesses' and experts' credibility, and whether they could handle serving on such a high-profile trial.

District Attorney George Brauchler characterized it as a "four- to five-month roller coaster through the worst haunted house you can imagine."

He asked prospective jurors if they could serve even if they hear no evidence of a motive, since prosecutors are required to prove only the 165 charges against Holmes — not why they believe he committed the crimes.

Holmes' attorney, Tamara Brady, focused on perceptions of Holmes and whether the jury candidates could be objective given the litany of charges against him and the public scrutiny they will face. She asked how they felt listening for nearly two hours as Judge Carlos A. Samour Jr. read a list of the charges, including each victim's name. She wondered if prospective jurors would be too sympathetic to survivors.

She said she was nervous "about whether Mr. Holmes can get a fair trial in this case or whether it's just too big."

Jurors' concerns

Potential jurors told attorneys they worry about what their neighbors might think if they reach an unpopular verdict and whether reporters would harass their families.

One man said he was reassured when the judge told him steps were taken to shield his identity. And many pledged they would not let their decision-making be influenced by concern about what others think.

Other candidates expressed trepidation about hearing graphic testimony and perhaps being overwhelmed by emotion.

What's taken so long?

Some prospective jurors have asked the judge why it has taken nearly three years for the case to come to trial.

Samour has said it's not an unusual amount of time for a trial this complex. The death penalty and insanity plea introduced complicated and time-consuming legal requirements.

After nearly three months, and almost three years since the mass shooting, the jurors were chosen from among hundreds who filled out written questionnaires, then returned for one-on-one sessions, where they were questioned about their views on the death penalty and mental illness. Court officials initially summoned an unprecedented 9,000 people.

What about other large trials?

In the amount of time it has taken so far in Denver, federal jurors in Boston convicted marathon bomber Dzhokhar Tsarnaev.

That case was accelerated by Tsarnaev's admission that he participated in the April 2013 bombings and that his brother, Tamerlan, was the mastermind.

The Texas trial for the killer of a former Navy SEAL depicted in the movie "American Sniper" was complicated by publicity about the film. But jury selection moved quickly because it didn't involve concerns about the large number of people affected by the crime.

What's next?

The chosen jurors will report to court the afternoon of April 27 for opening statements.

Associated Press