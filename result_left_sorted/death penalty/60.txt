Nearly four months after his arrest in the disappearance of a missing University of Virginia student, Jesse Matthew Jr. has been indicted on a murder count in the case -- but he won't face the death penalty and still isn't charged in the death of another student to which he's tied.

Jesse Matthew Jr., 33, was indicted on the first-degree murder charge Monday, Albemarle County prosecutor Denise Lunsford said Tuesday. Matthew already was charged with abduction with intent to defile in the case. Hannah Graham, 18, disappeared in September and was found dead in Albemarle County a few weeks later.

Lunsford declined to say why Matthew was not charged with the higher count of capital murder, which would have carried a possible death-penalty sentence. The abduction and first-degree murder charges are punishable by up to life in prison.

"A great deal of serious thought went into this determination, including the impact on the community, the Grahams and the need to provide Mr. Matthew with a fair trial," Lunsford said.

Matthew also faces two counts of reckless driving in the case.

The indictments were one sentence each and didn't offer any details on the charges.

Police have said forensic evidence also links Matthew to the 2009 disappearance and death of 20-year-old Virginia Tech student Morgan Harrington, whose body also was found in the county. Lunsford said "there are no pending charges" against Matthew in the Harrington case.

"The simple fact is the case involving Hannah Graham was ready to be charged first," she said.

Craig Maniglia, Hannah Graham's high school softball coach and a longtime friend of the family, said he is hopeful a conviction will bring closure.

"Once convicted, my hope is that Mr. Matthew will not go free and be in a position to harm innocent young women ever again," Maniglia said.

Graham vanished after a night out with friends Sept. 12. She left an off-campus party alone and texted a friend saying she was lost, she said.

In surveillance video, she can be seen walking unsteadily and even running at times, past a pub and a service station and then onto a seven-block strip of bars, restaurants and shops. Another video captured her leaving a restaurant with Matthew, who had an arm around her.

Graham's disappearance prompted a monthlong search involving thousands of volunteers as well as police. It ended when searchers found her remains Oct. 18 in rural Albemarle County, roughly six miles from the hayfield where Harrington's body was found in January 2010.

Harrington disappeared while attending a Metallica concert at U.Va in October 2009. Her T-shirt was later found on a nearby tree limb.

After police named Matthew a person of interest in Graham's disappearance, he fled and was later apprehended on a beach in Texas. He was charged with abduction with intent to defile, a felony that empowered police to swab his cheek for a DNA sample. That sample connected Matthew to a 2005 sexual assault in Fairfax County, according to authorities. He has pleaded not guilty in that case.

The DNA evidence in the Fairfax sexual assault, in turn, linked Matthew to the Harrington case.

Matthew previously had been accused of raping students at Liberty University and Christopher Newport University in 2002 and 2003. Matthew had played football at both schools. The cases were dropped after the women declined to press charges.

Matthew's first court appearance on the newest charges in the Graham case is scheduled for Feb. 18, Lunsford said. He will appear by video link from Fairfax County, where he is in jail awaiting trial in the 2005 rape case.

Matthew's trial in Fairfax County, originally set for March 9, was postponed last week. Matthew's public defender said she needed more time to analyze DNA evidence in the case, and prosecutors did not oppose the delay.

A new trial date will be set at a Friday hearing in Fairfax.

Associated Press