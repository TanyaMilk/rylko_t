The nearly seven-year legal saga of Jodi Arias ended Monday as a judge sentenced her to life in prison for killing her ex-boyfriend, and the victim's sisters unleashed their pain over the 2008 murder that captivated social media with its salacious details.

Three of Travis Alexander's sisters tearfully urged Judge Sherry Stephens to impose the harshest penalty available against Arias. They described Arias as "unrepentant" and "evil" and lashed out at her.

As Arias was leaving the courtroom, Alexander's younger sister, Tanisha Sorenson, said loudly, "Burn in hell," prompting her sister to try to quiet her down.

Samantha Alexander cried as she recalled walking into her brother's house after investigators had finished collecting evidence there. "He was there for five days," she said. "Five days he is there decomposing in the shower. I'm sure his soul was screaming for someone to find him."

Alexander's family and friends hugged each other with tears in their eyes but smiles on their faces after the judge imposed the most severe of two available sentences and denied Arias a chance to be eligible for release after serving 25 years. About a dozen jurors from the two trials were in the courtroom to witness the sentencing.

Before the sentence was handed down, Arias gave a rambling statement in which she stood by her testimony and accused police and prosecutors of changing their story during the investigation. She said she was sorry for the pain she caused Alexander's family and friends.

"I'm truly disgusted and I'm repulsed with myself," Arias said, recalling the moment she put a knife to Alexander's throat.

She has acknowledged killing Alexander but claimed it was self-defense after he attacked her. Prosecutors said Arias killed Alexander in a jealous rage after the victim wanted to end their affair.

Arias, wearing black and white striped jail clothing, cast her gaze away from Alexander's sisters as they spoke to the judge, but looked at her mother as she sought leniency.

The 34-year-old was convicted of first-degree murder last year, but jurors deadlocked on her punishment.

A new jury that was picked to decide her punishment had deadlocked last month over whether she was to be sentenced to death or life in prison, leaving it up to the judge to decide whether she would ever get a chance at release.

Eleven jurors at the second trial voted for the death penalty, while one juror spared Arias' life by insisting that she be sentenced to life in prison. The holdout juror was accused by her colleagues of having a bias toward Arias.

Defense lawyer Jennifer Willmott said Monday she has never seen jurors attended a sentencing in any of her previous trials.

Prosecutors say Arias killed Alexander after he planned a trip to Mexico with another woman. Arias shot Alexander and stabbed him nearly 30 times in his suburban Phoenix home. She was arrested weeks later and initially denied involvement.

International attention quickly followed the case after Arias gave two television interviews in which she told a bizarre story of masked intruders breaking into the home and killing Alexander while she cowered in fear. She subsequently changed her story and said it was self-defense after Alexander attacked her on the day he died. Her attorneys portrayed Alexander as a sexual deviant who physically and emotionally abused Arias.

Her 2013 trial became a media circus as details of their tawdry relationship and the violent crime scene emerged while the courtroom saga was broadcast live. Spectators traveled to Phoenix and lined up in the middle of the night to get a seat in the courtroom to catch a glimpse of what had become to many a real-life soap opera.

Willmott urged the judge not to be swayed by the "lynch mob from social media" that opposes her client. She pleaded with the judge for "just a possibility, a hope to live for."

Stephens was not swayed, imposing a life sentence in which Arias will begin serving in a maximum-security unit at a prison 30 miles west of downtown Phoenix. Willmott spoke to Arias after the sentencing and said she was in good spirits.

"She feels good. She's ready for the next part of her life," she said.

The Alexander family took some solace in seeing that Arias received a full life term, but they became emotional as they talked about the difficulty in enduring the trauma brought on by Alexander's murder.

"I don't wanna remember him anymore," said sister Hillary Wilcox. "Because it hurts too much to remember him alive."

Associated Press