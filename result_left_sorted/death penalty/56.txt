Last spring, the night before the final legislative session of his two terms ended, Gov. Martin O'Malley couldn't sleep.

Instead of continuing to fret about whether the legislature would pass his bill to raise the minimum wage, O'Malley wrote a 1,684-word letter to his staff about the victories and struggles of his years in office, outlining the six qualities he believed made a "good leader."

Among them, he wrote, "Good leaders make themselves vulnerable — it is the only way they get difficult things done."

O'Malley leaves public office Wednesday for the first time in more than 23 years, leaving behind a substantial record of accomplishments at the city and state levels. His tenure also makes him vulnerable to criticism that he moved the state to the left and raised more taxes than the electorate could tolerate.

Both Democrats and Republicans consider O'Malley extremely effective at pushing his policies through the legislature. Yet they describe the Democrat's legacy in sharply partisan terms. O'Malley has either been a charismatic, nationalleader who pulled Maryland through an economic recession or a tax-and-spend liberal who went too far.

"History will judge him as a governor who accomplished quite a bit, whether you like him or don't like him," said House Speaker Michael E. Busch, a fellow Democrat.

O'Malley ushered through landmark social policy bills legalizing same-sex marriage, granting discounted university tuition to some immigrants in the country illegally, repealing the death penalty and enacting strict new gun control laws.

Senate President Thomas V. Mike Miller said O'Malley has been the most effective Maryland governor since the mid-1970s, but his success in pulling Maryland to the left came with a price — one that was reflected in the November election, when Republican Gov.-elect Larry Hogan defeated O'Malley's preferred successor.

"Because of his politics, a majority of the subdivisions of the state felt neglected," Miller, a Democrat, said.

O'Malley closed the troubled 19th-centery House of Corrections in Jessup in his first term, but a gang scandal erupted at the state-run Baltimore City Detention Center during his second. While the state's online health insurance marketplace enrolled record numbers of people in private plans and Medicaid this year, last year it had one of the worst rollouts in the nation.

He increaseed sales taxes, income taxes on high-income earners and created new taxes on services. He also raised the gas tax for the first time in more than two decades and increased the corporate tax rate.

"O'Malley's legacy is the strongest Republican Party since the 1920s," said Joe Cluster, executive director of the Maryland Republican Party. "He was a strong campaigner, he won two elections, but in the end, his policies created an environment that will bring this state closer to being a two-party state."

Even some Democrats agree that the Republican wave that took the governor's mansion and a record number of House of Delegate seats was sparked, in part, by O'Malley's push to raise taxes.

"We were, in retrospect, a bit too aggressive in that department," said House Economic Matters Chairman Dereck Davis, a Prince George's County Democrat.

"The party led by the governor, point blank, was too aggressive in trying to maintain the status quo," Davis said. "You have to make tough choices, even if they're just temporary, you have to make those [budget] cuts. You have to slow things down. We were unwilling to say no to anybody, about anything."

O'Malley and his defenders argue the tax increases were necessary to keep investing in schools and to stave off painful cuts during the nationwide recession. O'Malley inherited a $1.7 billion structural deficit when he took over from Republican Gov. Robert L. Ehrlich in 2007. This year, that deficit stands at an estimated $750 million — a reduction of nearly a billion dollars.

"He governed during very difficult times, and he didn't want to use that as an excuse not to make progress," said O'Malley's former chief of staff, Matt Gallagher. "We wouldn't. … He blazed new trails, he found creative ways to get things done, and all going into a pretty hard economic headwind."

O'Malley also cut the state workforce by 6,000. By his count, the state government is now smaller per capita than it has been since 1973. And while O'Malley did increase the annual rate of state spending each year, he did it by smaller percentages than any of the four previous governors.

The governor acknowledged that he has developed a reputation for raising taxes. On his blog, he relayed an anecdote about setting up a heating oil account for his new home in Baltimore. He wrote that when the operator asked for his last name:

"O'Malley," I answered. "Like the out-going governor."

"Ah, yes," she said. "The 'tax man'…"

O'Malley said he has struggled to convey to Marylanders what he has been doing as governor — a task that was easier when he was Baltimore's mayor.

"I've had many people, even years later, thank me for helping them or a family member get into a drug treatment program during the city's turnaround; I've never had one of the 700,000 citizens who, for the first time now, have health care ever acknowledge the state's role in making this possible."

Plenty of others have taken notice of O'Malley's work. He used a data-driven method to manage government, earning him accolades from national magazines as wonky as "Governing" and as hip as "Esquire."

Among the data-based accomplishments he cites: Violent crime has been reduced by 27.3 percent; infant mortality has declined by 17.5 percent overall, and by 25 percent among African-Americans. But his published goals show he has also fallen short in a few areas. The recent heroin epidemic, for instance, reversed any progress O'Malley's administration had made toward reducing overdose deaths by 20 percent; instead, they're up 7.4 percent.