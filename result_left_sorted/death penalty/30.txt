Kathleen Larimer feels as though she's been punched in the gut whenever his picture pops up in the news. The mug shot with the brightly dyed orange hair, the wild-eyed expression — that one is the worst, she says.

She would like to erase his name and memory from the public consciousness, even as the days draw close to his trial.

Her son, Crystal Lake South High School graduate and Navy Petty Officer John Larimer, 27, was among a dozen people shot and killed in a Colorado movie theater nearly three years ago after a heavily armed gunman barged into a midnight showing of a Batman movie and began shooting indiscriminately.

Kathleen Larimer and her husband are among those who are due to testify at the trial of James Holmes, 27, who is pleading insanity in the death penalty case. Jury selection is underway, and testimony is scheduled to begin in the coming months.

"Don't give him the notoriety that I think he's seeking," Larimer said. The Crystal Lake woman recently retired from her job as a school nurse, and she expects to attend as much of the trial as she can.

"I understand, with people in journalism, you have to weigh the public's right to know," she said. "Many times, I think the victims get lost in this."

Larimer has largely shied away from media coverage over the past 21/2 years but said she agreed to be interviewed in hopes of shedding light on a movement that seeks to stop the media from naming or using images of her son's killer any more than necessary.

Larimer and her husband, Scott, share the view espoused by another grieving couple whose son also was slain that night in the theater.

Thomas and Caren Teves have been outspoken in their belief that mass shooters feed off the infamy brought by national news coverage, which they fear could influence others to commit heinous acts of violence. The Teveses, of Phoenix, have started a website called NoNotoriety.com that calls for the media to avoid the gratuitous use of the name or image of the man Thomas Teves refers to as "it."

"My son's death has become entertainment," Teves said. Just as John Larimer's date that night, Julia Vojtsek, credited him with saving her life, Teves' son, Alex, 24, also died while shielding his girlfriend from bullets, his father said.

"... What's relevant to me is there will probably be three or four shootings (that occur elsewhere) during the trial and more families will join the club no one wants to join," Thomas Teves said. "We just want to try to save some lives."

He believes that the news media disrespect his son every time they focus attention on his killer. He uses the Twitter hashtag #nonotoriety to maintain a "media shame list" that calls out news outlets deemed to have been irresponsible in covering the case.

Experts on public mass shootings say little research exists to prove or debunk the theory that less publicity will prevent murders. Few mass shooters survive, complicating efforts to try to determine their frame of mind. A shooter's mental history and access to weapons provide insight, but experts have found few ways to predict or prevent such incidents.

"While some cases may be instances of relatively indiscriminate killing, others involve assailants driven by particular hatreds that lead to the targeting of specific groups and can be considered hate crimes and investigated and prosecuted accordingly," according to a 2013 Congressional Research Service report on "public mass shootings in the United States."

"Still others can involve ideologically motivated killing, leading to terrorism-related investigations and charges."

Some experts agree on the potential for copycats, finding evidence that some mass shooters have studied violent massacres committed by others. One forensic scientist describes a type of mass murderer as a "pseudocommando" who kills in the public, in daylight, after planning the offense and arriving with an arsenal of weapons.

Driven by "strong feelings of anger and resentment," the pseudocommando believes he's been persecuted and wants to carry out a "personal agenda of payback," Dr. James Knoll IV, a psychiatrist at the State University of New York, wrote in a 2010 article published in the Journal of the American Academy of Psychiatry and the Law.

"Some mass murderers take special steps to send a final communication to the public or news media; these communications, to date, have received little detailed analysis," he wrote.

While mass murders are not new, "present day access to powerful automatic firearms, as well as glorification of the phenomenon by the media, are two factors making modern mass murders unique," Knoll wrote.

No motive for the theater shooting has been revealed. The defendant, a graduate student in neuroscience, had no criminal history but had shown signs of mental illness, according to news reports. His lawyers have acknowledged he was the shooter, but they say he was in the grip of a psychotic episode at the time.

As the Colorado trial date approaches, the Larimers don't expect to hear anything that could possibly explain his motivation, Kathleen Larimer said.

John Larimer was a Navy petty officer third class stationed at Buckley Air Force Base in Colorado who had excelled in his new role as a cryptologic technician, scanning satellite images for suspicious activity. He also was a huge fan of superhero movies and was attending the premiere of "The Dark Knight Rises" in Aurora, Colo., on July 20, 2012, when the shooting took place.

Twelve people were killed and dozens were wounded in the rampage. Many of the victims' families have communicated for months through social media and email, and they expect to meet face to face for the first time at the trial, Kathleen Larimer said.

In December, she found it difficult to bear when the shooter's parents issued a public plea for their son's life, describing him as mentally ill. Larimer wrote a letter to the parents, saying that her son, John, the youngest of five, wasn't given any choice the day he died.

She never mailed the letter.

"The one thing I don't want to become is angry and bitter," Larimer said. "That anger is counterproductive. That anger would eat me up. I fight that. That's a challenge sometimes."

The randomness of the shooting baffles her. She worries that someday her grandchildren will look up her son's name online and find only information about his killer.