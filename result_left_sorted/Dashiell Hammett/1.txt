On a clear afternoon about 100 years ago, a large British passenger ship started sinking off the coast of Ireland, listing violently as it was sucked down into the ocean.

“Desperate people ran helplessly up and down the decks,” one horrified eyewitness wrote, adding: “It was the most terrible sight I have ever seen. It was impossible for me to give any help.”

The distressed witness was Walther Schwieger, a German submarine captain who just minutes before had fired a torpedo into the ship, the Lusitania, killing 1,198 of its 1,959 passengers, in one of the most devastating maritime disasters in history.

Some 95 years later, Erik Larson, the author of best-selling historical narratives like “The Devil in the White City” and “In the Garden of Beasts,” came across Schwieger’s journal and other dramatic testimonials at the Hoover Institution at Stanford University. Schwieger’s complexity as a character fascinated Mr. Larson, and helped persuade him that he had found the subject of his next book.

Five years and eight drafts later, Mr. Larson’s new book, “Dead Wake,” to be released by Crown on Tuesday, is shaping up to be one of the biggest nonfiction titles of the year. Mr. Larson turns the already heavily chronicled destruction of the Lusitania into a sort of geopolitical thriller, one that reads like a brainy mash-up of James Cameron’s “Titanic” and Steven Spielberg’s “Jaws,” with the invisible menace of the submarine spreading panic among seafarers like a rogue great white shark.

“Dead Wake” jumped to the No. 1 spot on Amazon’s best-seller list several days before its release. Crown is printing 250,000 copies and is sending Mr. Larson on a robust 24-city book tour.

Though the Lusitania seems like the ideal story for Mr. Larson, who specializes in adrenaline-fueled historical narratives (in past books he’s written about serial killers, a killer storm and a homicidal doctor), he was reluctant to tackle the subject at first.

“It was, by my standards, low hanging fruit,” Mr. Larson, 61, said during a recent interview at his Upper East Side apartment overlooking Central Park. “I usually look for stories with barriers to entry, something complex enough that no one else is going to do it.”

But when he started reading through the archives at Stanford, he realized he didn’t know much about the sinking of the Lusitania, and that much of what he thought he knew was wrong. When an archivist brought him a plank from a wrecked Lusitania lifeboat, he took it as encouragement to keep digging.

“I’m always looking for a sign — not in a spooky, supernatural way, but in a neurotic writer kind of way,” Mr. Larson said.

He spent the next two years sifting through source material in Thorsminde, Denmark; London; Liverpool; and Cambridge, England. He studied intercepted war telegrams, code books used by the British military, love letters, diaries, autopsy reports, morgue photos, depositions from survivors taken during a trial after the disaster, and an insurance claim of more than 180 pages from a survivor who lost rare manuscripts and drawings by Charles Dickens and William Thackeray.

“It’s like being involved in a detective story, looking for that thing that nobody else has found,” Mr. Larson said.

Not everyone thinks he succeeded. In a review of “Dead Wake” in The New York Times Book Review on Sunday, the historian and author Hampton Sides noted that “from the standpoint of scholarship or human drama, there’s not much fresh ground here.”

Others argue that Mr. Larson’s book will counter widespread misconceptions about the attack. Though dozens of books have been written on the subject, many are riddled with inaccuracies, such as the view that the sinking drew the United States directly into the war, said Mike Poirier, a Lusitania enthusiast and co-author of a book on World War I naval disasters.

“In 100 years, there have only been three good books until Erik Larson came along,” Mr. Poirier said.

Mr. Larson’s publisher hopes to broaden the author’s sizable fan base by capitalizing on the centennial of the ship’s sinking, on May 7, 1915. In a clever and somewhat macabre bit of marketing, Crown is partnering with Travel + Leisure magazine, the recipe website TasteBook and Cunard Line, the cruise company that operated the Lusitania, on a Lusitania-themed sweepstakes. The winner will receive a signed copy of the book and two tickets on Cunard’s Queen Victoria for a weeklong anniversary cruise around England and Ireland (minus the submarine attack, presumably). Mr. Larson is also teaming up with Twitter for a week in early May, when he will tweet a detailed narrative timeline of the Lusitania’s crossing and sinking during the anniversary.

Such stunts could help draw in World War I buffs and maritime history enthusiasts, but it doesn’t take much to entice Mr. Larson’s fans. His six previous books have collectively sold 5.5 million copies. His most popular work, “The Devil in the White City,” an account of a serial killer stalking victims during the 1893 Chicago World’s Columbian Exposition, has sold more than 3.3 million copies.

Along with Laura Hillenbrand, whose best sellers “Seabiscuit” and “Unbroken” have sold more than 10 million copies, Mr. Larson occupies a rare niche of historical writers whose books seem to straddle the high and low end of the nonfiction market, reaching the masses but also attracting scholarly attention and landing in college and high school curriculums. “Dead Wake” will be displayed prominently in Costco, Target and Walmart and promoted with ads in The Chronicle of Higher Education.

“He’s the master of making us forget the history we think we already know,” said Amanda Cook, Mr. Larson’s editor at Crown. “It’s this extraordinary ability to build suspense when we all know how it’s going to end.”

Mr. Larson steeps himself in facts, but he seems to model his narrative arcs and prose style on fiction. He lists the crime writers Raymond Chandler and Dashiell Hammett as influences. He’s completed four novels, which he is determined to keep unpublished. His standard writing routine — an elaborate set of rituals inspired partly by the novelist Graham Greene — involves getting up around 4:30 a.m. to work with the goal of writing a single page a day. He sits at his computer with coffee and an Oreo cookie, or two if it’s a bad day, he says. He always stops in the middle of paragraphs or sentences, so that he knows exactly where to pick up. He maintains this strict regimen seven days a week, 364 days a year, taking only Christmas off, he says.

When he is between books, he sticks to his usual writing routine but sometimes experiments with different genres, mostly detective fiction. He has had offers to publish his novels, but decided he did not want to derail his nonfiction writing.

For now, he seems content to exhume stories from history, assembling facts in a way that no novelist could pull off without inviting disbelief. Toward the end of “Dead Wake,” Mr. Larson drops in the chilling detail that four months after Captain Schwieger sank the Lusitania, he torpedoed another passenger liner, killing 32 people. The ship carried the corpse of a Lusitania victim, Frances Stephens, whose body was being returned to Canada for burial.