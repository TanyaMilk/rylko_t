The headlines have been grim. Europe's Jews face "rising anti-Semitism"; in some countries, many are leaving in "record numbers." In separate incidents in recent months, gunmen have targeted Jews and Jewish institutions in Paris and Copenhagen. Even the Jewish dead have not been left in peace, with reports of graves being desecrated.

But the future of tolerance and multiculturalism in Europe is far from bleak. The bigotry on view has been carried out by a fringe minority, cast all the more in the shade by the huge peace marches and vigils that followed the deadly attacks. And some communities are trying to build solidarity in their home towns and cities.

One group of Muslims in Norway plans to form a "ring of peace" around a synagogue in Oslo on Saturday. On a Facebook page promoting the event, the group explained its motivations. Here's a translated version of the invite:

According to the Times of Israel, Ervin Kohn, a leader of Oslo's small Jewish community, had agreed to allowing the event on the condition that more than 30 people show up — a small gathering would make the effort look "counter-productive," Kohn said. Close to 1,000 people have indicated on Facebook that they will attend.

"We think that after the terrorist attacks in Copenhagen, it is the perfect time for us Muslims to distance ourselves from the harassment of Jews that is happening," 17-year-old event organizer Hajrad Arshad said in an interview with Norwegian television. 

"If someone wants to attack the synagogue, they need to step over us first," posted another of the event's organizers on Facebook.

Related links