Does Baltimore have a race problem? The nation has a race problem. Why should we be any different? But cutting through the thicket and thorns of general racism to get to the specific issue raised by the city's police commissioner takes some paring and parsing.

"When I go to Baltimore, on the East Coast, I'm dealing with 1950s-level black-and-white racism," Anthony Batts told a presidential task force last month. "It's [taking] a step back. Everything's either black or everything's white, and we're dealing with that as a community."

A week later, Batts told a C-SPAN interviewer: "When I came to Baltimore, it was like going back in time. It's about black and white racism in that city. It's all the things you dealt with in the 1960s."

The problem with those statements is this: They make Baltimore sound like a Jim Crow theme park rather than what it is — a post-industrial, majority African-American, Mid-Atlantic port city still adjusting to the loss of its blue-collar, middle-class economy after five decades of white flight.

What's even more jarring is the suggestion that Baltimore is somehow unique — that, while the rest of the country has moved to higher ground, we're stuck in the fetid boondocks of racism.

This is complicated stuff, not given to sound bites.

So let me tell you what I think the commissioner was trying to say.

Batts spent all of his law enforcement career in California before taking the Baltimore job 21/2 years ago. That means he's still a newcomer. And his statements about Baltimore reflect the first impressions we get from a lot of newcomers. They go something like this:

Gee, there are a lot of black people here. And the black people seem to live in black neighborhoods, some of them in terrible shape, while the white people seem to live in white neighborhoods, some of them in amazing shape.

The public schools are the same way, segregated along racial lines. And Baltimore has a lot of private schools, especially on the north side; they look mainly white and mainly for the children of affluent white families, many from the suburbs.

It's remarkable that you can drive around the city and see a well-maintained, stable neighborhood with flowers in window boxes, then, two blocks later, find yourself in a block or two of decrepit, vacant rowhouses.

Such observations are common and generally accurate.

But they beg for perspective. So, in what seems like ritual, a Baltimore long-timer provides the newcomer with some:

Maryland was a slave state and a border state during the Civil War, and Baltimore had enough Southern sympathizers that Union troops occupied the city. Baltimore struggled for a long time with its Southern-Northern split personality.

An industrial port city, it was a magnet for immigrants and for migrants from rural America.

By 1950, Baltimore's population was about 950,000 and about 40 percent of all Marylanders lived here.

But you can chart the city's population slide almost exactly from 1954, the year of the Brown v. Board of Education ruling. White people fled. By 2010, only about 11 percent of Marylanders lived here; the city's population was 622,000 in 2013, and it was 63 percent black.

What I just described in a paragraph happened over five decades, and it was traumatic.

Baltimore also experienced the loss of good-paying, blue-collar jobs — many of them union jobs — as manufacturing declined and disappeared. For numerous reasons, including racist housing practices, African-Americans became the city's majority population. Poverty became concentrated in the city, too. Thousands of rowhouses were abandoned. The drug trade thrived. The war on drugs sent thousands of black men to prison.

The result is how we live now: A majority black city, still in recovery from an epoch of decline, ringed by majority white suburbs while city neighborhoods remain generally segregated along racial and class lines. We don't live side by side; white kids and black kids do not generally attend the same schools. Several generations have accepted this reality.

If that's what Batts is talking about, then he's right — we're still segregated in that way.

But not in all ways.

We work together, black and white, and shop together; if you go to the Inner Harbor on any given day — or many other city events — the crowds are integrated.

Of course, that's not real integration. That's just tasting it.

The Next Baltimore, the one still developing, could be quite different. People are sorting themselves out. A new generation of like-minded, progressive, professionals — black and white — has started to populate the city, far less hindered by the prejudices and fears that sent their parents or grandparents to the suburbs. This new generation, combined with increasing numbers of immigrants, could change the city for good.

You can call that wishful thinking. I call it an informed theory about my adopted hometown. I was once a newcomer to Baltimore. I've been here a long time now, and despite that, remain optimistic.

drodricks@baltsun.com

Dan Rodricks' column appears each Tuesday, Thursday and Sunday. He is the host of "Midday" on WYPR-FM.