The Census Bureau asks Americans about subjects as varied as race, age, annual income and even their source of home heating. But there is one glaring demographic omission: The census does not ask people about their sexual orientation. As a result, there has long been a shroud of uncertainty around the geography of gay and lesbian Americans.

A new analysis of Gallup survey data offers the most detailed estimates yet about where people who identify as gay, lesbian, bisexual or transgender live.

The Gallup analysis finds the largest concentrations in the West — and not just in the expected places like San Francisco and Portland, Ore. Among the nation’s 50 largest metropolitan areas, Denver and Salt Lake City are also in the top 10. How could Salt Lake be there, given its well-known social conservatism? It seems to be a kind of regional capital of gay life, attracting people from other parts of Utah and the Mormon West.

On the other hand, some of the East Coast places with famous gay neighborhoods, including in New York, Miami and Washington, have a smaller percentage of their population who identify as gay — roughly average for a big metropolitan area. The least gay urban areas are in the Midwest and South.

Significant as these differences are, the similarities are just as notable. Gay America, rather than being confined to a few places, spreads across every major region of the country. Nationwide, Gallup says, 3.6 percent of adults consider themselves gay, lesbian, bisexual or transgender. And even the parts of the country outside the 50 biggest metropolitan areas have a gay population (about 3 percent) not so different from some big metropolitan areas. It’s a reflection in part of increasing tolerance and of social connections made possible by the Internet.

Frank Newport, the editor in chief of Gallup, notes that the regional variation in sexual orientation and identity is much smaller than the variation in many other categories. The share of San Francisco’s population that’s gay is only two and a half times larger than the share outside major metro areas. The regional gaps in political attitudes, religion and ethnic makeup are often much wider.

“For a generation, they all remember the moment they walked through their first gay bar,” said Paul Boneberg, executive director of the G.L.B.T. Historical Society in San Francisco. “But now they come out for the first time online, and that changes, for some people, the need to leave.”

Historians often trace San Francisco’s role as a gay refuge in part to World War II, when the Navy discharged gay sailors, because of their sexuality, at Treasure Island in San Francisco Bay. Many stayed in the city, which already had a reputation as a welcoming place for refugees and free spirits. Other gay people, in search of a safe place, followed.

San Francisco was prominently featured when Life magazine published a 1964 article titled “Homosexuality in America.” Mr. Boneberg said, “If you’re somewhere else in America and you’re looking at Life magazine and you see pictures of people like you in San Francisco, you come.”

San Francisco became a hub for L.G.B.T. organizing and later for AIDS activism. The city had some of the country’s first openly gay politicians, including Harvey Milk, the San Francisco supervisor who was assassinated in 1978. He is credited with turning the Castro district of the 1970s into perhaps the country’s most visible gay neighborhood, a community and tourist destination that still hums under a rainbow flag.

Though San Francisco has less of a pull, as other parts of the country have become less hostile, the city is still a destination, including for transgender people. And while other big cities on the West Coast like Los Angeles, Seattle and Portland also have large L.G.B.T. populations, San Francisco continues to draw people, including from nearby San Jose, which has one of the smallest shares of L.G.B.T. people among the nation’s largest metropolitan areas.

Oregon exemplifies the political division on gay civil rights. The state and local governments have proposed or passed a striking number of anti-gay measures, yet the share of L.G.B.T. people in the liberal enclave of Portland and the surrounding metropolitan area is second only to San Francisco.

Part of the reason is that L.G.B.T. people in the rural, conservative part of the state and neighboring states have flocked to Portland, as have people priced out of the West Coast’s other big, gay-friendly cities. Many of them have fought for some of the country’s farthest-reaching gay rights policies, including the recent expansion of Medicaid coverage for treatment like hormone therapy and counseling for transgender people. Oregon’s new governor, Kate Brown, is the nation’s first openly bisexual governor, advocacy groups say. Still, Jeana Frazzini, executive director of Basic Rights Oregon, says Portland has work to do before it can feel like a haven for many transgender people and gay people of color.

One factor behind the data — in Portland and elsewhere — may be people’s willingness to tell a pollster that they’re gay, lesbian or bisexual. In addition to having a larger gay population, places like Portland may also have a larger share of their gay population who publicly identify as such.

It’s no accident that some of the country’s most educated metro areas have some of the largest gay populations. Gay and lesbian Americans are not substantially more educated than the rest of the population, according to Gary Gates, a co-author of “The Gay and Lesbian Atlas,” who has studied the Gallup data. But university campuses — and the spillover neighborhoods from them, where college graduates congregate — have long been more accepting.

Boston, a city famous for its colleges, is the only East Coast metropolitan area to crack the top eight of Gallup’s ranking. Austin, home of the University of Texas, joins New Orleans as the only Southern city to be so high on the list.

Besides education, a few other factors correlate with having bigger gay populations. Metro areas with a greater share of gay and lesbian residents tend to lean Democratic. They have more adults under the age of 45 and larger Latino and Asian-American populations. On the flip side, they have smaller African-American populations on average and fewer children. But the usual warning applies: Correlation does not always mean causation.

It might seem surprising at first that the city most associated with the Mormon Church — which believes that sex and marriage should occur between only a man and a woman — has the seventh-highest share of L.G.B.T. people, at 4.7 percent.

But another aspect of the Mormon culture — the importance of community and family — goes a long way toward explaining the pattern, people in Salt Lake City say.

Though many gay people who were raised Mormon (or L.D.S., an acronym for the Church of Jesus Christ of Latter-day Saints) describe feeling expelled from the community, a large number still choose to stay close to their families and culture. “If you grow up L.D.S. like I did, you still have these deep Mormon values that are embedded in your DNA,” said Troy Williams, executive director of Equality Utah, an L.G.B.T. advocacy group.

That connection has helped the gay community and the church find common ground. This month, Utah passed a law, with the support of the church, prohibiting discrimination based on sexual orientation or gender identity in employment and housing. Salt Lake City also appears to have attracted L.G.B.T. people from nearby conservative states — like Idaho, Montana and Wyoming — that don’t have cities that are as big or welcoming.

“There are certain cities that are going to pop up not because they’re a national draw,” Mr. Gates said, “but because they’re in areas where social acceptance is so vastly different in cities than in outlying areas that the cities become a regional draw.”

Many of the areas with the lowest percentages of L.G.B.T. people are in the South, and none have a lower share than Birmingham, where 2.6 percent of the population identifies as gay. Throughout the South, conservative Protestantism has shaped many people’s views. As recently as 1988, Southern Baptists passed an official resolution calling homosexuality “depraved.”

Birmingham, a hotbed of racial tension in the 1960s, is again at the center of a civil rights debate. This month, the Alabama Supreme Court demanded a stop to same-sex marriages, even though a federal judge had ruled that the ban was unconstitutional.

Still, the greater awareness and acceptance of L.G.B.T. people is gradually permeating Birmingham, too, said Glenda Elliot, a retired professor at the University of Alabama at Birmingham and a civil rights advocate. The city has a new center for gay youths, and a few gay bars.

“If it wasn’t an accident of birth that you were born in San Francisco and happened to be queer, I don’t think there’s the immediate pull that you must move there anymore,” said Douglas Ray, who teaches queer literature in Birmingham and edited “The Queer South,” an anthology.

The other large metro areas with relatively small gay populations include Houston, Memphis, Nashville and Raleigh, N.C. (the last being on the list perhaps because nearby Durham and Chapel Hill are not officially part of the metro area). A few areas in the Midwest or near it, including Pittsburgh, Milwaukee and Cincinnati, are also near the bottom of the list.

Gallup’s numbers are based on surveys of 374,325 adults across the 50 largest metropolitan areas, conducted between June 2012 and December 2014. The margin of sampling error for each of the metropolitan areas is no more than plus or minus 1 percentage point. The margin of error is one reason we’ve focused here on the areas at the top and bottom of the list. You shouldn’t make much of tiny differences between areas. (The estimates for all 50 areas appear below.)

Before this Gallup analysis, the most detailed portrait of gay demography was the Census Bureau estimates of same-sex couples, including an analysis by the Williams Institute at U.C.L.A. Those estimates and Gallup’s new data show broadly similar patterns: Salt Lake City ranks high on both, and San Jose ranks low, for instance. But couples are clearly an imperfect proxy for a total population, which makes these Gallup numbers the most detailed yet to be released.

Gallup previously released estimates for the country as a whole and for each state. The estimates are based on the survey question, “Do you, personally, identify as lesbian, gay, bisexual or transgender?”

As with any survey, the data comes with limitations. Respondents are asked to place themselves in a single category — L.G.B.T. or not — even though some people consider sexuality to be more of a spectrum. The data also does not distinguish between center cities and outlying areas. Manhattan most likely has a larger percentage of gay and lesbian residents than the New York region as a whole.

And the data is affected by the federal government’s definition of metropolitan areas. Earlier, we mentioned that Raleigh’s percentage is low in part because its area does not include Durham and Chapel Hill. Boston’s percentage may be higher because its metropolitan area is relatively small, with fewer outlying areas. On the whole, however, there is no clear relationship between a metropolitan area’s size and the share of its population that’s gay.

Below is a list of all 50 of the largest metro areas, along with Gallup’s estimate of the L.G.B.T. population in each:

San Francisco, 6.2 percent

Portland, Ore., 5.4

Austin, Tex., 5.3

New Orleans, 5.1

Seattle, 4.8

Boston, 4.8

Salt Lake City, 4.7

Los Angeles, 4.6

Denver, 4.6

Hartford, 4.6

Louisville, Ky., 4.5

Virginia Beach, 4.4

Providence, R.I., 4.4

Las Vegas, 4.3

Columbus, Ohio, 4.3

Jacksonville, Fla., 4.3

Miami, 4.2

Indianapolis, 4.2

Atlanta, 4.2

Orlando, Fla., 4.1

Tampa, Fla. 4.1

Phoenix, 4.1

New York, 4

San Antonio, 4

Washington, 4

Riverside, Calif., 4

Philadelphia, 3.9

Baltimore, 3.9

Buffalo, 3.9

Detroit, 3.9

Sacramento, 3.9

San Diego, 3.9

Charlotte, N.C., 3.8

Chicago, 3.8

Dallas, 3.8

Cleveland, 3.7

Kansas City (Mo. and Kan.), 3.6

Minneapolis-St. Paul, 3.6

St. Louis, 3.6

Oklahoma City, 3.5

Richmond, Va., 3.5

Nashville., 3.5

Milwaukee, 3.5

Houston, 3.3

San Jose, Calif., 3.2

Raleigh, N.C., 3.2

Cincinnati, 3.2

Memphis, 3.1

Pittsburgh, 3

Birmingham, Ala., 2.6