An Indiana law that could make it easier for religious conservatives to refuse service to gay couples touched off storms of protest on Friday from the worlds of arts, business and college athletics and opened an emotional new debate in the emerging campaign for president.

Passage of the Republican-led measure, described by advocates as protecting basic religious freedom, drew fierce denunciations from technology companies, threats of a boycott from actors and expressions of dismay from the N.C.A.A., which is based in Indianapolis and will hold its men’s basketball Final Four games there beginning next weekend.

“We are especially concerned about how this legislation could affect our student-athletes and employees,” said the president of the N.C.A.A., Mark Emmert.

By Friday afternoon, influential national leaders, including Hillary Rodham Clinton and Tim Cook, the chief executive of Apple, had weighed in against the law, calling it a disappointing invitation to discriminate.

But Gov. Mike Pence of Indiana, a Republican who has not ruled out a 2016 presidential run, defended the law as an overdue protection when “many feel their religious liberty is under attack by government action.”

A similar furor was building in Arkansas on Friday as the State Senate adopted a version of the bill that has inflamed the state’s corporate giants, like Walmart, and high-tech companies the state is wooing.

The laws are modeled on a federal religious protection measure adopted in 1993 and subsequently passed by 20 states. But the latest push, and the vehement responses it has unleashed, reflect new passions surrounding the spread of same-sex marriage, with many conservatives invoking “religious freedom” as their last line of defense.

Indiana’s ban on same-sex marriage was overturned by federal courts last year, giving new energy to the campaign for the religious protection bill signed Thursday by Mr. Pence.

Advocates of equal rights for gays said the laws pose a threat of abetting discrimination, especially from business owners who object to participating in same-sex weddings.

“The possible discriminatory effects are real,” said Sarah Warbelow, legal director of the Human Rights Campaign, a gay rights group. At the same time, she said, the public understands that the debate over these laws is really about “tolerance towards L.G.B.T. people.”

The Indiana law opens the door for individuals or companies to refuse actions that impose a “substantial burden” on their religious beliefs. If that refusal is challenged in court, a judge must balance the religious burden with the state’s “compelling interest” in preventing discrimination, according to the law.

Eric Miller, who lobbied for Indiana’s new law as executive director of the group Advance America, said it could help Christian bakers, florists and photographers avoid punishment for “refusing to participate in a homosexual marriage,” protect Christian businesses that refuse “to allow a man to use the women’s restroom,” and insulate churches that refuse to allow their premises to be used for same-sex weddings.

Some legal experts say the potential reach of the Indiana law, and many like it, has been exaggerated by opponents.

“The hysteria over this law is so unjustified,” said Douglas Laycock, a law professor at the University of Virginia and a prominent defender of so-called religious freedom laws.

“It’s not about discriminating against gays in general or across the board,” he said of the Indiana law. “It’s about not being involved in a ceremony that you believe is inherently religious.”

Mr. Pence, at a news conference at the Capitol after he signed the bill, adamantly denied that it is intended to permit discrimination. “If I thought it legalized discrimination in any way,” he said, “I would have vetoed it.”

As legal experts debated the law’s impact, its passage provoked an unusually swift and broad outcry.

Mrs. Clinton, a likely Democratic candidate for president, denounced it in a message on Twitter on Thursday night. “Sad this new Indiana law can happen in America today,” she said, adding that Americans should not discriminate against people because of “who they love.”

The field of likely Republican candidates, eager to appeal to conservatives but sensitive to the worries of business leaders, seemed unsure of how far to go in embracing the law. Gov. Bobby Jindal of Louisiana forcefully endorsed it, but former Gov. Jeb Bush and Senator Marco Rubio, both of Florida, left it to aides to point to previous support for such protections of religious freedom. Last week, Mr. Bush said he believed that religious people “have a right” to opt out of providing services for a gay wedding.

Opponents of the law seized on Indiana’s role as a corporate headquarters, urging companies to suspend plans for conferences and conventions in the state.

Marc Benioff, the chief executive of Salesforce.com, a technology company with a major presence in Indiana, announced that he would cancel all company events in the state.

“Today we are canceling all programs that require our customers/employees to travel to Indiana to face discrimination,” Mr. Benioff wrote in a Twitter post.

The uproar was pronounced in the technology industry, which has emerged as a champion of gay rights.

Jeremy Stoppelman, the founder of Yelp, which publishes online reviews of businesses, issued a warning to states like Indiana that “it is unconscionable to imagine that Yelp would create, maintain, or expand a significant business presence in any state that encouraged discrimination.”

Several business groups tried to use their economic clout in the state to pressure leaders to roll back the law. Gen Con, a convention for game enthusiasts that draws nearly 60,000 people a year to Indianapolis, threatened to relocate its event out of state.

Mr. Pence found little relief from Indiana business leaders. The chief executive of the Indiana Chamber of Commerce, Kevin Brinegar, assailed the law as “entirely unnecessary.”

“The reactions to it are not unexpected or unpredicted,” Mr. Brinegar said. “Passing the law was always going to bring the state unwanted attention.”

As word of the law spread Friday, it stirred angry reactions well beyond the corporate sphere. The legendary Broadway actress Audra McDonald threatened to pull out of a coming Indiana performance, before deciding instead to donate proceeds to gay rights groups. The actor George Takei called himself “outraged” and suggested a boycott of the state.

Arn Tellem, a prominent sports agent whose clients include the basketball players Russell Westbrook, Anthony Davis and Jason Collins, the first openly gay N.B.A. player, offered a stinging assessment.

“The measure codifies hatred under the smoke screen of freedom and jeopardizes all that has been recently accomplished,” Mr. Tellem said in an email. He added, “It legalizes discrimination against L.G.B.T. individuals and will cause significant harm to many people.”

Mr. Tellem urged the Indiana Pacers and the entire N.C.A.A. “to not only condemn this blatantly unconstitutional legislation, but to take forceful action against it by re-evaluating their short- and long-term plans in the state.”

Mr. Pence, sounding at times defensive and frustrated at his news conference, said the law was being misunderstood, and he vowed to call angry business leaders to explain his rationale.

“We are going to reach out,” he said, when asked about the fury. “I am concerned about that.”