When the Supreme Court this month hears arguments over whether gay and lesbian couples can marry in all 50 states, one audience will be paying especially close attention: American businesses, which say they spend more than $1 billion a year navigating the nation's complex patchwork of same-sex marriage laws.

In a friend-of-the-court brief filed last month, 379 of the country's best-known businesses — from Apple and Amazon.com to General Electric and Walt Disney — urged justices to strike down same-sex marriage bans and provide every member of their workforce "equal dignity."

The brief marked corporate America's latest risky reversal from its traditional playbook of dodging thorny national debates, and follows a series of high-profile business protests of controversial "religious freedom" laws in Indiana and Arkansas.

But companies say they increasingly see marriage equality not just as one of this generation's defining social-rights battles, but as a critical business issue, the conclusion of which could dramatically change how they recruit, manage and retain nationwide employees.

"Five years ago, you wouldn't have seen nearly as many companies feeling comfortable — and not just comfortable, but feeling it's imperative — to speak up about this issue," said Todd Sears, a former investment banker who founded Out Leadership, a business advisory firm.

"The economics are definitely a driver, but it's also about that personal connection," Sears added. "When you create second-class citizens, it forces companies to create second-class employees."

More than 70 percent of Americans live in one of the 37 states, plus Washington D.C., where same-sex marriages are legal. But in states where those unions are banned, corporate offices say they struggle to manage shifting rules on tax policies, employee benefits and other administrative intricacies governing same-sex partners and their families.

They also cut down on companies' internal options, limiting how they can redeploy workers, open new offices or pursue other strategies. Sears offered one example: "Say your star employee's married to his partner in New York and you want to move him to Georgia, where not only is it legal to fire him if he's gay and his marriage isn't recognized by the state, but you're potentially putting him in a hostile situation."

Adding to the frustration, the companies argued, was that inconsistent policies kept them on the wrong side of their customers' feelings on this issue. About 60 percent of Americans now support same-sex marriage, recent polls show.

There's a "business case for equality. At the end of the day, companies are selfish actors. They want to increase value for their shareholders," said Human Rights Campaign spokesman Fred Sainz.

Since 2013, when the Supreme Court struck down the federal Defense of Marriage Act, the number of states that allow same-sex marriage has grown from nine to 37, forcing businesses to pay for an incredible amount of internal rewriting. Administrative, tax and insurance costs due to differing marriage laws, the brief argues, are expected to cost the private sector $1.3 billion this year alone.

But the burden also extended beyond companies' budgets, hurting their ability to both find new employees and persuade them to move to offices in states where their rights to marry or share in their partners' benefits are limited, the companies argue. The "fractured legal landscape," the brief says, "breeds unnecessary confusion, tension and diminished employee morale."

"If I walk down the street and I don't see visible signs of diversity — young or gay or people of color," said Richard Florida, an urban theorist and author, "then I start to worry this won't be a place for me."

About 90 percent of Fortune 500 companies now offer protection against discrimination for lesbian, gay and bisexual employees, up from about 60 percent in 2002, Human Rights Campaign data show. About 66 percent of those companies also extend medical benefits to same-sex spouses and partners.

The corporate push for equality measures has also played out in recent weeks in states like Indiana and Arkansas, where businesses joined supporters in protesting laws that would have allowed companies to cite their religious beliefs in turning away service to gays and lesbians.

In Indiana, the roundly criticized law drew fire from businesses operating far outside their borders, with companies canceling events, releasing statements or withdrawing business from the state in protest. Salesforce, a cloud-computing firm, offered to pay $50,000 to relocate an Indiana worker uncomfortable living under the new law.

Some of the corporate world's most visible leaders have been at the forefront of the gay-rights movement. In October, Apple chief executive Tim Cook, who has fought workplace discrimination bills and campaigned for greater gay rights, became the only leader of one of the country's 500 largest public companies to announce he is "proud to be gay."

And in 2013, Starbucks chief executive Howard Schultz told an investor who complained about the coffee giant's support of a same-sex marriage bill that he was free to sell his shares and invest somewhere else.

At an Out Leadership summit last year at New York University, Goldman Sachs chief executive Lloyd Blankfein joked that he was fine with competitors who turned away gay and lesbian employees, because it gave him a chance to recruit new talent.

"I don't see the downside, he said, "of taking a position that's so clearly on the right side of progress and history."

---

Post reporter Sandhya Somashekhar contributed to this report.