Last year Indiana chose “Honest to Goodness Indiana” as its new tourism slogan. Not everyone was charmed. Some critics said they’d have preferred something more cosmopolitan. Although it doesn’t seem likely they’d be happier with the runners-up, one of which was “Seasoned Just Right.”

Now, however, the slogan makes a kind of sense, especially if you throw in a little punctuation:

Honest to goodness, Indiana! Really, what were you thinking?

Last week, you may remember, Gov. Mike Pence of Indiana signed a “religious freedom” law that was widely decried around the country as an attack on gay civil rights. One state business pulled the plug on a planned expansion in protest. Conferences were canceled, events called off. The N.C.A.A., which is based in Indianapolis, was looking extremely uneasy. Other states and cities began imposing Indiana travel bans.

This is the exact same thing that happened in Arizona a year before, except that the governor there responded to the outcry by vetoing the law. Nevertheless, the Republicans who run Indiana claimed they were shocked, shocked by these totally unexpected developments.

“We have suffered under this avalanche for the last several days of condemnation, and it’s completely baseless,” Pence complained to George Stephanopoulos on ABC on Sunday. It was very possibly one of the worst appearances by a governor in television history. The best Pence could do was to babble desperately that “Hoosiers don’t believe in discrimination.”

Stephanopoulos: “Do you think it should be legal in the state of Indiana to discriminate against gays or lesbians?”

Pence: “George ...” He never could quite bring himself to just say no.

Pence and the Legislature’s majority party thought they were on safe ground because their law really did look like a federal freedom of religion act passed during the Clinton administration. Except for the part about giving businesses the right to refuse service on religious grounds. Like a bakery declining to provide a wedding cake for a gay couple. Which was absolutely not in any way the example the State Legislature had in mind. No, sirree.

The federal Religious Freedom Restoration Act is known as RFRA despite the unfortunate resemblance to the sound of a hoarse Labrador retriever. It was passed in 1993 in response to the problems of Native Americans in Oregon, who smoked peyote in a religious ceremony and were then fired from their jobs and denied unemployment benefits.

The Indiana law was passed at a time of major frustration by the state’s social conservatives over a court ruling that made it impossible to pass a constitutional amendment against same-sex marriage.

All those who believe the Indiana Legislature was acting out of concern over the right of Native Americans to use peyote raise their hands.

“The politics as it went through the statehouse was clear,” said Deborah Widiss, a law professor at Indiana University.

Pence did have another explanation for why Indiana needed a religious freedom law right now. He said he wanted to expand the Supreme Court’s Hobby Lobby decision, which held that corporations have the right to refuse to cover the cost of contraception under the Affordable Care Act. “With the Supreme Court’s ruling, the need for a RFRA at the state level became more important, as the federal law does not apply to states,” the governor wrote in The Wall Street Journal.

Think about that for a minute. Indiana passes a law that is widely regarded as a sop to the state’s social conservatives for their inability to ban same-sex marriage. The Republican establishment expresses dismay at this interpretation, and insists that its only intention was to deprive female residents of the right to get birth control.

Yippee.

We seem to have a pattern here. Last year in Arizona when the governor vetoed the anti-gay bill, the Legislature vented its frustration by passing a new anti-abortion law. The gay rights movement is winning, big time. But governments are still insisting on their authority to mess with the sex lives of heterosexual women.

Right now, Indiana is in a mess, and residents are worried about the loss of jobs and investment because of a meaningless and spiteful piece of legislation. They should feel free to blame their governor. Mike Pence was supposed to be one of those evenheaded fiscal conservatives that moderate Republicans point to as a potential presidential candidate. But he didn’t have the foresight to see how badly this would turn out, or the spine to push back. In Arkansas, Gov. Asa Hutchinson is watching him flounder and making a last-minute attempt to beat back a similar bill there.

Pence and the Legislature want to appease the business community by amending the law. They’re currently trying to find a way to accomplish that mission while not upsetting the social conservatives they made so happy just last month.

Good luck with that one, guys.