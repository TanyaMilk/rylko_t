WASHINGTON —  The Justice Department put the nation’s prisons and jails on notice on Friday that it regarded blanket policies prohibiting new hormone treatment for transgender inmates to be unconstitutional.

In court documents, the Justice Department backed a lawsuit brought by a prison inmate who says the state of Georgia illegally cut off the hormone treatment that she had been taking for 17 years.

It is believed to be the first time that the Justice Department has weighed in on the question of whether hormone therapy for transgender inmates is necessary medical care that states are required to provide. Though the Justice Department did not explicitly say the prison should provide the hormones, it argued that Georgia’s treatment policy and those like it were unconstitutional.

The guidelines, known as “freeze frame” policies, allow inmates to continue any treatment they were receiving before their arrest but prohibit them from expanding or starting new treatments. The Justice Department argued that the Constitution requires prison officials to make treatment decisions based on independent health assessments.

The prisoner, Ashley Diamond, 36, sued the Georgia Department of Corrections in February, saying that prison officials had terminated her female hormone therapy when she was imprisoned in 2012. She is being held at the Georgia State Prison, a men’s facility.

Though Ms. Diamond had been undergoing hormone treatment for many years before her arrest, prison officials did not identify her as transgender on prison intake forms, making her ineligible for continued treatment, according to her lawsuit.

Without the hormones, Ms. Diamond’s body “has been violently transformed,” her lawyers wrote. “Ms. Diamond has lost breast tissue and her female secondary sex characteristics have diminished,” they said. She has attempted suicide and self-castration multiple times, her lawyers said.

Gwendolyn Hogan, a spokeswoman for the Georgia Department of Corrections, said that the department could not comment on active lawsuits.

Prison medical records filed in court show that mental health professionals found Ms. Diamond had gender dysphoria, a recognized condition formerly known as gender identity disorder.

“Prison officials have the obligation to assess and treat gender dysphoria just as they would any other medical or mental health condition,” said Vanita Gupta, the Justice Department’s top civil rights prosecutor. “Freeze-frame policies can have serious consequences to the health and well-being of transgender prisoners, who are among the most vulnerable populations incarcerated in our nation’s prisons and jails.”

The Justice Department argued that denying Ms. Diamond an individual assessment and treatment plan violates the Eighth Amendment prohibition on cruel and unusual punishment. Under the Constitution, prisoners do not have a right to the medical care of their choosing, but must be provided adequate treatment for serious medical needs.

“Transgender inmates like Ashley have a right to proper medical care,” said Chinyere Ezie, a Southern Poverty Law Center lawyer who represents Ms. Diamond.

In 2005, Wisconsin passed an outright ban on hormone treatment for transgender inmates, regardless of whether the treatment was ongoing when they were arrested. Civil rights groups challenged the law on constitutional grounds and a federal appeals court overturned the law in 2011. Freeze frame policies are more common; civil rights groups say they exist in state and local jails around the country.

Until recently, the federal Bureau of Prisons also had a freeze frame policy. In 2011, the Obama administration settled a lawsuit over its policy and changed its guidelines. Treatment plans for federal inmates are now reviewed regularly and “hormone therapy may be a consideration,” regardless of whether the inmates received the treatment before being arrested.

In February, the Defense Department approved hormone therapy for Chelsea Manning, the former intelligence analyst convicted of providing classified documents to WikiLeaks. Ms. Manning, formerly known as Bradley Manning, was sentenced in 2013 to 35 years in prison. The day after the sentencing, she announced that she was a woman. A military court has recognized her as a woman.

With his action on Friday, Attorney General Eric H. Holder Jr., the nation’s first black attorney general, asserted that the campaign for the rights of gays, lesbians and transgendered people was a continuation of the movement that won equal rights for blacks in the civil rights era. He has been one of the Obama administration’s most outspoken voices on the issue of same-sex marriage, and he drew criticism from conservatives last year when he advised state attorneys general that they were not constitutionally obligated to defend bans on same-sex marriage.

In recent years, the Obama administration brought civil rights cases against school districts based on where the Justice Department said transgender students were being harassed, or discriminated against. In one instance, school officials in Arcadia, Calif., settled with the Justice Department in a case over whether a transgender boy who was born a girl should be allowed to use boys locker rooms and restrooms. The school district agreed to change its policies and treat him like other male students.

This week, the Justice Department sued Southeastern Oklahoma State University, accusing the school of discriminating against a transgender employee. Federal civil rights law does not explicitly ban discrimination against transgender people, but Mr. Holder announced in December that the Justice Department considered such bias to be prohibited under the same civil rights law that outlaws sex discrimination.

Mr. Holder also launched a program last year that trains police officers to better understand the transgender community. Prosecutors say transgender people have been discouraged from reporting crimes because of bad interactions with the police.