Three months into what allies once confidently described as a “shock and awe” drive to overcome his rivals and dominate the Republican presidential field, Jeb Bush’s early campaigning looks like the juggernaut that wasn’t.

He is grappling with the Republican Party’s prickly and demanding ideological blocs, particularly evangelical leaders and pro-Israel hawks. He is struggling to win over grass-roots activists in Iowa and New Hampshire, states he has visited only a handful of times. And Mr. Bush’s undisputed advantage — the millions of dollars streaming rapidly into his political organization — may not be enough to knock out other contenders.

For all the Republican “bundlers” who have signed up to raise money for Mr. Bush, others remain uncommitted or are hedging their bets by aiding more than one candidate. Some are privately chafing at what they view as the Bush camp’s presumption of their loyalty.

Other wealthy donors, mindful of their power to reshape the Republican race with “super PAC” donations, have been more direct: The casino magnate Sheldon Adelson recently made what two people briefed on it described as an “animated” call to one of Mr. Bush’s top supporters after former Secretary of State James A. Baker III, a Bush adviser, criticized Prime Minister Benjamin Netanyahu of Israel in a speech in March.

It is a far cry, party officials, activists and donors said, from the early success of George W. Bush, Mr. Bush’s brother, in securing the 2000 Republican presidential nomination.

For the Bush family, inevitability is not what it used to be. “There hasn’t been a coalescing around him like there was for his brother in 1998 and 1999,” said Ed Martin, who led the Missouri Republican Party until February and is now president of the Eagle Forum, the conservative group founded by Phyllis Schlafly. “I just don’t have a sense among big donors and Republican leaders that this is Jeb’s to lose.”

Aides have assured worried supporters that Mr. Bush, a former Florida governor, will spend more time on the stump, honing his message, cultivating grass-roots enthusiasm and winning over local leaders and ordinary Republican voters.

Fred Zeidman, a prominent bundler, said Mr. Bush had attracted an enormous number of supporters outside the family network. “You’re not seeing either of the old Bush teams running this campaign,” he said, “and I think it’s proof positive that people feel very strongly.”

But even among business leaders who see Mr. Bush as an attractive candidate, some have wondered about the series of major policy speeches his team promised, which he has been slow to deliver.

“The smart money is taking it slower and waiting to see who runs and how these candidates develop a platform,” said Scott Reed, the top political adviser to the U.S. Chamber of Commerce. “Raising money is an important part of the game. But it’s not the whole game.”

The first primary votes are still nearly 10 months away, giving Mr. Bush’s team plenty of time to adjust, and Republicans show no sign of unifying behind another candidate. Mr. Bush’s advisers say he never believed he would be able to clear the field. And they say his refusal to bend to the demands of people whose views he does not share demonstrates that he is running a different type of race.

“As Governor Bush considers the possibility of a run, he’s working hard to earn the support of people throughout the party and deliver a positive message about how conservative reforms can restore opportunity and prosperity for more Americans,” Kristy Campbell, a spokeswoman for Mr. Bush, said in an email.

In January, Mr. Bush’s allies started using the phrase “shock and awe” to describe his plan to lock up as many of the party’s major donors and policy experts as possible. The intent was to make him the sole credible contender for the blessing of the Republican establishment. Mr. Bush was successful in driving the 2012 nominee, Mitt Romney, from trying another campaign. And few doubt that he leads in fund-raising, though his organization will not have to disclose its finances until July.

But while Mr. Bush continues to position himself as Republicans’ best hope in the general election, recent polls show him performing no better against Hillary Rodham Clinton than Gov. Scott Walker of Wisconsin or Senator Rand Paul of Kentucky. And he is well behind Mr. Walker in Iowa, which has made a habit of rejecting candidates seen as their party’s inevitable nominees.

“If he’s going to run for the presidency because his father and his brother were president, is that why we should support him?” asked Colleen Platt, a City Council member in University Park, Iowa.

There is also lingering suspicion of Mr. Bush among the Tea Party grass roots, which arose while Mr. Bush was focused on business and philanthropic interests.

Ovide Lamontagne, the Tea Party candidate in the 2010 Republican primary for the Senate in New Hampshire, suggested that Mr. Bush had not reached out sufficiently to conservatives there. “A good number of candidates” have approached him, he said, but Mr. Bush has not.

Many big fund-raisers continue to shop around. Paul Singer, one of Wall Street’s most sought-after Republican donors, is holding a series of dinners to introduce his associates to various candidates.

And John A. Catsimatidis, a New York Republican, contributed $50,000 to a super PAC backing Mr. Bush and recently gave him a ride to Florida on his plane — but has also contributed to Mr. Walker’s political organization.

Several donors said it was too early to line up behind one candidate, or hinted that Mr. Bush should not take their support for granted.

“I don’t subscribe to ‘shock and awe,’ ” said John Rakolta Jr., a Michigan construction executive and former top fund-raiser for Mr. Romney. “I don’t think it’s a constructive strategy.”

Mr. Bush’s intense focus on fund-raising has left him to contend with a donor class far more restive than in his brother’s day, before the Supreme Court’s Citizens United decision and other court rulings amplified the power of wealthy contributors.

For example, Mr. Bush must navigate between the party base, which remains strongly opposed to same-sex marriage, and elite donors who have tried to steer the party to support it or leave the issue aside altogether.

In California last week, Mr. Bush told donors that he approved of a decision by Gov. Mike Pence of Indiana to seek changes to a new law billed as protecting religious freedom — just two days after Mr. Bush had firmly defended it on a conservative radio show.

Jewish donors also remain angry at Mr. Bush for a March 23 speech that Mr. Baker, who was secretary of state under Mr. Bush’s father, delivered to J Street, a liberal pro-Israel group. Mr. Adelson, perhaps the single largest Republican donor, quickly complained to Mel Sembler, a Florida developer who has long supported Mr. Bush. And Mr. Bush was pressed again about Mr. Baker’s speech at another California event last week.

In defending himself, Mr. Bush, who has described himself as “my own man” on foreign policy, pointed out that his brother had a strong record of support for Israel, one attendee said.

Mr. Sembler declined to discuss his call with Mr. Adelson, but maintained that most of the party’s big donors were moving to Mr. Bush. He acknowledged, though, that “a few” were saying they would “hold off and see who else is really going to get in.”

Tim Pawlenty, the former Minnesota governor and 2012 Republican presidential candidate, suggested that Mr. Bush’s candidacy was like a car built to endure the occasional speed bump. He predicted that Mr. Bush would begin to speak more loudly and clearly, and to define himself less by his surname.

“He’s going to have to stake out his claim here,” Mr. Pawlenty said. “And the sooner the better.”