BOULDER, Colo. —Another day, another town. Ryan T. Anderson, the conservative movement’s fresh-faced, millennial, Ivy League-educated spokesman against same-sex marriage, has another busy schedule.

There is an interview with conservative talk radio, a debate with a liberal professor at the University of Colorado’s law school and, after that, a lecture to Catholic students eager to hear Anderson’s view that the Constitution does not require that marriage be “redefined” to include same-sex couples.

The Supreme Court will soon be deciding just that question. And Anderson, a 33-year-old scholar at the Heritage Foundation, has emerged as a leading voice for those who resent being labeled hopelessly old-fashioned — or, worse, bigoted — for believing that marriage should be only between a man and a woman.

“Gays and lesbians undoubtedly have been discriminated against,” Anderson says. “But marriage is not part of that discrimination.”

 [A surprising poll on same-sex marriage] 

Anderson says he has no illusions that his arguments will turn the tide, at least for now, but that he feels it is important to “reassure” those who agree with him and try to make others think.

He has become a circuit rider with an iPad, filled with philosophy and (disputed) social science. Anderson says he is happy if Americans at least consider his message: that government’s interest in regulating marriage is to protect the offspring that come only from the male-female union, not to validate the desires of adults.

Justice Samuel A. Alito Jr. cited his work twice in his dissent from the court’s opinion in United States v. Windsor, which struck down part of the federal Defense of Marriage Act. Anderson is becoming a prominent face of the opposition in news media appearances.

His appeal in part owes something to counter-programming. A Princeton graduate with a doctorate in economic policy from Notre Dame, his views are at odds with other elite academics with whom he has so much in common. They are the opposite of those in his demographic. A devout Catholic, he nonetheless believes it a losing argument to oppose the legality of same-sex marriage on religious or moral grounds.

Also in his favor: He’s telegenic, an enthusiastic debater, and he can talk for hours.

“He’s a force of nature,” says Robert P. George, the prominent Princeton conservative thinker who hired Anderson as a research assistant and then discovered that “it was hard to know who was the professor and who was the assistant.” Along with Sherif Girgis, they wrote the book “What is Marriage? Man and Woman: A Defense,” which was cited by Alito.

Opponents have kind words as well — at least for Anderson’s presentation.

“He’s brought a level of sophistication and professionalism to their communications,” says Fred Sainz, vice president for communications at the pro-gay rights Human Rights Campaign. “He’s a smart operative and a good hire for Heritage — but at the end of the day, he’s on the wrong side of history.”

Other opponents criticize Anderson for muddy logic and say he cherry-picks social science to his advantage. They say his articulate arguments boil down to no more than circular reasoning: Marriage should not be extended to same-sex couples because his definition of marriage is open only to a man and a woman.

Anderson contends that he does not need to prove that his view of marriage is the correct one, only that the Constitution permits states to endorse it. “We’re having a national conversation about this, and that shouldn’t be short-circuited by the Supreme Court,” he says.

Before his Boulder appearances, he warms up with the conservative talk show host. He has a version of his argument, with varying degrees of specificity, for radio, television and print.

“We argue that marriage really exists to unite a man and a woman as husband and wife to then be mother and father to any children that that union creates,” Anderson says to the voice on the other end of the line.

“This is based on anthropological truths that men and women are distinct and complementary. It’s based on a biological fact that reproduction requires both a man and a woman. It’s based on a social reality that children deserve a mom and a dad.”

He barely needs a breath. “Our argument is that this is what gets the government in the marriage business,” he says. “It’s not because the state cares about consenting adult romance.”

You may have seen or heard Ryan Anderson: He has been coddled by Sean Hannity on Fox News Channel and literally talked down to by former CNN host Piers Morgan and financial guru Suze Orman. A popular YouTube video shows Morgan and Orman sitting on a stage, demanding to know why Orman should not be allowed to marry her partner while Anderson sits below in a chair in the audience.

His fans praised a recent appearance on MSNBC in which Anderson so frustrated liberal commentator Ed Schultz that the host pulled the plug.

“Cut his mike,” Schultz told his producers. “Cut his mike off. . . . I gave him a chance. We’ll bring him back if he wants to be courteous.”

Discourteous is not a description usually applied to Anderson. “He’s a smart, likable, very well-educated young person: an ideal spokesperson for the opposition to gay marriage,” Harvard University law professor Michael Klarman, who recently sparred with Anderson at a law school event, writes in an e-mail.

But Klarman, who wrote the book “From the Closet to the Altar: Courts, Backlash, and the Struggle for Same-Sex Marriage,” adds, “It’s almost inconceivable to me that he persuaded anyone in the audience by his arguments.”

Anderson says he has been taking unexpected positions all his life, as an antiabortion activist in college and as a conservative at the liberal Quaker Friends School in Baltimore, where he grew up.

He was the fourth of five boys, and his parents paid for all to get the rigorous academic background Friends School provided. But they told their sons that college would be on them.

Anderson paid for the part of his Princeton schooling that scholarships did not cover with the proceeds from a lawn business — Cutting Edge, he named it — started when he was an adolescent. His undergraduate degree is not in history or political science or philosophy, but in music. A percussionist, he keeps in his Washington apartment a marimba, a vibraphone, a hammered dulcimer and an electronic drum set.

“I always thought Ryan was going to be a musician or a composer or an entrepreneur,” says his oldest brother, Christian, a professor at California Polytechnic State University.

In a family filled with overachievers, Christian Anderson says, Ryan had the “most energy and intelligence.” That said, the two disagree on the issue that brought the younger brother to prominence.

“He’s a political philosopher” for whom principles are important, Christian Anderson says. “But there are principles, and then there is justice.”

Besides all of their advanced degrees, the Anderson brothers share something else: Only one has married.

“Working on it,” Ryan Anderson says. “Hopefully in the near future.”

It’s not so unusual for millennials with advanced degrees to delay marriage, he says.

“It’s always a little weird when people say: ‘You’re the marriage scholar. Why aren’t you married?’

“Partly, you don’t want to marry the wrong person,” he says, “if you believe, like I do, that marriage is until death do you part.”

The debate in the Colorado law school’s mock courtroom is on, and Anderson is rolling through his talking points.

“I don’t have to argue that my view about marriage is the truth,” he tells the sparse crowd. “My argument is simply that my definition of marriage is allowed by the Constitution” and thus states may limit it to heterosexuals.

He talks about marriage reflecting the state’s interest in keeping a father involved after a child is born. He considers the calamities that he says were caused by no-fault divorce.

He rejects the notion that keeping same-sex couples from marrying can be compared with laws that forbade interracial marriage.

“Every great thinker who has ever written about marriage, you never see a discussion of race,” he says. “Whether it’s Plato, Aristotle or Cicero, whether it’s the Jews, Christians and Muslims, whether it’s Augustine, Aquinas, Luther, Calvin, John Locke, Immanuel Kant, Muhammad, Gandhi — none of them talk about skin color; each and every one of them talk about sexual complementarity.”

He concludes: “This is the question I will close with: If you do assume marriage equality of the same-sex couple, on what basis do you deny marriage equality for the same-sex throuple?”

Heads turn. People whisper. No one seems sure what a throuple is.

He explains, not for the first or last time that day, that a throuple is three same-sex people who might want to marry. The New York Post reported on one in Indonesia, he says. Likewise, he talks about the “wed-lease,” an idea he say was “floated” by The Washington Post. He later clarifies that it was an op-ed from a Florida lawyer who proposed, instead of “wedlock” meant to last forever, renewable five-year marriage contracts.

Neither idea seems to have taken off, Anderson acknowledges, but both are staples in his warnings of the disasters that await if marriage is unmoored from its traditional role of keeping mothers and fathers together to raise their children.

Says his debate opponent, law professor Melissa Hart, “I don’t think allowing gay people to marry is going to cause more fathers to leave.” She allows that challenges about polygamy bans might be inevitable, but that government probably would be able to raise good reasons it should be limited to couples.

Anderson counters that government sanctioning of same-sex marriage is an official abandonment of the recognition that children do better with a mother and a father. There is no such thing as “parenting,” Anderson says, but “mothering and fathering.”

In response to a question, Anderson says that same-sex relationships are less stable than ­opposite-sex relationships. A ­female-female relationship is the most short-lived, he says, “not because it’s a lesbian relationship,” but because it involves two women, who are more likely to leave when their emotional needs are not met.

Male-male relationships, he says, tend to be less stable, “not because it’s a gay relationship,” but because men are more sexually permissive. “That’s where you tend to get the concept of ‘monogamish’ — a two-person relationship but sexually open.”

Hart seems taken aback. “Wow,” she says. “There was a lot there.”

“I did two things for my children my husband could not do: bearing them and I nursed them.” She says Anderson diminishes the roles that loving parents of either sex can provide for their children. His generalizations about men and women “are terrifying to me as an employment discrimination scholar,” Hart says.

Anderson replies that he is merely describing what he believes social science shows. “All the more reason not to rush into this one way or the other at the constitutional level.”

“When we’re old and gray, I will either be proven right or wrong,” he says. “We’ll be able to look back and say what Ryan Anderson said was prophetic or he was an idiot. But if we constitutionalize it, we stop the debate now.

“I’m inclined not to rush to a conclusion.”

But that is a distinct possibility when the Supreme Court considers Obergefell v. Hodges, which asks whether same-sex couples have a constitutional right to marry that cannot be prohibited by states.

 [How Jim Obergefell became the face of the same-sex marriage case] 

The tides are against Anderson. Unlike when the Supreme Court took up the issue of same-sex marriage in March 2013, when such unions were legal in nine states and the District of Columbia, nearly 3 in 4 Americans now live in states where same-sex couples are free to marry.

Lower courts have overwhelmingly read the 5-to-4 decision in Windsor to mean that state prohibitions must fall. Pollsters and social scientists say they have never seen any social movement surge in public support the way same-sex marriage has.

If his side is to lose, Anderson says, perhaps Obergefell will become the next Roe v. Wade. That Supreme Court decision was supposed to settle the issue of abortion, Anderson notes. Instead, state laws restricting abortion are on the rise, and “my generation is more pro-life than my parents’ generation.”

The Human Rights Campaign’s Sainz says just the opposite is likely to happen, as more Americans see their friends and neighbors marry.

On Anderson’s Facebook page and Twitter feed, same-sex marriage supporters regularly tell Anderson that it is easy for him to advocate a wait-and-see approach: It is not his rights that are being denied or delayed or suggested for referendum.

He seems almost surprised at the reaction he provokes.

“On the marriage issue, they don’t think you’re just wrong, they think you’re evil,” he says. “And that your views are bigoted. I count it as a success if I can at least get someone to say, ‘I disagree with you, but I don’t think you’re crazy or full of animus. I think you’re wrong, but I understand why you believe what you believe.’ ”