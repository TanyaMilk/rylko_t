When Gov. Mike Pence signed the state's Religious Freedom Restoration Act last week, he probably didn't expect Indiana to become the center of a national firestorm. But that's what happened. Groups and companies that fear mistreatment of gays and lesbians roundly denounced the measure and threatened to take their business elsewhere.

Gay rights organizations said the law would sanction discrimination on the basis of sexual orientation. Apple CEO Tim Cook said laws like this "have the potential to undo decades of progress toward greater equality." Salesforce, a pioneer in cloud computing, canceled all events in the state and vowed to "dramatically reduce" its investments there.

The concern about the rights of gays and lesbians is amply justified. But the uproar obscures the value of these laws. The federal government has had one since 1993, and 19 other states, including Illinois, have them as well — without the consequences predicted in Indiana and without the fierce objections heard this time. Support in the House and Senate was nearly unanimous when Illinois passed its law in 1998.

Pence, however, certainly knows the political climate around the issue has changed. Many of these laws were enacted before the advent of same-sex marriage in much of the country. But now some proponents see this type of legislation as a way for religious people to refuse to recognize such unions. Some politicians have seized the opportunity to pander to anti-gay sentiment.

So religious liberty protections that once sounded commendable or innocuous now appear dangerous.

But the protections are still needed to assure that religious minorities are as free to live out their beliefs as mainstream faiths are. During Prohibition, churches were allowed to provide wine for communion, because Christians had the political power to get an exemption into law. But the Supreme Court allowed the firing of two Oregon drug counselors who used peyote as part of a Native American Church ritual. That unequal treatment led to the passage of the federal RFRA.

What good does this type of legislation do? In Arkansas, a related law ensured that a Muslim prison inmate could grow a beard in accordance with his beliefs, after his lawyers proved the whiskers posed no security risk. Orthodox Jews may request exemptions from government-mandated autopsies, which their faith does not allow.

When a law inflicts a burden on people practicing particular religions, those people may request accommodations. And if the government can't show a "compelling interest" to justify the burden, it has to make allowances.

Most Americans now believe in equal treatment for gays and lesbians. They think sexual orientation — like race, sex and religion — deserve legal protection against discrimination.

That's the sticking point here. Gay rights groups are understandably worried that the Indiana law would allow a florist or baker to refuse to do business with a same-sex couple planning a wedding — and Pence has fanned such fears by declining to say whether they are right.

In practice, the concern may be misplaced. University of Virginia law professor Douglas Laycock, a religious liberty expert who favors same-sex marriage, says that "no one has ever won a religious exemption from a discrimination law under a RFRA standard."

Indiana, unlike Illinois, has no state law forbidding discrimination on the basis of sexual orientation — which means retailers may already refuse to do business with gays. The RFRA might be invoked in those Indiana cities that have anti-discrimination ordinances, but there's no guarantee the religious objectors would prevail in court.

No doubt feeling the pressure of national criticism, the Republican leaders of the Indiana House and Senate said Monday that they're looking at how they could clarify the law to make clear it does not permit discrimination against gays and lesbians. They're apparently not considering a vote to repeal.

So here's our advice to our neighbor: Keep the valuable religious liberty protections, but revise the law to stipulate that it may not be used to authorize discrimination against gays. While you're at it, pass a state law banning such discrimination entirely.

Ensuring equality for gays and lesbians is a vital matter. So is upholding freedom of religion. Illinois has found that the two can coexist quite comfortably. Indiana should follow suit.