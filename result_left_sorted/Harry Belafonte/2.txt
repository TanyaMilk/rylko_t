No single film or book or TV special could do full justice to the story of Frank Sinatra or thoroughly capture the essence of the man.

His life was too densely packed with dramatic incident, his artistic achievements too plentiful, his defeats too devastating, his highs too high and his lows too low to be adequately addressed by anyone. Fiction writers couldn't persuasively invent a life like Sinatra's, and nonfiction chroniclers couldn't hope to document all the twists and turns in the tale of the greatest popular singer-entertainer of the 20th century.

But "Sinatra: All Or Nothing at All" certainly gives it a heroic try. Premiering Sunday and Monday nights on HBO to mark this year's Sinatra centennial, director Alex Gibney's richly appealing, four-hour marathon features more details of Sinatra's life, more clips of his music and more words from the man himself than any other film. Barring some omissions, the documentary stays faithful to what happened and gives a microphone to those who were there, from first wife Nancy Sinatra to second wife Ava Gardner to third wife Mia Farrow (though fourth wife Barbara Sinatra and much of the rest of Sinatra's final years are, alas, eclipsed by everything else).

The avalanche of events in Sinatra's life — which began on Dec. 12, 1915, and ended on May 14, 1998 — tends to overrun the film, leaving little room for in-depth analysis of how his music continually evolved or a deep understanding of how Sinatra perpetually reinvented himself as singer, actor and personality. Then again, perhaps an artist of Sinatra's stature and monumental gifts never really could be explained or interpreted, his persona most eloquently expressed by his music and his films. Everything else is verbiage.

What we get in "Sinatra: All Or Nothing At All," then, is an oft-thrilling ride through the roller coaster of his life, illuminated by what matters most: his music and his words.

As a child in Hoboken, N.J., he often found himself picked up and plopped onto a player piano, where he'd sing along with the tunes.

"And it was a horrendous voice," Sinatra says in the film. "Terrible. I mean, it was a like a siren. It's a wonder I ever got anywhere starting that way. … One day I got a nickel or a dime, whatever it was, and I said, 'This is the racket. This is what you gotta be doing.'"

The film doesn't mention his brief, failed foray into newspapering, but it's unflinching in detailing the inauspicious start of his musical journey as a high school dropout and his father's sharp disapproval: "You'll be a bum," his dad unceremoniously told him.

But once bandleader Harry James heard Sinatra singing at the Rustic Cabin in New Jersey, in 1939, a soon-to-be-star was born. James took Sinatra on the road, then generously released him from a contract so that Sinatra could join Tommy Dorsey's more celebrated band in 1940.

"He sang about eight bars, and that whole theater became so quiet, you could have heard a pin drop," remembers singer Jo Stafford of encountering Sinatra's first notes with Dorsey's organization. "You just knew that you were hearing something quite unique."

Even at that early stage, when Sinatra still was crooning in the manner of the biggest pop singer of the day, Bing Crosby, the younger man stood out for the charismatic manner of his delivery, the luster of his vocal tone and the unerring precision of his pitch. Already, he was at least Crosby's equal.

By studying trombonist Dorsey's work, Sinatra was about to surpass everyone.

"I watched him," Sinatra says of Dorsey in the film. "And I could never see him breathe, 16 bars at a time. I wondered how he does that. If you can visualize how a trombone player holds the mouthpiece, he was breathing in the corner of his mouth. … And that was my theory. Do not break a phrase, if you can do that, and keep the audience listening for the rest of the phrase."

Sinatra's minimal use of vibrato, uncanny insight into the meaning of a lyric and sleekly modern way of singing ultimately rendered Crosby old-fashioned by comparison. Which was precisely the idea.

"Now I figured that Crosby was the only man on top," Sinatra says in the documentary. "He was the only guy on top. And I thought to myself: Somebody's gotta challenge this bum, some time or other it's gonna happen. So I began to think to myself: Jesus, why don't I do it for myself?"

So Sinatra split with Dorsey, who extracted a draconian price for setting him free: an onerous contract giving Dorsey a third of Sinatra's future earnings for life. Union officials pressured Dorsey to release Sinatra from that dreadful pact, but rumors began to swirl that Sinatra's mob friends had strong-armed his former boss.

"I've been accused of the mob got me out of the band," we hear Sinatra saying in the film. "Some unidentifiable creature (allegedly) went to Tommy Dorsey (and) said: 'You better let him out of the contract.' Well that never happened."

Regardless, by 1942, Sinatra became the first teen idol singing at the Paramount Theater in New York, the bobby-soxers queuing up for hours to see and hear their emaciated hero caressing love songs, thereby reminding them of boyfriends. The film doesn't mention that Sinatra's shrewd publicist, George Evans, hired girls to faint on cue and engaged an ambulance to be ready to whisk them away, heightening the melodrama of the occasion.

Even so, there was no denying the genuine hysteria that Sinatra sparked. By 1945, "Anchors Aweigh" made Sinatra a bona fide movie star, as he danced astonishingly well alongside no less than Gene Kelly. At the same time, Sinatra proved that he was way ahead of his time in championing religious freedom and civil rights, as in the short film "The House I Live In" of 1945.

"The film deeply moved me," Harry Belafonte says in "All Or Nothing At All." "It was one of the most engaging documents that I had seen on the issue of race. Now a lot of movies had been made before that time that attempted to deal with the issue of race, but nobody from the height of pop culture had done it quite the way Sinatra did."

All the while, Sinatra — since 1939 a married man — was conducting multiple love affairs of varying duration, bringing anguish to wife Nancy.

"All the rich, glamorous girls were after the celebrity," she says in the film. "He was attractive, he was oriented that way, in a way. He felt he had to possess any woman he thought was attractive. A lot of men are like that. And they get that way more so with more success."