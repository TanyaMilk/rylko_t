President Obama took the stage for about 20 minutes on Saturday night for his annual roast — he zinged everything from Ted Cruz to CNN to Joe Biden’s massages. Here were some of the highlights:

— “Welcome to the fourth quarter of my presidency…The fact is I feel more loose and relaxed than ever. Those Joe Biden shoulder massages — they’re like magic. You should try one.”

— On his friendship with VP Joe Biden: “We’ve gotten so close that in some places in Indiana, they won’t serve us pizza anymore.”

— “One big story was the brutal winter — the polar vortex caused so many record lows they renamed it MSNBC.”

— “I am determined to make the most of every moment I have left. After the midterm elections, my advisers asked me ‘Mr. President, do you have a bucket list?’ I said, ‘Well, I have something rhymes with bucket list.’ Take executive action on immigration? Bucket. New climate regulations? Bucket.”

— “My new policy is paying off. Look at my Cuba policy: the Castro brothers are here tonight. Amigos! Que pasa? What? It’s the Castros from Texas? Oh. Hi, Joaquin. Hi, Julian.”

— “A few weeks ago, Dick Cheney says he thinks I’m the worst president of his lifetime. Which is interesting because I think Dick Cheney is the worst president of my lifetime. Quite a coincidence.”

— “Six years into my presidency, people still say I’m arrogant. Aloof. Condescending. People are so dumb. No wonder I don’t meet with them.”

— “I’m happy report that the Secret Service, thanks to excellent reporting by White House correspondents…they finally figured a foolproof way to keep people off my lawn. It works! (Photo of McCain swinging a broom.) It’s not just fence jumpers. A few months ago, a drone crash landed out back. That was pretty serious, but don’t worry, we installed a new state of the art security system.” (Photo of Biden swinging a bat.)

[Full transcript of President Obama’s White House Correspondents’ Association dinner toast]

— “Being president is never easy. I still have to fix the broken immigration system. Issue veto threats. Negotiate with Iran. All while finding time to pray five times a day.”

— “At this point, my legacy is finally able to take shape. Economy is getting better. Nine in 10 Americans now have health coverage. Today, thanks to Obamacare, you no longer have to worry about losing your insurance if you lose your job. You’re welcome, Senate Democrats.”

— “Just this week, Michele Bachmann predicted I would bring about the biblical end of days. Now that’s a legacy. That’s big. I mean, Lincoln, Washington, they didn’t do that.”

— “I have one friend, just a few weeks ago, she was making millions of dollars a year — and she’s now living out of a van in Iowa.”

— On Cecily Strong’s impersonation of CNN journalist Brooke Baldwin on “SNL”: “Usually the only people impersonating journalists on CNN are journalists on CNN.”

— “Turns out Jeb Bush identified himself as Hispanic back in 2009…It’s an innocent mistake, it reminds me of when I identified myself as American back in 1961.”

— “Ted Cruz said that denying the existence of climate change made him like Galileo. Now that’s not really an apt comparison. Galileo believed the Earth revolves around the sun. Ted Cruz believes the Earth revolves around Ted Cruz. …when a guy who has his face on a ‘HOPE’ poster calls you self-centered, you’ve got a problem.”

— “Donald Trump is here. Still.”

— “I look so old John Boehner’s already invited Netanyahu to speak at my funeral. Meanwhile, Michelle hasn’t aged a day. I ask her what her secret is, she says ‘fresh fruits and vegetables.’ It’s aggravating.”

[Complete coverage of the 2015 White House correspondents’ dinner]

— “I can’t wait to see who the Koch brothers pick.  It’s exciting. Marco Rubio, Rand Paul, Ted Cruz, Jeb Bush, Scott Walker, who will finally get that red rose?”

— On the Koch brothers’ sponsoring of campaigns: “A billion dollars. From just two guys. Is it me, or does that feel excessive? I know I’ve raised a lot of money too, but my middle name is Hussein. What’s their excuse?”

— “As we know, Hillary’s private emails got her in trouble. Frankly, I thought it was going to be her private Instagram account”:

— “Bernie Sanders might run. I like Bernie. … Apparently, some folks want to see a pot-smoking socialist in the White House. We could get a third Obama term after all!”

— “I often joke about tension between me and the press, but honestly, what they say doesn’t bother me. I’m a mellow sort of guy. And that’s why I invited Luther, my anger translator, to join me.”

(Keegan-Michael Key of “Key and Peele” takes the stage as Luther)

“Luther”: “HOLD ON TO YOUR LILY WHITE BUTTS.”

Obama: “In our fast-changing world, traditions like the White House correspondents’ dinner are important.”

“Luther”: “I MEAN REALLY, WHAT IS THIS DINNER AND WHY AM I REQUIRED TO COME TO IT?!”

Obama: “Because despite our differences, we count on the press to shed light on the most important issues of the day.”

“Luther”: “AND WE CAN COUNT ON FOX NEWS TO TERRIFY OLD WHITE PEOPLE WITH SOME NONSENSE!”

Read more:

Obama vows to get Post reporter freed from Iran

Forget celebrity fashion: Here’s how normal guests dressed for correspondents’ dinner

Worlds collide at the pre-correspondents’ dinner ‘garden brunch’

A light turnout of Hollywood glitz at White House correspondents’ parties

Can we nominate Larry Wilmore for next year’s WHCD headliner?

Five myths about the White House correspondents’ dinner

Ethical optics remain cloudy at White House correspondents’ dinner

Former D.C. mayor Adrian Fenty headlines WHCD New Media party