All they want are sandwiches from Wagshal’s deli , so I bring corned beef, chopped liver and turkey. And coleslaw, of course. And pickles. The menu is awfully casual for the setting: Sara Ehrman’s stylish Kalorama apartment, dominated by a 300-year-old, hand-painted Chinese screen and a crystal chandelier dangling from an 11-foot ceiling, and dotted with framed photos of Ehrman with Bill and Hillary Clinton and other world leaders.

Ehrman has laid out her favorite Syrian tablecloth — white cotton, with gold and silver metallic threading — and topped it with a small spray of purple flowers to give the dinner a festive feel. Her high dining-room window affords a commanding view of the Washington Monument.

During the next 3 1/2  hours, we talk about their lives — their long, very accomplished lives marked by careers that would be noteworthy in any era, but that are particularly remarkable because they were launched before women had the audacity or the opportunity to do such things. Ehrman was born more than a year before women won the right to vote , for crying out loud, and Jodie Z. Bernstein and Patricia M. Wald came along soon after. ¶  But here they are, three power hitters in the intertwined worlds of government, law, politics and international relations, career women who overcame stereotypes and discrimination to rack up an extraordinary number of firsts and feats after taking years off to raise children. They were housewives when that was the expectation. They worked for less-than-equal pay when that was the only option. They were one step ahead of the women’s rights movement and, perhaps ironically, disinclined to join it.

Altogether, they have lived 269 years , raised 10 children and been appointed by, honored by and befriended by presidents. They have traveled the world in the pursuit of peace and justice. They are honest-to-God bigwigs, yet — in a city bursting with egos — these decades-long friends are refreshingly down-to-earth.

Start with Ehrman, the elder stateswoman of the group and a close friend of mine. At 95, she is a senior adviser to the S. Daniel Abraham Center for Middle East Peace, a founder and board member of both Americans for Peace Now and American Friends of the Yitzhak Rabin Center, and a go-to person for Democrats seeking advice on politics and fundraising. That’s just now. In the past half-century, she: worked for two senators; helped run George McGovern’s presidential campaign; lobbied for the governor of Puerto Rico; crafted the potent political strategy of the American Israel Public Affairs Committee; moved to Little Rock to work on Bill Clinton’s 1992 presidential campaign, then aided his transition to the White House; and served as deputy political director of the Democratic National Committee.

Accomplishments aside, Ehrman is best known as the woman who tried to dissuade Hillary Rodham from moving to Arkansas to marry Clinton because she thought it would constrain her young friend’s career, a projection the former president ribs her about to this day.

Next comes Bernstein, who at 88 remains one of the foremost experts on consumer protection and privacy law. She was the first woman to serve as general counsel of the Environmental Protection Agency and, afterward, as general counsel of the Department of Health, Education and Welfare (now Health and Human Services), where she oversaw all the lawyers who worked for the Food and Drug Administration and the Health Care Finance Administration. She was the first woman director of the Bureau of Consumer Protection at the Federal Trade Commission. She also chaired the Commission on Wartime Relocation and Internment of Civilians, which spurred the government to pay reparations to Japanese Americans interned during World War II.

Wald, 86, is the baby of the group and perhaps the most famous. She was the first woman appointed to the U.S. Circuit Court of Appeals for the District of Columbia, the second most-powerful court in the country, and ultimately became chief judge. After leaving the bench, she served as the U.S. representative to the International Criminal Tribunal for the Former Yugoslavia, on the Commission on the Intelligence Capabilities of the United States Regarding Weapons of Mass Destruction, and on the Council of the Administrative Conference of the United States, which reviews federal legal actions, to name a few. 

In 2013, President Obama awarded Wald the Presidential Medal of Freedom, the nation’s highest civilian honor. Currently, she serves on the Privacy and Civil Liberties Oversight Board, created to monitor government activity in the fight against terrorism. 

Washington is full of smart people who have done consequential things. But even in that environment, these three stand out. The stuff they did mattered.

And they did it without equal rights and, often, without basic civility: Bernstein was excluded from eating lunch with her male colleagues; Ehrman was scolded for setting foot on the male-only Senate floor.

Yet they persevered and made it. Big. I want to know how they did it. 

Ehrman and Bernstein reach for corned beef on rye. Wald opts for turkey on a kaiser roll, which she slathers with extra mayonnaise.

 They sip wine. We talk:

 Jodi Enda: I’m really curious about your influences, especially your childhood influences, and what in the world made you think you could be anything other than what most women were when you were kids, which was housewives?

 Jodie Bernstein: One of these early influences in my life, certainly, is my father’s youngest sister. She went to medical school in the ’30s when I was just a kid. She told me later that she didn’t really want to go, but the boys said, “We don’t have a boy to send to medical school and we have to have a doctor.”

 JE: What was her name?

 JB: Mary Zeldes. … I sort of thought, Well, gosh, if she can go be a doctor. ... I’m not good at chemistry, so maybe I can find something else.

[We all laugh.]

 JE: And how did your parents feel about that?

 JB: My father was wonderfully supportive of both me and my brother. He said, “I will help both of you get as much education as you can do as long as I can afford to help you.”

Bernstein, born Joan Zeldes, grew up in Galesburg, Ill. Her mother emigrated from Poland as a toddler, grew up in Peoria, where her father owned a pawnshop, and graduated from the eighth grade — an unusual accomplishment for girls in her community of Jewish immigrants. She worked as a linen buyer in Bergner’s department store before marrying in 1925. Bernstein’s father left Ukraine, where he had been trained as a tailor, at 13. He owned a clothing store in Galesburg until the Depression forced him to close it. When Prohibition ended, he and his brother opened a tavern, despite his wife’s admonition that it was not respectable. Later, he opened a liquor store, which was, in his wife’s eyes, very respectable.

 JE: What about you, Pat? You were being basically raised by a single mother, right? 

 Patricia Wald: Yeah, my mother raised me. My mother never went beyond high school. She didn’t graduate. … She was part of a family of eight.

My grandparents came over from Ireland. And some of my other aunts and uncles, two of them, did graduate from high school. But nobody ever went to college or anything like that.

Wald, then Patricia McGowan, grew up in Torrington, Conn., a factory town. Her father, an alcoholic, disappeared when she was 2. She lived with her mother, her grandparents, one uncle and three aunts, each of whom worked for the Torrington Co., which produced sewing and surgical needles and, in World War II, bearings.

 PW: And actually that’s where I worked in the summertime.

 JE: What did you do?

 PW: I had the night shift, and I used to work from 10 o’clock till 3 or 4 in the morning greasing ball bearings. … My mother was determined that I was going to go to college. 

Wald was high school valedictorian and received a scholarship to Connecticut College. During her last months in Torrington, she was influenced by three things: young women who got married but never left town, a union strike and a male assembly line worker who told her, “You don’t want to do this kind of thing for the rest of your life.”

Wald’s comments remind Bernstein of her jobs as a teenager during World War II.

 JB: One summer, because men were gone, we detasseled corn. That was the worst job in the entire world. It was like 100 in the shade. … And I signed up to do that because it was patriotic.

 Sara Ehrman: And do you remember how much you got paid? Fifty cents an hour?

 JB: It was like 65 cents an hour. We started at 6 o’clock in the morning, allegedly before the sun came up because otherwise it was like a sauna. … I would have quit after the first, like, three days because it was so horrible, but my mother said to my father at breakfast, “She’ll never make it; she won’t survive,” and I thought, [here she bangs the table with the palm of her right hand three times] “I gotta stick it out.”

Bernstein also worked in a factory, making trip flares and rifle flares.

 JE: How about you, Sara? Who influenced you?

 SE: My mother died when I was 8 1/2   . We lived in a little fishing village on the south shore of Long Island, called Baldwin Harbor … and the question was, What to do with Sara? My family didn’t want me to live with my father because they felt he wasn’t qualified to or able to raise a young girl. So my brother, William, stayed with my father in Baldwin Harbor, and I went to live with my mother’s sister [in Manhattan].

My mother’s sister had at the time 10 living children. Most of them were still at home. And they were a very unusual family, very cultured, very verbal, very left wing. And at my age, the cousin who had the most influence on me was very much older than I. He was a lawyer. And this you have to remember is during the Depression, so that things were really tight, and my cousin, Cousin Abe Klein, would take me on — because he loved my mother — he would take me on his lap at night after supper, and he would tell me stories about the munitions makers in Czechoslovakia, the Skoda Ironworks, the depredation of the Spanish left wing. … At a very young age, they sent me out on the street with a can to collect money for the Abraham Lincoln Brigade, which was the American brigade that went to volunteer in the Spanish Civil War.

At 13, Ehrman, then Sara Teitelbaum, moved back in with her father and brother, who then were in Flushing, Queens. She did well in high school until she flunked chemistry. To graduate, she had to retake the course in summer school, which she refused to do. She was conditionally admitted to Barnard College but couldn’t attend without a high school diploma.

 SE: I never went to college. I went out marching.

 JE: Did you expect that you would have a career of some sort, or did you expect you would get married and stay home?

 SE: I expected that I would be a force in the dictatorship of the proletariat.

[Bernstein and Wald laugh.]

Instead, she got married and, at 22, moved with her husband, Libert, to Winston-Salem, N.C., where he was stationed during World War II. Ehrman got her first “real” job working with economists analyzing the impact of civilian aircraft accidents: She was tasked with tearing the perforated edges off computer paper. She also got her first look at Jim Crow.

 SE: I learned more in that job than almost anything I had done before. I learned about the South, I learned about people.

I ask Wald and Bernstein about their decisions to become lawyers. Wald says she became interested in labor law after the Torrington factory strike. Bernstein says she and her high school friends were active in state politics, and she thought she’d like to run for office. 

 JB: We would campaign for our local guy who was going to go to Springfield, which was a very big deal, and I was very active in that, so I thought, well, gee, you know, I can do that, too. … Why couldn’t I be governor of Illinois? … And then somebody said to me, “Well, you know, they’re all lawyers.”

So Bernstein went off to the University of Wisconsin at a time when most of the men were at war. When she graduated in 1948, she headed to Yale Law School and, on her first day, met Wald. For three years, they were roommates and the lone women on the staff of the Yale Law Journal. Most of the girls they grew up with had been married several years by then.

Wald and Bernstein say they were treated well by their male classmates and professors. The outside world was another story.

 JB: We were not interviewed by the law firms that came up to school.

 JE: They didn’t want women?

 JB: Well, they just made it pretty clear: We weren’t going to be interviewed, and we weren’t going to be hired. So Pat and I went down to New York and had names at different law firms where we knew somebody. … We just kind of knocked on the door. And I ended up the last day, the last hour of the day of interviews, at Shearman & Sterling. And Monty Singer was there. Remember Monty Singer?

 PW: He was a year ahead of us.

 JB: And he already had a job there. So I called Monty, and I said, “Can I get interviewed? Because I’m here, you know. I need a job.” And he said, “Well, it’s ridiculous — they’re not gonna hire you.” And he said, “It’s a miracle they hired me — I’m Jewish.” [Everyone laughs, but many law firms did not hire Jews in those days, making Bernstein’s search doubly hard.] And I said, “Well, give it a shot,” you know, so he did. And the hiring partner — he’s now deceased — was named Henry Harfield. … So he decided to interview me at the end of the day. He commended me for my résumé and how impressive it was and this and that, and then he said, “Well, I see that you’re single, Miss Zeldes. … What would we do if you became pregnant?”

 SE: Ohhhh.

 JB: Well, I thought, this is over, for sure. So I said, “Well, Mr. Harfield, so far [dramatic pause] I’ve been lucky!” He burst out laughing. … He said, “I’m hiring you.”

Not all of Bernstein’s new colleagues — or clients — were eager to have a “girl.” One maitre d’ tried to block her from meeting a client at a “men’s” dining room. After two years, she married Lionel Bernstein, a gastroenterologist whose career took them to Chicago. There, Bernstein worked briefly for a law firm (the one that excluded her from lunch), then in local politics in a futile attempt to get a state government job.

Wald started out as a law clerk — rare for a woman in 1951 — for Judge Jerome Frank of the U.S. Court of Appeals for the 2nd Circuit. Most notably, she worked on the appeal of Julius and Ethel Rosenberg. She married a law school classmate, Robert Wald, and worked briefly at Arnold, Fortas & Porter in the District. The couple moved to Virginia Beach, where Bob Wald finished his military service, then returned to Washington.

After the war, Ehrman and her husband settled in Virginia.

All three women had babies — Wald had five, Bernstein three and Ehrman two — and stayed home for a decade or more.

 PW: I made one visit back to the law firm, and it was clear that if I wanted to come back, I could come back. But I didn’t really want to. I really was into motherhood.

 SE: For me it was inconceivable to leave my babies with anybody.

 JB: I am the outlier here. I would looove to have gone back to work. I always felt like, “Oh, okay, I guess I have to do this now.” [She chuckles.]

Bernstein did try to return to work before her third child was born. She interviewed for a part-time job at a savings and loan.

 JB: And the guy said to me, “Mrs. Bernstein, don’t you have a husband?” “Yes, I have a husband.” “And does he have a job?” “Yes, he has a job.” “Well, there’s certainly no reason for you to be working!” [She slaps the table.] And that was the end of that!

 JE: And what was your response?

[She blurts out an expletive, then quickly adds that she didn’t actually say it at the time.]

 SE: In those days, you didn’t argue. You thought, Well, he’s right.

 JB: No, I’m a woman. What was the point of confronting him?

Ehrman recalls a similar experience, but her “bad guy” was a woman. It was the early 1960s, and she had just landed a job working for Sen. Joseph Clark, a liberal Democrat from Pennsylvania.

 SE: I was there about three months, and I realized that my colleague who sat right next to me … was making twice as much as I was. I went to the office manager and … she looked at me straight in the eye and she said, “Your husband has a good job. You don’t need the money.” And you know what I said — and I am ashamed of myself for it — I said, “You’re right, Marie, you’re right.” Imagine a woman saying that now.

Given their experiences, it seems natural that these three would have supported the women’s rights movement. I ask them about Betty Friedan’s 1963 game changer, “The Feminine Mystique.”

 PW: I never read it. None of the feminist writings had any influence whatsoever on me. I read them eventually, and I agreed with a lot, but they didn’t influence the course of my life.

 SE: I totally agree with you.

 JB: I do, too — 

 JE: Is it because you were already doing what you were doing?

 PW: Yeah, I was doing what I wanted to do. And by the time she wrote that, actually, [I had] my first really good job. … And I didn’t need it.

 JB: We’d already sort of fought those battles. 

 PW: I thought what little I read sometimes, it was too extremist for me. 

 JB: I didn’t feel like I was exploited.

 SE: Or held back. 

 JB: Well, there were times when I was held back. I knew, because there were people who didn’t offer jobs to women.

 SE: But you knew — 

 JB: But we’d kind of been through it already, and — 

 SE: And you knew how to maneuver it.

Later, Ehrman tells me that she eventually warmed to feminism. She and her husband divorced, and she realized the movement effectively had increased her wages. 

Wald and Bernstein (whose family moved to Washington in 1967) credit Jimmy Carter with giving their careers a boost. The new president made it a priority to find qualified women for high-level federal jobs.

 JB: It was just a huge change, a sea change in the operation of the federal government, I think, in terms of opportunities.

Carter nominated Wald for the Court of Appeals and Bernstein for general counsel of the EPA, then of HEW. I ask if they got any pushback. Wald says conservative Republicans accused her (the mother of five) of being anti-family, citing her writings on juvenile rights and drug education. She also attributed some of the opposition to her liberal Democratic politics, rather than her gender.

Ehrman, Bernstein and Wald now are officially “retired,” but each still works.

 PW: My husband is dead now — he died four years ago — so I wouldn’t know what to do if I didn’t have something to do.

 SE: Working is a blessing, in a way, when you get older.

 PW: Oh, God, yes. 

 SE: We are three pretty old women, and we’re all fully engaged in the activities that have always motivated our lives.

 PW: If I had a fear, it’s the fear that you wake up one day and you can’t do it anymore. 

 SE: Then you’re gonna — 

 PW: Die. [She laughs.]

Ehrman begs to differ.

 SE: You’d be reading, you’d be sleeping late, you’d be being a pain in the neck. 

For now, we simply dig into fruit salad and blueberry gelato, and call it a night.

 

Jodi Enda, a writer based in Washington, has covered the White House, politics and the media. To comment on this story, e-mail wpmagazine@washpost.com or visit washingtonpost.com/magazine.

E-mail us at wpmagazine@washpost.com.

For more articles, as well as features such as Date Lab, Gene Weingarten and more, visit The Washington Post Magazine.

Follow the Magazine on Twitter.

Like us on Facebook.