TEL AVIV — Prime Minister Benjamin Netanyahu and his Likud party won a “resounding victory” in Israel’s parliamentary elections, the Associated Press reported early Wednesday, citing official results.

Netanyahu overcame a strong challenge. Early results showed that he might finish in a virtual tie with his main opponent, Labor Party leader Isaac Herzog, according to exit poll results reported by Israel’s three largest television news stations.

After the initial exit poll results were announced, Netanyahu declared victory on Twitter. He said he had already begun to call potential coalition partners about forming a new government.

“Against all odds, we achieved a great victory,” Netanyahu later told his supporters in a packed hall in Tel Aviv about 1 a.m. Wednesday. “Now we have to form a strong and stable government.”

But Herzog said Netanyahu’s victory declaration, ahead of final results and coalition formation, was premature.

“We’re going to wait for the true results,” Herzog told his supporters. “Everything is still open.”

Exit surveys released immediately after the polls closed at 10 p.m.showed Netanyahu and his rightist Likud party drawing 27 or 28 seats in the next parliament, against the 27 forecast for the center-left Zionist Union alliance of Herzog and his running mate, former peace negotiator Tzipi Livni. A coalition of Arab Israeli parties, called the Joint List, emerged in third place, according to the exit polls.

A final count of actual votes may not be completed until Thursday, but exit polls in Israel are viewed as good predictors of eventual results. If the returns track the exit polling, either Netanyahu or Herzog may be called upon to form the next government — although it appeared that Netanyahu might have an edge in building a coalition from the top 11 parties that drew enough votes to seat their candidates in parliament.

The election was being closely watched in Washington, where Netanyahu gave a speech to Congress two weeks ago opposing the Obama administration’s attempts to reach a deal with Iran to rein in Tehran’s nuclear program, angering the White House. Some analysts predicted that tensions would deepen after the Israeli leader’s pronouncement Monday that he would not support the creation of a Palestinian state, a reversal of his earlier stance.

White House press secretary Josh Earnest said President Obama “remains committed to working very closely” with whoever wins the Israeli premiership.

Turnout was high, with almost 72 percent of eligible voters going to the polls. Leaders of the Arab parties suggested that Arab Israeli turnout reached 65 percent, far higher than in the country’s last national elections, in 2013.

 [Why Israel’s top hawk wants people to “stop apologizing”] 

Netanyahu and Likud appeared to come from behind. The last round of pre-election opinion surveys on Friday showed Likud trailing Herzog’s alliance by three or four seats.

Pundits were beginning to write the first drafts of Netan­yahu’s political obituary. Reporters asked him in interviews what he planned to do in retirement.

But in the past five days, Netan­yahu took to the airwaves, warning repeatedly that Herzog and the left were going to turn over land to the Palestinians and divide Jerusalem in half.

The virtual tie, as seen in exit polls, surprised many ordinary Israelis. The electorate here has been trending rightward for years.

“Israelis said very clearly yes today to Prime Minister Netan­yahu and Likud to continue to lead the country,” said Silvan Shalom, a Likud party leader who said he was confident that Netanyahu would succeed in forming the next government.

“It’s a big victory for the Likud,” said Likud member Danny Danon, a former deputy defense minister. “This is a win for the right, and all my friends on the left need to acknowledge this win.”

Israeli President Reuven Rivlin will now ask party leaders to come to his residence and signal to him whom they want to lead the next coalition government. He suggested Tuesday night that he would press for a government of national unity combining Labor and Likud.

 [How Netanyahu could win even he loses] 

With the indications so far of a virtual dead heat, there could be days or weeks of wheeling and dealing before a new Israeli government is formed — and the party with the most votes may not be the one that leads a new governing coalition. It will depend on which party can piece together a puzzle from factions of the left, right and center — including parties that represent ­ultra-Orthodox Jews.

It is also possible, if neither Herzog nor Netanyahu can put together a governing coalition, that they will be forced to form a national unity government — as Rivlin advocated.

“It is far from clear what the outcome is,” said Yohanan Plesner, president of the Israel Democracy Institute. “Both may try to form a government.”

A leader of the Labor Party, Nachman Shai, conceded that Netanyahu may have an easier time forming a government, but he said it was too soon for either side to declare victory.

“I just advise everyone to wait 24 or 48 hours; just wait and see,” Shai said.

The kingmaker of Israeli politics may now be Moshe Kahlon, a former Likud minister who left that party to form his own, called Kulanu, which won nine or 10 seats, according to exit polls. Kahlon tweeted that it was a “great success.”

Kahlon became popular with voters after he broke up cellphone monopolies and the prices­ for mobile minutes plummeted. His party’s candidates include Michael Oren, a former Israeli ambassador to the United States, who rebuked Netanyahu for addressing Congress in the polarizing speech on Iran this month.

Before the election, Kahlon refused to say whether he would join a coalition led by Netanyahu or Herzog. But many analysts think that he may ultimately side with Netanyahu, because ­Kahlon’s politics are more closely aligned with Likud than with Labor.

 [A guide to the political parties battling for Israel’s future] 

As voting was underway Tuesday, Netanyahu said his government was “in danger,” notably from a turnout of Arab Israeli voters. The alarm from Netan­yahu reflected the tight margins in his bid to hold back a surging challenge from Herzog.

Netanyahu, 65, who has served as prime minister for nine years spread over three terms, appeared set to easily win an unprecedented fourth term when he called the election in December, two years early.

Herzog, 54, the son of a former president and grandson of a prominent rabbi, mounted an unexpectedly strong campaign, capitalizing on economic malaise and what some voters described as weariness with Netanyahu.

“The right-wing government is in danger,” Netanyahu said in a video posted on Facebook on Tuesday before the polls closed. “Arab voters are coming out in droves to the polls.”

Earlier, referencing his nickname, Netanyahu charged that unnamed “foreign powers” were pouring money and resources into an “anyone but Bibi” campaign.

 

Ruth Eglash contributed to this report.

   

 What the United States could learn from Israel’s electoral system 

 Netanyahu’s fears over the possible nuclear deal with Iran