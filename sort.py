#!/usr/bin/python
# coding: utf-8

import os
import codecs

import functions


def sort(typ):
    keywords  = [(x, 'keyword') for x in functions.get_keywords(typ)] 
    keywords += [(x, 'name') for x in functions.get_names(typ)]
    result_dir = 'result_%s'%typ 
    result_dir_sorted = 'result_%s_sorted' % typ 

    if not os.path.exists(result_dir_sorted):
        print u"Создаём папку %s" % result_dir_sorted
        os.mkdir(result_dir_sorted)

    for magazine in os.listdir(result_dir):
        print u"Смотрим журнал %s" % magazine

        for f in os.listdir(os.path.join(result_dir, magazine)):
            fobj = codecs.open(os.path.join(result_dir, magazine, f), 'r', 'utf-8')
            article = fobj.read()
            fobj.close()

            for kw in keywords:
                keyword = kw[0]

                if functions.found_kw_tuple(kw, article):
                    print u"В статье %s/%s найдено слово ''%s''" % (magazine, f, keyword)

                    if not os.path.exists(os.path.join(result_dir_sorted, keyword)):
                        os.mkdir(os.path.join(result_dir_sorted, keyword))

                    i = len(os.listdir(os.path.join(result_dir_sorted, keyword))) + 1
                    print u"Создаём файл %s/%s/%s." % (result_dir_sorted, keyword, '%i.txt'%i)

                    fobj = codecs.open(os.path.join(result_dir_sorted, keyword, '%i.txt'%i), 'w', 'utf-8')
                    fobj.write(article)
                    fobj.close()


sort('left')
sort('right')
