Last summer, three brave NYC singles agreed to wed three other complete strangers in the controversial FYI-channel reality series “Married at First Sight.”

Now the show is back for Season 2 on a bigger network and hoping to match or beat its Season 1 success rate, where two out of the three arranged couples remain happily married.

The series sees the couples meet at the altar in Episode 1, then live together for the next six weeks — getting to know families, merging social lives and building intimacy — before deciding whether to stay together or get divorced.

Ahead of Tuesday’s premiere (9 p.m. on A&E), The Post talked to the new singles looking for love about what led them to get married at first sight.

See if you can pick out whom the show’s four matchmaking experts will pair as husband and wife.

Davina Kullar, 34, Upper East Side

Kullar has a sassy personality and a strong career as a biopharmaceutical sales rep — and makes no apologies for her success.

Dating history: A few serious relationships and lots of casual meetings on “every dating website you can imagine.”

Ideal qualities in a spouse: Witty sense of humor, set in his career, ambitious and financially stable.

Why she wanted to get married at first sight: “I dated so many different types of men . . . I started to doubt whether I was even looking for the right qualities. I thought perhaps relinquishing a bit of control in this might be good.”

Biggest fear: Not being attracted to her husband.

Jaclyn Methuen, 30, Union, NJ

Methuen is an outgoing sales rep for Tito’s Handmade Vodka and needs a man who can keep up with her social lifestyle.

Dating history: College sweetheart for four years. Single for past seven years. No sex in two years.

Ideal qualities in a spouse: Personable, secure and confident.

Why she wanted to get married at first sight: “I saw that six-month follow-up episode, and it seemed really genuine.”

Biggest fear: Jumping into a relationship after being single for such a long time.

Jessica Castro, 30, Queens

A receptionist at a law firm, Castro has held a job since she was 14 and wants a man who values hard work — in life and in love.

Dating history: Engaged to a man she dated for seven years, which ended because he cheated on her. Single for two years.

Ideal qualities in a spouse: Close with family (especially his mother), independent, strong work ethic, loving and affectionate.

Why she wanted to get married at first sight: Castro applied for Season 1 and wasn’t matched but watched every episode. “I saw two successful marriages in Season 1, and thought, ‘I want that.’ ”

Biggest fear: Not being able to communicate effectively with her husband.

Ryan DeNino, 29, Staten Island

The ambitious De Nino earned a degree in business management/marketing and sales and now owns his own consulting business.

Dating history: One girlfriend of five years. Single 1½ years.

Ideal qualities in a spouse: Honest, trustworthy, outgoing, personable, family-oriented, caring and genuine.

Why he wanted to get married at first sight: “Knowing there were experts that would . . . get to know who I am, hopefully find me a match that would be compatible for me.”

Biggest fear: “Dealing with somebody [I’m not] compatible with and then having to live with them for six weeks.”

Sean Varricchio, 35, Jackson, NJ

ER/trauma nurse Varricchio leads an active lifestyle, enjoying daily workouts, bowling, flag football and running half-marathons.

Dating history: Late bloomer, played the field in his 20s, several relationships lasting 12 to 18 months.

Ideal qualities in a spouse: Athletic, intelligent, genuine, good heart, good family values, motivated and professional.

Why he wanted to get married at first sight: “I have a science background, I’m very analytical and I thought there’s got to be something to this.”

Biggest fear: That he gets his heart broken.

Ryan Ranellone, 29, Long Island

Real estate agent Ranellone lives with his mother and 12-year-old niece, whom he has helped raise since her parents died.

Dating history: Two long-distance relationships (longest lasted for one year). Single for eight months.

Ideal qualities in a spouse: Loyal, honest, dedicated and family-oriented.

Why he wanted to get married at first sight: “When it comes to dating, [I’ve been] unorthodox [like having long-distance relationships]. That’s the way I’ve liked it.”

Biggest fear: “If it doesn’t work out, oh God, I’m a divorcé.”