There’s a substantial appetite among American voters for an option other than the two major parties in this year’s elections — but there’s little taste for any of the third-party or independent candidates currently competing, according to the latest poll by The Washington Times/JZ Analytics.

While voters said they’d like more options, the vast majority conceded they usually only look to the Republican and Democratic parties for their choices, and said there are structural barriers that prevent third-party candidates from getting in the race or winning the kind of attention it takes to make a splash.

The desire for other choices was strong among both independents and Republicans, while Democrats — who control the White House and the Senate — were less eager to see alternatives.

“Regardless of whether folks are open to a third party today, there’s almost an expectation that, ‘Hey, this whole thing could be open to a third party,’” said John Zogby, the pollster who conducted the survey. “There’s one kind of waiting to be formed. There’s just that sense, from conservatives and moderates in particular.”

Speculation about a third-party bid has increased in recent weeks now that Mitt Romney appears to have sewn up the GOP’s presidential nomination and President Obama went mostly unchallenged for the Democratic nomination.

Earlier this month, former New Mexico Gov. Gary E. Johnson, who left the GOP in December, won the Libertarian Party’s presidential nomination, and he has vowed to try to win over supporters of Rep. Ron Paul, who is still competing for delegates for the Republican nomination.