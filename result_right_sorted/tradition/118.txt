How can anyone reach adulthood and not know how to boil water?

It happens.

Many high schools no longer offer home-economics courses. Working parents, whose time for
cooking is more limited, might rely on packaged ingredients, premade meals or restaurants to help
feed their families.

If children don’t learn how to cook at home or at school, even the most accomplished student
might graduate from high school without any cooking skills.

That’s what happened to Columbus native Stacy Bartlett.

“It’s pretty much a running joke in my family, about my lack of any cooking skills
whatsoever,” said Bartlett, 22.

She left home for Brown University in Providence, R.I., after graduating in 2010 from
Reynoldsburg High School. Four years later, she emerged with an undergraduate degree from Brown and
acceptance into the Ivy League university’s medical school.

Yet she struggled to make spaghetti.

“I honestly didn’t know when water boiled,” said Bartlett. “They talk about rolling bubbles —
a rolling boil — but I didn’t know what that means.”

Her parents, she said, kept her well-fed while growing up, but little cooking went on in
either of their homes.

“We ate out a lot,” Bartlett recalled. “My parents would cook, but nobody enjoyed it. I wasn’t
 eating pizza every day; they cooked enough to keep me healthy and nourished.”

She didn’t worry about her lack of culinary skills as an undergraduate: “I was one of the
only seniors on the full meal plan all the way through college.”

After graduation, though, she faced the prospect of having to cook for herself for the first
time — and was stymied.

Learning to cook takes time, motivation and practice.

Among the avenues for learning:

“Our philosophy is that cooking, like anything else, needs practice,” said chef Tricia Wheeler,
owner of the Seasoned Farmhouse cooking school in the Clintonville neighborhood of Columbus. “You
have to give some attention to it.”

Wheeler said her school tries to emphasize basic techniques, such as sauteing or braising,
and to teach students the proper way to perform them.

“We want home cooks to feel really comfortable, and we want to teach skills and techniques.”

Basic techniques are transferable from one dish to another. Sauteing a chicken breast, for
example, can be applied to sauteing a fish fillet or a pork chop.

Chef Seth Carroll, cooking instructor at Sur La Table at Easton Town Center, said his
students range from experienced home cooks seeking to learn something new to people like Bartlett,
who are starting at ground zero.

He tries to teach that making mistakes is OK.

“If it’s not exactly perfect the first time, don’t worry,” he said. “Have fun with it, make
it creative.”

Bartlett’s situation, Carroll said, is more common than many people realize.

During the three years that he served as operations manager for the commissary at Ohio State
University, he oversaw the daily preparation of food for nine campus cafes. Carroll had 50 students
on his staff and most of them had little or no cooking skills when they arrived, he recalled. Those
who had cooked made only simple dishes, such as pasta or eggs.

Carroll said a gradual cultural shift in the United States took place when home cooking
became less important.

“For a while, everyone moved to eating out more,” he said. “Food moved to more processed,
more heat-and-serve.”

He said over the past 10 years, the pendulum has changed direction — “People are starting to
come back to cooking.” Carroll hopes more families will start their children cooking at an early
age.

“A lot of people are afraid to put a knife in their kids’ hands,” he said. “(But) the younger
you can get them excited about it, the better off they are.”

Wheeler, an Akron native, grew up with parents who both loved to cook. For her, a rare treat
was having a TV dinner for supper.

Wheeler tries to show adults who don’t cook how food can enhance their connection with
family.

“I had a woman cry recently in one of my classes,” she recalled. “She said, ‘Hearing you
talk, I realize I’ve done all three of my daughters a terrible disservice’.”

The woman was sending her three college-age children into the world without the basic skills
of how to cook for themselves, Wheeler said — but, more important, she realized that she had missed
the opportunity to connect with her daughters and create traditions through food and cooking.

In her quest to learn to cook, Bartlett has explored most of the obvious options.

She has asked for advice and recipes from family and friends who are skilled cooks, and she
received more than one cookbook as a college-graduation gift.

Bartlett is essentially teaching herself by doing and practicing.

She gets together with a friend every Thursday, she said, and they work their way through a
new recipe each week. For help, she relies on her computer as well as phone calls to friends and
relatives.

“I had to look up the difference between dicing and mincing,” she said. “And I need to
actually figure out what a pinch is. It’s things like that, I find useful to look up online.”

The scientist in her approaches recipes literally, which can trip her up from time to time.

“With browning, how brown is it supposed to be? Is it just when it initially changes color or
should it get really brown? And sauteing, I can move things around in the pan, but I don’t know if
I’m actually doing it right.”

Through her trial and error, however, Bartlett is discovering a personal taste.

“I’m finding that I like things more highly seasoned,” she said, “and I like a lot more
vegetables than I thought I did.”

Perhaps most important, she is discovering that cooking is easier and more enjoyable than she
had expected.

“I’m liking it more, so it doesn’t seem like so much work.”


 
labraham@dispatch.com

 
@dispatchkitchen