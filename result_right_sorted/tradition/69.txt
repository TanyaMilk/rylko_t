FRANKFURT—The euro is a symbol of European unity. But the euro symbol is falling apart and it will cost a lot of euros to fix.

In the center of the eurozone’s financial capital stands a 46-foot-tall icon of the common currency. And like the currency itself, the sculpture faces a crisis.

Light bulbs need to be replaced inside the symbol, which consists of a giant blue “€” spangled with 12 yellow stars. Bumper stickers mar its exterior. Vandals recently painted one star red. 

The euro-statue’s troubles are even upsetting tourists. An Austrian visitor to Frankfurt recently wrote an open letter to the mayor in a local newspaper saying he was “appalled” at the “miserable” condition of the sculpture. “The sculpture is more than a simple structure. It stands for the currency and with that the future and hopes of over 300 million people,” the letter said. 

No doubt: The euro symbol needs a bailout.

Like euroskeptics who scorn the common currency, some locals are fed up with the statue. 

“I think we should just tear it down,” says Oliver Reese, artistic director of the Schauspiel Frankfurt theater, looking at the statue from the balcony of his office. The longtime critic of the sculpture wrote an article in a local paper more than three years ago saying the symbol should be sent to a museum. 

“It is shameless. It glorifies money and mocks the poor,” says Matthias Altenburg, a local writer of crime novels. “If no Moses comes to destroy this golden calf, then we should blow it up in the air, ground up the debris, put it in a bag and present it to the artist at his front door.”

Perhaps not surprisingly, the artist disagrees. “I think that it should stay where it is,” says euro-sculptor Ottmar Hörl. “My idea was to do an advertisement for the euro, for the European Union,” says the German artist. “It wasn’t anything more.” 

The European Central Bank is largely quiet on the future of the symbol. President Mario Draghi and his predecessors for 13 years gazed upon the object from ECB headquarters in a building dubbed the Eurotower. But when the bank recently moved to new premises outside the city center, the sculpture stayed behind. 

That is because the ECB doesn’t own the sculpture, although it pitches in an undisclosed amount of euros for its upkeep. “We will continue to support the maintenance of the euro sign sculpture,” an ECB spokeswoman said. 

The ECB plans to put banking supervision employees in the Eurotower. “This decision means that the ECB will maintain its link with the Eurotower in Frankfurt, together with the large euro symbol in front of it, which has become something of a landmark not only for the ECB but also for the city of Frankfurt,” it said in a 2013 statement. 

The sculpture is owned by a civic group called the Frankfurt Culture Committee, which also organizes tourist-information posters around the city. 

Its chairman, retired Deutsche Bank historian Manfred Pohl, says his group generally spends between €15,000 and €30,000, or about $16,200 to $32,300, annually to repair and light the symbol. But while the euro-currency’s value has recently fallen, the euro-sculpture’s upkeep cost is rising.

Replacing traditional light bulbs with more efficient LED lights and other repairs could run to €60,000 this year, Mr. Pohl says. That could exceed half the Culture Committee’s entire annual budget.

To prop up the euro sculpture, he is asking Frankfurt banks to ante up. The committee’s website dangles an offer to “Become a sponsor of the Euro sculpture.” Donors who give over €1,000 will get their name on a plaque, the website says. It may be working. The Austrian visitor said in his open letter that he was making a contribution and challenged the city to do likewise. 

City officials think the sculpture should stay where it is or move to the new ECB headquarters. But it isn’t opening the municipal wallet. “At this point, no city funds are planned,” says spokesman Mark Gellert. 

The sculpture was always envisioned more as marketing than art. It went up in 2001 as the European Union was preparing to roll out euro coins and bank notes. Like the larger Eiffel Tower in Paris, which also sparked outrage at its erection, the euro symbol was initially only intended to stand for only a few months. But in 2002 the city allowed it to stay longer. 

Mr. Hörl, the artist, had already installed a euro sculpture in one of Frankfurt’s best-known institutions, its giant international airport. The smaller version was removed in 2012 to make way for a rail line. The airport has “no plans to put it back,” a spokesman said. 

Mr. Hörl says he isn’t bothered by his handiwork’s link to lucre. After all, he notes, money is part of Frankfurt’s history. He prefers to focus on civic pride. “Frankfurt has always been a trading city,” he says. “It’s the city of banks.” Indeed Frankfurt’s history with money, and even money printing, goes back centuries. In 1555, the city received an “imperial privilege” to mint coins. 

Today, the euro-symbol statue may be losing its symbolic power. When anti-capitalist protesters recently ran rampage across Frankfurt, burning police cars and smashing windows to mark the ECB’s official move to its new headquarters, the sculpture was barely touched though the police guarded it thoroughly. By contrast about three years ago, protesters camped out around the sign for months. 

Mr. Reese, the euro-sculpture skeptic, says that today the only people who pay it any attention are tourists and “journalists who report about money and the ECB” and need a good visual. 

Indeed it may be those tourists that will keep the sign there for the duration. “I’m in favor of [the] euro and I like the sign here,” said Marek Kwiek, a professor from Poland visiting Frankfurt on a rainy April morning. 

 Write to Todd Buell at todd.buell@wsj.com