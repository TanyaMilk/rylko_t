Moments before Xi Jinping stepped onto a stage in 2012 for a ceremony that introduced him as China’s new leader, he told the man who would be translating his words into English to tweak the meaning of one line so that it would be more understandable to a global audience.

The phrase in Chinese could be literally translated as “to be forged into iron, the metal itself must be strong.” But that translation loses an essential part that Chinese speakers would already understand: the sense that someone is working the iron to begin with. The final translation read, “if you want to work with iron, you must be tough yourself.”

Chinese translator Huang Youyi this week recalled the story – and the resulting controversy – to illustrate how Mr. Xi is a leader who understands that “what we do here [in China] has some kind of effect on the world” — and therefore requires explanation.

The government translator has had an important role in shaping another big international communication initiative: “Xi Jinping: the Governance of China.” Mr. Huang was a senior English translator for the compilation of 79 speeches and interviews by the president that China calls a best-seller and will be officially launched in the U.S. next month at a book fair in New York City.

“Chinese President Xi Jinping realizes we have to speak out,” according to Mr. Huang, a 61-year-old who has had several official roles including doing translation work for each of the past three Chinese presidents, and who is now secretary general of the Translators Association of China.

While Mr. Xi “uses a lot of traditional idioms,” the translator says, the president’s desire to be understood overseas creates new opportunities for creative license to massage the message for foreign ears.

Now, Mr. Huang is calling on more Chinese companies to – like Xi Jinping does – consider how to improve their global reach. Their English websites too often today tend to read, as he put it, like too much “blah, blah, blah.” He said, “They know how to speak to 1.3 billion Chinese people but do not know how to speak to the [world’s] other 5.7 billion people.”

Mr. Huang spoke Thursday on the sidelines of a conference in Shanghai about his career and the book that officials hope will be the most widely read work by a Chinese leader since the publication of Mao Zedong’s Little Red Book. Here is an edited transcript.

How did you become a translator?

After middle school in the mid-1960s, universities were closed. Like millions of other Chinese [at age 16] I was sent to an army farm in the northeast in Heilongjiang province. I went and I was prepared to die on the battlefield. The previous winter there was a border clash between Chinese and Russian troops.

Then [two years later] universities began to open. I was very lucky. It was not my choice. My team leader said, “you know, universities are recruiting. You should go back to study.”

That’s how I went to Beijing Foreign Languages Institute. I got to the school and they told me “you are among 160 people assigned to the English department.” At that time you had no right to decide what to do.

In my last year of university [1975] I was in classroom practice. We were translating U.N. documents because U.N. documents had been sitting for years and years, not translated into Chinese.

One day the teacher was reading some of the papers we did. And this one sounded very familiar. I realized it was my piece. Hey, I could translate.

How has translation in China evolved?

For so many years, we were translating everything from foreign languages into Chinese: economics, technology, management, things like that.

But in 2011, suddenly the figures showed that 54% of the job is translating from Chinese to English. [Subsequently, the figure had risen toward 60%, according to Mr. Huang’s research associates.] One [reason] is the Chinese companies are exporting more, like Huawei. That did not happen 10 years earlier. From a professional point of view that represents the rise of China’s economy. … But we need to understand the world better so we need to keep translating from foreign languages into Chinese.

Does it make sense for foreigners to study Chinese?

If you really want to understand Chinese philosophy, the way Chinese people think, the best way is still to learn the language.

Having said that, if there’s only one language in the world left it’s not Chinese. It’s English.

What are the differences between oral and written translation?

As a young person you get a lot of opportunities to do oral translation, simultaneous translation. But as you get older you work more on the written part.

How is it translating for state leaders?

I covered Jiang Zemin, Hu Jintao. That kind of thing is easy because when leaders meet, they talk about principles. They talk about policies. That’s the kind of thing, as a translator, you are familiar with.

Looking back, it is not difficult to translate the state leaders. It is difficult to translate scholars.

How did you end up working on Xi Jinping’s book?

I got in at the later stage of the translation when the rough translation was already done. We work in four processes: First, rough translation. And then we submit this to foreign editors, native speakers, to improve the translation. Then we come in, senior translators. The English looks good but it’s too far away from the Chinese. And then another senior Chinese translator comes in to check it. I joined the third stage after the foreign editors have edited it. I was the third and fourth person. I didn’t do the whole thing. There were a couple of us working on it. But everyday we got together, not only us but also the foreigners.

At one point, everyone had finished but we were not happy with the result. So we put it on the screen. Finally we realized what the difficulty is. In Chinese we tend to use a lot of adverbs: “we comprehensively do this,” “we actively do that.” If it is word to word with the Chinese but it doesn’t read well in English, what can we improve? Finally we decided to take away all the adverbs.

Did you meet Xi Jinping during this translation?

Not on this book but I’ve met him a couple of times. He doesn’t know I’m working on this book.

Was he consulted on the translation?

Not by me. Some people talked with him.

What did you learn about the president’s character from this work?

As a translator, no matter who you translate, you try to get the feeling that you are him speaking. Then you can do a good translation.

When [the president] talked about the countryside, that’s where I put in myself. He was in the countryside. I worked on a farm. I could even imagine myself looking at him eye to eye. Because we were born in the same month, same year, in our early life, we had similar experiences. Going to school then going to work on a farm. Learn from the farmer. Went through a lot of hardship.

How well does Xi Jinping speak English?

I don’t think he has a very strong command of English. Li Keqiang has good English.

Some government statements that may sound right in Chinese don’t sound good in English, such as “comprehensively,” “unswervingly” and the “three represents.” What’s the reason?

In English it sounds better if you take out all the adverbs. But you read the Chinese, you take out the adverbs and the Chinese is not Chinese anymore. It’s a cultural difference. One of the things we should be trying to do is, when you are trying to translate from Chinese into English, take out all the adverbs. But when translating from English into Chinese, add adverbs.

When I first joined China Language Press, we were translators. But who were our readers? Professors, scholars who studied China. China watchers.

But now we are trying to reach 5.7 billion foreigners out there who have some kind of interest in China. They are not China experts. We should make our translated word more targeted toward their reading habits, their level of understanding China.

We should be even bolder as translators to change some of the Chinese way of speaking. For example, “comrade.” I think we should say, “dear colleagues.” Within the party, within government, we should translate into “dear colleagues.”

But if Xi Jinping goes to the countryside, meets farmers, you know, “dear colleagues” wouldn’t be the right way to do it. What we can do is say, “fellow countrymen.” That’s my proposal. It’s not accepted by the translation field. But there are other things we should be trying.

Have you made this kind of adjustment in Xi Jinping’s book?

If you read this book, you will find somewhere where he speaks and he uses the word “comrade,” now we have translated it into “friends.” In the future, probably you will see less words like “comrades.”

– James T. Areddy. Follow him on Twitter @jamestareddy

Sign up for CRT’s daily newsletter to get the latest headlines by email.

For the latest news and analysis, follow @ChinaRealTime