Illegal   immigrant students and members of the House sued the Senate   this   week  to try to overturn the upper chamber’s filibuster rule,   arguing   that  the 60-vote supermajority requirement violates the   Constitution  and  is  blocking important legislation such as   legalization for  illegal   immigrants.

If successful, the lawsuit, filed Monday,  would rewrite  the way the    Senate operates — though courts generally have been  reluctant to  meddle in internal congressional rules.

Still,  the  effort mixes two thorny issues in separation of powers   and   immigration,  and is likely to keep the Dream Act at the forefront   of  the  national  debate. The bill would legalize illegal immigrant    students and  young  adults who were brought to the U.S. as children    and who are seen  as  among the toughest cases in the immigration    debate.

In late 2010,  the Dream Act passed the House but was  blocked from   action  by a  filibuster in the Senate. The 55-41 vote  fell five shy of   the 60  needed  to overcome a filibuster.

“The  filibuster is exactly that — it’s a  rule that’s crippled our   system   of government. Undocumented youth,  perhaps like no other  group,    understands about the legislative  process,” said Caesar  Vargas, an    illegal immigrant who went through  college and law school  and could    benefit from the Dream Act  legalization bill. “We have  lived it; we   have  shed tears for it. And we  have seen a minority  able to cripple   dreams.”

The filibuster is  not found in the Constitution, but  rather is a   Senate  rule that stems  from its tradition of extended  debate.   Lawmakers used to  use that  extended debate privilege to talk    legislation to death, so the  chamber  came up with the “cloture” rule, which, after several changes,  now allows  debate to be cut off if 60 of    the 100 senators vote to do so.