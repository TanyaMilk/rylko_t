For the past three months, Darcy Baxter has spent many of his lunch hours channeling his inner Paul Bunyan.

Yet the midday activity doesn’t require flannel or an ax.

The 28-year-old program supervisor for the Westerville Parks and Recreation Department has been teaching himself since December how to logroll — to run in place on a log in water — at the Westerville Community Center indoor pool.

Previously, central Ohioans could see demonstrations of the sport — more common in northern states — at special events such as the Ohio State Fair.

Beginning next month, though, the Westerville center will offer the public a chance to test such a skill through logrolling classes.

“We see it as an opportunity to be a trendsetter, innovative,” Baxter said. “You can look at it as a sport or a fitness activity, or both.”

Logrolling dates from the turn of the 20th century, when men hired to ward off logjams on rivers learned to roll on the spinning logs to stay dry — and safe.

Loggers began to challenge one another to see who could last the longest.

The first unofficial logrolling championship took place in 1898 in Omaha, Neb. — and such a contest has since become a staple of lumberjack and outdoor-oriented competitions, according to various news and national logrolling organizations.

Logrollers historically used pine or fir before moving on to the faster, more buoyant red cedar.

Wanting to host lumberjack games at the community center, Baxter hoped to include logrolling.

He had no idea, though, how he would get a solid-wood cylinder weighing hundreds of pounds to Westerville — let alone one that could be used safely by all ages in a pool.

Then he learned about Key Log Rolling, a Minnesota company that produces synthetic rolling “logs” from polyethylene, a type of plastic.

A Key Log weighs only 65 pounds (when not filled with water), making it easier to move in and out of the water, and to store.

Plus, the log includes three “trainers” — or paddle wheels — that slow and stabilize its movement for beginners. The training wheels are placed at the ends and middle of the log, and removed as a logroller becomes more advanced.

“We’ve built in this progression of learning,” said Abby Hoeschler, president of Key Log Rolling.

“Without the trainers, teaching someone to logroll is like teaching them to ski by sending them down a double black diamond.”

Her logrolling family created the Key Log to grow the sport in areas of the United States besides the North and make it more accessible to people by adding it to the already-structured programming at summer camps, college campuses and community centers.

(The RPAC Aquatic Center at Ohio State University — which bought two Key logs last year — is developing its own logrolling programs for students and the public.)

After he discovered the Key Log, Baxter decided to ditch the idea for the lumberjack games and provide the log for everyone to use.

City employees were eager to try out the log.

David Chambers, business-development manager for Westerville Parks and Recreation, understood the issue of balance but didn’t expect the physicality of the sport.

“It’s a lot harder than it looks,” said Chambers, 35. “It’s not just the balance but the physical strain it puts on your lower body, your core.

“I got tired really quickly.”

Still, having rolled the log only a few times, he has gained a better understanding of how it moves and what he has done wrong when he stumbles.

He hopes to reach a point that he could challenge someone.

In traditional logrolling, two competitors try to knock each other off by changing the speed and direction of the log.

“People have asked, ‘Are you a lumberjack now?’  ” Chambers said. “It’s surprising that most people are excited to hear about it, even though they poke fun at it. They want to know where they can see it. It’s not something you hear about all the time, especially around here.”

The adult logrolling class in April, with four sessions, filled up in less than a day — a fact that Marisa Akamine, Westerville clerk of courts, lamented.

Having tried logrolling earlier this year during a citywide wellness initiative, Akamine was excited about taking the class.

“You get up there and think you can do it, and then it knocks you off, but you want to get right back up there,” the former gymnast said. “I wanted to master the log and not have the log master me.”

In the future, the center plans to add more classes and drop-in sessions.

The classes, Baxter said, will start with a short lesson in safety, as in how to “bail off” the log correctly.

“You’re not falling belly-smacker or headfirst,” he said. “You actually just step off.”

Novice logrollers will hone techniques before challenging one another in the final session.

Baxter hopes that students and others who try logrolling at the center will find as much novelty and enjoyment in the activity as he does.

“It’s a nice little challenge and not too hard on the body,” he said. “You don’t have to worry about tearing your ACL or getting a concussion.

“It has become a little passion of mine.”

@AllisonAWard