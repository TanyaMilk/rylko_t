For some couples, daring to be different means swapping out a classic wedding cake for a modern day dessert bar.

Others, however, definitely aren’t afraid to think outside of the box.

We’re talking way, way outside of the box!

We asked a handful of wedding guests and planners to dish the deets on the most unconventional wedding themes they’ve ever seen.

From dinosaurs to Dungeons & Dragons, we’ve officially heard it all now.

 

 

 

“When I was younger I was the flower girl in my parent’s friends’ NASCAR-themed wedding. The bride and groom were both really into racing and had me throw hot wheels out as I walked down the aisle instead of flowers. The bride nearly tripped on one of the cars!” — Dawn

 

“When I worked in catering we had a Lilly Pulitzer-themed wedding and literally everything you could imagine was pink and green. The getaway car was even a Lily-wrapped Jeep!” — Francesca

“I recently worked on a farm to table wedding that was completely out in the middle of a field. There were no floral decorations, only farm fresh fruits, garlic, vegetables and herbs. The guests had to bring their own dinner plate, wash it after they ate and have the bride and groom sign it before they left as a momento.” — Paula

“While the couple wore traditional wedding attire, their décor was full on Dungeon & Dragons, including personal crests on 13-feet tall banners set all around the ceremony site. When guests moved into the reception they were greeted with a seating chart right off the board game and then had to locate their table, which may have easily been the three-headed monster! And, of course, there was a dragon wrapped around the wedding cake.” — Courtney

This article originally appeared on BRIDES.