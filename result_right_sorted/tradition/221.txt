When most people look at a pain au chocolat, they see an indulgent breakfast treat, but award-winning pastry chef (and, yes, creator of the Cronut) Dominique Ansel sees room for improvement.

“It’s never enough chocolate,” says Ansel, who is opening a new bakery, Dominique Ansel Kitchen (137 Seventh Ave. S.), on Wednesday. “I thought it would be fun to do a 2.0 version.”

Nearly everything at his new place, including the chocolate croissant, is finished to order, allowing Ansel to make far fresher pastries than the ones slowly hardening in cafes around the city.

The classically trained Paris native says traditional French pastries were long overdue for a makeover.

“People have been doing the same thing for centuries,” says Ansel. Have a look at all that goes into his revolutionary chocolate croissant.

The chocolate shards aren’t put on until after baking, allowing Ansel to use high quality, 66 percent Valrhona chocolate, instead of a baker’s chocolate that would require additives to hold its shape while in the oven.

Ansel first melts the chocolate and spreads it on a sheet to set. It’s then broken into shards.

The 2.0 chocolate croissant has far more chocolatey goodness than the original version. “It’s not a croissant with a little bit of chocolate,” Ansel says with a chuckle.

“It takes three days [to make], just like a Cronut,” says Ansel. “A day to mix the dough, a day to let it rise and incorporate all the butter, [and] a day [to] shape it and bake it.”

The dough starts with a simple mix of flour, water, and levain — a homemade leavening agent. “You start with a mix of flour and a bit of sugar and water, and you let it ferment, and you keep on feeding it, adding flour every day,” explains Ansel. He started his “mother” levain when he opened Dominique Ansel Bakery in 2011, and he’s been feeding it ever since.

Ansel uses Beurremont butter from Vermont. It contains 83 percent butterfat and, says Ansel, “it’s very comparable to French butter.”

The croissant dough is formed into a cylindrical shape and baked fresh daily. When the pastry is ordered, the top is cut off, revealing its interior. “We can see what we call the honeycomb, the nice structure of the croissant,” Ansel says.

Small cubes of butter flavored with orange zest and orange blossom are dotted on top of the croissant before it’s warmed up. 

They’re meant to melt on top of the croissant, imparting a subtle citrus flavor. “[It’s] just to add a little bit of freshness, a tiny bit of acidity,” says Ansel.

“Time is an ingredient,” Ansel says. With his pain au chocolat, the croissant portion is baked in the morning, but the whole treat isn’t assembled until someone orders it.

Then the top of the croissant is cut off and small cubes of orange-blossom butter are placed on top of it. It goes back in the oven to toast and melt the butter — “the outside is hot [and there’s] a nice crunch,” says Ansel.

Finally, delicate chocolate shards are layered atop the buttery bread. As complicated as it sounds, Ansel reveals the whole operation takes less than 90 seconds. “It’s pretty quick,” he says.

Maldon sea salt is sprinkled on the chocolate shards before they set. “It’s always good to add a tiny bit of salt,” says Ansel. “It goes very good with the butteriness of the croissant, the freshness of the orange butter.”