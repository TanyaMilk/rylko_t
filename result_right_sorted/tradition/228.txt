The United States is one of the most diverse and beautiful places on Earth.

From the sweeping coastal vistas of Big Sur, Calif., to the breathtaking drama of Niagara Falls, N.Y., the country contains some of the world’s most incredible natural wonders.

Here are the most spectacular locations in all 50 states:

Spanning more than 6 million acres and home to Mount McKinley, the continent’s highest peak, the stunning park and preserve has become Alaska’s most popular attraction. The picturesque landscape is home to bears, wolves, moose and caribou, as well as dozens of dinosaur fossil footprints.

With an average depth of 4,000 feet and running for 277 miles, the Grand Canyon National Park draws millions of visitors from across the globe each year. Pictures don’t do it justice. This place should be on everyone’s bucket list.

With multiple campsites, 80 miles of hiking trails, three visitor centers, and picnic areas, there are many ways to get up close and personal with these incredible wonders.

The park boasts 359 miles of trails, more than 150 mountain lakes, glaciers, ski runs, incredible wildlife, and multiple waterfalls. The park’s most famous attraction is the Trail Ridge Road — the road at the highest elevation in the country.

The tracks were believed to have been made by a Dilophosaurus — a three-toed, raptor-like theropod that was a fierce predator during the Jurassic age and stood about 8 feet tall and grew as long as 20 feet. The park and visitor center are open all year round.

It is home to more that 150,000 ducks and geese that stop here during October and November on their way to their northern breeding grounds. As of 2009, the refuge management programs recorded 302 species of birds living in the reserve. The park has a popular visitor center, multiple observation towers, nature trails even an auto-tour route.

It is also home to more than 1,000 species of plants, 20 percent of which are not native to the area. The park provides visitors with a multitude of activities, including boat tours, camping, hiking, canoeing, biking, and even a tram ride.

It is the origin of the St. Mary’s River and the Suwanee River, the latter of which channels 90 percent of the swamp’s water down to the Gulf of Mexico. The vast majority of the swamp is a National Wildlife Refuge, and is home to various amphibians and reptiles, including toads, frogs, lizards, turtles, snakes and alligators. It is also the natural habitat of the Florida black bear.

Visitors to the island are regularly able to view active lava flows from Mount Kilauea (depending on the level of activity), as well as explore the cratered summit. Mauna Loa, also on the Big Island, is the world’s largest volcano and climbs 30,000 feet from its base on the Pacific Ocean floor, making it taller than Mount Everest.

On the island of Maui, there is the Haleakala National Park, where visitors can drive to the summit of the Haleakala volcano — a popular location to watch the sun rise or set from above the clouds.

Idaho is also one of the few places you can witness a “fire rainbow,” where the sun reaches more than 58 degrees above the horizon and ice crystals in the atmosphere refract the light into a stunning rainbow.

More than 13 miles of trails lead to clifftop lookout points and picturesque waterfalls. For the less energetic visitor, there are trolley tours and a wine-tasting room where you can sample the locally produced offerings.

Designated a National Natural Landmark in 1972, the Wyandotte Caves also incorporates Monument Mountain — believed to be the world’s largest underground mountain, at more than 135 feet. The caves are home to nine species of bats, including the endangered Indiana bat.

The flat plains of Iowa are the perfect place to witness the Armageddon-esque Asperatus Clouds. These frightening-looking formations were only given their own classification in 2009, and look like waves flowing through the sky. (The clouds’ full name, “undulates asperatus,” actually means agitated wave.)

The clouds are created by an elevated layer of warm air over a layer of cooler air, but the specifics of the clouds’ development and evolution is still under discovery. Regardless, they make for some very impressive skylines.

The largest measures more than 27 feet in diameter. The rocks have been a meeting place for Native Americans and early pioneers, such as John C. Freeman.

Vast rock chambers are connected by a complex maze of passageways, some just big enough to crawl through, and archeological finds in the depths of the caves show the passageways have been used by humans for thousands of years.

Boat tours can take tourists on an adventure through the waterways where they will encounter endless wildlife and hear stories of Bigfoot, pirates and nature’s power. Bayou Bartholomew is also the world’s longest, at just over 375 miles. The region is truly one of the most unique landscapes on earth.

The Desert 0f Maine, a seemingly out-of-place 40-acre attraction, was uncovered over 200 years ago as a result of man-made erosion. Created by the slow movement of Ice Age glaciers, this mini desert even has its own microclimate, with temperatures on the sand becoming much hotter than surrounding wooded areas.

These feral ponies, according to local legend, are said to have been originally left on the island following a shipwreck off the coast which left them stranded. The state park, taking up more than 800 acres, contains 350 campsites and is home to more than 300 species of birds — particularly waterfowl — making it perfect for wildlife lovers looking for a beachside getaway.

Extending from Cape Ann to New Hampshire, this huge expanse of marshland extends more than 20,000 acres and includes barrier beaches, a tidal river, an estuary, a mudflat and several uplands islands. Experts in the region, an important area for bird life, are focused on preserving the many species of breeding and migratory birds who call the marsh home.

Voted “The Most Beautiful Place in America” on ABC’s “Good Morning America,” the dunes provide incredible views of Glen Lake and play host to the time-honored family tradition of sand skating — where people attempt to climb the dunes at speed before sliding back down again. Trails, paths and campsites surround the picturesque area so visitors can stay and enjoy the beautiful beaches and warm inland waters.

The area is world-renowned for canoeing and fishing on its many lakes, making it the most visited wilderness area in the whole United States.

There these gigantic trees, some more than 15 feet in girth and 100 feet tall, became petrified, turning them — seemingly — to stone. Visitors can walk the 13 miles of hiking trails and visit the museum featuring petrified wood from all over the globe, as well as dinosaur footprints and other fossils.

Some of them stand more than 20 feet tall and weigh more that 600 tons. Visitors can hike the trail that leads between these giants through the Elephant Rock State Park, or grab lunch at one of the parks 30 picnic sites.

In summer, the 700 miles of hiking trails treat adventurers to breathtaking views of the landscape and the chance to see some amazing wildlife, including bears, mountain lions, mountain goats, deer and eagles. But during the winter, the park transforms into an icy wonderland where most of the lakes freeze over, allowing visitors to ski or snowshoe through this spectacular national park.

Made from clay, volcanic ash and sandstone, it is believed to have been created as the harder sandstone protected the pillar as it broke away from the nearby retreating cliff line.

Made from bright red sandstone, the rocks seem to glow in the sunshine. The park’s highlight is the Fire Wave — a curved wall of layered rock accessible via a mile-and-a-half hike through the red rocks. Look out for rattlesnakes and kangaroo rats, which populate the area.

Located in the White Mountain National Park, the mountain is a popular hiking, hang-gliding and skiing area, where visitors can ski as late as Memorial Day.

The river valley is now protected by the Delaware Water Gap National Recreation Area, where visitors can go rafting, canoeing, fishing, swimming, hiking and rock-climbing in a beautiful landscape surrounded by wildlife.

The fleeing swarm is an impressive phenomenon, with the bats flooding the sky for as long as three hours as they corkscrew through the air in a giant group. Visitors can also experience the caverns’ historical cave chambers — some of the biggest in the world.

The three waterfalls that make up the collection — Horseshoe Falls, American Falls and Bridal Veil Falls — create the highest combined flow rate of any waterfall in the world. Said to be the unofficial honeymoon capital of the world, the falls attract more than 25 million tourists a year, making it one of the most visited attractions on earth.

The Big Pinnacle, nicknamed “the Knob,” sits atop the mountain like a hat, and has colorful bare rock sides with a vegetation-covered rounded top.

It is home to wild bison, feral horses, elk, sheep, deer, ferrets, and prairie dogs, as well as 186 species of birds.

The spectacular rock formations have created caves, gorges, waterfalls, and cliffs, making for an exceptional place to camp, hike, gorge walk, bird-watch and go spelunking. There are more than 200 campsites throughout the park, most of which have electricity, and the park recently installed a zip-line attraction.

Millions of years ago, the plains of Oklahoma were flooded by the ocean, which eventually became cut off and evaporated, creating the Great Salt Plains. The 11,200-acre park is now a wildlife refuge, protecting migratory birds that use the plains as a stopover. Visitors flock to the park to swim in the lake, fish, canoe, hike, mountain bike, or even horse trek the miles of trails.

Best seen an hour before high tide to an hour after high tide, the waves put on an impressive and powerful display of nature. Visitors are warned to stay well back from the unpredictable and forceful natural phenomenon.

This incredibly rare and breathtaking sight is thanks to the lack of light pollution in the area. Twice a year, the park hosts “star parties,” where novice stargazers and professional astronomers marvel at the incredible skyscape.

The cliffs also feature a steep stairway with more than 140 steps to the sand below.

It is also one of the highest remaining deciduous forest canopies in the world and is home to armadillos, bald eagles, bobcats, feral pigs, opossums and a multitude of amphibians.

Sable-toothed tiger, aquatic rhino, and even ancient camel remains have been found in the region, and the park has been used by Native Americans as a hunting ground for more than 11,000 years.

Scientist are baffled as to why this incredible species performs this way, but for visitors, it makes for a special viewing opportunity. The synchronous firefly is the only species in America that can synchronize its flashing patterns. The insects live as adults for only around 21 days.

It is the largest pink granite rock in the United States. What makes it so special is the fact it is an “exfoliation dome,” meaning it is made up of onion-like layers below the surface.

The red desert is scattered with more than 2,000 stone arches as well as many other individual rock formations. Although rock climbing is prohibited on the formations, there are plenty of other places designated for climbing throughout the park, and other recreation activities include camping, hiking, biking and off-road auto tours.

The Burlington Burled Forest is a small patch of woods located on a sandy bluff over the North Beach where Mother Nature has truly left her mark. Each tree in the woods sports burls (large bumps) all over their trunks.

These burls were created by the fierce winds off Lake Champlain, which manipulate the fast-growing box elders into these warped and gnarly natural wonders.

Designated a Historic National Landmark, the bridge was a sacred site for the Native American Monacan Tribe, which has lived in the area for centuries. Visitors can hike the trail that leads under the bridge and heads towards the Saltpeter Cave.

It is the highest peak in Washington and third-highest in the U.S. The mountain is popular with glacial climbers, who take up to three days to reach the summit. Lower down, its slopes there are great opportunities for hiking, skiing, and camping.

Huge boulders, overhangs, deep crevices and cliffs are surrounded by stunning woodlands throughout the 110-acre park. Only open from April to October, the park offers visitors a picturesque area to hike, picnic, camp, and bird-watch.

In the winter, sea caves become frozen inside by suspended waterfall ice, creating a sparkling jagged wonderland. When the conditions permit, the caves can be hiked to, and some years the lake freezes solid, allowing the adventurous to skate or snowshoe from island to island.

The largest hot spring in the state and third-largest in the world, this incredible attraction produces a striking rainbow-colored display — created by pigmented bacteria growing in the warm spring waters. Truly eye-catching.

This article originally appeared on Yahoo Travel.