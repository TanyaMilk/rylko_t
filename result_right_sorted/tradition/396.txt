Befitting a game best known for Mad Hatter dimensions and a deep appreciation for all things pirate and/or windmill, the National Building Museum’s minigolf exhibit is by turns whimsical and thought-provoking, kitschy and surreal.

It’s also brutal on visitors’ scorecards.

“I don’t know if anyone has shot par,” said Holly Wiencek, a Building Museum intern. “Everyone I’ve heard from is way, way over.”

Surprise. Delight. A backdoor architectural education. Relief from Washington’s soggy summer heat. Many, many mulligans.

Such are the elements of the museum’s newest attraction, an indoor, fully playable 12-hole minigolf course designed and built by local architects, construction firms and urban planners.

Eschewing traditional minigolf tropes — think fanciful castles, papier-mache dinosaurs and scale replicas of the Egyptian Sphinx — the exhibit features a series of unique concepts, including one hole shaped like a skateboarding half-pipe and another inspired by the glowing innards of a smartphone.