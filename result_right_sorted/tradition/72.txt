Political parties in Hong Kong could be forced to find new manufacturers for their Lunar New Year products after mainland authorities seized thousands of rolls of toilet paper printed with the face of the city’s chief executive.

The city’s Lunar New Year market, held each year in the week run-up to the holiday, is a prime fundraising and marketing opportunity for local political groups. Politically-themed items have become increasingly popular at the market in recent years as the city’s political climate has heated up, jostling for attention against sellers of more traditional festive products.

At this year’s event in Victoria Park, pro-democracy political parties flogged an array of goods inspired by the so-called Umbrella Movement, in which tens of thousands took to the streets and occupied main roads last year from September to December.

Heightened political sensitivity in Hong Kong and mainland China this year has forced the League of Social Democrats, one of the city’s more left-leaning parties, to turn to manufacturers in Taiwan and Hong Kong instead this year to make its goods.

“It’s a big risk in particular for the manufacturers on the mainland,” said Avery Ng, vice-chairman of the LSD. “I mean, the worst that can happen for us is that we get our goods confiscated.”

The party, headed by lawmaker Leung Kwok-hung, or “Long Hair,’ is this year selling t-shirts printed with the phrase “Civil Disobedience” and the faces of Martin Luther King Jr. and Nelson Mandela. Mr. Ng said the party sourced the T-shirts from Taiwan, the first time they had done so.

“We used to print T-shirts with LSD logos on them in China, when the political climate was less heated,” said Mr. Ng, adding that the party will try to source more products in Hong Kong in the future.

The Democratic Party is another pro-democracy party with a big presence at the Lunar New Year market this year. Mainland Chinese authorities recently confiscated more than 7,000 rolls of toilet paper printed with Hong Kong leader Leung Chun-ying’s face, according to the party’s chief executive Lam Cheuk-ting.

Instead of toilet paper, the party is offering hand towels printed with Mr. Leung’s face with wolf ears, a nod to his opponents who often liken him to a wolf.

– Isabella Steger

Sign up for CRT’s daily newsletter to get the latest headlines by email.

For the latest news and analysis, follow @ChinaRealTime