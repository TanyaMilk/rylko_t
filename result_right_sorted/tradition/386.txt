LOS ANGELES — Dick Clark, the ever-youthful  television host and producer who helped bring rock ‘n’ roll into the  mainstream on “American Bandstand” and rang in the New Year for the  masses at Times Square, has died. He was 82.

Spokesman Paul  Shefrin said Clark had a heart attack Wednesday morning at Saint John’s  hospital in Santa Monica, where he had gone the day before for an  outpatient procedure.

Clark had continued performing even after he suffered a stroke in 2004 that affected his ability to speak and walk.

Long  dubbed “the world’s oldest teenager” because of his boyish appearance,  Clark bridged the rebellious new music scene and traditional show  business, and equally comfortable whether chatting about music with Sam  Cooke or bantering with Ed McMahon about TV bloopers. He long championed  black singers by playing the original R&B versions of popular  songs, rather than the pop cover.

Ryan Seacrest, who took over  main hosting duties on the countdown show from Clark after years of  working beside the legend, said in a statement Wednesday that he was  “deeply saddened.”

“I idolized him from the start, and I was  graced early on in my career with his generous advice and counsel,”  Seacrest said. “He was a remarkable host and businessman and left a rich  legacy to television audiences around the world. We will all miss him.”