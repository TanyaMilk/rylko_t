Dressed in a black military jacket, black skinny pants and cherry-red Doc Martens, North West traipsed through Paris Charles de Gaulle Airport like a sullen starlet last week.

The only clue to the adorable tot’s age — besides her small size — was the glittery “Frozen” wheelie bag she dragged behind her.

“She has a signature look: sophisticated rocker chic,” says Fraser Ross, founder of the hip Los Angeles-based Kitson store chain, a Kardashian family favorite.

At just 21 months old, Kim Kardashian and Kanye West’s daughter is already a fashion influencer — with two blogs and a Pinterest board chronicling her every outfit.

Her moody wardrobe tends toward dark, urban ensembles that are the antithesis of princess dressing.

Nori, as she is called, toddles in rough-and-tumble Doc Martens and Timberland boots and slings a $1,450 miniature Saint Laurent studded black leather bag across her baby belly.

She’s always carefully coordinated with Mom, so family photos look art-directed and Instagram-ready.

“[Kim and Kanye] are always going to push the envelope. It’s part of their family culture,” says New York-based stylist and fashion editor Jennifer Smith of North’s attention-grabbing wardrobe.

North ditches girlie get-ups for slick black, gray, white or army-green separates, faux-fur coats and vests, concert tees (from Daddy’s Yeezus tour), biker or MA-1 bomber jackets and a mix of designer leather and affordable Kardashian Kids pleather from Babies ‘R’ Us.

“She’s Kim Kardashian’s Mini-Me,” says New York-based stylist and fashion editor Mindi Smith, pointing to their matching ensembles, often custom-made by designer friends like Balenciaga’s Olivier Rousteing. “Cut from the same cloth. Literally.”

And retailers are seeing sales of edgy unisex kids’ clothing rise as a result.

North’s November trip to a Beverly Hills nail salon in Appaman’s $124 black faux-fur chubby resulted in a petite sales bump, according to the company. (Kardashian posted a picture of the outfit on her Instagram account, which has 27.9 million followers.) Sales of North’s $44.99 Rothco children’s flight jacket doubled in February (although cold weather surely also played a part).

“I’m the first person to say the Kardashians are the best thing for the economy,” says Ross, who has also witnessed the “North effect” at Kitson, where sales of black fur vests and black tights are up, as are cool-kid boots. “I [saw] the biggest spike in the Timberlands and the Doc Martens,” he adds.

Still, Kardashian’s penchant for dressing her darling in pint-size grown-up styles has drawn criticism.

“[North] isn’t an accessory — she shouldn’t be wearing ugly couture clothes,” Sharon Osbourne told Closer magazine earlier this month, referring to Nori’s fur coats, which include the probably real $3,500 fox fur she rocked at Daddy’s Adidas fashion show in February.

And, until recently, North’s key color was taboo for kids — with some traditionalists still frowning upon the baby-Goth look.

“Black used to have a very strong association with mourning. It wasn’t an appropriate color choice for either a boy or a girl,” says Jo Paoletti, associate professor of American studies at the University of Maryland and author of “Sex and Unisex: Fashion, Feminism and the Sexual Revolution” (Indiana University Press).

But that changed in the past decade, with unisex children’s clothing in shades of black and gray coming out of Northern Europe, and fashion houses like Stella McCartney, Gucci and Versace adding kids’ lines in muted colors.

More recently, the trend has moved into the mainstream, with chain-store retailers like Baby Gap and Gap Kids, H&M, Zara Kids and J.Crew’s Crewcuts now offering plenty of black and gray options for girls.

“When I go to the trade shows, I see many more lines that are doing [crossover collections],” says Bronagh Staley, owner of Sweet William clothing stores, which has two NYC locations. “Harem pants, which can be for a boy or a girl, sweatshirts that can go either way.”

They represent fresh options for parents like Kimye who want to avoid stereotypical feminine froufrou.

“Everything’s pink and patterned, and that’s just not what I want to dress my daughter in,” Kardashian told Lucky magazine last year.

Ordinary parents are also hailing alternatives to pinkification.

“I think it’s wonderful there’s more choice for the girl who’s not so prissy,” says stylist Mindi Smith. “It’s not about bows and tutus anymore.”

Partly thanks to North West, modern parents are choosing funky black and gray clothing for their daughters.

The pieces can be dressed up, don’t get dirty too easily and pair well with different colors. Plus, since many are unisex, they can be passed down to younger siblings.

Here’s a look at six top sellers.