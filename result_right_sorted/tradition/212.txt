One-time Wall Street hopeful Paige Jennings — a k a Veronica Vain — reveals how she went from to financial-industry worker to porn star, first stripping to make ends meet in college before moving full-on into the flesh-movie industry.

This article was originally published on XOJane.com.

“Paige, you’re just TOO sexual!”

I’ve heard this statement relentlessly in varying forms by everyone from friends to boyfriends to family members. Apparently, there is something grossly wrong with having an incredibly demanding libido. It just makes people uncomfortable — I suspect partially because most people aren’t particularly comfortable with their sexuality.

But I am.

I wasn’t always so comfortable in my own skin. With as strong a sexual presence as the perpetual leering looks from men of all ages tell me I have, there were times in my life when I wondered if sex was all anyone would ever want from me. Becoming a stripper at the age of 18 in order to pay for school without sacrificing too much time did little to help matters emotionally. Even more recently when I tell people I was a stripper, they will say things like, “Oh really! Well, I won’t judge.”

I graduated from high school with a high GPA and significantly above average SAT scores. It was enough to get me accepted into a top 20 ranked university, but not enough to enable me to attend. So I took a full scholarship to the University of Florida, thinking I would kill it there and then go to a more academically prestigious school for an MBA or a JD. Unfortunately, scholarships do not pay the bills, and I was short on family members to help me. I am an ambitious high achiever, so getting a full-time job while simultaneously attending school to me was out of the question. I didn’t want to burn out, and I wanted to be successful.

I first started looking for nontraditional ways to earn money on Craigslist. There I was confronted with a plethora of escorting advertisements, nude modeling gigs, the occasional low-end sugar daddy, and a few pornography castings. Escorting and porn were out of the question — the former seemed too risky and the latter seemed idiotic: Why would I advertise myself doing something generally rejected by society and harm my chances for a normal future?

I was intrigued by the idea of a sugar daddy, however, because it seemed more discreet and less transactional. Shortly after I found myself using a reputable sugar daddy dating site, and soon I was going on dates with wealthy men just looking for a little spice in their life who had no problem covering things like textbooks and rent and even buying me a new car.

The trade-off in these types of relationships, however, is that it’s tough to try to date someone your age. These men want to feel like you really like them for them, which may or may not actually be the case.

Boy, was I right.

I was great at stripping. I loved to perform, I was impressively athletic on the pole, and I was conversational and charming and humorous. All of that combined with my oozing sexuality led to me usually being one of the top earners at the clubs in which I worked.

I went on like this for years. I got myself a boyfriend my age, paid my bills, and was able to buy basically whatever I wanted and maintained a 3.7 at UF. Of course, it wasn’t all cinnamon buns and rainbows: I still had a huge chip on my shoulder that I had to even do any of it in the first place while these upper-class white kids got to skate through their college careers out of touch with the real world and its struggles.

I came to New York City in my last semester at UF seeking a way to break into the competitive finance industry. I was smart and hungry, and popular culture told me that was enough.

It wasn’t.

I transferred to Fordham University thinking closer proximity to the industry and more time with which to take advantage would make me more competitive. I spent two years there first stripping, and then later deciding that if I wanted to be serious about my career, I probably should avoid having potential employers see me naked on a pole.

I knew I had what it took to get things by my own intellectual merit, and I fiercely wanted to prove that to myself.

I did just that. I landed the internship I had at a top Wall Street firm by pure initiative — I reached out to the group that hired me over LinkedIn and passed six rounds of interviews to land the offer.

Eight months later, they wanted me to stay on full time after graduation. I, however, had become disillusioned with what “normal future” I thought I had been pursuing since I first graduated high school.

Normal was boring, desks were boring, and bureaucracy was frustrating. I had learned that I am not a person who fits well into boxes, and I had this intense sexuality brimming at the surface that seemed to hinder more than help me.

Sometime around October 2014, I started exploring options where I could combine interests I was passionate about with business, since I had this shiny new finance degree asserting my ability to read an income statement and calculate discounted cash flows.

For me, this meant searching for industries in which I could work that may be more creative and less conservative than the finance industry. I knew I liked fitness, video games, art, and sex. I stumbled into researching the porn industry as an offshoot of my regular porn-watching habits, and due to a particularly intense sex session with my boyfriend who afterwards asked breathlessly, “Babe, why have you never thought of trying porn? You’d be an amazing porn star!”

In truth, my years spent as a stripper had introduced me to various people in adult entertainment and I had been offered to do scenes. I always had thought I would be a great porn star, but I knew the money was not as great as it seemed and the upside did not seem to substantially mitigate the risks.

I found that many of these risks I had formerly perceived, such as STD risks and drug addiction in the industry, were not nearly as severe as the public is led to believe. Performers are regularly tested every two weeks with the most advanced early detection tests (compare this to the general population who gets tested maybe once a year), and drugs are not significantly more prevalent than in the general population and usually comprised of recreational drugs like pot instead of hard drugs like meth or heroin.

Finally, I found that the business can actually be quite lucrative with the right marketing, business acumen, and development of multiple revenue streams. I was intrigued.

At this point, I was mostly concerned with how to successfully break into the industry. The amount of beautiful women entering the business every year is staggering, and while I was confident, I wasn’t sure I was attractive enough to stand out from the competition, objectively speaking.

So, instead, I started researching potential jobs in the industry that would hire a qualified individual like myself.

Porn companies are companies like any other, and I figured they would have use for business analysts. While looking for such opportunities, I stumbled across The Sex Factor, a reality porn competition seeking individuals who had never shot pornography who wanted to get into the industry. The show was associated with some top names in the business, and I figured if I could do such a thing, perhaps that path to the top would become more tangible.

This was early January 2015 when I discovered and applied to the competition with some sexy selfies on my phone. I did not think they would actually call me — I assumed they had thousands of applications from much prettier girls.

They called me in three hours.

The next day I found myself Skype-ing with the producer of the show, who proceeded to tell me I was exactly what they were looking for and who loved my financial services background. He invited me to the final casting call in Vegas that day, and recommended I start a Twitter account and start drumming up followers to help solidify selecting me for the show.

I started posting some sexy selfies on the account, and while I was walking around the office that week accompanied by the usual trailing eyes, it dawned on me that guys on Twitter might find it really hot if I took some sexy pictures of myself at work.

The office fantasy is prevalent in adult entertainment, after all. I started doing just that, and when The Sex Factor started retweeting those pics, my followers went up exponentially. I knew I had something, and that this was the kind of marketing that would solidify my presence on the show and possibly win me the competition.

By the Friday after I had created my Twitter account, my boss’s boss called me to inform me that some “revealing pictures” of me had surfaced on the Internet and that “it was best if I just did not come back to the premises” because apparently many people on the floor were aware of the Twitter account.

I was flabbergasted — the pictures were in no way associated with my real name and I only had a couple thousand followers. Although I had already quit and the conversation was somewhat lighthearted (I think the executive in question found the situation a bit comical), I found myself suddenly anxious. When I posted the pictures, I knew I was hitting the eject button on my finance career, but this phone call made the situation suddenly very and scarily real.

That same day, however, Brobible tweeted at me that they wanted to interview me and tell my story.

Apparently, a senior editor at the publication had overheard someone on his sales force talking about me, who in turn had heard about it from someone who had worked with me. I still have no idea who this could have been as I wasn’t particularly close with anyone at work.

I had never heard of Brobible before, so I figured an interview couldn’t hurt and I could keep my real identity anonymous while drumming up perhaps a few extra thousand followers. In any case, I figured that a little publicity would help my cause for being on the show as long as I performed well in Vegas.

On Tuesday, Jan. 13, 2015, just four days after being asked to leave my firm and about a week after applying to The Sex Factor and making my Twitter account, Brobible published my interview to their front page.

My followers started to shoot up by several thousand, and my attendance at the University of Florida got me some attention from the UF party crowd. I was happy with the result, and went to bed that night confident about getting onto the show and enabling myself to start in the adult industry on good footing.

The next morning I woke up, and on whim wondered what would come up if I were to Google myself.

I was completely shocked by the results.

In just one evening, Business Insider had found the Brobible article, discovered who I was, the firm and even the individual group in which I had worked.

I was all over the Internet, and I was even in Dealbreaker, which basically every finance guy gets in his email daily.

I was freaking out. I hadn’t wanted all of this information about me publicized. I just wanted to be Veronica Vain, The Wall Street Porn Star.

A few hours later, the New York Daily News had slipped into my apartment building and was knocking on my door. I was literally hiding in the closet, calling my boyfriend at his office and going completely insane.

He had seen the news earlier than I and thought it was great for me. At the time, I did not agree.

I was just scared.

What if someone started stalking me? What did it mean that now people knew my real name? I reluctantly answered the door to the Daily News and shooed away their photographer. They were so interested. I did not think I was that interesting. I was just a girl trying to figure out what she really wanted to do in life; why was everyone flipping out about it?

It’s just sex.

By the next day, I had adapted. I suddenly saw the immense opportunity created by this new development. I was flooded with emails from top media sources as well as some of the top porn companies in the business. It seemed like everyone wanted a piece of me. Clearly, this was the path to the top tier of the industry for which I had been searching. My business acumen and confidence kicked back in.

The Sex Factor started courting me much more than before, and getting onto the show was no longer the question. Whether or not I should do the show in the first place was the question.

By the end of the week, my porn star idol Kayden Kross called me with an incredibly intriguing offer for my first movie in which I would get to do a scene with the goddess herself. The offer also featured product placement, a revolutionary development in the industry, and one only I could really propel with my mainstream media attention.

I had already wondered why such a thing did not exist, and I was eager to be on the forefront of business innovation in the stagnated adult industry. I put the deal at the top of my growing list of opportunities and waited to see what others surfaced when I attended the Adult Expo and Film Awards the following week.

Over the course of my days in Vegas, I met with most of the top production companies as well as The Sex Factor, and Kayden Kross and her company, Arrangement Finders. I was fielding offers otherwise unattainable by girls of my industry experience, all because I had managed to create such a tremendous amount of hype without once doing a scene.

The world was eager to watch me have sex, and they couldn’t simply search for me on a tube site like they were able to do with previous mainstream personalities who had forayed into the porn industry. After a bit more negotiation, I decided to go with Arrangement Finders primarily because of the business precedent it set (a mainstream company sponsorship of a porn film for marketing purposes), and because I actually did have successful experience with their product — sugar daddy dating.

I also agreed to do The Sex Factor after some persuasion by the producers, but I refused to have sex on camera as that content was too valuable and I refused to sign anything doing away with my right to shoot for other companies (a key requirement of the show).

I have since had quite an experience shooting my first movie, and am now strategizing my next move in the industry.

I’ve honestly had a blast — a stressful and at times bewildering blast, but a blast nonetheless.

I must say: I certainly do not advocate for 18-year-old girls entering the sex industry whatsoever and in any capacity. If this is something a girl wants at a young age, I would highly recommend her to first get an education and a strong sense of self before she even considers such a thing.

However, I know this is the right path for me: I have the brains, sexuality, passion, confidence, and personality to be successful if I play my cards right. Still, I hope I do not become some sort of inspiration to women to follow in my footsteps unless they, too, struggle with an immense libido and above average interest in sexuality combined with common sense and intelligence.

But, as for me, I have no regrets. I can’t go back, and I wouldn’t if I could.

I am Veronica Vain.

This article was originally published on XOJane.com.