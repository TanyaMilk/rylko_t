SEMINYAK, Bali—When Janssen Nitimihardja decided to try making ice cream, he turned to YouTube for instruction.

Years and hundreds of experiments later and his artisan creamery is turning up the competition in Bali by making frozen desserts on demand using nitrogen.

A financial technology consultant by day, Mr. Nitimihardja and his wife Emilia, a wedding designer, opened Lycklig just over a month ago off a quiet lane at the far end of Seminyak.

The name is a double entendre – the Swedish word for happiness and also the way to describe how someone eats an ice cream cone.

“It was a perfect marriage,” said Mr. Nitimihardja, who wanted something that would help speak for his product.

Setting up shop, however, was more a labor of love.

The couple surveyed the competition and saw that a lot of people were buying ice cream from minimart freezers. There were also a handful of other big players, making them the “little fish” in the big Bali pond.

To stand out, Mr. Nitimihardja decided to offer three different types of bases – ice cream, gelato and sorbet – allowing people to choose creamy, rich or icy on top of their flavor preference.

“We want to be able to cater to as big a market as possible,” he said.

What else makes Lycklig different? It doesn’t do scoops. Instead it mixes flavor, puts them into a Kitchen Aide blender and then adds shots of liquid nitrogen to freeze the dessert almost instantaneously.

He realizes the shop can’t please everyone so mixing and matching is encouraged. It also gives customers the creativity to make the exact ice cream they want. And because it is created on site, it tastes different than just smashing two scoops together.

Customers have mixed everything from passion fruit and strawberry to apple, mango, yakulk and pineapple – a “crazy mix,” as Mr. Nitimihardja described it.

“It’s your own custom ice cream and we make it on the fly.”

All the production is done on site in view of the customers, and each order generates a cloud of nitrogen, stored in a giant silver cylinder behind the counter.

Making ice cream using traditional batch freezers can take up to 36 hours, while dry ice takes around six and can leave tiny residual chunks in the ice cream that are dangerous to eat.

Nitrogen is the fastest way to whip up frozen desserts, but it’s also the most expensive, said Mr. Nitimihardja.

And difficult to find in Bali, which gets at the main challenge he says he faces in running the creamery: sourcing ingredients.

Lycklig makes everything fresh, including the pressed juices that set the base of the sorbets. And until it finds a reliable supplier who can deliver in small quantities, stocking up for the day will mean lots of runs to different markets and grocery stores on the island.

The 20 flavors Lycklig currently has on offer come from an inventory of 47 time- and friend-tested choices. Seven are gelato, seven sorbet and six described as “quirkies galore.”

They’re special blends that include flavors such as Cookies n’ Cream, Tiramisu – made with real Mascarpone, shortbread and bits of ladies fingers.

The two most popular flavors currently are Rice Bubble Nutella – an ice cream base mixed in with Nutella, Rice Krispies, a coating of chocolate and a syringe of chocolate on top – and Crunchy Doughnut, gelato mixed with a Crispy Crème donuts and another deep fried donut smashed and crumbled over as a topping. A bit of jelly is mixed in for a final touch.

Lycklig is still rolling out new flavors, and will only release its ice cream-based options on April 17.

Jl. Petitenget 501, Shop 1 (across the road from Biku), Seminyak, Bali
www.lyckligco.com
Instagram: @lyckligbali
Facebook: facebook.com/lyckligbali
Email: hello@lyckligco.com
Price range: 25,000 – 50,000 rupiah ($2-4)

(Correction: The name of the shop is Lycklig. A original version of this story contained a different spelling)

__________________________________________________

Other Food Fridays popular on Indonesia Real Time:

Food Fridays: Something Spicy from South Tapanuli

Food Fridays: Hunting for Good Raman in Jakarta? Try Ikkousha

For the latest news and analysis, follow @WSJAsia