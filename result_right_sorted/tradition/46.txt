Himanshu Verma mixes turmeric and lime water in a bowl and, by some process of herbal alchemy, the color turns from saffron yellow to deep rust red.

Behind him bags of arrowroot, sandalwood, henna and neem teeter on a dining table. Clay pots and metal tea caddies form a treacherous obstacle course on the floor.

The smell is like a new-age perfume shop, but Mr. Verma is hand-producing the colorful powders – or gulal – that will be used as benevolent arsenal in the coming festival of Holi on March 6.

Modern-day Holi festivities are usually a mashup of psychedelic synthetic hues as revelers daub each other with bright powder paint.

But Mr. Verma’s pigments are subdued and earthy and certainly truer to Holi’s traditional roots.

Also known as the “Festival of Colors,” Holi has celebrated for centuries, possibly millennia, the onset of spring, harvests and fecundity.

The festival derives its name from the ancient, demonic, would-be murderess Holika, whose death by fire is re-enacted each year to symbolize the triumph of good over evil.

As to how “playing with colors” – throwing bright powder over other revelers — entered the picture, there are plenty of beliefs and legends, predominantly revolving around the Hindu gods Shiva or Krishna, depending where the festival takes place.

Still, ask powder-wielding teenagers about Holi’s significance and they’ll likely come up blank, as would many adults. And at Holi parties around the world this weekend, the festival is mostly just about unadulterated fun.

Enter Mr. Verma. He spends several weeks each year as a one-man production line in his apartment in Delhi turning out plant-based Holi colors, more in keeping with his grandparent’s era than the 21st century.

The ingredients spread throughout the small apartment yield 14 colors, but the most time-consuming are flower-based and account for the heaps of rose petals and marigolds – to which he is allergic.

“It’s a long process,”Mr. Verma acknowledged, sniffling.

“Drying takes about a week, then there’s cleaning and sorting. It’s slow work, and grinding is the only mechanized step.”

Thirty kilos of marigold flowers yields only one kilo of gulal. With his total output around 200 kilos, that’s a lot of blooms but Mr. Verma’s diminutive cottage industry isn’t even a blip on the Holi radar screen.

An array of vessels – tea glasses, copper lotas, ceramic jars and woven baskets – hold the various combinations of color, each spiked with aromas like jasmine and saffron. Each one is hand-covered with multi-hued fabric. There are even specially designed silk pouches to carry them in.

So how much more do they cost than the store-bought colors?

About seven times more. Prices start at 550 rupees (or about $9) for a kilogram of color in a silk-encased pot and top off at 4,500 rupees (or about $73) for a thali, or platter, stuffed with nine colors kept alongside a colorful sari in a pottery jar.  Synthetic colors cost about 200 rupees, or $3, per kilogram.

“There’s a niche market that appreciates these artisanal products,” said Mr. Verma.

And that market is growing each year, he said, through word-of-mouth and his website, where the colors are promoted as part of his “bringing back traditional aesthetics into the urban space” ideology.

Each Holi, Mr. Verma says he won’t be making colors for the next one.

“It’s a super-loss if I factor in my time,” he sighs, standing in his apothecary’s den. “But you know, I just can’t help myself.”

Victoria Lautman is a freelance journalist currently based in Delhi. 

For breaking news, features and analysis from India, click here and follow WSJ India on Facebook.