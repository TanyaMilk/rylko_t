Barack Obama became the first Democrat in 44 years to end the Republican stranglehold on the southwestern corner of Ohio in the 2008 election, winning in Hamilton County, which includes Cincinnati and its conservative suburbs.

Mr. Obama flipped counties across the Rust Belt, helping him carry Ohio and Indiana and win more traditionally Democratic states such as Pennsylvania, Michigan and Wisconsin.

But now, as Mr. Obama is in the middle of a two-day “Betting on America” bus tour across Ohio and Pennsylvania, political analysts said he will have to reassemble the “hope and change” demographic coalition of 2008 that relied on a high turnout of youths and blacks, and winning a larger-than-usual percentage of Hispanics and whites.

“Somehow, he again has to drive up the turnout among blacks, Latinos and other minorities who strongly supported him four years ago,” said Mark J. Rozell, a George Mason University political science professor. “But also, and perhaps most difficult, he somehow has to reach working-class white voters who have turned away from him in large numbers since 2008.”

By most accounts, that will be easier said than done, especially after the elections in 2010, when voters sent a strong message of discontent to Mr. Obama by electing Republicans in nearly every competitive race in the Rust Belt and pulling the plug on the Democrats’ four-year reign in the House.

Furthermore, each of Ohio’s 88 counties, including the Democratic stronghold of Cuyahoga, overwhelmingly supported State Issue 3 in 2011, which aimed to undercut the president’s health care law.