A federal judge is allowing the fired director of the Ohio State University marching band to sue
the school alleging reverse sex discrimination, but he tossed out all other legal complaints that
Jonathan Waters made against the university in a September lawsuit.

In a ruling issued yesterday, U.S. District Judge James L. Graham kept Waters’ suit alive but
narrowed it significantly by making it only a discrimination case. He said that Waters failed to
make an argument that the school violated his constitutional due-process rights.

The ruling was in response to an Ohio State motion to dismiss the entire lawsuit in its early
stages. Both sides made their cases before Graham this month in advance of his decision.



>>> PDF:
Read the judge's decision <<<





>>> Full coverage: Jon
Waters' firing from Ohio State <<<





“We’re gratified that Judge Graham has seen the potential merit in our reverse-discrimination
claim,” said David Axelrod, a lawyer representing Waters. “We have always said that Jon was treated
very unfairly by Ohio State. We’re anxious to start talking to witnesses to get to the truth.”

Ohio State fired Waters last summer after a school investigation found that he tolerated a “
sexualized culture” in the band and mishandled sexual-assault cases involving students.

Waters denied the allegations and said he was made a scapegoat for traditions that date back
decades.

Ohio State issued a statement last night saying that “the court’s thorough ruling confirms
that Mr. Waters was not deprived of due process as a result of the university’s investigation and
his subsequent termination.”

Of four broad legal arguments made in Waters’ lawsuit, three said his firing violated his
constitutional due-process rights — in other words, his basic legal rights. Graham dismissed all
three of those claims.

Ohio State’s key defense in firing Waters has been that he was hired under an at-will
contract that let the university fire him at any time for almost any reason.

But Waters’ attorneys said that, because OSU treated him as though he were a faculty member,
he had a broader “implied contract” with the school. Waters argued that he should have been given
firing procedures that apply to faculty members.

Evidence for the implied contract included official marching-band policies that describe
Waters as a faculty member. But the judge disagreed with that claim, saying much of the evidence
predated Waters’ 2013 hiring as full-time director. Waters had been assistant band director and had
worked on the directing staff since 2000.

In the lawsuit, Waters said Ohio State sullied his reputation before and during the firing,
and then refused to give him a public hearing to clear his name, with access to OSU officials and
the media. He said that denial violated his constitutional protections of a “good name and
reputation.”

But Ohio State provided records showing that it had offered Waters a room and a microphone to
hold a public hearing, although without other accommodations Waters had demanded. In his ruling,
Graham said that Waters could have followed up with OSU to negotiate details of the hearing.

“Rather, in response to the university’s offer to provide a public hearing, the plaintiff
quickly filed this lawsuit,” Graham wrote in his dismissal.

He also tossed out a claim by Waters that Ohio State acted so egregiously that it “shocked
the conscience” and violated Waters’ rights.

The three claims Graham dismissed were the only ones that directly name OSU President Michael
V. Drake and Provost Joseph Steinmetz as defendants. The sex-discrimination case is against the
university. As a result, Drake and Steinmetz are no longer defendants.

The surviving part of Waters’ lawsuit sues Ohio State under federal Title IX rules that
forbid gender discrimination. In his case, Waters said OSU had been lenient with women in similar
situations, such as a cheerleading coach who was allowed to keep her job briefly despite alleged
sexual misconduct by others on the coaching staff. OSU ultimately fired Coach Lenee Buchman.

In its statement, OSU also said: “As for Mr. Waters’ sole remaining claim, that he was
terminated because he is a man, we look forward to providing the factual support to enable early
dismissal of that claim, as well, at the next opportunity presented by the proceedings.”

Members of majority groups, including men, have a higher legal threshold to prove that they
are victims of discrimination. But that standard is used for weighing evidence, Graham wrote, not
whether a complaint can survive a motion to dismiss. For now, Graham primarily had to weigh whether
the facts back up Waters’ claim.

“They support a reasonable inference that the plaintiff was terminated from his employment
while similarly situated female employees were treated more leniently and permitted to retain their
employment despite condoning misconduct similar to that the plaintiff is alleged to have condoned,”
Graham wrote.

In his initial complaint, Waters asked for a trial to determine whether OSU must give him his
job back, along with at least $1 million in damages.




cbinkley@dispatch.com



@cbinkley


A federal judge is allowing the fired director of the Ohio State University marching band to sue
the school alleging reverse sex discrimination, but he tossed out all other legal complaints that
Jonathan Waters made against the university in a September lawsuit.

In a ruling issued yesterday, U.S. District Judge James L. Graham kept Waters’ suit alive but
narrowed it significantly by making it only a discrimination case. He said that Waters failed to
make an argument that the school violated his constitutional due-process rights.

The ruling was in response to an Ohio State motion to dismiss the entire lawsuit in its early
stages. Both sides made their cases before Graham this month in advance of his decision.

“We’re gratified that Judge Graham has seen the potential merit in our reverse-discrimination
claim,” said David Axelrod, a lawyer representing Waters. “We have always said that Jon was treated
very unfairly by Ohio State. We’re anxious to start talking to witnesses to get to the truth.”

Ohio State fired Waters last summer after a school investigation found that he tolerated a “
sexualized culture” in the band and mishandled sexual-

assault cases involving students.

Waters denied the allegations and said he was made a scapegoat for traditions that date back
decades.

Ohio State issued a statement last night saying that “the court’s thorough ruling confirms
that Mr. Waters was not deprived of due process as a result of the university’s investigation and
his subsequent termination.”

Of four broad legal arguments made in Waters’ lawsuit, three said his firing violated his
constitutional due-process rights — in other words, his basic legal rights. Graham dismissed all
three of those claims.

Ohio State’s key defense in firing Waters has been that he was hired under an at-will
contract that let the university fire him at any time for almost any reason.

But Waters’ attorneys said that, because OSU treated him as though he were a faculty member,
he had a broader “implied contract” with the school. Waters argued that he should have been given
firing procedures that apply to faculty members.

Evidence for the implied contract included official marching-band policies that describe
Waters as a faculty member. But the judge disagreed with that claim, saying much of the evidence
predated Waters’ 2013 hiring as full-time director. Waters had been assistant band director and had
worked on the directing staff since 2000.

In the lawsuit, Waters said Ohio State sullied his reputation before and during the firing,
and then refused to give him a public hearing to clear his name, with access to OSU officials and
the media. He said that denial violated his constitutional protections of a “good name and
reputation.”

But Ohio State provided records showing that it had offered Waters a room and a microphone to
hold a public hearing, although without other accommodations Waters had demanded. In his ruling,
Graham said that Waters could have followed up with OSU to negotiate details of the hearing.

“Rather, in response to the university’s offer to provide a public hearing, the plaintiff
quickly filed this lawsuit,” Graham wrote in his dismissal.

He also tossed out a claim by Waters that Ohio State acted so egregiously that it “shocked
the conscience” and violated Waters’ rights.

The three claims Graham dismissed were the only ones that directly name OSU President Michael
V. Drake and Provost Joseph Steinmetz as defendants. The sex-discrimination case is against the
university. As a result, Drake and Steinmetz are no longer defendants.

The surviving part of Waters’ lawsuit sues Ohio State under federal Title IX rules that
forbid gender discrimination. In his case, Waters said OSU had been lenient with women in similar
situations, such as a cheerleading coach who was allowed to keep her job briefly despite alleged
sexual misconduct by others on the coaching staff. OSU ultimately fired Coach Lenee Buchman.

In its statement, OSU also said: “As for Mr. Waters’ sole remaining claim, that he was
terminated because he is a man, we look forward to providing the factual support to enable early
dismissal of that claim, as well, at the next opportunity presented by the proceedings.”

Members of majority groups, including men, have a higher legal threshold to prove that they
are victims of discrimination. But that standard is used for weighing evidence, Graham wrote, not
whether a complaint can survive a motion to dismiss. For now, Graham primarily had to weigh whether
the facts back up Waters’ claim.

“They support a reasonable inference that the plaintiff was terminated from his employment
while similarly situated female employees were treated more leniently and permitted to retain their
employment despite condoning misconduct similar to that the plaintiff is alleged to have condoned,”
Graham wrote.

In his initial complaint, Waters asked for a trial to determine whether OSU must give him his
job back, along with at least $1 million in damages.