WASHINGTON — Entering the seventh year of his presidency and facing a Congress controlled by
Republicans, Barack Obama likely will offer a bit of confrontation with a blend of conciliation in
his State of the Union address at 9 p.m. Tuesday.

As Obama has made clear in a series of speeches during the past two weeks, the prime-time speech
before a joint session of Congress “will be a chance to talk about America’s resurgence” but also
how to “help more Americans feel that resurgence in their own lives through higher wages and rising
incomes and a growing middle class.”

Although the president is expected to forcefully propose ideas to expand family medical leave
and raise taxes on the wealthy — ideas that have little chance of winning GOP approval — he also
has the opportunity to find common ground with Republicans to expand international trade and
improve cybersecurity to guarantee greater privacy for Americans.



Related story: Ohioans in Congress list their 2015 priorities



“Obama has a chance to lay out what he wants to accomplish and show voters ... to come with him
because he kept his word and he got the job done despite the odds and the opposition of Republicans
in Congress,” said Mary Anne Marsh, a Democratic consultant in Boston.

Carmel Martin, executive vice president of the Center for American Progress in Washington and a
former aide to the late Sen. Edward Kennedy, D-Mass., predicted that Obama “will lay out a bold
agenda and demand that Congress deliver for the American people.”

Yet even as Senate Majority Leader Mitch McConnell, R-Ky., said cybersecurity and providing
Obama with authority to negotiate trade agreements “are two areas where we’re likely to end up in
the same place,” many Republicans are convinced that Obama plans a combative approach on
Tuesday.

“Based on the way he has acted so far, he will give an in-your-face ‘do things my way,’  ” said
Whit Ayres, a Republican political consultant in suburban Washington. “He’s shown no inclination or
ability to find common ground and work within the common ground.”

Ted Hollingsworth, a lobbyist in Washington and onetime chief of staff to former Republican Sen.
George V. Voinovich of Ohio, predicted “there are not many prospects for long-term cooperation”
because “there is nothing short of mutual contempt” between Obama and congressional
Republicans.

Strong-willed presidents such as Lyndon B. Johnson have relied on the State of the Union to
advance audacious ideas, such as in 1965 when Johnson proposed his Great Society to eradicate
poverty and provide health coverage for the elderly and poor. That speech cleared the way for
Congress to approve Medicare, which pays for health coverage for the elderly, and Medicaid, which
provides health care for low-income Americans.

In 2010, Obama offered bold plans to toughen regulations on financial institutions and overhaul
the nation’s health-care system to extend insurance to millions of Americans without coverage. By
the end of the year, Obama had signed the sweeping new health law known as Obamacare.

But that year, Obama spoke to a Congress controlled by the Democrats. By contrast, his top aides
think the Republicans have opted for a combative approach of their own, such as chipping away at
the health law, curbing financial regulations approved in 2010 and attempting to block executive
orders to delay deportation of undocumented immigrants.

Yet Presidents Dwight Eisenhower, Bill Clinton and Richard Nixon, who also faced a Congress
controlled by the opposition party, forged compromises to build the nation’s interstate highway
system, curb federal deficits and reduce air pollution.

“Both Richard Nixon and Bill Clinton had very productive years working with a Congress of the
other party,” Ayres said.

In speeches during the past two weeks in Michigan, Arizona, Tennessee, Maryland and Washington,
D.C., Obama has taken the unusual step of revealing his plans for Tuesday, joking, “Why wait for
the State of the Union? It’s sort of like you’ve got your presents under the tree, you kind of
start shaking them a little bit.”

He focused on issues likely to appeal to Democrats, who are in the minority in Congress for the
first time since 2006. In Baltimore on Thursday, he threw his support behind a “seven-day sick-day
policy across the country,” which guarantees pay for up to seven days off for workers.

Earlier this month in Knoxville, Tenn., Obama said that a “college degree is the surest ticket
to the middle class,” proposing a plan to make it free for students to attend community
colleges.

Yet neither idea is likely to get very far with the new Congress. House Speaker John Boehner,
R-West Chester, pointed out on Friday that the community college plan would cost taxpayers $60
billion during the next decade.

“The American people want us to find a way to address their concerns,” Boehner said last week. “
That was the big message out of the elections. I am hoping the president heard the same
message."

Although Obama has outlined ideas to overhaul federal taxes on businesses — an idea Republicans
support — he also plans to call for an increase in the capital-gains tax, which is paid on the
profits from the sale of stock and real estate. The plan, which would raise $320 billion during the
next decade, would finance a new work tax credit for families and an increase in the child tax
credit.

The new tax increase — which would impact couples earning more than $500,000 a year —– was
denounced quickly by Republicans.

Although Obama has a knack for being an excellent speaker, traditionally the State of the Union
has been long on policy ideas and short of electrifying rhetoric. As Marsh said, the State of the
Union is “more a to-do list than a vision statement.”


jtorry@dispatch.com



@jacktorry1