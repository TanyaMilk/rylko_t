Posing with her pals on the steps of St. Patrick’s Cathedral, Carol Markel beams as passersby admire her hand-decorated hat and colorful Alpana Bawa outfit. Some tourists are so taken with the spectacle that they snap selfies with the 71-year-old artist milliner and her five immaculately dressed friends.

“Everyone keeps asking us if we’re going to church,” says the Lower East Side bon viveur with a laugh. In fact, the recent Post photo shoot was only a dress rehearsal for the posse’s appearance Sunday at the annual Easter Parade and Bonnet Festival, when hundreds of revelers flaunt their festive headwear down Fifth Avenue.

While some of the outrageous hats wouldn’t be out of place at the circus of Mardi Gras, Markel and her cohorts are traditionalists — “women of a certain age” who prefer to top themselves off with something elegant.

Many hark back to the event’s golden era, immortalized in Irving Berlin’s 1948 movie “Easter Parade,” when there was no such thing as costumed pets in sombreros.

“Wearing hats used to be a fashion statement, but now it’s gotten to be more of a silly statement,” says Valerie, co-writer of the Idiosyncratic Fashionistas blog, who, like her blogging partner Jean, goes only by her first name. “But we are the old guard. We don’t wear silly hats — we wear real hats.”

That said, Valerie concedes that it depends on your point of view. One of her favorite toppers — made by noted millinery brand Ignatius — is in the shape of the Guggenheim Museum, complete with an “anatomically correct” skylight of black netting and Swarovski crystals.

Jean, 65, bought her own $180 cream-colored Ignatius piece at the Philadelphia Museum of Art Craft Show, likening it to “something out of Dr. Seuss.”

The three women — along with three of their friends — have attended the parade for years, strolling along the eight blocks near St. Patrick’s Cathedral and St. Thomas Church,posing for pictures and, when they’re lucky, savoring the spring sunshine.

Their latest tradition is a post-parade lunch at nearby The Modern restaurant– one of the backdrops for The Post’s photo shoot – whose pastry chef presents them with chocolate goodies such as bunny and rooster figurines

It’s a lovely way to relax after walking and occasionally braving the rain or wind — although Lynn Dell Cohen, owner for 52 years of Upper West Side vintage store Off Broadway Boutique, concedes that fashion always comes first. “I am all about drama and making an entrance, so I don’t care about the wind and tend to wear bigger hats,” says Cohen, 82, of her annual outing.

“Hats are the last, most important element to your outfit,” she adds. “I am always in a hat, turban, or I make sure I have my head wrapped. I have long, wonderful hair, but I never show it.”

Her entire outfit was made in her store’s workshop, while theatrical milliner Cigmond Meachen, of Brooklyn Heights, also designed her own creation, inspired by classic movies.

“When you dress up [in a hat], you become your own art object,” says Meachen, 55. “You become a walking piece of art.”

She should know. Meachen, who has a workshop in Manhattan’s Fashion District, is responsible for the stunning hats worn in the Broadway revival of “Gigi,” opening Wednesday.

That musical’s set in Paris in the early 1900s, when hats were an obligatory and elegant addition to a woman’s wardrobe.

“We don’t live in a time when hats are standard operating procedure for fashion anymore,” says Meachen, growing misty-eyed. “But going to the Easter parade, we can pretend we do.”

The New York City Easter Parade and Bonnet Festival takes place Easter Sunday from 10 a.m. to 4 p.m. on Fifth Avenue between 49th and 57th streets. 

Additional reporting by Anastazia Martinez