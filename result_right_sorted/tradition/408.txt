LONDON — Woe betide the hapless backpackers who stumble upon the Olympic Park in London’s East End this summer. They may feel the might of Britain’s biggest security operation since World War II come crashing down upon them.

Camping essentials like compasses and water bottles are just some of the items banned from Olympic venues in an attempt to avoid Occupy protests that have disrupted city centers and financial districts around the world.

Other prohibited items include oversize hats and excessive amounts of food, as well as the more traditional knives, guns and liquids in amounts over 3.4 fluid ounces.

The “security operation will be the [United Kingdom’s] largest ever peacetime logistical operation,” Home Secretary Theresa May said in outlining security plans for the Olympics earlier this year.

Anti-capitalist demonstrators are not the only ones targeted in the security effort, which will include the mobilization of 13,500 troops, more than the number on active duty in Afghanistan.

Alongside them in the $900 million operation — double initial cost estimates — will be 12,000 police officers, many drafted from other forces across the country, and nearly 24,000 venue security personnel working directly for the Olympic organizers.