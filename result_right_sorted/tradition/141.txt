There’s a running joke among students at Ohio State University that President Michael V. Drake,
who has been on the job for nine months, has never been on campus.

On a brutally cold day, the joke flew around on social media: “Why would Drake cancel classes?
He never sets foot on campus anyway.”

It isn’t true, of course. But buried in that barb is a worry among some on campus that, in a
highly public job at one of the biggest colleges in the country, Drake prefers to keep a low
profile.



Video: Drake answers criticism



The image is hard to avoid after E. Gordon Gee, the ebullient president who seemed to be
everywhere at all times, putting off sleep to surprise students at one more party. Students admired
him for it.

In Drake, Ohio State gained a chief who might shy from some of that spotlight but who is also
unlikely to make the type of off-the-cuff comments that got Gee in trouble.

“I just feel like we really don’t have much of a sense of him, who he is,” said Steven Conn, a
history professor and director of the public history program.

But as Drake prepares for his Tuesday investiture –– a sort of re-introduction gala often held
for new presidents after they find their bearings –– he is trying to bolster his role as a campus
fixture. Looking at a recent month, Drake said he spoke to 14 campus groups and met 3,000
students.

“It’s been a few months. Some of my predecessors were here for a couple of decades, and more
people get a chance to see you in a couple of decades,” Drake, 64, said in a 
Dispatch interview. “In one way, it’s flattering that everybody would like to see me every
day. And in another way again, with 100,000 students, faculty and staff, if I see 5,000 people, I
haven’t seen 95,000.”

Some critics, though, contend that his absence goes beyond the campus handshakes.

Josh Coy, president of the campus government for graduate students, said Drake has visited the
group only once and has spoken individually with Coy only a few times.

“He has not made time for us,” he said. “There are a lot of serious negative things going on
with graduate students right now that I don’t even know if he knows about.”

But evaluating Drake as a leader, Coy sees untapped potential, not indifference. When the two
talk, Coy always leaves feeling that Drake was earnestly listening, he said.

“It’s hard not to like him when you sit down with him. He’s extremely charismatic. He’s very
personable,” Coy said. “I don’t think he has taken advantage of that.”

That’s a kinder criticism than Drake has fielded from others.

A month into the job, he made enemies when he announced the firing of marching-band director
Jonathan Waters amid accusations of a sexual culture in the band.

Fiery emails flooded Drake’s inbox for weeks fighting the decision. Some urged Drake to resign. “
 If you haven’t already felt it, you are going to certainly feel the backlash of your heinous
actions,” one band member wrote in July.

Amid the controversy –– which has dragged on in a federal lawsuit filed by Waters –– some
critics drew comparisons between Drake and former OSU President Karen Holbrook, who left in 2007
after five years.

Holbrook cracked down on off-campus parties, tailgaters and riots after football games but drew
accusations that she was ruining tradition. Drake has faced the same criticism. Fans once booed
Holbrook when she spoke at a football game at Ohio Stadium.

In a similar moment in January, Drake, sporting a Buckeyes sweater and cap, tried to energize a
crowd of 50,000 fans gathered at Ohio Stadium to celebrate the football team’s national title. Amid
the cheers were audible jeers.

“I didn’t hear that so much. There was a lot more cheering,” Drake said.

But that’s the flip side of being the president, he said: Even for decisions that he doesn’t
make alone, he gets the blame. If he’s bothered by the criticism, though, he doesn’t show it.

“I think that’s a normal part of leadership. I get credit for things that I have not done as
well,” Drake said. “If you’re swayed, or you kind of bounce back and forth with every little thing
that comes up, I think you can’t really make any real progress along the way.”

Some on campus said they’re giving Drake time to establish his role at Ohio State.

“It’s something that I know he’s working on. His time is being torn in many directions,” said
Celia Wright, president of the OSU Undergraduate Student Government.

Although Gee was a constant presence, his attention seemed divided, Wright said. Even brief
visits from Drake, though, have gone a long way in building ties with students, she said.

“He’s not going to be going to parties on the weekend, and we understand that,” she added. “But
the time he makes, he makes it really meaningful.”

Drake’s work at the OSU Wexner Medical Center — including recruiting a new CEO — has consumed
much of his time so far, said Alex Shumate, vice-chairman of the university board of trustees. But
the trustees believe that Drake is on track to achieve goals they set for him, including one to
build relationships across campus.

“I think he’ll be more visible. There have been a lot of things to put in place,” Shumate said. “
 It takes time to really get your arms around this great institution.”

Conn, the history professor, is giving Drake time, too, but is guarding his expectations. He
said there’s a sense among some professors that Drake, who is a medical doctor specializing in
ophthalmology, was hired to oversee a precarious time at the medical center, which built a $1.1
billion hospital last year.

“The fact that we haven’t seen him much, in some ways, is not necessarily surprising to us,
because we assumed he wasn’t going to be much interested in this part of campus anyway,” Conn
said.

Although Drake comes from a medical background, he led the University of California-Irvine
before being hired at Ohio State. Faculty members at Irvine have said Drake wasn’t very visible
early on there. Over time, he emerged as a campus presence, though, attending every basketball game
and theater performance with his wife, Brenda.

Tuesday could be the chance for a similar emergence for Drake. At his investiture speech, he is
expected to flesh out his vision for Ohio State as a modern land-grant university. It could add
substance to earlier speeches and reveal the map he’s charting for the next segment of his
tenure.

It could also outline a new set of expectations for Drake beyond his predecessors’, which he has
tried to shed.

“What they should expect from me is the kind of performance I’ve had over my entire career, and
they should expect the university to be able to move forward like every place I’ve been has moved
forward,” Drake said. “And that’s what we should all work together on.”


cbinkley@dispatch.com


@cbinkley