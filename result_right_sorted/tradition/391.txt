The Interior Department will finalize a rule Monday that would grant permits to let wind farms kill eagles for up to 30 years, six times longer than current permits allow.

While acknowledging that the science is uncertain and standards on the best ways to reduce eagle deaths have not been implemented, the U.S. Fish and Wildlife Service said the eagle population has remained steady for the past 40 years and that it’s worth testing a longer permit for bird kills.

Wind farms are the fields of turbines that dot the landscape. They kill about 440,000 birds a year, including some iconic bald eagles, golden eagles and other protected species, raising questions about the balance between renewable energy production and the environment it is supposed to be helping.


														
														
															SEE ALSO: Executive order: Obama ups green-energy mandate on feds to 20 percent
														
														

“Permits to kill eagles just seems unpatriotic, and 30 years is a long time for some of these projects to accrue a high death rate,” said Sen. David Vitter, Louisiana Republican and a critic of the Interior Department.

He and fellow Republicans said the administration has been tougher on traditional energy sources such as oil and gas when it comes to bird kills, but has been more lenient on renewable energy.

Sen. Lamar Alexander, Tennessee Republican, said the new rule amounts to “federal hunting licenses” for wind farms to kill eagles.

SPECIAL COVERAGE: Energy and Environment

“We need even treatment of the law for energy development, regardless of source, and this revised rule misses the mark by giving continued leniency to renewable energy projects, such as wind farms,” Mr. Alexander said.

The rule was released last week and will be published officially Monday.

The Fish and Wildlife Service said that although it is raising the maximum permit to 30 years, it could issue more restrictive wind farm permits.

The permits also may include requirements for extra steps the wind farms must take to try to reduce eagle kills — particularly if evidence suggests the turbines are killing more eagles than expected.

But the service said it makes sense to grant a longer eagle-kill permit because renewable-energy projects’ life spans are generally far longer than the five years currently allowed.

Federal law generally prohibits killing bald and golden eagles but gives the government power to grant exemptions.

“Permits may authorize lethal take that is incidental to an otherwise lawful activity, such as mortalities caused by collisions with wind turbines, power line electrocutions, and other potential sources of incidental take,” the agency said in its official rule posting.

Although the agency said eagle populations have been stable for 40 years, it acknowledged a lot of uncertainty about wind farms and their effects.

“In the case of managing eagle populations in the face of energy development, there is considerable uncertainty. For example, evidence shows that in some areas or specific situations, large soaring birds, specifically raptors, are especially vulnerable to colliding with wind turbines,” the agency said.

“However, we are uncertain about the relative importance of different factors that influence that risk,” the agency said. “We are also uncertain which strategies would best mitigate the effects of wind energy developments on raptors. Populations of raptors with relatively low fecundity, such as golden eagles, are more susceptible to population declines due to new sources of mortality.”

Late last month, the Obama administration made headlines when it fined Duke Energy Corp. $1 million for killing 14 eagles and 149 other birds at wind farms.

That was the first time the federal government had fined wind farms for bird kills.