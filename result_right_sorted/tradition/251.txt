MOSCOW — As the economic crisis sweeps through Russia, a dangerous trend is emerging in this heavy-drinking country: the rise in consumption of potentially lethal moonshine, medical alcohol or even cleaning products.

Layoffs, wage cuts and price increases are combining to worsen the problem of alcoholism, which has long been a major public health issue, by increasing the mix of dangerous products in the market. Those who can no longer afford store-bought drinks are turning to “under the counter” alternatives that can cause serious damage, even death.

Alexander Polikarpov, the head doctor of the Alcospas chain of alcohol rehab clinics in Moscow, says he has noticed a “wave” of complications in patients, such as delirium tremens — a symptom of withdrawal also known as “the shakes” — and epilepsy.

Polikarpov’s staff of up to 40 doctors specializes in providing emergency detox for drinkers whose families are desperate to end a multi-day binge. Their patients are more likely to be vodka drinkers scaling down to low-cost, lower-quality varieties. The more desperate cases of alcoholics using industrial products tend to occur in more remote, rural regions.

“A number of patients who previously could afford expensive spirits are now forced to reorient in the sense that they use cheaper and lower-quality spirits,” Polikarpov says in his consulting room in the capital’s suburbs.

Sales of legal beer and vodka have fallen sharply as prices rise, buoyed in part by the rising cost of imported ingredients after the ruble’s value tumbled last year. Analysts say falling sales likely don’t mean that demand is falling, simply that it is being pushed into an illegal and dangerous black market.

The alternatives to increasingly costly legal alcohol are many and varied. At the safer end of the scale is vodka production diverted and sold on the side by workers at legitimate distilleries, but some products, such as industrial spirits or moonshine made by inexperienced or unscrupulous distillers, can be lethal. Some of the most harmful yet popular alternatives to legal alcohol are liquids designed “for hair growth or for cleaning the bath,” says market analyst Vadim Drobiz.

Sales of beer, Russia’s most popular alcoholic drink, were down 10.5 percent year-on-year by volume in January, according to figures from Nielsen Russia. The beer market remained stable by value because prices rose to a new high, Nielsen spokesperson Ekaterina Lukina told the Associated Press.

Vodka, which is more popular among older drinkers, continued a long-term decline in sales, which fell 17.6 percent by volume and 2 percent by value. That drop is mainly due to government actions to combat alcoholism by instituting a legal minimum price for vodka. While that minimum of 185 rubles ($2.96) for a half-liter bottle is low by European standards, it is costly for low-earning Russians in the poverty-stricken provincial towns where moonshine is most popular.

Earlier this year, President Vladimir Putin ordered the minimum price cut by 16 percent. While the move was celebrated in Russian media, it did not fully reverse last year’s 29 percent rise, never mind similar hikes in previous years.

That well-intentioned push to fight alcoholism has swelled the black market, Nielsen analyst Marina Lapenkova says.

“Today the share of illegal vodka market adds up to half of the total market,” she said in emailed comments, adding that recession will just exacerbate the problem.

Russians consume the fourth-highest amount of alcohol per capita in the world, according to World Health Organization data from 2010, the last year for which statistics are available, and alcohol has been a thorny issue for countless Russian governments.

In the czarist era, moonshine producers undermined the vast and hugely profitable state monopoly on vodka production. By the mid-1980s, alcoholism was wrecking the productivity of the Soviet economy to such an extent that then-leader Mikhail Gorbachev launched a crackdown on vodka sales. Three years of public discontent and a moonshine boom followed before the policy was abandoned.

The most affected regions are typically poor and remote, often where Soviet-era factories have shut down, Drobiz says. He noted moonshine trade is independent of religion — Russia’s Muslim regions have plenty of illegal alcohol production.

Unusually for an industrialized nation, home distilling for personal consumption is legal in Russia and does not require a license, giving rise to an entire industry focused on the moonshine market.

Not all moonshine is a cheap and dangerous product, however.

In fact, making old-fashioned Russian moonshine, known as samogon, is a traditional activity for Russians at their dachas — country cabins. Legally produced samogon-style spirits have even become something of a cult drink in some of Russia’s hipster bars.

Rishat Ibatullin from the central city of Kirov runs an online retailer selling stills to Russia’s army of moonshine makers and says sales are rising.

He suspects some of his customers are setting up illegal businesses, but is keen to portray distilling as an honorable hobby.

“People do it to get a product where they know what it’s made from and how it’s made,” Ibatullin said.