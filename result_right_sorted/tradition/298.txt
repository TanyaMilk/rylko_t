Republicans took control of Congress on Tuesday promising to whittle down Obamacare but still struggling to figure out how far to go — and whether any of their efforts can succeed with President Obama still in the White House.

Even the small tweaks the GOP has planned for the near term face opposition, including a White House veto threat on a bill to roll back the part of Obamacare that defines a full week’s work as 30 hours rather than the traditional 40 hours.

But Republicans want to have bills ready in case a Supreme Court decision on Obamacare subsidies expected this summer throws the issue back to Congress.


															
															
																SEE ALSO: Incoming Senate Majority Leader Mitch McConnell equipped to succeed in tough job
															
															

On Tuesday the House unanimously passed a bill aimed at trying to help veterans get jobs by exempting those still covered by military-related insurance from Obamacare’s threshold for businesses. Employers with fewer than 50 workers don’t face the mandate to provide coverage.

The chamber will follow it up this week by voting to define full-time work under Obamacare as 40 hours instead of 30. At least two Senate Democrats have backed an identical bill in the upper chamber, an early boon for Republicans who took control of both chambers Monday and have pledged themselves to sober and transparent leadership.

“I think you make the changes that you can, and you lay the groundwork and hopefully win the presidential election next time. Then I think we’ll be in much better shape,” said Rep. Tom Cole, Oklahoma Republican, looking ahead to the 2016 presidential contest.