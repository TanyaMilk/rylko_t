The White House insisted Tuesday that the dangers posed by climate change are greater for average Americans than the threat of terrorism.

Speaking to reporters, White House press secretary Josh Earnest said Americans no longer face the same threat of attack that they did prior to Sept. 11, 2001, crediting the administration’s efforts to degrade organizations such as al Qaeda.

Today, Mr. Earnest said, climate change is a greater threat for everyday Americans.

“There are many more people on an annual basis who have to confront the direct impact on their lives of climate change, or on the spread of a disease, than on terrorism,” he said. “When you’re talking about the direct daily impact of these kind of challenges on the daily lives of Americans, particularly Americans living in this country … more people are directly affected by those things than by terrorism.”

President Obama made the same argument during an interview with Vox released this week.

In its national security plan released last week, the administration also ranked climate change alongside more traditional threats such as nuclear proliferation and terrorism.