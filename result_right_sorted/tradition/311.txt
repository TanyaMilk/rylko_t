Wisconsin gave Gov. Scott Walker a renewed lease Tuesday, voting to   keep the Republican in office in a recall election that amounted to an   embrace of his union-busting, government-trimming agenda in this   traditionally Democratic-leaning state.

Mr. Walker staved off a  challenge from Milwaukee Mayor Tom Barrett in a  race that had appeared  ripe for Democrats to win just a few months ago.

But national  Democrats were late to get involved, and President Obama  kept his  distance, leaving the field to Mr. Walker and outside groups on  both  sides that poured tens of millions of dollars into television   advertising and turn-out-the-vote operations.

“Tonight we tell  Wisconsin, we tell our country and we tell people  across the globe that  voters really do want leaders who stand up and  make the tough  decisions,” Mr. Walker said at his victory party. “Now it  is time to  move on and move forward in Wisconsin.”

He painted himself and his  supporters as modern-day heirs of the  country’s founders who met in  Philadelphia, but he also sounded a  conciliatory note, saying he learned  lessons from the past year and a  half in office.

He also said he would gather the Legislature later this month to seek common ground.