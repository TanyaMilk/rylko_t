As the shiny black 2015 Yukon Denali with tinted windows, leather bucket seats and DVD screens pulls up to the curb, an impossibly handsome blond prepares to hop out. But not before his personal butler hastens to open the door for the VIP passenger — Keaton Evans, a 9-year-old from Greenwich, Conn. 

“He picked me up from acting class in a tuxedo, and I felt like I was a famous actor,” says the third-grader. “It’s just like a fun car service.”

Well, not exactly.

At $175 per hour, the service makes Uber prices look bargain-basement.

“It gives me agita [when kids in the city] ride in taxis,” explains Keaton’s mom, luxury-travel writer Kim-Marie Evans.

And so, when the mother of four needed to shuttle her little guy around NYC in March, she hired a “playdate valet” — a white-gloved butler — who comes with his own chauffeur and a luxury SUV. 

“It sounds over-the-top, but it’s so sweet,” she says.

“I plan to keep using it. It’s not cheap, but trusting somebody with your child shouldn’t be cheap.”

The “playdate valet” is just one of many luxury perks now being offered to city children as part of Red Carpet Kids Concierge — a new offshoot of the popular party service. 

And with its jaw-dropping array of extravagant services, it’s taking the outsourcing of traditional parental responsibilities to new, luxurious heights. 

Well-heeled parents looking to spoil their spawn can hire a pair of gussied-up fairies to read curated bedtime stories ($350 an hour), a zoologist to host an “educational safari” at the Bronx or Central Park zoos ($1,500 to $7,500) and runway consultants for a “Fashion Week experience” where junior fashionistas walk in a staged fashion show ($4,000). 

Over the holidays, Upper East Side mom Christina Johnson hired a pair of 20-something “fairy-tale fairies” to read to her two young daughters — Irene, 7, and Helen, 5 — who were holding court in a sleepover with a few friends.

“It was a surprise [for the girls],” says Johnson.

Neon-haired and tutu-clad, the fairies brandish bubble wands and bring along angel wings and tiaras for the girls, plus scrolls for them to write down a story. There’s face-painting, too.

What little girl wouldn’t swoon? 

“It’s so fabulous,” gushes Johnson. “When a fairy comes to your door, that’s pretty much the greatest thing that can ever happen. It was like Disney in our living room!” 

Another client, Upper West Side mom Laura Simms, spent $700 to host a puppet show for her 6-year-old son, Ronin, to cheer him up after the delivery of a new sibling — “a birthing gift, so to speak.” 

As an added bonus, Ronin helped create the characters and story, which centered around an alligator named Albert.

“It’s definitely on the more expensive side,” concedes Simms. “But there’s a tremendous amount of value in what you’re getting. Nothing’s cheap.”

According to Red Carpet Kids Concierge, the services are designed to educate and stimulate kids’ creative juices.

“My mission is to bring back innocence,” says co-founder Eva Shure. “Let kids be kids. I hate how technology has a grasp on kids.”

But not financial constraints. 

One family from the United Arab Emirates ponied up $50,000 a year to keep the company on retainer. Last summer, they rented a luxury charter yacht so that their son — who was obsessed with pirates — could play Jack Sparrow of the “Pirates of the Caribbean” movies.

There was even a mock attack staged by smaller vessels with well-trained actors in full pirate regalia — peg legs, gold teeth and “arr matey” lingo were all part of the package deal — as well as a private chef on the yacht.

“That 8-year-old kid is going to remember being a pirate on the Hudson for the rest of his life,” says Shure.

Maybe, but all this extravagance has experts concerned. 

“Parents are layering the icing before they have made the cake,” says Kathryn Smerling, a family therapist based on the Upper East Side. 

“Special events happen, but no child will appreciate the simplicities of the joy of sharing an intimate experience with a parent if this is the norm. 

“Storytelling is a great parent-child activity — it enforces communication, attunement, literacy and the relationship — but outsourcing it . . . gives little value to story time as a profound bonding experience.”

That’s left to a ballooning concierge team of 30 — all independent contractors, many of whom are actors with child care experience who found Red Carpet Kids through the classifieds in Playbill.

It’s not uncommon for them to dress up as butlers, then do a quick-change into a magician’s robe for an imaginary trip to Wizard U — it’s all in a day’s work. 

When studying Shakespeare at prestigious Carnegie Mellon, budding actor David Andrew Laws didn’t imagine that one day he’d be donning polyester tails, while minimasters of the universe laze in luxury for his role as “playdate valet.”

“Growing up in Kentucky, there wasn’t anything close to this,” says Laws, who serves his pint-size passengers fruit juice and Clif bars on demand. The kids “definitely want it to be an everyday thing.”

And while most of his charges are well-behaved, some are enfants terribles.

“Some of the kids will probably always refer to me as ‘butler,’ ” says Laws. “They say, ‘Bye, butler.’” 

Still, co-founder Shure insists the concierge service does a child good. “Yes, it’s at a price point you don’t do every week, but you’re creating an heirloom memory.”

But, parents be warned:  once you go “playdate valet,” it’s hard to go back. 

“He still talks about it,” says Keaton’s mom, about introducing her impressionable youngster to the good life so soon. “Now he wants to know why we’re riding the subway and not having a guy in a tuxedo picking him up.”