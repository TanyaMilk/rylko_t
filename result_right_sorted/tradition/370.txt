Online retailers are coming under fire from Washington and state governments for not collecting sales tax from customers who purchase goods and services through websites such as Amazon and eBay.

The tax itself is not new. The only difference is that online retailers could soon be required to collect it — just like their traditional bricks-and-mortar peers — instead of trusting customers to pay it on their own to the state, which is a requirement few heed.

The movement to step up the collection of online sales tax is gaining momentum from traditional retail stores that want to level the playing field and, more recently, state governments that are desperate for money.

In the Senate, the Marketplace Fairness Act would allow states to require online retailers to collect sales tax. In the House, the Marketplace Equity Act, which has a hearing next week, would essentially do the same thing.

Both have growing bipartisan support.

Even before Congress decides whether to give states this power, a growing number of governors are striking deals with online retailers to collect sales tax.