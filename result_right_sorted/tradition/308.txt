Facing more bad news on the housing front and a surge in the polls by his Republican rival, President Obama changed his tune Thursday night about some roundly criticized comments that entrepreneurs don’t build businesses on their own.

“I was saying the other day, we take pride in individual initiative and … we don’t like handouts,” Mr. Obama told supporters in West Palm Beach, Fla. “We don’t expect government to solve every problem and we don’t think the government should help people who don’t want to even help themselves.”

The president’s message differed in style from the one he delivered last Friday in Roanoke, Va., when he told a crowd: “If you were successful, somebody along the line gave you some help. … If you’ve got a business, you didn’t build that. Somebody else made that happen.”

Since Mr. Obama’s remarks a week ago, presumptive Republican presidential candidate Mitt Romney and others in the GOP have seized on the president’s comments as proof that he doesn’t understand or appreciate the American system of entrepreneurship.

Thursday night in Florida, Mr. Obama tried to clarify his thinking about the common good, praising collective action without seeming to scold individualism.

“We also remember that that GI Bill that educated so many people — we did that together,” he said. “Hoover Dam, Golden Gate Bridge, we built together. The Internet was created because of investments that we made together. That’s how we sent a man to the moon — together. We’re not going to abandon those values and those traditions that ultimately made this the country that we love.”