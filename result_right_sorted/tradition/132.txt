FAIRBORN, Ohio — The president of Wright State University and its dining services vendor
have apologized for a Black History Month menu that featured fried chicken and collard greens.

The menu screens at the Ohio school also offered mashed potatoes and cornbread under photos
of the Rev. Martin Luther King Jr. and other black leaders. 
The Dayton Daily News reported that people
circulated images of it on social media, calling it offensive and disrespectful. Fried chicken has
long been associated with racial stereotyping in the United States.

President David Hopkins sent an email Thursday asserting the university’s commitment to
diversity and pledged to prevent a repeat.

“I apologize to anyone hurt by the display,” Hopkins wrote. “To our credit, the menu was
quickly removed. But the larger question remains: Why was it done? I will find out. We will take
steps to prevent this kind of behavior occurring in the future.”

Chartwells Higher Education Dining Service said it could have done a better job providing
context for what was meant as “a cultural dining experience.”

The company said in a statement that it tries to help celebrate national events on campus
with “authentic and traditional cuisine. … In no way was the promotion associated with Black
History Month meant to be insensitive.”

Kimberly Barrett, the school’s vice president of multicultural affairs and community
engagement, said she was pleased that the menu signs were removed.

“I think many times, in attempts to be inclusive and to honor diversity, people who might be
in the majority community, or in communities other than the ethnic groups they’re trying to honor,
sometimes get it wrong,” said Barrett, who is black.

“I was really hurt … extremely hurt,” said Billy Barabino, a senior and president of the
school’s Black Student Union. “For me, it was a knock in the face for African (and)
African-American individuals who have fought for us to be progressive. I was extremely offended by
it because it minimizes who we are as a people.”

The school had hosted a panel discussion this week of the daughters of slain activists Medgar
Evers, Malcolm X and Viola Liuzzo.