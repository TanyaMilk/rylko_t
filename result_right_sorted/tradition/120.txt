Gulping down his traditional State of the City Red Bull, Mayor Michael B. Coleman gathered
himself, grabbed the lapels of his crisp, dark blue suit and walked onstage of the Palace Theatre
to begin his farewell tour.

“The state of our city is strong,” said Coleman on Thursday night, in his final State of the
City speech, which featured more laughs and emotion than the city’s longest-serving mayor has
offered in his previous 15 speeches.

Coleman, 60, wasn’t just talking about the city’s health at the moment, but was referencing his
legacy after nearly 16 years in office.

He tallied his successes during four terms as Columbus’ leader, such as Downtown growth. He also
briefly touched on some failures, such as his inability to get the level of educational reform in
Columbus City Schools that he pleaded for two years ago.

Overall, the point was to express Coleman’s belief that he will be leaving the city better than
he found it when he became mayor in 2000.

“From the bottom of my heart, I thank you,” Coleman read from a speech that was 284 words, the
shortest State of the City address of his tenure.

“I am forever grateful for the opportunity you have given me to serve as your mayor.”

Before Coleman took the stage, a video played for the crowd highlighted the projects developed
under his administration: the development of Columbus Commons and the Scioto Mile; the rebirth of
the Short North and redevelopment in Franklinton; getting jobs for ex-convicts; creating the
Domestic Partnership Registry; and a 24 percent drop in what Coleman called “violent crime.”

The video ended with the message, “A city transformed.”

Coleman said he wants to leave office at the end of this year known for “building those
partnerships” that brought “swagger” to a city once overlooked in the Midwest.

The speech was light on new plans, largely because the city does not have much additional cash
to create the programs of years past. This year’s $814 million budget is nearly flat compared with
last year’s budget.

In prior years, Coleman’s 18-, 20- and, once, 30-page State of the City speeches featured new
education initiatives and neighborhood redevelopment in Franklinton, the South Side, Near East Side
and Weinland Park.

Those programs have accounted for millions of dollars in new spending that this year collided
with a slower growth in income-tax revenue than at any point since 2010.

Coleman touted a development deal with Rogue Fitness, a Columbus-based manufacturer of exercise
equipment. Rogue plans to relocate its operation from Steelwood Road on the Northwest Side of
Columbus to the former Timken site in the Milo-Grogan neighborhood.

Rogue will build a 500,000-square-foot manufacturing headquarters and consolidate business
activities near the intersection of Cleveland and E. 5th avenues.

There were some lighthearted moments during the videos, musical numbers and tributes at last
night’s State of the City address.

Coleman sat down onstage with Angela Pace, former WBNS-TV (Channel 10) news anchor who now
handles community affairs for the television station, and answered scripted questions.

Pace tried to get Coleman to give his answers quickly, at one point saying, “We gotta move. 
Scandal is on tonight.”

After the speech, and away from the crowd on the quiet seventh floor of the theater, Coleman
said he wanted to talk more about how the city needs to keep up its “national image.” He also
pushed for one of his favorite projects that’s never happened, saying the next mayor needs to
ensure rail transit becomes part of the city.

And he gave a few hints about his future plans.

“It will be in the private sector. I want to have an engagement in the civic and business
environment and I think I have things to offer,” Coleman said. “I have no intention of interfering
with the management of the city, but there’s a whole lot more in this community to be engaged
in."


lsullivan@dispatch.com



@DispatchSully