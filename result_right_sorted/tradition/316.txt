KABUL, Afghanistan  (AP) — A suicide bomber struck a crowd of Shiite worshippers who packed a  Kabul mosque Tuesday to mark a holy day, killing at least 56 people,  and a second bombing in another city killed four more Shiites. They were  the first major sectarian assaults since the fall of the Taliban a  decade ago.

A third attack, a motorcycle bomb in the southern city  of Kandahar, killed one civilian, but police said it did not target  Shiites as they commemorated Ashoura, which marks the seventh-century  death of the Prophet Muhammad’s grandson Imam Hussein.

Religiously  motivated attacks on Shiites are rare in Afghanistan, although they are  common in neighboring Pakistan. No group claimed responsibility for the  blasts, reminiscent of the wave of sectarian attacks that shook Iraq  during the height of the war there.

In Kabul, the bomber blew  himself up in a crowd of men, women and children gathered outside the  Abul Fazl shrine. Mahood Khan, who is in charge of the shrine near the  presidential palace, said the explosion occurred just outside a packed  courtyard where dozens of worshippers were lined up as they filed in and  out of the crowded building.

Some men were beating themselves in mourning, an Ashoura tradition, and food was being distributed.

“It  was a very powerful blast,” Mr. Khan said. “The food was everywhere. It was  out of control. Everyone was crying, shouting. It is a disaster.”