Lauren Singer composts, buys in bulk and has used so little waste over the past two years that all of her trash fits into a mason jar. 

But when it comes to sex, she was, until recently, like a lot of women: Sustainability wasn’t as big a concern as safe intercourse.

That was until she learned how condoms are made while taking a sustainable economics course at NYU: Mass-marketed rubbers are often produced using toxic chemicals, unfair wages, child labor or production methods that are harmful to the environment.

So Singer ditched the Durex and went fair-trade.

Yes, like coffee, chocolate and cotton, rubbers are now coming under the scrutiny of fair-trade advocates, who say the industry has been overlooked and underserved by cheaply made products.

“I want to be a conscious consumer about everything, from my food to my condoms,” says Singer, 23, a Williamsburg resident who runs the Simply Co., a company that sells chemical-free detergent.

Would she actually stop short of having sex without a fair-trade condom? She offers a “no comment.”

The professor she had at NYU, Jeffrey Hollender, is the founder of Seventh Generation, which sells eco-friendly cleaning products. He’s now turning his attention to condoms. 

He and his daughter Meika Hollender toured latex plantations around the world to find the best place to responsibly source the rubbers, eventually landing on a place in southern India.

What a lot of people don’t know and don’t talk about is there is a lot of child labor in the rubber industry says Meika, 27, who lives in the West Village.

In July, the Hollenders debuted Sustain Condoms, the world’s first certified fair-trade condoms. 

They look and feel like regular condoms (though a female friend reports they left a slightly slimy residue). They’re available at Whole Foods and other health stores.

Sustain is hardly the only conscientous condom on the market. In 2013, Tiffany Gaines launched Lovability Inc., a condom company that uses fair-trade latex. 

Packaged in cute tins and available at lovabilitycondoms.com, they’re designed to appeal to ladies.

“Women are very mindful of the cultural system that’s affected by the product they purchase,” says Gaines, a 24-year-old Union Square resident.

On top of fair-labor issues, many traditional condoms are made with casein, a milk protein that makes them nonvegan.

The animal issue attracted Rob Blatt, a 33-year-old marketer in Park Slope, to Sir Richard’s condoms when he went vegan three years ago. 

The brand, which launched its casein-free prophylactics in 2008 and has been moving toward using fair-trade materials, made sense when he started re-examining all the products he used regularly.

But he says he wouldn’t refuse to sleep with someone if only a nonvegan condom were available ­— though he’s usually packing his own.

“‘Always be prepared’ is a pretty good motto in this case,” he says.