Sen. Ted Cruz of Texas announced Monday that he is running for president, making him the first official candidate in the 2016 race for the White House.

“I believe God isn’t done with America yet,” Mr. Cruz said during a speech at Liberty University, sending a strong signal that he plans to compete for the evangelical Christians that traditionally play a big role in the GOP nomination race.

“I believe in you. I believe in the power of millions of courageous conservatives rising up to reignite the promise in America,” he said. “And that is why today I am announcing that I am running for president of the United States.”

The big question for Mr. Cruz is whether he can build a big enough coalition to claim the mantle of the conservative alternative to the establishment candidate in a Republican race that will likely also feature former Florida Gov. Jeb Bush, Wisconsin Gov. Scott Walker and Sen. Rand Paul of Kentucky.

“He potentially can rebuild the Reagan coalition by adding a populist, anti-Washington message that attracted disaffected Democrats and Independents,” said Craig Shirley, a biographer of President Reagan.

Liberty University was founded in 1971 by the late Jerry Falwell, the televangelist preacher who also led the formation of the moral majority that helped propel Ronald Reagan to the presidency in the 1980 election.