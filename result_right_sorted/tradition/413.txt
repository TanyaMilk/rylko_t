In the wake of President Obama’s executive action to shield illegal immigrants from deportation, the White House also launched a campaign Tuesday to encourage more legal immigrants to become U.S. citizens.

White House aides said the “citizenship awareness campaign” will make use of social media and other traditional media to reach the more than 8.8 million legal residents who are eligible to become U.S. citizens but haven’t taken that step.

Cecilia Munoz, director of White House domestic policy, said the effort is “just part of good governance” and isn’t related directly to Mr. Obama’s executive action last year to grant deportation amnesty to millions of illegal immigrants.

A White House task force released a report Tuesday outlining the administration’s goals, which include “welcoming and integrating immigrants” nationwide.

“Nothing in this report … is connected to the expansion of DACA [deferred action for childhood arrivals],” Ms. Munoz said.

The administration is looking at using AmeriCorps volunteers to assist local communities in integration programs, and launching a “made it in America” campaign to tell the success stories of immigrants.