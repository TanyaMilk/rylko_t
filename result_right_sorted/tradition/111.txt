WASHINGTON — Insisting that states hold the power to decide marriage laws, Ohio Attorney General
Mike DeWine argued on Friday that the state’s ban on same-sex marriage does not violate the U.S.
Constitution.

In legal papers filed with the U.S. Supreme Court, DeWine’s office vigorously asserted that the
federal courts should not impose a nationwide definition of marriage.

Reminding the justices that Ohio voters in 2004 approved an amendment to the state constitution
banning same-sex marriage, DeWine’s office warned that the high court “would forever place into our
Constitution only one perspective on marriage” if it struck down Ohio’s ban.

“Such a ruling would say that the decade-long debate in the states has been improper,” DeWine’s
office contended. “It would eliminate the possibility of (and requirement for) each community to
confront this issue and reach a consensus that respects all sides.”

The Supreme Court has scheduled an unusual 150-minute argument on April 28 on whether Ohio and
three other states violated the Constitution when they prohibited same-sex marriage. Customarily,
oral arguments before the justices last one hour. Gay-marriage advocates filed their legal
arguments a few weeks ago.

The legal papers filed for Ohio were drafted by Eric Murphy, the state solicitor. He urged the
justices to stand by last year’s decision by a three-judge panel of the 6th U.S. Circuit Court of
Appeals in Cincinnati that upheld the Ohio ban.

In a 5-4 decision in 2013, the justices struck down as unconstitutional a 1996 federal law
defining marriage as between a man and woman. But the justices have yet to rule on whether a state
can ban same-sex marriage.

Attitudes toward same-sex marriage have shifted dramatically in the past 20 years. Although
President Bill Clinton signed the 1996 federal law on marriage being between a man and a woman, he
and most Democrats have since declared their support for same-sex marriage.

Thirty-seven states now permit same-sex marriage. States such as Ohio have argued that they
should not have to recognize the legality of same-sex marriages performed in other states.

DeWine’s office cited the rapidly changing views on same-sex marriage as a prime reason for the
high court to refrain from interfering with how states decide their marriage laws, claiming that “
by nationalizing domestic relations, the court would erode the very federalist structure that made
same-sex marriage possible.”

The high court must strike a balance between two legal principles. The first is that states
should have broad authority to write their own laws. But legislators cannot write laws that violate
the Constitution by discriminating against a class of people.

James Obergefell of Cincinnati and his husband, John Arthur, challenged the Ohio ban in federal
court after the justices struck down the federal law in 2013. Obergefell appealed the 6th Circuit
decision to the U.S. Supreme Court. Arthur died in 2013.

DeWine’s office also defended state voters from the doctrine of “animus,” which essentially
means a desire to harm others. Instead, DeWine’s office wrote, “all agree that traditional marriage
arose for purposes unrelated to prejudice.”

“And when acting to retain that definition, Ohioans had an obvious motive — to keep democratic,
in-state control of this important issue,” the state argued. “To hold that Ohio laws were driven by
animus, by contrast, would demean millions of Ohioans by treating their deeply held beliefs about
marriage as sheer bigotry.”

The court is expected to issue a ruling by the end of June.


jtorry@dispatch.com



@jacktorry1