Kicking off a slate of speakers at the annual Conservative Political Action Conference (CPAC), retired neurologist Ben Carson alluded to his recently being added — and removed — from an extremist watch list in criticizing what he called the left’s propensity to “relabel and name things.”

“For instance, if you’re pro-life, then you’re anti-woman,” he said at the Gaylord National Resort and Convention Center. “If you’re pro-traditional family, then you’re a homophobe. If you’re white and you oppose a progressive black person, you’re a racist. If you’re black and you oppose the progressive agenda, you’re crazy.

“And if you’re black and you oppose the progressive agenda, and you’re pro-life, and you’re pro-family, they don’t even know what to call you,” he continued. “I mean, you end up on some kind of watch list for extremists.”

Amid significant criticism, the Southern Poverty Law Center recently apologized and removed an “Extremist File” it had posted in the fall on Mr. Carson.

Mr. Carson has been near the top of recent polling on potential GOP presidential contenders in 2016, and has recently secured several commitments for staffers who would handle finances on his would-be presidential campaign.

He rose to prominence in some conservative circles after criticizing President Obama’s health care overhaul in front of Mr. Obama at the National Prayer Breakfast in 2013. He later said he declined a suggestion from one of the organizers that he apologize.