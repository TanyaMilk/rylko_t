The U.S. Navy is paving the way to discard the tradition of shaving a new recruit’s head on the first day of boot camp.

A three-month pilot program is set to begin that will allow women to pick from a number of approved Navy hairstyles. The program may be applied to men if it’s successful.

The Navy Times reported Thursday that the pilot program will be implemented at Recruit Training Command  Great Lakes in Illinois and Officer Training Command Newport in Rhode  Island.

PHOTOS: Elite U.S. Navy SEALs in action

“We  are constantly considering fleet feedback and policy changes to  improve  training and quality of life during initial accession and   indoctrination into the Navy,” said Rear Adm. Rich Brown, commander of   NSTC, in the release, Navy Times reported.

The news wasn’t well-received by some of the old-timers.

“How many people really know the reason for the shaved head? It ensures that parasites like Lice don’t get spread in such tight quarters such as the barracks,” Warren Jameson of Colorado commented on the Navy Times article. “It also added a sense of Uniformity and the initial breakdown move required to build up a Sailor, Marine, Soldier or Airman. I am glad that I don’t have to answer the dreaded ‘Why?’ question all recruits and military members are now allowed to ask. I hate the new ‘It’s too hard, I need a break,’ mentality. Suck it up Buttercup, you are in the military now, and your life depends on you being able to tread water for 5 minutes. Signed: Old Navy Salt.”


																	

																	
																		SEE ALSO: Congress snubs Navy SEALs, rejects funding again for specialized training centers
																	
																	

Sean Kimball of Rhode Island was more succinct: “This is ridiculous.”