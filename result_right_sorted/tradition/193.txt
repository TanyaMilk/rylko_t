Millionaire murder suspect Robert Durst may soon be scrounging for money.

The Post has learned his brother, real estate giant Douglas Durst, settled a lawsuit against filmmaker Andrew Jarecki, confirming that Robert was the source of leaked materials about the family for “The Jinx,” the HBO documentary that led to the accused killer’s recent arrest.

The deal, inked Thursday, means Douglas can move to freeze $74 million of his older sibling’s $100 million fortune.

A source told The Post during litigation seeking to determine Jarecki’s sources that if Douglas could prove Robert Durst leaked the information, “the next step is freezing his money.”

Douglas’ lawyer, Charles Moerdler, said his client is mulling his next move.

The money is tied to a 2006 court deal that paid Robert to cut ties with the multibillion-dollar family business, The Durst Organization.

The hit could affect Robert’s ability to pay a team of top-notch defense attorneys to fight pending gun and murder charges — at least without selling off some real estate.

The HBO documentary showed how Robert’s top-dollar Texas legal team was able to win acquittal on charges that he killed his neighbor and threw the dismembered body in Galveston Bay in 2001.

Robert Durst, 72, has been locked in a New Orleans jail since his March 14 arrest, when FBI agents found a revolver and marijuana in his hotel room. He’s also facing possible extradition to California to face charges that he killed his longtime friend Susan Berman after she’d been contacted by the Westchester County DA.

Before Berman’s December 2000 murder, the DA had reopened the investigation into the mysterious disappearance of Robert Durst’s wife Kathleen in 1982.

Douglas Durst, 70, withdrew the Manhattan Supreme Court suit against Jarecki after The New York Times published an article in March revealing that Robert gave Jarecki “unrestricted access” to a trove of 60 boxes stored at a friend’s home.

The trove included confidential family information aired in the HBO series.