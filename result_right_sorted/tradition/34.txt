A new biography of Narendra Modi, which grants rare access to the Indian prime minister, assesses his campaign to become leader of the world’s largest democracy from a spin doctor’s perspective.

“The Modi Effect: Inside Narendra Modi’s Campaign to Transform India,” written by Lance Price, a former adviser to Tony Blair when he was prime minister of the United Kingdom, is the result of hours of meetings with Mr. Modi at his official residence in New Delhi after his election.

The author, a key Labour Party spin doctor in the late 1990s, examines Mr. Modi’s brand, his social media dominance and his campaign that harnessed technology like never before in an Indian election.

In the extract below, Mr. Price reports on the Indian prime minister’s sense of style that was central to the creation of his brand.

Much has been written about Mr. Modi’s fashion savvy: President Barack Obama even commented on it during his visit to India, suggesting that the Indian prime minister out-styled the First Lady Michelle Obama.

But, until now, the prime minister himself has not commented in public about his wardrobe.

Mr. Price quotes Mr. Modi as saying “God has gifted me the sense of mixing and matching colours. So I manage everything on my own. Since I’m God gifted I fit well in everything. I have no fashion designer but I’m happy to hear that I dress well.”

Nalin Kohli, a spokesman for Mr. Modi’s Bharatiya Janata Party, said many people had written about the prime minister and tried to interpret him.  “In a nut shell, Mr. Modi is clearly seen as an icon of developmental politics, strong leadership and charismatic campaigner,” Mr. Kohli said.

An extract from the chapter of Mr. Price’s book titled “Brand Modi.” 

A man in his mid-sixties with glasses, grey hair and a beard is not everybody’s idea of a fashion icon. 

But it is one of the many apparent contradictions that make Modi so unusual that this self-proclaimed ‘man of the people’ is as fashion conscious

as any leader on the world stage. Even the New York Times fashion blog has been moved to note that ‘Mr Modi stands out. Literally and strategically.’ 

I remember the lengthy discussions inside 10 Downing Street when Tony Blair realized for the first time that he needed glasses. He fancied

himself in a pair of Calvin Klein’s, but we in the communications team insisted he should get an NHS pair so he wouldn’t be accused of being too flash. 

Narendra Modi had no such inhibitions. It has been widely reported that his glasses are from Bvlgari, his watch is a Movado and the pen that often pokes from his top pocket is by Mont Blanc. The shirts themselves, a short-sleeved version of the traditional round-necked kurta, have become an internationally sought-after brand. He has them hand-made, and is often seen to change them several times a day, even switching colours so they go well with the background when he speaks. 

His own account of the birth of the Modi Kurta has itself become legendary. He claims it was merely a way of making life easier before he was famous and was just a nomad travelling the country with a small bag. ‘I had to wash my own clothes. 

So I thought my shirt occupied too much effort in washing and space too. I cut the sleeves myself. So my shirt became half sleeved. I have been wearing such clothes for over twenty-five years. Yes, I like to dress up well and stay clean. God has gifted me the sense of mixing and matching colours. So I manage everything on my own. Since I’m God gifted I fit well in everything. I have no fashion designer but I’m happy to hear that I dress well.’ 

Modi’s fashionable tastes are said to be one reason that some traditionalists in the RSS, with its austere customs, became suspicious of him. In a sense his clothes and accessories are a very visible expression of what does indeed set him apart from the strict nationalist tradition. For him they symbolise his commitment to modernisation, ambition and good-quality products, although I’m sure there is a touch of vanity behind his choice of clothes too. 

Men across India didn’t exactly rush out to buy short-sleeved shirts once Modi and his kurtas were on the TV every night, but the garment

industry realised there was a marketing opportunity to exploit and both high-end and cheaper versions of the kurtas soon started appearing in shops and markets. 

Every craze in the world, it seems, generates its own t-shirts and the Modi phenomenon was certainly no exception. Some of the first to appear were designed even before he became the candidate, by young supporters who had created an online campaign called ‘Modi-fying India’. 

Tajinder Pal Singh Bagga had represented the party’s youth wing on the BJP national executive for four years when he decided more should be done to make politics, and Modi in particular, more attractive to younger voters. 

‘If you see the scenario two years back,’ he told me, ‘when you spoke with the youth they would say I am not interested, everybody is a thief. But they saw a ray of hope in Modi.’ 

Having watched Bruce Springsteen perform for Barack Obama, he and some friends staged a small rock concert. Concerts need t-shirts and these, too, were a deliberate attempt to copy the Obama campaign, although with a very Indian twist. 

‘We launched t-shirts because there were many people blaming Modi for the 2002 riots,’ said Bagga.

‘The first t-shirt was with the quote from Modi, “India First is my definition of secularism”. And the second t-shirt had Modi’s face with the tagline, “Face of Development”.’ 

They were hardly the sort of slogans you can imagine fashion-conscious young Indians wanting to see emblazoned on their chests, but this was just the beginning. Once his campaign was properly up and running a huge market opened up for Modi-related products. 

An online shop, thenamostore.com, was quickly established and there were t-shirts galore available at the click of a button. Most of the designs prominently displayed his face, while others featured political slogans like ‘sip the change’, alongside a kettle pouring out a cup of tea. 

One simply replaced the ‘S’ in the iconic Superman symbol with an ‘N’. 

Modi was well aware of the use being made of his image and wholeheartedly approved. Another volunteer recalled going to show him some of the products. ‘We showed him various things like cups and Superman tees with his face on it. He loved it and he said, “Go ahead and do it”.’ As the merchandising really took off, modimania.com joined the online sales drive and new products were made available. 

There were better-designed clothes for both men and women, stationery, watches, key-rings, mouse mats and toys. With a keen eye for the news agenda, some products were launched on particular dates, like NaMo chocolates for Diwali and the NaMo pepper spray that came out on International Women’s Day. 

When a specific controversy arose, new t-shirt designs could be quickly produced to take advantage of the situation.

So when Modi decided to fight back against the chai wala jibes, Tajinder Pal Singh Bagga and his friends at Modi-fying India printed hundreds of thousands of t-shirts with a picture of Modi and the lotus symbol on the front and a rather lengthy slogan in Hindi on the back that read, ‘Why can’t a tea-seller become PM? Why can’t the one who changed Gujarat change India too? Modi – my PM.’ The garments were then distributed to tea vendors all over India. 

Inevitably, cheaply produced copies of some of the t-shirts and other merchandising started to appear on market stalls just about everywhere. This presented no problem for the campaign team. They had no patents to protect. Indeed, quite the reverse, the rip-off products were just another kind of free advertising and it was obvious for all to see that there was no equivalent demand for Rahul Gandhi apparel. At the same time, promoting Brand Modi had to be about more than clothes, key-rings and assorted accessories. To do the job properly, the party needed some help from the experts. 

Piyush Pandey is the Don Draper of India’s advertising world. Not quite so young and dashing perhaps, although he told me working on the Modi campaign had made him feel twenty years younger. The allusion to the hit American show Mad Men brought a smile to his lips under his huge bushy moustache. 

We were sitting in his stylish apartment, tastefully decorated and with jazz posters on the walls. He stretched out and picked up his second large whisky. ‘No, life is very different to Mad Men. We actually have to work hard.’ In a short but intensive campaign, Pandey and his team regularly put in up to twenty hours a day trying to keep up with the demands of the election schedule. ‘I think this campaign was the most memorable of my life,’ he said, ‘because for six weeks every day God was on my side.’ Not that he needed divine inspiration. 

He signed up for the role only in February 2014, by which time the market was more than receptive to what he was about to market. He didn’t demur. 

‘How many times do we get a product so easy to sell?’ Pandey knew Modi of old. The company he worked for as executive chairman and creative director, Ogilvy & Mather, had handled the contract for Gujarat’s tourism push for nearly four years. But the founder of O&M, David Ogilvy, had made it a rule not to do political advertising, and so Pandey had turned down the contract for the 2012 Gujarat elections and did so again when Modi asked him to handle the general election. 

Then, at the end of January, the phone rang again. ‘He said, “You must reconsider, I think I need you”. So I agreed to think it over.’ The solution was to take on the job not on behalf of O&M but the subsidiary company Soho Square, which he also chaired. 

The man in charge of the day-to-day running of Soho Square, Samrat Bedi, readily agreed, and a meeting was quickly arranged with Piyush Goyal, chair of the Election Information Campaign Committee. Goyal shared the fi ndings of the public opinion survey he had commissioned, which had shown that Modi’s personal popularity was running around 20% higher than that of the BJP. 

He wasn’t just out-performing his party, but was far ahead of any of his colleagues in the leadership. That meant, in advertising speak, according to Bedi, ‘Mr Modi had far more equity than any other leader in the party, which led them to take a decision that Mr Modi would be the face of the campaign and that a presidential form of campaign would be run. It was unanimously decided that, instead of having multiple leaders, there would be just one brand, one face and one leader, Mr Modi. Just like it is done in US. That was the starting point for us.’

–“The Modi Effect: Inside Narendra Modi’s Campaign to Transform India,” by Lance Price,  is published in the United States on March 24 by Quercus Books, price $26.99.