Maryland  Gov. Martin O'Malley said Sunday that the stumbling U.S.  economy will  not prevent President Obama from winning re-election.

Mr. O’Malley,  chairman of the Democratic Governors Association, in an  appearance on  CNN’s “State of the Nation,” said American voters  understand that the  president’s jobs proposals and economic plans have  been stymied by  congressional Republicans.

“We have a very, very obstructionist wing of the Republican Party,” the governor said.

“None  of us is really better off than we were before this huge Bush   recession,” he said, but voters, he predicted, would back the president   over a GOP candidate.

Mr. O'Malley defended the president against  criticism that Mr. Obama is  stoking a class war with his rhetoric about  taxing the wealthy.

“It’s difficult to talk about the gross  inequality … without those  people in the highest income bracket  becoming defensive about it,” he  said.