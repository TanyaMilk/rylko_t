JACKSON, Tenn. (AP) — Herman Cain is firing up the crowd at a tea party rally in this West Tennessee town when the generator powering his sound system shudders to a halt.

Cain stands awkwardly for a few moments then suddenly begins to sing. Slowly at first but gaining in speed, he belts out “Impossible Dream” in the rich baritone he’s honed in church choir.

“You know, when it’s your rally, you can do what you want to do!” Cain says as he finishes with a raucous laugh. The 500 or so supporters who have jammed the strip mall parking lot to hear the Republican Party’s newest star speak roar their approval.

Momentum restored, Cain launches into a pitch for his signature 9-9-9 tax plan, and the crowd is right there with him, chanting 9-9-9 along with the Georgia businessman. The plan would scrap the current tax code and replace it with a 9 percent tax on personal income and corporations as well as a new 9 percent national sales tax.

The 65-year-old’s improbable campaign for the presidency is all about momentum right now. How does he maintain the wave he’s riding in recent polls that have catapulted him from an also-ran in the GOP race to the elite top tier?

There are many reasons his bid could fade as quickly as it rose. He acknowledged Friday that he will trail former Massachusetts Gov. Mitt Romney and Texas Gov. Rick Perry significantly in fundraising. Cain has never held elected office and could wilt under the rigors of the campaign trail and the withering scrutiny coming his way.