Chris Kofins, Democratic strategist, said Friday that the Republican party is going through “internal civil war,” which could lead to trouble in the mid-term elections.

“If you look at the law few cycles, what you’ve seen is the Republican party has been its own worst enemy,” he said Friday on MSNBC.

Vice President Joseph R. Biden said Friday at the House Democratic caucus retreat that there is no Republican party because it is so fractured, especially between tea party and more middle-of-the-road Republicans.

“There isn’t a Republican Party,” Mr. Biden said. “I wish there was a Republican Party. I wish there was one person you could sit across the table from and make a deal. Look at the response of the State of the Union — what were there, three or four?”

Mr. Kofins said one of the biggest problems facing the Republican party now is that the candidates who win GOP primaries are so far to the right and “out of touch” that they end up losing.