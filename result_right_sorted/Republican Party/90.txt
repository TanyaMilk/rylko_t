Retired neurosurgeon and possible 2016 presidential candidate Ben Carson didn’t name names, but took a shot at the cadre of political elites who form the Republican Party’s “political class” and said what America really needs are leaders who believe in God.

“Our founders wanted to make sure there was no such thing as a political class,” Mr. Carson said during a South Carolina Tea Party Coalition Convention speech, Politico reported. “A select, small group of people out of which we consistently pull our leaders. That hasn’t worked so well for us.”

Mr. Carson didn’t name those he thought were members of that “select” group — but his speech comes shortly after former Massachusetts Gov. Mitt Romney announced he was mulling another run for the White House, and former Florida Gov. Jeb Bush is considered a key GOP contender, Politico reported.

“What we need are intelligent people who have common sense, some humility … and faith in God,” Mr. Carson said, Politico reported.