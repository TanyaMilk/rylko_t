In 1994, Lindsey Graham, then a 39-year-old South Carolina legislator, ran for Congress in a district that he said had not elected a Republican since Union guns made it do so during Reconstruction.

He promised that in Washington he would be “one less vote for an agenda that makes you want to throw up.” He was elected to the Senate in 2002 and soon almost certainly will join the Republican presidential scramble, enlivening it with his quick intelligence, policy fluency, mordant wit and provocative agenda.

He has the normal senatorial tendency to see a president in the mirror, and an ebullient enjoyment of campaigning’s rhetorical calisthenics. Another reason for him to run resembles one of Dwight Eisenhower’s reasons. Graham detects a revival of the Republicans’ isolationist temptation that has waned since Eisenhower defeated Ohio’s Sen. Robert Taft for the 1952 nomination.

Graham insists he is not running to stop a colleague: “The Republican Party will stop Rand Paul.” But Graham relishes disputation and brims with confidence. “I’m a lawyer. He’s a doctor. I argue for a living.” If Paul is nominated, Graham will support him and then pester President Paul to wield a big stick.

Graham believes that events abroad are buttressing the case for his own candidacy. He says national security is the foremost concern of Republicans in Iowa, New Hampshire and South Carolina. He sees the 17,000 members of the Iowa National Guard who were deployed overseas as the foundation of a Graham plurality among the 120,000 Iowans expected to participate in the caucuses.

He wants voters to ask each candidate: Are you ready to be commander in chief? Do you think America is merely “one nation among many”? Are you committed to putting radical Islam “back in the box” (whatever that means)? Do you understand that any Iranian nuclear capability “will be shared with terrorists”? Do you realize that if that had happened before 9/11, millions, not thousands, might have died?

He wants 2016 to be “a referendum on my style of conservatism.”

Voters might, however, wonder if it is the no-country-left-unbombed style. Suppose, he is asked, you could rewind history to 2003. Knowing what we know now — the absence of WMD, the difficulty of occupation, the impossibility of nation-building and democracy-planting — would you again favor invading Iraq? “Yes,” he says, because “the Saddam Hussein model” of governance is “unsustainable” and “on the wrong side of history.”

Good grief. Barack Obama repeatedly says, as progressives must, that history — make that History — has an inevitable trajectory toward sunny uplands and will eliminate many bad things. Perhaps it will, eventually, but we live in the here and now, where we must answer this question: Is America’s duty-bound role to be history’s armed accelerant?

Yes, Graham answers, because Arabs, too, are eligible for “the American value set.” And, he adds, Ronald Reagan’s “peace through strength” is inadequate to the stormy present because “there can be no peace with radical Islam.”

So, the “box” Graham wants it put in is a coffin.

Graham is equally provoking and more convincing regarding domestic policy, warning that “the way the American dream dies is we fail to self-correct.” He forthrightly says something indubitably true and, given the distempers of the Republican nominating electorate, semi-suicidal: The retirement of more than 70 million baby boomers by 2030 means that the nation needs immigrants — to replenish the workforce — as much as they need America.

When Graham was born in 1955 there were nine workers for every retiree; today there are three, trending toward two. We need many immigrants to sustain the entitlement state — unless, he says, many Americans volunteer to have four children after age 67.

“Not too many people raise their hands [to volunteer for this] at my town meetings.”

As the economy’s growth limps along at 2 percent, Graham warns that 10 percent growth would not erase trillions of dollars of unfunded entitlement liabilities. He will tell Republican voters that a grand bargain — pruned entitlements and increased taxes — is necessary. They may think this is one of his jokes.

“I’m somewhere between a policy geek and Shecky Greene,” the comedian.

Campaigning, he says, “brings out the entertainer in you,” so his town hall meetings involve “15 minutes of standup, 15 minutes of how to save the world from doom, and then some questions.”

He at least will enlarge the public stock of fun, which few, if any, of the other candidates will do.