The 2010 midterm elections were a dream come true for the Republican Party at the ballot box but a nightmare for its bank accounts.

Its commanding takeover of the House, significant gains in the Senate and gubernatorial victories across the nation drained the coffers. By the time Republican National Committee Chairman Michael S. Steele left his post in January 2011, the RNC was $24 million in debt with a paltry $300,000 in the bank.

That was then. Going forward into this year’s elections, GOP officials eagerly tout the recent headway they’ve made in fundraising. Just how well they match up to the Democrats’ efforts depends on who’s being asked.

While the RNC and the Republican House and Senate fundraising committees collectively trail their Democratic counterparts in money raised thus far in the 2012 election cycle, the RNC has erased more than half of its debt since early 2011. It also enjoyed a more than $5.5 million cash-on-hand advantage over the DNC through February, the most recent numbers available from the Federal Election Commission show.

The RNC, the National Republican Senatorial Committee and the National Republican Congressional Committee collectively have $7.2 million more in the bank compared with their Democratic equivalents.

And the RNC out-raised the Democratic National Committee four out of the past seven months, despite President Obama’s re-election campaign making significant donations to the DNC.