President Obama told Democratic donors Wednesday night that most Americans don’t have time to think about the details of the Benghazi terrorist attack or the problems with Obamacare.

Speaking at a fundraiser at the Los Angeles home of “Grey’s Anatomy” and “Scandal” creator Shonda Rhimes, the president took his usual shots at the Republican Party, suggesting its members are focused on the wrong issues and frequently “obfuscate and they bamboozle and sometimes don’t say what’s true.”

Furthermore, the president said, Americans cannot be bothered  with the GOP’s focus on the Benghazi attack and the administration’s  response to it, or with the rampant problems and constant changes to Mr.  Obama’s signature health-care reform law.

PHOTOS: See Obama's biggest White House fails

“Over time, they start thinking, ‘you know what, all politicians  are the same.’ And most folks don’t have the time to sort out all the  intricacies of Obamacare or Benghazi, or this or that. They don’t have  time for that,” he said. “All they know is it’s not working for them.  And so people then pull out and they drop out, and they don’t work. And  that further entrenches those who are protecting an unjust status quo.”

Mr. Obama added that “the Republican Party has been taken over by people who just don’t believe in government.”

The president was introduced by Ms. Rhimes and by actress Kerry Washington, an outspoken supporter of the president who worked on his re-election campaign.