The leaders on Capitol Hill have long been unpopular with voters nationwide, but Senate Majority Leader Harry Reid’s favorability rating has taken a steep plunge this election year, showed a new poll released Monday.

The Gallup poll found that the favorability score for Mr. Reid, Nevada Democrat, dropped from 27 percent in April to 21 percent today.

“Reid’s image has consistently tilted negative since he emerged as a nationally known figure in 2008. Before that, the majority of Americans had either not heard of him, or had no opinion of him. But now his net favorable score (the percentage viewing him favorably minus the percentage viewing him unfavorably) is the worst of his career, at negative −24,” the pollsters said.

It’s not all bad news for Mr. Reid, who has become a favorite target of Republicans on the campaign trail this year. His favorable rating remained near 50 percent among Democrats, even as it declined by half or more among independents and Republicans.

House Speaker John Boehner, Ohio Republican, is only slightly more popular across the country with a favorability score of 28 percent. But his rating has taken as bit of a hit as Mr. Reid‘s, falling just 3 points from 31 percent in April.

Still, Mr. Boehner fares worse with his own party than Mr. Reid. The speaker is nearly underwater with Republicans nationwide, with 40 percent viewing him unfavorably and 46 percent viewing him favorably.

“Boehner’s image among Republicans started to sour in 2012, along with the broader slide in favorability toward the Republican Party’s image. However, after recovering somewhat earlier this year, his favorability recently got significantly worse, with his unfavorable score among Republicans rising 11 points since April,” Gallup said.