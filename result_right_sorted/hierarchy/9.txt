Rep. David Dreier, the chairman of the powerful House Rules Committee, said Wednesday that he won’t seek re-election this year, ending a more than three-decade congressional career and setting up a potential scramble for his panel’s chairmanship.

The Rules Committee post, while not well known beyond Capitol Hill, is considered a crucial slot in the House hierarchy and one that is closely linked to the party’s leadership. Often called the chamber’s “gatekeeper,” the committee sets the rules for debate on bills on the floor and — a major source of its clout — decides which amendments proposed by members can be considered by the full House.

The California Republican, whose re-election prospects were complicated because of redistricting, said he contemplated retiring three years ago but decided against it because he wanted to stay in Congress to push for spending cuts, free-trade agreements and national security enhancements.

“This work is far from over, and I intend to spend this year working toward greater bipartisan progress,” he said. “Our economy and our job market remain in peril, and the effort to rein in the deficit has only just begun.”

Mr. Dreier, 59, considered a moderate in the House GOP caucus, said he took the unusual step to announce his retirement on the House floor because he was a “proud institutionalist.” He said he thinks the House is “as great as it has ever been.”

House Speaker John A. Boehner, Ohio Republican, called Mr. Dreier “a great patriot and dedicated public servant whose retirement will be a loss for the people’s House.”