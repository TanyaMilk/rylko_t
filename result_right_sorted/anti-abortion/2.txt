118 feature length films will be shown at this year’s Tribeca Film Festival®. To help you decide which ones you want to see, we’ve taken the liberty of condensing the plot for every one of them into 140 characters (or less).

American Express Card Members can get early access to tickets now through April 5 at 11 a.m. EST; sales for the general public start at 11:00 a.m. EST on April 6.

Ashby. Mickey Rourke is a terminally ill former CIA assassin who takes a socially challenged high-school boy under his wing. Yes, it’s a comedy.

Back to the Future. Celebrate the 30th anniversary of the film with a free screening, plus a sneak peak at the upcoming documentary “Back in Time.”

Come Down Molly. A struggling mother unwinds with good friends and even better mushrooms during a sojourn to a mountain house.

Dirty Weekend. Matthew Broderick enlists co-worker Alice Eve to recreate the events of an alcohol-fueled night that has haunted him for years.

Grandma. Lily Tomlin is a washed-up, heartbroken poet who stirs up trouble during a road trip with her teenage granddaughter.

Man Up. When Simon Pegg mistakes Lake Bell for his blind date, Lake Bell just goes with it.

Monty Python And The Holy Grail. Celebrating 40 years of flesh wounds and knights who say “ni!”

Monty Python’s Life Of Brian. The comedy troupe’s seminal classic about Brian, the unheralded boy born one manger over from Jesus.

Monty Python’s The Meaning Of Life. “Finally, monsieur – a wafer-thin mint.”

Sleeping With Other People. Jason Sudeikis and Alison Brie star as awesome people who ruin relationships because they can’t stop sleeping with other people.

Slow Learners. Adam Pally and Sarah Burns star as uncool people who can’t find relationships, so they vow to start sleeping with other people.

Speedy. DJ Z-Trip adds a new soundtrack to classic silent film about an unemployed Yankees fan trying to save New York’s last horse-drawn trolley.

Tenured. A foul-mouthed 5th grade teacher rejiggers the school play to try to win back his estranged wife.

Tumbledown. Jason Sudeikis charms Rebecca Hall while penning a biography about her deceased folk-singer husband.

When I Live My Life Over Again. Christopher Walken is a past-his-prime crooner. While plotting a comeback, his starving-artist daughter, Amber Heard, returns home.

Aferim! In 19th century Romania, a police officer and his son hunt a runaway gypsy slave.

Aloft. Cillian Murphy searches for his mother, Jennifer Connelly, who disappeared after a mysterious run-in with a traveling healer.

Anesthesia. The chain of events that lead to the brutal mugging of a Columbia professor bring together a disparate group of individuals.

Bad Hurt. Struggling with a special needs child and another with PTSD, the Kendalls life is rocked when a secret from their past is revealed.

Being 14. The excitement and anguish of adolescence are captured in this observational style film about the final year of middle school.

Bridgend. A girl falls prey to depression after moving to a town inspired by a real-life Welsh county that has recorded 79 suicides since 2007.

El Cinco. The embattled veteran star of his city’s soccer team contemplates retirement in this coming-of-middle-age tale.

Cronies. A lifelong friendship is strained after a simple trip to the store turns into a visit to a drug dealer.

The Cut. After surviving the 1915 Armenian genocide, a man travels to America in the hope that his daughters are still alive.

The Driftless Area. When Anton Yelchin falls for Zooey Deschanel he is pulled into a cat-and-mouse game involving John Hawkes and a duffel bag full of cash.

Far From Men. During the Algerian War, Viggo Mortensen is tasked with transporting the man accused of killing his cousin to trial.

Franny. Richard Gere is a rich eccentric who worms his way into Dakota Fanning’s life after her father, a friend of Gere’s, dies.

Good Kill. Ethan Hawke is a drone pilot who begins questioning his mission after racking up kills from a trailer in Nevada.

GoodFellas. A 25th anniversary screening of the seminal mobster movie, which was remastered under Scorsese’s supervision.

Jimmy’s Hall. The reopening of a public dancehall pushes back on oppressive religious and political margins in 1930s Ireland.

King Jack. Hardened, fatherless 15-year-old boy growing up in a rowdy rural town is softened by the arrival of a younger cousin.

Lucifer. An angel falling from heaven to hell inadvertently lands in a small Mexican village.

Ma. A virgin mother treks to Las Vegas in this modern-day retelling of Mother Mary’s pilgrimage.

Meadowland. Olivia Wilde and Luke Wilson are pushed to the brink of devastation after their son’s strange disappearance.

Men Go To Battle. Two brothers attempt to keep their estate afloat in Kentucky at the onset of the Civil War.

Necktie Youth. Two friends from suburban Johannesburg deal with the suicide of a friend with sex, drugs, and misdeeds.

Shut Up And Drive. Jane’s boyfriend moves from L.A. to New Orleans. Heartbroken, she enlists Laura, his wild childhood friend, to take a roadtrip to visit him.

Stranded In Canton. A young entrepreneur from The Democratic Republic of Congo gets sidetracked by karaoke bars and romance while trying to get rich in China.

Sworn Virgin. In a small Albanian village, woman can choose to live as men – and avoid a life of servitude – if they take a vow of chastity.

Viaje. Couple meets at party. Couple decides relationships are stupid. Couple explores nature and falls in love.

Virgin Mountain. A burly, socially awkward 43-year-old virgin who still lives with his mother joins a dance class at the urging of his family.

The Wannabe. Vincent Piazza is shunned after failing to fix the 1992 trial of John Gotti, so he teams up with Patricia Arquette to rob the mob.

We Are Young. We Are Strong. The hours before a 1992 anti-immigrant riot in Germany as seen by a factory worker, a local politician, and the politician’s teenage son.

Wondrous Boccaccio. In plague-stricken Florence, 10 men and women escape to a country estate where they spend their days telling each other stories.

Backtrack. Adrien Brody is a troubled psychotherapist who uncovers a horrifying secret that all of his patients share.

Bleeding Heart. Jessica Biel is a yoga instructor whose placid life is thrown for a loop when her sister seeks help escaping an abusive relationship.

Emelie. When their regular babysitter can’t make it, a couple turns to their friend Anna – who is not what she seems.

Hungry Hearts. When Mina gets pregnant after a whirlwind romance in NYC, she becomes obsessed with protecting her baby from the outside world’s pollutants.

Hyena. A corrupt London cop finds a conscience while trying to protect a young Albanian woman from the underground sex trade.

Jackrabbit. A fringe hacker and a computer technician team up to decipher the message left behind on a friend’s hard drive after his suicide.

Mojave. Garrett Hedlund is a suicidal Hollywood producer who meets Oscar Isaac, a drifter claiming to be the devil, in the Mojave Desert.

Scherzo Diabolico. The impulsive kidnapping of a young girl by a sonata-loving accountant goes awry.

Stung. After a mishap with toxic plant fertilizer, a fancy garden party in a remote country villa is overcome by giant killer wasps.

Slow West. Michael Fassbender has curious reasons for accompanying a 16-year-old on his search for a lost love in the American frontier circa 1900.

Sunrise. A Social Service officer’s pursuit of a kidnapping ring leads him to Paradise, a seedy nightclub in Mumbai.

The Survivalist. The post-apocalypse world has gone agrarian, and two new arrivals at a man’s farm threaten his food supply.

Wednesday 04:45. Drastic measures are taken by a struggling jazz club owner when a shady Romanian gangster calls in his debts.

All Eyes And Ears. When Jon Huntsman was appointed U.S. Ambassador to China, he moved his entire family there – including his adopted Chinese daughter.

All Work All Play. Video games have been rebranded as eSports, and the best pro gamers compete for huge cash prizes at the Intel Extreme Masters championship.

Among The Believers. An amazing look inside the Red Mosque, an Islamabad training ground for kids to devote their lives to jihad.

Angry Sky. ESPN presents the story of Nick Piantanida, an amateur skydiver who set out to break the world record for highest parachute jump.

The Armor Of Light. A Christian minister and anti-abortion activist seeks to change the culture of guns among the Evangelical community.

As I Am: The Life And Times Of DJ AM. An insider’s look at the life and death of America’s first million-dollar deejay.

A Ballerina’s Tale. How Misty Copeland became the first African-American female soloist at New York’s American Ballet Theatre® in two decades.

The Birth Of Saké. Yoshida Shuzo is a 100-year-old family-run brewery that practices the traditional, labor-intensive art of making Saké.

Bodyslam: Revenge Of The Banana! The Seattle Semi-Pro Wrestlers struggle to survive after Paul “The Banana” feels slighted and attempts to have the group disbanded.

Chef’s Table. A preview episode of the upcoming Netflix docu-series focusing on Dan Barber of New York’s Blue Hill restaurant.

Code: Debugging The Gender Gap. The startling facts behind the gender gap in the America’s burgeoning tech sector.

Crocodile Gennadiy. Crocodile Gennadiy tangles with dealers as he works to rescue homeless, drug-addicted youth from the streets of Mariupol, Ukraine.

Democrats. Zimbabwe struggles to implement a democratic government after Robert Mugabe’s highly criticized 2008 presidential win.

The Diplomat. David Holbrooke attempts to capture the legacy of his larger-than-life father, Ambassador Richard Holbrooke.

Down In The Valley. NBA superstar turned mayor Kevin Johnson battles to keep his beloved Kings in Sacramento.

dream/killer. 20-year-old Ryan Ferguson went to prison for a murder that he did not commit; his father battles to prove his innocence.

Drunk Stoned Brilliant Dead: The Story Of The National Lampoon. Rare, never-before-seen archival footage traces National Lampoon’s evolution from underground countercultural movement to mainstream brand.

The Emperor’s New Clothes. Russell Brand and Michael Winterbottom team up to expose the reality of economic inequality after the financial crisis.

Every Day. Joy Johnson, age 86, was the oldest woman to run the 2013 NYC Marathon. Her story is pretty darn inspiring.

Fastball. How hitting a 100 mph pitch with a narrow stick defies logic and skill, approaching something akin to sheer intuition.

A Faster Horse. Ford’s Chief Program Engineer Dave Pericak shows how perilous redesigning the iconic Mustang can be.

Gored. Antonio Barrera, the “Most Gored Bullfighter in History,” grapples with the end of his career.

In My Father’s House. Grammy Award–winning rapper Che “Rhymefest” Smith reconnects with his father, who abandoned him over 20 years ago.

In Transit. Interviews with runaways, adventurers, and loners on Empire Builder, America’s busiest long-distance train route.

Indian Point. The controversial history and uncertain future of the aging nuclear power plant, residing just 35 miles from Times Square.

Kurt Cobain: Montage Of Heck. The first fully authorized documentary of the iconic grunge rocker’s hermetic life and tragic death.

Live From New York! A love letter to Saturday Night Live on its 40th anniversary, chock full of insightful commentary and memorable moments.

Lovetrue. An exclusive preview of clips from director Alma Har’el’s latest film-in-progress about three challenging relationships.

Mary J. Blige – The London Sessions. 10 days of behind-the-scenes access with the acclaimed singer as she records her 13th studio album in London.

Monty Python: The Meaning Of Live. Watch the revered comedic troupe prepare for their last-ever live show. Laugh often.

Most Likely To Succeed. An examination of how a broken education system lags behind technological innovation.

A Nazi Legacy: What Our Fathers Did. Two sons of infamous Nazi Governors discuss their polar opposite views of their fathers’ hand in the war.

Orion: The Man Who Would Be King. How Sun Records gave a grieving public what they wanted – hope the Elvis had faked his death – in the form of masked impersonator Orion.

Palio. A young jockey faces off against his former mentor in the world’s oldest and most dangerous horse race.

Peggy Guggenheim – Art Addict. The niece of the famed museum builder lived a life that was as colorful and salacious as the artwork she revered.

Play It Forward. The story of Tony Gonzalez, widely considered the best tight end in NFL history, and his brother, whose football ambitions never panned out.

Prescription Thugs. As Americans consume 75% of the world’s prescription drugs, documentarian Chris Bell challenges the assumption that legal means safe.

Requiem For The American Dream. Noam Chomsky shows how policies have helped concentrate wealth in the hands of a few at expense of everyone else.

Roseanne For President! Did you know Roseanne Barr ran for president in 2012? Well she did, and this is what happened.

Song Of Lahore. Artists in Lahore, a city world-renowned for its music prior to the Islamization of Pakistan, struggle to keep the tradition alive.

Steak (R)Evolution. Chefs, farmers, butchers, and critics weigh in on what makes a great steak in the age of sustainable farming.

(T)Error. A 63-year-old FBI informant attempts to befriend a suspected Taliban sympathizer and build a fraudulent case against him.

Thank You For Playing. Ryan and Amy Green have spent 2 years working on “That Dragon, Cancer,” a videogame about their son’s fight against the disease.

Thought Crimes. The polarizing case of New York’s “Cannibal Cop” raised the question: can you be guilty of a crime you only thought about committing?

Tom Swift And His Electric Rifle. Taser claims its stun guns save lives, yet people have suffered taser-related deaths at the hands of officers. Who’s to blame?

Toto And His Sisters. Siblings in a Bucharest slum try to survive for 15 months while awaiting their mother’s release from jail.

Transfatty Lives. The onetime NYC deejay and Internet superstar documents his life after being diagnosed with ALS and given only years to live.

Very Semi-Serious. A behind-the-scenes look at The New Yorker’s cartooning legends, who have inspired (and confused) us for decades.

The Wolfpack. Raised by an overprotective father, the Angulo brothers learned about the outside world by watching movies – until one of them escaped.