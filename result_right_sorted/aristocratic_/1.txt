Now that Lady Rose and Atticus Aldridge have tied the knot, what better way to cement the bond between their British aristocratic families than with a grand shooting party? This Sunday’s “Downton Abbey” season 5 finale will find the Crawleys joining their new in-laws, Lord and Lady Sinderby, at their majestic rented castle, but as you can see from this clip, the tensions are still running high.

Although he appears to have accepted Rose as his daughter-in-law, the imperious Lord Sinderby has no qualms about treating her father, “Shrimpie,” as an outcast. And it’s not because the snobbish head of the Jewish Aldridge family has a distaste for men named after shellfish – it’s because Rose’s parents are divorcing. While participating in a shoot in the lush hillsides of Northumberland, Lady Mary (Michelle Dockery) attempts to reason with Lord Sinderby (James Faulkner), without much success.

Lady Sinderby (Penny Downie), on the other hand, is more inclined to make friends with her new relatives. While paired up with Tom Branson (Allen Leech) – who’s hanging around for the finale despite his all-but-confirmed plans to set off for Boston – Atticus’s mum bonds with the Crawley-family black sheep over their mutual difficulties in bringing such different worlds together.

The season finale of “Downton Abbey” airs Sunday, March 1 on “Masterpiece” on PBS at 9 p.m. ET.

For the latest entertainment news Follow @WSJSpeakeasy