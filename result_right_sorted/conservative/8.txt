It’s been a rough year for love in India.

Kissing protestors demonstrated in November against alleged harassment from Hindu hardliners who said public displays of affection are not part of Indian cultural values, even though some say the kiss originated in India. 

One college lecturer was asked by the college authorities to justify her actions when caught in a lip-lock with her husband at the “Kiss of Love” protest.

Before that, Hindu activists warned women to beware of ‘Love Jihad’ that they said was a conspiracy to convert females to Islam through marriage. Critics said the idea was nonsense.

Zafarul Islam Khan, president of the All India Muslim Majlis-e-Mushawarat, an association of Muslim organizations in India, said the campaign was “a weapon for hate-mongering” by Hindu groups who were putting a spin on interreligious marriages to “malign one community.”

And there’ll be no escaping scrutiny for the amorous this Valentine’s Day that falls on Saturday.

A conservative Hindu group’s leader said volunteers with the Akhil Bharat Hindu Mahasabha (All India Hindu Assembly) will fan out across malls, metro stations and parks in Delhi to find romancing partners and bring them back to their offices to wed in accordance with Hindu tradition.

“We will gently explain this is against our culture. If you are really in love, we will help you get married and make you swear never to let your heart or eyes stray again,” said  Chandra Prakash Kaushik, president of the assembly.

Conservative Hindu groups in the past have protested celebrations of Valentine’s Day in India, warning that it is a cultural import from the West and can lead to “a moment’s happiness, immoral behavior and destruction.”

Here’s a brief timeline of Valentine’s Day travails in India.

2009: Hindu hardliners were reported to have attacked women in a pub in Mangalore in the weeks leading up to Feb. 14. Some women were physically assaulted and dragged out by their hair in the attack that was filmed.

2010: Pune city police issued warnings against “indecent acts” on Valentine’s Day telling couples to avoid public displays of affection from Feb. 3 to 14, according to a report in the Indian Express newspaper. “Any public show of affection that is tantamount to obscenity,” was banned, according to the report.

2012: Let’s turn Valentine’s Day into “Worship Your Parents Day” proposed a self-styled Hindu godman, Asaram Bapu.

Suggested celebrations included school children offering flowers, sweets and garlands to their parents.

2014: About 200 tourists gathered around a bonfire under the full moon at a Valentine’s Day beach party to accuse police of beating them, according to the Bangalore Mirror.

2014: According to a report in the Hindu newspaper, members of the hardline Hindu organization Vishwa Hindu Parishad threw rotten tomatoes at dozens of couples gathered by the banks of the Sabarmati river in the western Indian city of Ahmedabad.

To be sure, India is not to the only country grappling with Valentine’s Day.

Pakistan’s media regulator in 2014 asks television and radio stations to avoid offending religious sentiments and corrupting the nation’s youth in their Valentine’s Day broadcasts.

The Bangkok Post reported that the government this year urged teens to think dinner plans instead of sex with their partners on Valentine’s Day.

Indonesia’s top Islamic authority urged the government to look into incidents where condoms were on sale for Valentine’s Day with boxes of chocolates, saying it offended conservative sentiments.

“We reject the condomization of society,” said Ma’ruf Amien, chairman of the Indonesian Council of Ulema, an Islamic organization representing different Muslim groups.