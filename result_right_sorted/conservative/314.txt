The good news for President Obama out of Monday’s Supreme Court immigration ruling is that the justices all seemed to agree that he has broad discretion over whom he chooses to deport.

The bad news for him is that he is about to face extreme pressure to grant a blanket exemption to most illegal immigrants, particularly those who now will be found by local police in Arizona.

While the justices ruled 5-3 to strike down most of Arizona’s law, all eight gave police tentative approval to check the immigration status of those they have reasonable suspicion are in the country illegally, and to report those immigrants to federal authorities.

But their ruling also made clear that they believe the president has broad authority to decide who gets deported.

“The pressure’s on him,” said Alfonso Aguilar, a conservative pushing for legalization.

He said Mr. Obama has been slow to act. “If he really has that discretion, he could stop those deportations. So why doesn’t he do this?”