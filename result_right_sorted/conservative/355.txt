President Johnson declared a War on Poverty in 1964. Over the subsequent three decades, the federal government created a maze of welfare programs to distribute a growing mountain of taxpayer money. But year after year, poverty seemed to win the war.

In 1996, conservatives forced President Clinton to try a new approach. Our welfare reforms replaced a failed New Deal program with Temporary Assistance for Needy Families (TANF). Conservatives built TANF around two simple ideas: States would get a level, predictable amount of funding with the flexibility to use it as they thought best. In return, able-bodied adults getting TANF checks would have to work, look for work, take classes or otherwise prepare themselves to regain their independence.

On one hand, we gave states the freedom to experiment and innovate. On the other, we expressed tough love that encourages people to take responsibility for their lives. The formula succeeded even beyond expectations.

Unfortunately, we never used the TANF model to reform the rest of welfare. Down 10-0, conservatives scored one goal and high-fived all the way to the locker room. Meanwhile, liberals spent the next 16 years trying to undo the accomplishments of 1996. The latest attack came just recently, when the Obama administration conjured an illegitimate legal excuse for rolling back TANF’s work requirements.

It’s time for conservatives to get back in the game.

The American welfare state has failed the poor. It has squandered decades, dollars and good intentions, while families and communities have suffered the consequences. Liberals only offer more of the same. We can do better.