With the general election campaign unofficially under way, Mitt Romney’s effort to win back female voters got off to a rocky start Wednesday when his campaign was stumped temporarily over whether he supports the 3-year-old Lilly Ledbetter Fair Pay Act.

Democrats jumped on the incident — which came as Mr. Romney’s domestic policy staffers were attacking President Obama’s jobs record when it comes to female workers — saying it calls into question the former Massachusetts governor’s own commitment to a fair shake for the fairer sex.

The Romney staffers paused when asked what Mr. Romney’s position was on the law, then promised to get back to reporters with an answer. Within an hour, they had it: Mr. Romney would not repeal the law, which was the first legislation Mr. Obama signed in 2009.

The miniflap underscores a critical reality for both campaigns: Women are a majority of the population and an early battleground in this year’s election, and Mr. Obama’s recent success in winning their support has boosted his overall standings in the polls.

“In every presidential election since 1980, there’s been a gender gap where women are far more likely to favor Democrats than men are,” said Jennifer Lawless, director of the Women & Politics Institute at American University’s School of Public Affairs. “The margin by which they favor the Democrat is what decides elections.”

Ms. Lawless said now that former Sen. Rick Santorum has dropped out of the race, essentially sealing Mr. Romney as the Republican nominee, it gives Mr. Romney a chance to try to reconnect with female voters. When Mr. Santorum was in the race, she said, the focus on conservative social issues was hurting the entire Republican field.