More New Yorkers are keeping their nests empty.

The city’s birth rate is the lowest since 1936 — having steadily declined over the past decade, according to data obtained by The Post.

“This is a very troubling trend,” said Conservative Party state chairman Mike Long. “The economy is hurting families and the development of families.”

“If we don’t produce enough young people, society won’t be able to pay for Social Security and Medicaid,” he warned.

Half the city’s newborns were delivered by foreign-born moms, with Asians boasting the highest rate, 17.1 per 1,000 residents, according to the 2013 data, which is the most recent available.

And a significant number of births are from the city’s poorest neighborhoods — nearly 6 in 10 moms were on Medicaid or government-financed health insurance for the needy.

And about 40 percent of the births overall were to unwed mothers.

Blacks had the lowest birth rate citywide, with 12.7 per 1,000 — and more black women are having abortions than babies, at a rate of 55 percent.

“When the abortion ratio reaches nearly 60 percent in some communities, we as community leaders need to examine the choices we are making as we educate our young people,” the New York Metropolitan Clergy for Better Choices said.

But some groups are being fruitful and multiplying.

Borough Park, heavily Orthodox Jewish, is easily the city’s baby capital, with a robust birth rate of 27.9 per 1,000 residents, according to the data. In 2013, some 5,458 babies were born in the Brooklyn enclave.

“Our community is making up for the rest of the city,” said Borough Park state Assemblyman Dov Hikind, who has six grandkids of his own.

“In our community, having children is fundamental. They are our most precious resource,” he said.

Bayside, Queens, had the lowest count with just 652 babies or 5.5 per 1,000.

There are fewer unplanned pregnancies, data show. The teen birth rate has plummeted 10 percent since 2012 and 37.6 percent since 2004.

“We attribute the drop in teen pregnancy to a decline in sexual activity and an increase in the use of hormonal or highly effective birth control,” the city Health Department said in a statement.