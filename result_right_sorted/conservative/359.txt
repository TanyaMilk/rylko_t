President Obama will coast down Pennsylvania Avenue on Tuesday evening, emerging from his limousine to address the nation as a leader with neither muscle nor momentum.

Gone is all the hope that Mr. Obama brought to the House chamber for his first State of the Union speech five years ago, hope that he would bring transparency, bipartisanship and change to a capital stymied by partisan gridlock.

The adjective nearly everyone applies to him now is “weak.” This is the word that must never, ever apply to the leader of the free world, whether conservative or liberal, Democrat or Republican. The president seems oblivious to his position.

The latest ABC News-Washington Post survey counts a majority of registered voters who disapprove of the way the White House has handled health care and the economy. Sixty-two percent say America is on the wrong track “moving forward,” as the cliche goes.

Given the Obamacare fiasco, rampant unemployment and no economic growth, how could it be otherwise? What ought to concern all the president’s men is how tarnished Mr. Obama’s reputation has become.

When Barack Obama took the oath of office on a chilly January day in 2009, he did so on a wave of joyous enthusiasm. Even those who did not share the enthusiasm could nevertheless feel it. Seventy-two percent of voters told ABC’s pollsters on that day that Mr. Obama “understands the problems of people like us.”