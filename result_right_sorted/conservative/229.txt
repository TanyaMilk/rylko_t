Some state Republican Party leaders and influential conservatives say they are worried that the early stages of Mitt Romney’s campaign have been marked by missteps and missed opportunities in the bid to unseat President Obama in November.

Several point to the failure of the Romney campaign’s top advisers this year to exploit Republican gains in the key electoral state of Pennsylvania by pushing through a change in the formula for how such votes are awarded. Pennsylvania holds 20 electoral votes.

Instead, Romney aides apparently settled on a strategy of placing all their chips on three swing states — Virginia, Ohio and Florida — closely aligning the campaign staff with the Republican National Committee operation in each state into a single planning-and-spending unit. Virginia, with 13 electoral votes, Ohio, with 18, and Florida, with 29, are seen as critical to the Republican’s hopes in what polls suggest will be a tightly fought race.

“When you add up all the seven or eight states that are competitive and likely to go to Romney and those likely to go to Obama, the three left up for grabs are Virginia, Ohio and Florida,” said Virginia GOP Chairman Pat Mullins. “To win, Romney or Obama will have to carry two of these three states. That’s my conclusion and the general consensus I gathered from traveling around.”

But the strategy carries risks and could leave the Romney campaign with no margin for error.

“Independents are a little to the right of center, and Romney hasn’t closed [the] deal with them yet,” Ohio GOP Chairman Bob Bennett said.