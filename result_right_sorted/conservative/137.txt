The role of New York’s high cigarette taxes in the death of Eric Garner drew more scrutiny Sunday as his widow said that he was targeted not because of his race, but because he continued to defy local ordinances by selling loose cigarettes.

Esaw Garner said police in Staten Island knew her husband sold individual cigarettes and would harass him, calling him “cigarette man” and her “cigarette man wife.”

“I feel that he was murdered unjustly. I don’t even feel like it’s a black-and-white thing, honestly, in my opinion,” Ms. Garner said on NBC’s “Meet the Press.” “I feel like it’s just something that he continued to do. And the police knew.”

New York has the highest state taxes on cigarettes in the nation at $4.35 per pack, while New York City tacks on an additional $1.50 in taxes, bringing the total tax rate to $5.85 per pack. The taxes bring in an estimated $1.8 billion per year.

Conservative talk show host Rush Limbaugh said Sunday that police targeted the 43-year-old Garner “because they’re so eager for tax collection.”

“What was Eric Garner doing? He was selling cigarettes, loose cigarettes,” Mr. Limbaugh told “Fox News Sunday.” “And the police in New York, because they’re so eager for tax collection — what is being done here [pertains] to taxes and the state’s desire to collect them no matter what.”