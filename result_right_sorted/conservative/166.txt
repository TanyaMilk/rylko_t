During his first-ever guest appearance on HBO’s “Real Time” last summer, libertarian author Nick Gillespie knew his role. The villain. The talk show equivalent of a pro wrestling heel. The closest thing to — gasp — a designated conservative voice.

Of course, that’s what made it fun.

Mr. Gillespie looked the part: black shirt, trademark black jacket, sardonic air, everything but a dastardly handlebar mustache. He sparred with host Bill Maher over the debt ceiling and global warming and was jokingly — make that half-jokingly — challenged to a fistfight by Democratic mayor John Fetterman of Braddock, Pa. By the show’s end, Mr. Gillespie had picked up hundreds of new Twitter followers, many of whom were unimpressed by his political points of view.

Not to mention his wardrobe.

“I was roundly criticized for that,” said Mr. Gillespie, the editor of Reason.com. “On Twitter, someone sent me a note with a hashtag ‘superdouche.’ Going on the show was like going into a blast furnace and getting a face peel. It was incredibly invigorating to be the object of that much hate over the course of a weekend, ” Mr. Gillespie laughed.

“To paraphrase Gandhi, first they try to ignore you, then they laugh at you, then they call you a douche, and then they start engaging you in conversation.”