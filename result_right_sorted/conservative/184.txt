IRS emails released Wednesday show that just before the tea party targeting scandal was revealed last year, Lois G. Lerner and her colleagues at the tax agency were talking with the Justice Department about making examples out of nonprofit groups that they felt were violating campaign laws by playing political roles.

The emails, obtained by Judicial Watch, also show that Ms. Lerner was reluctant to face questions from Congress even before her first hearing, at which she asserted her right to remain silent. It was an indication that she distrusted Republican motives from the start.

The emails also show how important congressional Democrats were in pushing for action at the Internal Revenue Service and the Justice Department, which talked about how to follow up on a senator’s demand to pursue a test case against a nonprofit group that was spending money on politics.

SPECIAL COVERAGE: Tea Party Voices

“Not only do these emails further prove the coordination among the IRS, the Federal Election Commission (FEC), the Justice Department and committee Democrats to target conservatives, they also show that had our committee not requested the Inspector General’s investigation when we did, Eric Holder’s politicized Justice Department would likely have been leveling trumped up criminal charges against Tea Party groups to intimidate them from exercising their Constitutional rights,” Rep. Jim Jordan, an Ohio Republican who is leading an investigative subcommittee looking into the IRS, said in a statement.

In May 2013, just before the IRS targeting was publicly exposed, Ms. Lerner fielded a request from the Justice Department to talk about following up on Senate Democrats’ push to prosecute groups that the lawmakers thought “lied” when they told the FEC that they wouldn’t be conducting political activities but made political expenditures anyway.

The Justice Department requested a meeting to talk about what to do and wanted to know whether that would interfere with the IRS.