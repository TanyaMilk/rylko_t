Police in a New Jersey borough confirmed that a black flag associated with the Islamic State militant group has been voluntarily removed from the front of a resident’s home after a photo of the flag went viral.

Garwood Police Chief Bruce D. Underhill confirmed to the Washington Free Beacon that officers were “aware of the situation” and residents at the suburban home “voluntarily” agreed to take down the flag. A Turkish flag reportedly remains displayed at the home.

“Please be assured that the Garwood Police are aware of the situation and have taken the appropriate steps,” Chief Underhill wrote in an email to the Free Beacon on Wednesday. 

The original photo of the home drew harsh criticism early Tuesday, even gaining the attention of conservative pundit Glenn Beck.

Twitter users posted an updated photo of the same home that appeared to confirm that the Islamic State flag had been removed, the Free Beacon reported.

An earlier report said the flag was ordered to be removed, but police say that is not true. Chief Underhill also declined to tell the Free Beacon what “appropriate steps” were taken.