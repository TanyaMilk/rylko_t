A federal court shot down a tea party group’s effort to permanently bar the Internal Revenue Service from targeting conservative groups for special scrutiny, issuing a ruling Thursday that says the tax agency has taken enough steps to correct the problem.

Judge Reggie B. Walton also refused the request by True the Vote, a Texas-based group that tries to combat election fraud, to make Lois G. Lerner and other current and former IRS employees pay a penalty for having blocked the group’s tax-exempt status and made intrusive inquiries into the group’s activities.

Without ruling on whether the initial targeting was unlawful, Judge Walton said there was no longer a case because the IRS eventually did approve tax-exempt status.

PHOTOS: Conservatives in Hollywood: Celebrities who lean right

True the Vote argued that the IRS was pressured into stopping the targeting but could restart it at any time. The group asked the court to issue an order prohibiting the IRS from targeting, but the judge declined.

“The defendants’ grant of tax-exempt status to the plaintiff, and the defendants’ suspension of the alleged IRS targeting scheme during the tax-exempt application process, including remedial steps to address the alleged conduct, coupled with the reduced ‘concern about the recurrence of objectionable behavior’ government actors … convinces the court that the ‘voluntary cessation’ exception is not applicable here,” wrote Judge Walton, who was appointed to U.S. District Court for the District of Columbia by President George W. Bush.

A True the Vote spokesman didn’t have an immediate comment when reached by phone Thursday. The IRS also didn’t comment.