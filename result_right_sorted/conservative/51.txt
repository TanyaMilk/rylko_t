During one of her weekly sessions at SoulCycle last year, Noa Mintz, the founder of Nannies by Noa, had an epiphany: In order to get her booming 3-year-old agency to the “next level,” she would need to hire a CEO to take on day-to-day business matters.

She simply couldn’t handle the “excruciating hours” and “hundreds of emails” she was fielding per day.

Fair point. After all, the mini-mogul is only 15 and just now indulging in a rare vacation — a ski trip out West with her family.

“It’s hard to step away,” she admits. “I never get peace, so I’m very much enjoying this.”

Her decision to delegate has paid off. Her full-service agency now serves 190 clients in the tri-state area, providing them with both baby sitters and full-time nannies. And in a bid to grow her brand, she just struck up a partnership with the Ivy Key tutoring and test-prep service.

Nearly three years ago, while most sixth-graders were texting their friends and watching the Disney Channel, Mintz spent the first half of the summer interning at a nonprofit — and the second half hatching her nanny agency idea.

That’s when the Upper West Side big sister of four — who always found flaws with her own baby sitters — figured there must be a better way to pair families and child caretakers. She set out to do just that, first for her own family, and then for her parents’ friends.

“For what you’re paying, your kids should be more stimulated,” says the teen. “At 7, I would tell my mom, ‘You need to get more bang for your buck.’ It would drive me insane!”

She initially found caregivers by networking with nannies in her circle, as well as recruiting at SoulCycle and on college-career websites. For every match, she charged clients $100 to $200. Word of mouth soon spread.

A divorced Upper West Side dad of two, Eric Gibbs was one of her early clients.

“I’d used an agency before,” says the orthodontist, “and someone said, ‘You have to try this young girl,’ who was 13 at the time. And I was like, ‘No, really?’ ”

With the help of her father — who works in private equity and filed the LLC when the minor couldn’t — Mintz has rapidly grown her business. She now charges a standard 15 percent of a nanny’s initial gross salary, which ranges from $50,000 to $80,000. For baby-sitting services, clients are charged a flat fee of $5 an hour.

According to the agency, it’s currently placing about 25 full-time nannies and 50 baby sitters, who work an average of 15 hours per week — putting a conservative estimate on revenues at $375,000, a figure Mintz won’t confirm. “Because we’re a privately held company,” she says, “we cannot disclose our financials.” (She says she doesn’t draw a salary “for now” — but won’t rule out one in the future.)

Mintz prides herself on high standards — like cultivating “engaged nannies who don’t sit on the side at the playgrounds on their phones.” There are probing questions and a rigorous social media screening, in addition to the usual reference and background checks, interviews and résumés with photos for clients.

Now veteran nannies are switching over to her.

“Noa interviewed me on the phone. I had no idea she was a kid. I was intimidated — she’s so well-spoken,” says Dahlia Weinstein, 37, a 10-year nanny veteran placed by Mintz.

In July, Mintz hired CEO Allison Johnson, 26, a licensed social worker who had initially applied to be a nanny.

“It was a little bit of a challenge at first,” Johnson admits of taking orders from a kid — and negotiating her salary with one.

“She was 14! [But] I’m a feminist, and I really support women who do things for themselves and get their visions out there.

“We’re in touch every day — phone and email,” she continues. “She’ll get back to me during study hall. She can’t shut off.”

Mom Meredith Berkman, a former journalist, isn’t surprised by her daughter’s ambition.

“Noa is a natural-born serial entrepreneur — from 6 or 7, she was always trying to start these mini-companies,” says Berkman.

“She’d reach out to my friend and ask to be a consultant for birthday parties. There are kids whose lives are absorbed by tennis or acting — but this is her baby, her startup. It’s not a hobby for her. This isn’t a lemonade stand.”

Inside the Midtown conference room where she meets weekly with her CEO, Mintz takes it all in.

“It’s crazy to look back and see that I gave people jobs. It’s amazing to see what I’m capable of,” she says. “Telling people I’m in high school now, it’s more reassuring, I think, than saying I’m a middle schooler. What mom in her right mind would trust someone so young?

“I always say, ‘Don’t let my age get in the way.’ ”