Texas Gov. Rick Perry on Sunday again reached out to Iowa’s religious conservatives in an effort to resurrect his flagging presidential campaign.

On “Fox News Sunday,” he repeated his criticism of the Obama administration’s decision to allow gays to serve openly in the military by overturning the “don’t ask, don’t tell” policy.

The Texas governor, who has been running a new commercial in Iowa touting his Christian faith, called the White House position “irresponsible.”

“This administration’s values are different than, I would suggest, certainly the people in Iowa,” he said.

In his commercial, Mr. Perry says, “I’m not ashamed to admit I’m a Christian. But you don’t need to be in the pew every Sunday to know there’s something wrong with this country when gays can serve openly in the military, but our kids can’t openly celebrate Christmas or pray in school. As president, I’ll end Obama’s war on religion.”

In the latest Republican candidates’ debate Saturday night, Mr. Perry landed jabs on both front-runners in the GOP field, former House Speaker Newt Gingrich and former Massachusetts Gov. Mitt Romney.