Republican Sen. Ted Cruz, who has vowed to repeal “every word” of ObamaCare if elected president next year, will soon be signing up for coverage under the plan.

Cruz, according to media reports, had been covered under the health plan of his wife, Heidi, who is taking a leave of absence from Goldman Sachs to help his campaign.

“We will presumably go on the exchange and sign up for health care, and we’re in the process of transitioning over to do that,” the Texas lawmaker told the Des Moines (Iowa) Register on Tuesday.

Under President Barack Obama’s signature Affordable Care Act, members of Congress seeking insurance must sign up through an exchange.

“Well, it is written in the law that members will be on the exchanges without subsidies just like millions of Americans,” Cruz told the Register, adding: “I think the same rules should apply to all of us. Members of Congress should not be exempt.”

In September 2013, efforts by Cruz and House conservatives to gut ObamaCare by holding up a government spending bill led to a 16-day government shutdown.

Cruz, a conservative firebrand who is a favorite of the Tea Party movement, on Monday became the first major figure in either party to enter the 2016 presidential race.