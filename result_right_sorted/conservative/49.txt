WASHINGTON — Democrats in the White House and Congress accused 47 GOP senators of undermining President Barack Obama in international talks to curb Iran’s nuclear program, saying that trying to upend diplomatic negotiations was tantamount to rushing into war with Tehran.

“The decision to undercut our president and circumvent our constitutional system offends me as a matter of principle,” Vice President Joe Biden said in a statement.

In an open letter Monday to the leaders of Iran, Republican lawmakers warned that any nuclear deal they cut with Obama could expire the day he walks out of the Oval Office. The letter was an aggressive attempt to make it more difficult for Obama and five world powers to strike an initial agreement by the end of March to limit Iran’s nuclear program, which Tehran insists is for peaceful purposes.

Republicans worry that Iran is not negotiating in good faith and that a deal would be insufficient and unenforceable, allowing Iran to eventually become a nuclear-armed state. They have made a series of proposals to undercut or block it — from requiring Senate say-so on any agreement to ordering new penalty sanctions against Iran to threats of stronger measures.

The Republicans’ move comes just days after Israeli Prime Minister Benjamin Netanyahu spoke to a joint meeting of Congress at Republican House Speaker John Boehner’s invitation. In his address, Netanyahu bluntly warned the United States that a deal would pave Iran’s path to a nuclear bomb.

“I think it’s somewhat ironic that some members of Congress want to make common cause with the hard-liners in Iran,” Obama said about conservative Iranians who also are leery of, or downright against, the negotiations. “It’s an unusual coalition.”

The letter, written by freshman Sen. Tom Cotton, was addressed to the “Leaders of the Islamic Republic of Iran” and presents itself as a constitutional primer to the government of an American adversary. The signature of Senate Majority Leader Mitch McConnell of Kentucky is on it, as are those of several prospective presidential candidates.

Explaining the difference between a Senate-ratified treaty and a mere agreement between Obama and Iran’s Ayatollah Ali Khamenei, the senators warned, “The next president could revoke such an executive agreement with the stroke of a pen, and future Congresses could modify the terms of the agreement at any time.”

Cotton defended the letter in a series of television appearances Tuesday morning, denying emphatically that it undermines Obama’s negotiating position with Iran.

“No,” he said. “We’re making sure that Iran’s leaders understand that if Congress doesn’t approve a deal, Congress won’t accept a deal.”

Appearing on MSNBC, Cotton said Iran “has a very clear and simple path,” calling on Tehran to renounce nuclear weapons, disarm and allow unobstructed, unimpeded international inspections. The Arkansas Republican accused Iran of seeking “a nuclear umbrella so they can continue to export terrorism around the world.”

Iranian Foreign Minister Mohammed Javad Zarif responded to the letter via state media Monday, dismissing it as a “propaganda ploy” and noting that many international deals are “mere executive agreements.” He suggested the senators were undermining not only the prospective deal with Iran but other international agreements as well.

Senate Minority Leader Harry Reid said Republicans were driven by animosity toward Obama and unwilling to recognize that American voters had twice elected him president. Reid said that even at the height of Democrats’ disagreement about the war in Iraq with former President George W. Bush, they would not have sent a letter to former Iraqi leader Saddam Hussein.

“Republicans don’t know how to do anything other than juvenile political attacks against the president,” Reid said.

Biden said, “In 36 years in the United States Senate, I cannot recall another instance in which senators wrote directly to advise another country — much less a longtime foreign adversary — that the president does not have the constitutional authority to reach a meaningful understanding with them.”

Sen. John McCain of Arizona, the GOP’s 2008 presidential nominee, called the White House’s objections to the letter “a tempest in a teapot.” Congress obviously will want a voice in any deal with Iran, McCain told reporters, suggesting the Democrats’ protests might be “a diversion from a lousy deal.”

Not all Republican senators are united. One significant signature missing from Monday’s letter was Bob Corker of Tennessee. As chairman of the Senate Foreign Relations Committee, he said he wants to focus on a bipartisan effort that can generate a deal.

Negotiating alongside the US are Britain, China, France, Germany and Russia. Nuclear negotiations resume next week in Switzerland.

Officials say the parties have been speaking about a multi-step agreement that would freeze Iran’s uranium enrichment program for at least a decade before gradually lifting restrictions. Sanctions relief would similarly be phased in.

The Obama administration believes it has authority to lift most trade, oil and financial sanctions that would be pertinent to the nuclear deal in exchange for an Iranian promise to limit its nuclear programs. For the rest, it needs Congress’ approval. And lawmakers could approve new Iran sanctions to complicate matters.