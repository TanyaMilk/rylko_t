JAKARTA, Indonesia (AP) — Lady Gaga will have to cancel  her sold-out show in Indonesia following protests by Islamic hard-liners  and conservative lawmakers, who said her sexy clothes and dance moves  will corrupt young people.

National police spokesman Boy Rafli  Amar, responding to the pressure, said Tuesday that the permit for her  June 3 “Born This Way Ball” concert had been denied.

Indonesia, a  nation of 240 million people, has more Muslims than any other. Although  it is secular and has a long history of religious tolerance, a small  extremist fringe has become more vocal in recent years.

Hard-liners  have loudly criticized Lady Gaga, saying the suggestive nature of her  show threatened to undermine the country’s moral fiber. Some threatened  to use physical force to prevent her from stepping off the plane.

Lawmakers and religious leaders, too, have spoken out against her.

Worried  they could not guarantee security, local police recommended the permit  for the show be denied, said Mr. Amar, adding that national police decided  Tuesday to comply.