Locked into a fierce GOP primary fight that has Democrats dreaming of an unexpected chance to pick up a U.S. Senate seat, Sen. Richard G. Lugar of Indiana is struggling to deflect an onslaught of attacks by tea-party sympathizers trying to oust him in favor of state Treasurer Richard Mourdock.

As early voting began Monday, the National Rifle Association and Club for Growth unrolled television and radio ads painting the seasoned lawmaker as an establishment sellout and Mr. Mourdock as the true conservative. Separated by seven points in the most recent Indiana poll, the two are scheduled to debate Wednesday.

The unusually small lead is worrisome for the 80-year-old Mr. Lugar, who has served longer than any other Republican in the Senate and hasn’t faced a serious battle for his seat in decades. If Mr. Mourdock pulls out a victory on May 8, analysts say it could give a strong edge to Democratic challenger Rep. Joe Donnelly in November.

Like other veteran lawmakers in recent years, Mr. Lugar is finding his lengthy tenure and diplomatic style of legislating are becoming dangerous liabilities, with his opponents blasting him for voting for the bank bailout and President Obama’s Supreme Court nominees and initially supporting Mr. Obama’s immigration plan.

“If Lugar does hold on, he can be very thankful he wasn’t running in 2010, because he fits the profile of a lot of those senators who got knocked off,” said John Krull, a journalism professor at Franklin College.

Down to the wire