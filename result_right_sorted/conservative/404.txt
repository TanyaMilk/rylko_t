Former New York City Mayor Rudy Giuliani said he was reluctant to say this — but he didn’t think President Obama “loves America” one bit.

“I do not believe — and I know this is a horrible thing to say — but I do not believe that the president loves America,” he said during a dinner at the 21 Club that was attended by Wisconsin Gov. Scott Walker, business executives and conservative media members, Politico reported.

His reasons?

The president is soft on foreign policy and weak on fighting terrorism, he said, Politico reported.

“He doesn’t love you,” Mr. Giuliani said, Politico reported. “And he doesn’t love me. He wasn’t brought up the way you were brought up and I was brought up through love of this country.”