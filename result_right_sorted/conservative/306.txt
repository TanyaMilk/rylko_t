The showdown between Rick Santorum and Newt Gingrich over which candidate is the true “conservative alternative” to Mitt Romney was supposed to be the big story in Tuesday’s GOP contests in Alabama and Mississippi — but polls show the former Massachusetts governor is poised to rewrite the narrative that he can’t win in the Deep South.

Mr. Romney, a Mormon from a Northeast state, expected a degree of difficulty in the South — at one point, he acknowledged that the Alabama and Mississippi contests represented “an away game.” But polls in both states show Mr. Romney, Mr. Santorum and Mr. Gingrich running neck-and-neck, with Rep. Ron Paul of Texas bringing up the rear.

“I see a real close three-person race,” said Richard C. Fording, political science professor at the University of Alabama. “There is a scenario for Romney to win, and I wouldn’t be surprised if he did. That would be a big symbolic victory for him. Symbolic in that at least he could claim he won a deep red state in the South.”

Mr. Santorum bolstered his argument over the weekend that he is best equipped to go head-to-head with Mr. Romney after easily defeating the former Massachusetts governor in the Kansas caucuses.

Mr. Santorum won 33 of the state’s 40 delegates, pushing him into second place, ahead of Mr. Gingrich, in the chase for the 1,144 delegates needed to wrap up the Republican nomination.