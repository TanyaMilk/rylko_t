setSandboxHeight = function(id, height) {
    if ( height > 5000 ) return;
    document.getElementById("ifrm_" + id).height = height+ "px";
}

    Indian Prime Minister Narendra Modi sought to convince German industry that India is a reliable place to do business on his first visit to Europe’s largest economy as India’s leader.

“I am here to assure German companies that India is now a changed country — transparent, responsive and stable,” Mr. Modi told a delegation of Indian and German business leaders on Monday.

The Indian leader insisted that he was committed to introducing a “predictable” business environment in his country, as German Chancellor Angela Merkel welcomed him for the start of the Hannover Messe trade fair.

As the official partner country of the Messe—the world’s largest industrial trade fair—India is promoting its industrial and technological assets as Mr. Modi seeks to draw German industrial investment to his country.

Months after his election last May, Mr. Modi, of the conservative Bharatiya Janata Party, launched a “Make in India” campaign to promote the country as an investment destination and manufacturing center. His goal is to market India as an industrial hub for foreign players eager to take advantage of the country’s large workforce, raw materials and infrastructure.

Click to continue reading.