The nonpartisan Judicial Watch has petitioned the Office of  Management and Budget to rein in an IRS proposal that will place  “substantial” record-keeping burdens on 100,000 nonprofits, lawyers for the conservative watchdog group said.

The Treasury Department is currently reviewing the IRS’s new policy proposal. Judicial Watch sent a letter to OMB  requesting the agency order Treasury to withdraw the proposal.

Judicial Watch said in an emailed statement that the IRS goal was to  change the definition of “political activity” in such a way that it  would cause “substantial … record-keeping and collection of information  burden[s]” on more than 100,000 nonprofits.

In short, the IRS under President Obama would insert a new term in  the definition of political activity from “candidate” to  “candidate-related.” The switch means that any nonprofit partaking in  political activities that are related to candidates — rather than simply  and directly involving the actual candidates — would be subjected to  the new reporting and record-keeping rules.

Attorney Alan Dye said in the letter to OMB that the new definition  would constitute a significant policy shift that would bring costly and  onerous regulations onto nonprofits.

One problem, he wrote: Nonprofits rely on significant numbers of  volunteers to carry out their missions, and “anyone who has worked with  volunteers knows that recordkeeping can be notoriously difficult. … How  many volunteers are going to be turned off from civic engagement due to  this paperwork burden?”