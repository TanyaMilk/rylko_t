Senate Majority Leader Harry Reid has a message for those  who say Obamacare enrollment has left them bereft of insurance, or  paying much more than they can afford for the federal plan: Your stories  just aren’t true.

But conservatives have a message back to Mr. Reid: Your comments are “astounding,” as one said, Fox News reported.

Mr. Reid first said on the Senate floor that these Obamacare “horror stories” are untrue, Fox News reported.

SPECIAL COVERAGE: Health Care Reform

But then — facing fire —  he walked back a bit on that claim, instead saying that he only meant  the “vast majority” of featured ads put out by conservative groups like  Americans for Prosperity or by conservative funders, like the Koch  brothers, were untrue — and that moreover, the Koch brothers were “un-American,” Fox News said.

Specifically, he said, Fox News reported: Americans for Prosperity  hires actors to tell untrue stories about Obamacare and canceled  policies, and “all of [their stories] are untrue. But they’re being told  all over America.”

Conservatives were swift to condemn Mr. Reid’s characterization.