MYRTLE BEACH, S.C. — With two wins under his belt, Mitt Romney holds a significant lead among national Republican voters, though there is still room for conservative voters to unite against him and back an alternative to be the GOP presidential nominee, according to the latest The Washington Times/JZ Analytics Poll.

His showing puts him on par with the past three Republican nominees at this point in 1996, 2000 and 2008, and just as striking, he seems to have quelled much of the anyone-but-Romney sentiment that has characterized the race until now.

Only 9 percent of those surveyed chose him as the one candidate they could never vote for — well behind more controversial candidates Rep. Ron Paul of Texas and former House Speaker Newt Gingrich.

Overall, Mr. Romney drew support from 32 percent of those surveyed, followed by Mr. Gingrich and former Sen. Rick Santorum of Pennsylvania each at 17 percent and Mr. Paul at 15 percent. Texas Gov. Rick Perry garnered 3 percent.

“As long as the conservative alternative is split, then there is no conservative alternative,” said John Zogby, the pollster who conducted the survey. “Romney’s numbers are solid and strong. However, they don’t match the combined numbers of the conservatives in the race. But at this point in time, there’s a second war going on. It’s not just Gingrich and Romney, it’s Gingrich and Santorum.”