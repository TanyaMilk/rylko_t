So Brian Williams goes out (for six months) humiliated and derided. Jon Stewart goes out (permanently, one hopes) the same day, but on a giant Comedy Homecoming King float, with a 21-gun salute from the media, his path strewn with roses and teardrops.

Why?

Brian Williams lied about his personal exploits a few times. Jon Stewart was unabashedly and habitually dishonest.

Though Stewart has often claimed he does a “fake news show,” “The Daily Show” isn’t that. It’s a real news show punctuated with puns, jokes, asides and the occasional moment of staged sanctimony.

It contains real, unstaged sound bites about the day’s events and interviews about important policy matters.

Stewart is a journalist: an irresponsible and unprofessional one.

He is especially beloved by others in the journo game. (For every 100 viewers, he generated about 10 fawning profiles in the slicks, all of them saying the same thing: The jester tells the truth!)

Any standard liberal publication was as likely to contain an unflattering thought about Stewart as L’Osservatore Romano is to run a hit piece on the pope.

The hacks have a special love for Stewart because he’s their id. They don’t just think he’s funny, they thrill to his every sarcastic quip. They wish they could get away with being so one-sided, snarky and dismissive.

They wish they could skip over all the boring phone calls and the due diligence and the pretend fairness and just blurt out to their ideological enemies in Stewart style, “What the f–k is wrong with you?”

Most other journalists aren’t allowed to swear or to slam powerful figures (lest they be denied chances to interview them in future). Their editors make them tone down their opinions and cloak them behind weasel words like “critics say.” Journalists have to dress up in neutrality drag every day, and it’s a bore.

Yet Stewart uses his funnyman status as a license to dispense with even the most minimal journalistic standards. Get both sides of the story? Hey, I’m just a comedian, man. Try to be responsible about what the real issues are? Dude, that’s too heavy, we just want to set up the next d- -k joke.

Stewart is often derided by the right as having minimal impact and low ratings. That’s not true. He and Stephen Colbert ruled the late-night ratings among 18- to 34-year-olds for most of the last five years, though Jimmy Fallon has lately surpassed both.

About 522,000 Americans in that age range watch “The Daily Show” on an average night, but that means many millions of occasional viewers, with millions more watching clips online.

To a key audience, he was a strong influence. Longtime Cooper Union history professor Fred Siegel says his students constantly came to him repeating Stewart’s talking points.

College students, of course, are both little acquainted with realities of adult existence and walled off from conservative views, so they’re the perfect audience for Stewart’s shtick, which depends on assumptions that are as unquestioned as they are false.

This week’s “Daily Show” segment in which Stewart defended Williams was distilled, Everclear-strength Stewart. It was as amazing as watching Barbra Streisand run through a medley of her greatest hits in only seven minutes: In this little chunk of error, cliche, preening and deception, Stewart managed to pack an example of just about everything that is unbearable about his style. It bears close study.

Stewart made some mild jokes at the anchordude’s expense, interrupted with insufferable Jerry Lewis-style mugging, baby talk, high-pitched silly voices and the inevitable reference to whether Williams was “high” (authority figures getting high: always comedy gold to the campus audience).

Stewart slipped in a line of blatant editorializing: “Being caught is punishment enough, no?” Really? Why? If so, argue it, don’t just point the sheep in the direction you want.

Williams is a news anchor. A guy whose three main skills are being good-looking, an ability to read the English language out loud and seeming credible. To put his case in Stewart-ese: “If you want to be considered a trustworthy source of facts, maybe try NOT LYING!!!”

Declaring that media coverage of Williams’ lies was “overkill,” Stewart then built a wedding cake of bullcrap, layer after layer of untruth.

His first move was to change the subject. He used a variant of the rhetorical fallacy known as the “tu quoque” argument, or calling out alleged hypocrisy. Taken to its endpoint, tu quoque (“you, too”) reasoning means no one would ever slam anyone for anything because, hey, we’re all imperfect.

Tu quoque-ism is a generally meaningless gotcha game that can, of course, be turned right around on Stewart: Hey, Jon, you really think you’re the guy to call foul on nuking media personalities who have made misstatements?

In high dudgeon, as though the thought weren’t already a cliche we’d all seen many times on Twitter and Facebook, Stewart declared sarcastically, “Finally, someone is being held to account for misleading America about the Iraq War.”

Then came the inevitable gotcha sound bites: News figures discussing intelligence on Saddam Hussein’s WMD program. Why such a bizarre tangent into an unrelated matter? Because in Stewart’s mind, and those of his viewers, everything has to be the fault of an evil Republican, preferably George W. Bush.

Near the end of the segment, Stewart, with the prototypical combination of blustering self-righteousness and sarcasm that crystallizes his appeal to the college mentality, wondered whether the news shows will now start examining the “media malfeasance that led our country into the most catastrophic foreign policy decision in decades.”

Then (using comic bathos) Stewart cut to more newscasters making apparently trivial points about Williams’ lying. Stewart’s logic is this: The media can’t report negatively on anything anymore, because they dropped the ball on Iraq.

Stewart doesn’t actually believe that: It’s just a cheap gambit meant to get his buddy Williams off the hook by minimizing his serial lying. If Stewart were a public defender, he’d be even funnier than he is as a comic.

What judge or jury could fail to bust out laughing if a defense attorney said, “I have no rebuttal of any of the charges against my client, but lots of other people not in this courtroom are guilty of stuff, too!”?

I look forward to the next time a Republican assistant municipal treasurer in Dirt Falls, Idaho, says something awkward about race and Stewart says, “I forgive this guy given that the actual vice president of the United States once said of Barack Obama, ‘I mean, you got the first mainstream African-American who is articulate and bright and clean and a nice-looking guy.’”

Let’s look at the media reports on Iraq that Stewart is arguing make Williams’ untruths pale in comparison. Problem: Those reports were not lies. Journalists trying to figure out whether the war was justified called up credible experts with experience in the field and passed along what they said. As a more honest version of Stewart might say, “Dude. That’s not malfeasance. That’s Re. Por. Ting.”

Stewart added that “it’s like the Bush administration hired Temple Grandin to build a machine that kills the truth.” Even the audience of devotees seemed to find this simile baffling.

The idea that “Bush lied” is itself a lazy, ill-informed and false statement.

As Judge Laurence Silberman, co-chairman of the Commission on the Intelligence Capabilities of the United States Regarding Weapons of Mass Destruction, wrote in The Wall Street Journal last week, essentially nobody in the Washington intelligence community doubted the major report that Iraq had an active WMD program in 2002.

The National Intelligence Estimate delivered to the Senate and President Bush said there was a 90 percent certainty of WMDs. Democrat George Tenet, the Clinton CIA director who continued to serve under Bush, said the case for WMDs was a “slam dunk.”

John Kerry, Hillary Clinton, Chuck Schumer, Harry Reid and Joe Biden all looked at the intelligence and voted to authorize force. Sen. Jay Rockefeller argued strongly for the war. Then, years later, when it wasn’t going so well, he published a highly politicized report ripping Bush.

There is a serious case to be made against the Iraq War, but it’s a lot more complicated than the playground taunt, “Bush lied about WMDs.” (“Hey, I’m a comic, you expect me to do serious? Please welcome our next guest, Henry Kissinger!”)

Yet another lie on top of that is the absurd implication that the news media were too soft on Bush. The only way you could possibly consider the media to be too conservative would be if you were an extremist well to their left, which Stewart is.

During the Iraq War buildup, even as overwhelming majorities in both houses of Congress authorized the use of force, 59 percent of the sound bites aired by the evening newscasts were antiwar, 29 percent pro-war.

To take another of innumerable examples, in 2006 Bush had about the same approval ratings that Obama suffered in 2014. The network news both commissioned far more polls when Bush stood to suffer, and reported on the Bush results far more.

Again, this isn’t close: The score was 52 to 2, as in 52 mentions of low Bush approval ratings versus two mentions of (even lower, at times) Obama approval ratings.

In every Gallup poll this century, more Americans called the media “too liberal” than “too conservative.” The numbers were 45 to 15 in 2003, the year of the Iraq invasion. In 2008, as Obama was being elected, it was 47 to 13. Last fall it was 44 to 19.

Thanks to polemicists and clowns, the myth that “Bush lied” has caught on, and now a majority of Americans believe it. Stewart-ism won the day.

Liberal comics make things up, liberal journalists chortle and praise and internalize the lies.

Before you know it, if you point out that Bill O’Reilly’s audience is just as well informed as NPR’s (as a Pew poll found), or that Sarah Palin never said, “I can see Russia from my house” (that was “Saturday Night Live”), you’re just a buzzkill.

Brian Williams has become a joke for telling lies, but Jon Stewart is a liar for the way he told jokes.