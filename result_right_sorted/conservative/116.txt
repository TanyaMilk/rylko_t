As the administration touted new health insurance enrollment numbers on Monday, Sen. Ted Cruz of Texas predicted a Republican president would undo the entirety of President Obama’s signature domestic achievement upon taking office in 2017.

“I believe in January of 2017 a new Republican is going to enter the White House and in 2017 is going to sign legislation repealing every word of Obamacare,” Mr. Cruz said, speaking at the “Politics and Eggs” event in Manchester, N.H.

Mr. Cruz, who is considering a presidential bid of his own, tweaked “graybeards” in Washington and people who predicted the fall 2013 battle over the law, spearheaded by Mr. Cruz and other conservatives in Congress, that led to a partial shutdown of the federal government would guarantee Senate Minority Leader Harry Reid would keep his job as majority leader in the U.S. Senate.

“If that had happened, there’d be a lot of voices on TV going, ‘I told you so. We told you so. That numbskull Cruz persisted in fighting against Obamacare and look what happened,’ ” he said. “Well, the old rule: What’s good for the goose is good for the gander.”

He said in 2014, Republicans won an “epic, historic, tidal wave” election and said Obamacare was the No. 1 issue.

“And it’s interesting — it doesn’t seem to have occurred to anyone in Washington that energizing and mobilizing millions of people across this country, elevating debate about the disasters — all of the people who’ve lost their jobs, lost their health care, lost their [doctor] because of Obamacare may have played some part in an historic tidal wave that changes control in Washington based on the exact issue that energized and mobilized those millions of people,” he said.