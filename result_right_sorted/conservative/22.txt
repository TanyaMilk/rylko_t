Sen. Rand Paul launched his presidential campaign on Tuesday, throwing his hat into what is expected to become a crowded Republican ring. Here’s a look at the candidate.

* * *

Who’s running?

Rand Paul

What’s his background?

Born on Jan. 7, 1963 in Pittsburgh and raised in Lake Jackson, Texas, Mr. Paul attended Baylor University and Duke Medical School, and became an ophthalmologist. He established his medical practice and raised his family in Bowling Green, Ky.  He is the son of former GOP Rep. Ron Paul, a libertarian leader who served a quarter century in the House and ran three long-shot campaigns for president. He has a wife, Kelley, and three sons.

What’s he doing now?

He was elected to the Senate from Kentucky in 2010, his first time in elected office. He was initially viewed as a tea party troublemaker because he beat an establishment backed GOP candidate in the primary. But he has since formed a strong alliance with Senate Majority Leader Mitch McConnell, a fellow Kentucky Republican who had backed his primary opponent. He is up for reelection to the Senate in 2016, and is running for president and Senate re-election at the same time.

What positions is he best known for?

He drew national attention in 2013 with a 13-hour Senate filibuster against the Obama administration’s drone policy. He is a big critic of government surveillance programs and advocate for civil liberties protections. He has been trying to reach out to black voters, by visiting places like Howard University and Ferguson, Mo. He has sponsored bipartisan legislation to overhaul the criminal justice system to reduce the number of prisoners doing time for minor drug offenses or other non violent crimes. Unlike his mostly-hawkish GOP colleagues, he is much less inclined to support the use of military force abroad and foreign aid. However, he has backed U.S. strikes against Islamic State and aid to Israel.

What’s his path to victory?

Mr. Paul hopes to carve a unique niche in the crowded candidate field by building on his father’s network of libertarian supporters while also reaching out to young people and others who have not traditionally gone with the GOP. He is vying with Sen. Ted Cruz and other conservatives in a “primary within the primary” to emerge as the leading alternative to more establishment candidates like former Florida Gov. Jeb Bush. Mr. Paul hopes to have a strong showing in Iowa and New Hampshire, the first two states to vote, and which were strongholds for his father in 2012. He may have a harder time in South Carolina, where the military is a big presence and Republicans might be more nervous about his restrained foreign policy.

More reading: 

Rand Paul’s Challenge: Charting His Own Course

Rand Paul, McConnell Forge Unlikely Partnership

Video:

Rand Paul Launches 2016 Campaign in Speech

* * *

Like Father, Unlike Son: Where Ron and Rand Paul Differ

***

Jerry Seib: Rand Paul Runs Against Washington

______________________________________________________

Politics Alerts: Get email alerts on breaking news and big scoops. (NEW!)
Capital Journal Daybreak Newsletter: Sign up to get the latest on politics, policy and defense delivered to your inbox every morning.
For the latest Washington news, follow @wsjpolitics
For outside analysis, follow @wsjthinktank