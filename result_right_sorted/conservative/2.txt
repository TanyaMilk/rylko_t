NASHUA, N.H.—The biggest gathering yet of Republican presidential hopefuls this past weekend sharpened divisions in the broad field of candidates over the path to return the GOP to the White House, making the 2016 primary race a moment for the party to define its national identity.

On one side are Jeb Bush and Sens. Rand Paul and Lindsey Graham, who emphasized that the party’s nominee needs to reach beyond the GOP’s base of mostly white and older conservative voters, given how the general electorate is growing increasingly diverse.

On the other sits a handful of presidential contenders, including Sen. Ted Cruz and Louisiana Gov. Bobby Jindal, who say the party’s nominee needs to be a dedicated conservative who can mobilize the religious right to turn out in droves in 2016.

Republicans in recent years have wrested control of Congress from Democrats, decisively in the case of the House, and they control a plurality of state governments. But President Barack Obama’s two wins showed how a Democrat can win nationally through a diverse coalition of voters without relying on traditional Republican strongholds.

Party leaders say Republicans can’t afford to choose between the two approaches: The party’s nominee has to both excite grass-roots conservatives and tap slices of the electorate that don’t lean traditionally Republican.

“Of course candidates have to appeal to the Republican base to win a primary,” said Glenn McCall, a South Carolina Republican party leader and a co-author of the party’s 2013 report that urged the GOP to embrace an overhaul of immigration law. “But you also have to talk about new ideas and issues that relate to these communities that we have to make better headway in, the Hispanic, African-American and Asian-American communities.”

Added Republican National Committee Chairman Reince Priebus in an interview Sunday: “I see this as a no-brainer. Just do better everywhere.”

 The two-day conference organized by the New Hampshire Republican Party attracted 18 declared and potential candidates to the state that hosts the first-in-the-nation primary. One by one, the contenders addressed some 600 Republican activists in a packed hotel ballroom, hoping to stand out in a competition viewed as the most wide open in decades. Upstairs, the state GOP cordoned off a floor for those offstage to woo potential supporters. In the lobby, young volunteers pressed stickers, buttons and other campaign paraphernalia into the hands and onto the lapels of anyone who passed through.

The party’s debate over the best general-election approach, like many policy debates within the GOP, tends to pit activists seeking a conservative champion against a political establishment and donor class focused on electoral strategy.

There are differences even within the two camps. Messrs. Bush and Graham say Hispanic voters and other minorities will resist the GOP if the nominee doesn’t offer a solution to the millions of undocumented workers living in the U.S. The 2012 Republican nominee, Mitt Romney, received 27% of the Hispanic vote after recommending “self-deportation” for illegal immigrants.

Mr. Paul, of Kentucky, voted against a Senate bill that would have overhauled the U.S. immigration system. At the same time, he is visiting black colleges and urban charter schools and calling for changes to a criminal-justice system he says treats minorities unfairly.

“We need to talk to rich, poor, white, black, brown,” Mr. Paul told hundreds of activists on Saturday.

Speaking two days earlier at an event in Concord, Mr. Bush, a former Florida governor, questioned President Barack Obama’s commitment to fixing the immigration system so he could blame Republicans and drive a wedge between the GOP and Hispanic voters.

“That’s their political strategy and it seems to me to be working, unfortunately,” said Mr. Bush, who says an immigration overhaul would help stimulate the economy. “By doing nothing, we lose elections and have tepid growth.”

Mr. Graham, of South Carolina, who sponsored the Senate immigration bill, said in an interview: “If we’re still playing this game of denying the reality of immigration-reform politics, forget about 2016.”

 The latest Republican to launch a campaign, Florida Sen. Marco Rubio, also sponsored the legislation but has since retreated and said it was a mistake to offer illegal immigrants a chance to earn citizenship before addressing questions over border security. Instead of trumpeting the bill, Mr. Rubio talks of his upbringing by working-class Cuban immigrant parents when he discusses the economic struggles of middle-class Americans.

Mr. Cruz, of Texas, who voted against the immigration bill, said his 2012 election in a heavily Hispanic state proves that Republicans can win without supporting “amnesty” for illegal immigrants. Mr. Romney fared poorly with Hispanic voters, he said, because they didn’t think he was in touch with their concerns. Of voters who said “cares about people like me” was a top concern, only 18% backed Mr. Romney, exit polls show.

“I spent a great deal of time [in 2012] talking about my family experience—my father coming penniless from Cuba, not speaking English, washing dishes, making 50 cents an hour—that shared immigrant experience in the Hispanic community, that binds us together,” Mr. Cruz said in an interview Saturday. “It is absolutely critical that Republicans communicate and earn a very substantial share of the Hispanic vote and I am working hard to do so.”

Mr. Cruz said Democratic front-runner Hillary Clinton would be elected president in 2016 if the GOP nominates a candidate who doesn’t fight for conservative causes.

“I believe, based on my record, that I am in the best position to energize and mobilize those millions of conservatives who have stayed home, and win the general election,” he said.

Rep. Debbie Wasserman Schultz, chairwoman of the Democratic National Committee, dismissed the idea that the GOP candidates presented a new appeal.

“With this field, it isn’t about what’s new,” Ms. Wasserman Schultz said. “It’s about their old ideas—the same, tired Republican policies that have failed again and again in the past.”

Mr. Cruz, along with Mr. Jindal, former Texas Gov. Rick Perry and former Arkansas Gov. Mike Huckabee, is trying to win over the conservative base in part by raising issues seen as ideological litmus tests: immigration policy and the Common Core national academic standards.

Mr. Jindal, who once supported Common Core, drew applause Saturday when he talked about going to court to extricate his state from the grade-level benchmarks in math and English. He also called for the end of “hyphenated Americans,” saying his parents came from India to Baton Rouge to assimilate, not to hold on to their homeland.

Wisconsin Gov. Scott Walker is aiming to sell himself as someone who can appeal to both the conservative base and the middle—although without adopting centrist positions. Mr. Walker, who is expected to formally launch a campaign by July, noted Saturday that he won 96% of Republican voters in his 2014 re-election. Mr. Walker later said his approach appeals to base voters and centrists.

“I don’t know that one’s more important,” Mr. Walker said after meeting with four dozen voters Sunday in Derry. “I want to do both.”

Former George W. Bush press secretary Ari Fleischer, also a co-author of the GOP’s 2013 autopsy, said a successful general-election approach would appeal to independents without alienating conservatives.

“The winning formula is to be conservative in ideology and moderate in temperament,” Mr. Fleischer said. “If you’re conservative in ideology and unyielding in temperament, it’s hard to win.”

 Write to Beth Reinhard at beth.reinhard@wsj.com and Reid J. Epstein at Reid.Epstein@wsj.com