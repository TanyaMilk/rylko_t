In a new opinion piece, Sen. Rand Paul says President Obama must come to Congress to start a war and that he acted against the Islamic State without “true constitutional authority” since the country was not under attack at the time.

“But in either case, this war is now illegal,” the Kentucky Republican and likely 2016 presidential contender wrote in the Daily Beast. “It must be declared and made valid, or it must be ended.”

Mr. Paul also asks where Democrats who stood up to Republican presidents over war are now — and said conservatives need to stand up against Mr. Obama’s executive actions more broadly even on issues where they may agree, such as the campaign against the Islamic State.

“[C]onservatives can’t simply be angry at the president’s lawlessness when they disagree with his policies,” he wrote. “They should end their conspicuous silence about the president’s usurpation of Congress‘ sole authority to declare war—even if (especially if) they support going after ISIS, as I do.”

Mr. Paul has to a certain extent tried to shirk the label of “isolationist” some have tried to foist upon him, but said questions of the law cannot be a matter of convenience, regardless of the issue.

“This is important,” he continued. “We can’t be for the rule of law at our own convenience. It matters how we act both when we agree and when we disagree with the president.”