NEW YORK — Long a lightning rod for conservative criticism, the Girl Scouts of the USA are now facing their highest-level challenge yet: an official inquiry by the U.S. Conference of Catholic Bishops.

At issue are concerns about program materials that some Catholics find offensive, as well as assertions that the Scouts associate with other groups espousing stances that conflict with church teaching. The Scouts, who have numerous parish-sponsored troops, deny many of the claims and defend their alliances.

The inquiry coincides with the Scouts’ 100th anniversary celebrations and follows a chain of other controversies.

Earlier this year, legislators in Indiana and Alaska publicly called the Scouts into question, and the organization was berated in a series aired by a Catholic broadcast network. Last year, the Scouts angered some conservatives by accepting into a Colorado troop a 7-year-old transgender child who was born a boy but was being raised as a girl.

Some of the concerns raised by Catholic critics are recycled complaints that have been denied by the Girl Scouts‘ head office repeatedly and categorically. It says it has no partnership with Planned Parenthood, and does not take positions on sexuality, birth control and abortion.

“It’s been hard to get the message out there as to what is true when distortions get repeated over and over,” said Gladys Padro-Soler, the Girl Scouts‘ director of inclusive membership strategies.