Glenn Beck, conservative talk show host and publisher, had a blunt prediction for the White House, circa 2016: It’s going to be filled with the Clinton family once again.

“[Hillary] is going to win,” he said, during his recent radio broadcast. How? She will remind voters of the “golden years” of her husband’s administration, and win big at the ballot box, CNN reported.

“Here’s what Hillary is going to do. [She’ll say], ‘Do you remember when America was good? Do you remember when we had jobs and we were building towards a brighter future and things were really happening? Clinton administration — we had it under control. Things were good, and they weren’t great, we’re going to do better. We’re going to replant our flag in the traditional things that you understand.’”

Mr. Beck said the former secretary of state will be successful with her campaign messaging in part because Republicans aren’t on the ball.

“While we are talking about technicalities and the past, they are going to be talking about a past that was brightly remembered and they will take about the America we will become,” Mr. Beck said, CNN reported. “We will win.”

Mrs. Clinton has not yet announced her decision on a White House run.