SEOUL — Seoul’s special election for mayor on  Wednesday yielded a resounding defeat for the conservative ruling  party’s candidate in a political contest widely seen as a harbinger for  parliamentary elections in April and the presidential race in December  2012.

Political newcomer Park Won-soon, 55, received more than 53 percent of  the vote to defeat former judge Na Kyung-won, 47, of the ruling Grand  National Party (GNP) in their contest to become mayor of South Korea’s  capital city.In televised comments just before midnight, Ms. Na, perhaps sensing  imminent defeat, said: “If I am beaten, I will reflect deeply and will  congratulate the new mayor.”

Mr. Park, a social activist and former lawyer who founded a national  charity group, has no party affiliation. He was nominated in a primary  election held among opposition parties and is supported by the  Democratic Party (DP), the largest opposition group.

The special election was called after Mayor Oh Se-hoon of the GNP  resigned, following a losing struggle with liberal city council members  over welfare issues.

“It’s the prequel to the big elections next year in that the parties are  aligning themselves and establishing candidates,” said Michael Breen,  author of “The Koreans: Who They Are, What They Want, Where Their Future  Lies.”

“In some regards, this is a proxy for the presidential candidates, though it is a local election.”