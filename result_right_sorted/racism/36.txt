One of Hollywood’s most revered said this week that the recent  government shutdown had less to do with Republican objection to liberal  policy or reluctance to incur more debt, and more to do with anti-Obama  sentiment that rose to the level of racism.

During an interview on CNN, actor and director Robert Redford first  expressed sympathy for President Obama: “I think he’s a good man. I  think he’s an intelligent man, a compassionate man who can’t function in  that environment.”

He then spoke of the partisan atmosphere on Capitol Hill — finding fault especially with those on the right.


															
															
																SEE ALSO: New congressional fight emerges; GOP braces for battle on sequesters
															
															

“It’s so divided now with the people that are so narrow and so  limited that they would take us back into the past,” Mr. Redford said,  as United Press International reported. “And I was trying to figure out,  why are these people behaving so stupidly? Why are they behaving so  horribly that it’s crippling our whole country?

Mr. Redford, 77, then answered his own question.

“I think it has to do with fear. I think it’s a group of people that  are so afraid of change, and they’re so narrow-minded that some people —  when they see change coming — get so threatened by change, they get  angry and they get terrorized and then they get vicious. I think that’s  who these people are. They’re so afraid of change that they’re behaving  miserably,” he said, as UPI reported.