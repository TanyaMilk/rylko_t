LOS ANGELES (AP) — Michael Jackson’s doctor learns his punishment Tuesday for ending the life and career of one of pop music’s greatest entertainers and for leaving his three children without a father.

Conrad Murray is set to be sentenced for involuntary manslaughter after a six-week trial that presented the most detailed account yet of Jackson’s final hours but left many questions about Murray’s treatment of the superstar with an operating-room anesthetic as he battled chronic insomnia.

Prosecutors want Superior Court Judge Michael Pastor to sentence Murray to a maximum four-year term that likely would be cut at least in half due to jail overcrowding. Defense attorneys want probation for the cardiologist, saying he will lose his ability to practice medicine and likely face a lifetime of ostracism.

Jackson’s family members will have an opportunity to speak before Murray is sentenced, although it remained unclear if any planned to make a statement. The singer’s mother Katherine and several siblings routinely attended the trial, and members of the family cried after Murray’s verdict was read in court.

Jackson’s death in June 2009 stunned the world, as did the ensuing investigation that led to Murray being charged in February 2010.

Murray told detectives he had been giving the singer nightly doses of propofol to help him sleep as he prepared for a series of comeback concerts. Propofol is supposed to be used in hospital settings and has never been approved for sleep treatments, yet Murray acknowledged giving it to Jackson then leaving the room on the day the singer died.