It’s been rocked by controversy, the death of Joan Rivers and the departure of key panelists, so few were surprised when “Fashion Police” turned in its badge earlier this month.

Following the resignations of both founding “cop” Kelly Osbourne and new host Kathy Griffin, E! announced that the celebrity-savaging series will be going on an extended hiatus, returning in September with a revamped format.

But it’s going to take more than a lick of HD-friendly paint to rehabilitate the 5-year-old series (around longer in the form of awards-show specials), which faces accusations of behind-the-scenes bitching, cutthroat competitiveness and credibility plunging further than J.Lo’s necklines in the wake of Rivers’ death in September.

The loss of the show’s acerbic anchor sparked a “cataclysmic” cycle of events, a show insider tells The Post. “It all started to unravel after Joan passed,” the source reveals. “Everyone was subservient to her, and she was the glue that held the entire show together.”

When Osbourne quit amid a firestorm of publicity last month, it was widely reported that co-host Giuliana Rancic’s offensive comments about Zendaya Coleman’s Oscars hairstyle — she said that the dreadlocked actress probably smelled of “patchouli oil” or “weed” — had sparked her departure.

“I do not condone racism,” Osbourne tweeted after the incident. Days later, she resigned.

Sources close to the star reveal that Rancic’s remarks were just the straw that broke the camel’s back, though — and Osbourne was “relieved” to have an excuse to leave to pursue other projects, including the imminent VH1 reboot of reality series “The Osbournes.”

“Kelly was very close to Joan, and she was unhappy when [fellow co-host] George Kotsiopoulos was pushed off the panel after [Rivers’] death,” an insider confides. “She felt it was disloyal to have him replaced with Brad Goreski when George had been there since the start.”

A source told Hollywood gossip site defamer.com, “[Griffin] had George fired because she didn’t want the gay guy that had been laughing at Joan’s jokes all these years sitting next to her. She wanted to bring her own guy in.”

Even before “Zendayagate,” there was no love lost between Osbourne and Rancic, sources tell The Post.

“They both wanted to be ‘alpha panelist,’ ” the show insider says. “Joan always kept the bitchiness at bay, but when she died, the differences in Kelly and Giuliana’s personalities seemed to become even more marked. Any mutual dislike was only exacerbated.”

While Osbourne and Rancic angled for the top-dog position, The Post’s Cindy Adams reported in early March that Griffin “made a grab for the job while Joan lay on life support. I know. I was right there in her hospital room holding my forever friend’s hand.” (Griffin has denied this claim.)

“Joan always had her jokes prewritten and prepared, and she presided over the show in such a way that she gave the other panelists material to bounce off and a chance to shine,” a network insider reveals.

“Kathy decided to just wing it, and it didn’t work. Her lack of interest in or knowledge of fashion was evident, and it was left to Kelly, Giuliana and Brad to prop her up. Kathy wasn’t happy that the others were delivering funnier lines than her.”

After just seven shows, Griffin handed in her resignation.

“I do not want to use my comedy to contribute to a culture of unattainable perfectionism and intolerance towards difference. I want to help women, gay kids, people of color, and anyone who feels underrepresented to have a voice and a LAUGH,” Griffin insisted.

“Kathy was troubled that people were finding the show to be sexist and even racist after the Zendaya comments,” an associate of the comedienne says.

But some claim Griffin jumped before she was pushed from a sinking ship — and that Rivers’ Louboutins proved too big to fill.

“There was so much pressure on her to carry on Joan’s legacy, it was like accepting a poisoned chalice,” the network source reveals.

What’s next for the show? Network execs and producer Melissa Rivers are scouring celebrity lineups for a new anchor and a fashion-literate sidekick to join Rancic and Goreski, who will keep their “Police” credentials.

Even amid controversy and backlash, people are clamoring for the job. Last Tuesday, “RuPaul’s Drag Race” judge Carson Kressley threw his name in the hat for a position, telling “Access Hollywood”: “I have the chops to talk about what they’re actually wearing.”

Let’s hope the new lineup is less of a haute mess.