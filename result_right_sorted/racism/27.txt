The Seattle School Board voted unanimously on Wednesday for schools to observe “Indigenous Peoples’ Day” on the same day as the Columbus Day federal holiday.

The resolution, in part, said the board “recognizes the fact that Seattle is built upon the homelands and villages of the Indigenous Peoples of this region, without whom the building of the City would not have been possible,” a local Fox affiliate reported.

The resolution also says the board “has a responsibility to oppose the systematic racism towards Indigenous people in the United States, which perpetuates high rates of poverty and income inequality, exacerbating disproportionate health, education and social crises.”

The Seattle City Council will vote Monday to decide whether it will also celebrate “Indigenous Peoples’ Day” on Columbus Day, the station reported. Columbus Day is observed on the second Monday in October.

“We know Columbus Day is a federal holiday, we are not naive about that, but what we can do and what you have seen is a movement,” said Matt Remle, who supports the change.

During a Seattle City Council committee meeting last month, Italian Americans expressed their concerns over the idea of replacing Columbus Day.

“For most Italian Americans, Columbus Day is a symbol of pride in our heritage,” said Audrey Manzanares, the Fox affiliate reported.