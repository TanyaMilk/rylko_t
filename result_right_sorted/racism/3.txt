As rioting in Baltimore this week mirrored last year’s violent clashes in Ferguson, Mo., and protests in other places where black men have died at the hands of police, Americans grasped for reasons why this keeps happening. The title of Wednesday’s episode of a Science Channel series offers one potential answer in the form of a question: “Are We All Bigots?”

Morgan Freeman hosts “Through the Wormhole,” a series that tackles big questions by spotlighting scientific studies that chip away at them. Wednesday’s season premiere (watch an exclusive clip above) explores humans’ instinct to stereotype, along with research that points to how we can override it, including a study suggesting that rats showed empathy for rats of a different color.

In a recent interview (before the protests in Baltimore boiled over), Freeman said, “There is a great hope and belief that something good will come out of all of is, including current events and our investigation into the impulses that might be generating them.”

In upcoming episodes, “Through the Wormhole” (entering its sixth season) wrestles with such questions as “Are We Here For a Reason?”, “Why Do We Lie?”, and “Do We Live in the Matrix?”

Bernadette McDaid, one of Freeman’s fellow executive producers who joined him on the interview, described the mission of the series as “physics meets philosophy.” She said, “We want to make sure we’re asking questions that people would genuinely ask themselves.”

Producers got started on the bigotry episode about a year ago. As they tracked down the research, including a psychologist’s study to find out if subjects were more likely to open fire on a suspicious black man than a white man, it overlapped uncannily with dire real-world events, including the death of Michael Brown in Ferguson.

The science in the episode suggests that evolution has hard-wired us toward subconscious bias. Executive producer James Younger said, “A lot of people react to racism by thinking that other people are racist and that’s what’s holding society back. This show says that bias is everybody’s problem, and we’re all guilty of it to some extent.”

For his part, Freeman is cautious about proposing that science can help erase racism. “Is there a reasonable expectation of change? There’s a big question there, and I don’t have an answer to it,” he said.

Asked whether the casting process in Hollywood is prone to stereotyping, Freeman makes a distinction between typecasting and the racist screen caricatures of the past.

“What you see in Hollywood today is not what you saw in Hollywood yesterday, in terms of stereotyping,” he said. “Let’s take me, for instance. I’m a Hollywood actor, and I am almost criminally stereotyped. I started out with a nice little track record of having range. I could do different characters believably. But in the past, what, 15 years now I have become the voice of reason. I’ve become the gravitas of the movies, the narrative morality. That’s stereotyping.”

Indeed, Freeman’s baritone voice and august presence has made him the go-to for stately characters, from Red in “The Shawshank Redemption” to God in “Bruce Almighty” and “Evan Almighty.” And yet, in a career that spans 50 years and dozens of films, it’s been rare to see him in an onscreen romance. Did that have anything to do with the fact that he’s an actor of color?

“I don’t think so. Being an actor of color doesn’t stop you from doing romantic roles. Interracial romantic roles? Yeah, perhaps. I think I just don’t come across as romantic,” he said with a chuckle.

The actor says his love of science dates back to a high-school physics class he took Nashville, Tenn. But that doesn’t mean he could have been a scientist in another life: “Absolutely not, under no circumstances at all could I have ever begun to think that I could pursue a life in science. I’m strictly a mimic, a pretender.”

For the latest entertainment news Follow @WSJSpeakeasy