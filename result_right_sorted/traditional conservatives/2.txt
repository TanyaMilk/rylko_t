Virginia Gov. Bob McDonnell said the rise of the tea party and Rep. Ron Paul’s supporters within the Republican Party will push the GOP platform this year to focus more on matters such as the deficit and constitutional liberties.

But Mr. McDonnell, head of the platform committee, would not commit to a provision requiring an audit of the Federal Reserve — a chief goal of supporters of Mr. Paul and his son, Sen. Rand Paul of Kentucky, and one that was backed by nearly every Republican in the House on Wednesday.

“We’re talking about that,” Mr. McDonnell told editors and reporters at The Washington Times on Friday. “I’m a big believer in accountability. We’re having all of those discussions. There’s a lot of great ideas that Rand and Ron have.”

Mr. McDonnell said he is trying to manage the broad spectrum of opinions within the GOP, including the libertarian bent of many of those who supported Mr. Paul’s presidential bid.

He said there is about 80 percent overlap of ideas between traditional conservatives within the party and the libertarian wing, and that his goal is to produce a document that both sides say “reflects, in essence, the views that we have.”

Mr. Paul, a Texan who has ramped down his campaign but has not withdrawn from the race, will have hundreds of delegates at the convention. That includes many who are pledged to back another candidate for the presidential nomination but who are free to express their views about the rest of the party’s business, including the platform.