WASHINGTON—President Barack Obama, facing a bitter struggle within his own party on trade policy, warned that China would step into the economic vacuum the U.S. would create if it fails to complete and enact a free-trade deal with Asia.

“If we don’t write the rules, China will write the rules out in that region,” Mr. Obama said in an interview Monday with The Wall Street Journal. “We will be shut out—American businesses and American agriculture. That will mean a loss of U.S. jobs.”

Mr. Obama also warned of rising anti-globalization sentiment in Washington, reflected in Democratic opposition to the trade agreement, Republican efforts to kill the Export-Import Bank and congressional unwillingness to approve new rules for operation of the International Monetary Fund.

“What we can’t do, though, is withdraw,” Mr. Obama said, adding: “There has been a confluence of anti-global engagement from both elements of the right and elements of the left that I think [is] a big mistake.”

Mr. Obama and his negotiators are working to finish the Trans-Pacific Partnership, a trade deal among 12 Pacific nations that has come to be known as TPP, while also fighting to win “fast track” negotiating authority from Congress to expedite approval of the deal later this year. The trade agreement will be a topic of conversation between Mr. Obama and Japanese Prime Minister Shinzo Abe, who is scheduled to visit the White House this week.

The Senate Finance Committee easily passed the fast-track bill last week with strong bipartisan support. But in the House, the Ways and Means Committee passed the measure with the support of only two Democrats.

That House vote underscored the depth of opposition among Democrats, particularly on the party’s left, where both Massachusetts Sen. Elizabeth Warren and Richard Trumka, president of the AFL-CIO, have been beating the drum against the deal. 

Critics on the left, particularly those in the labor movement, are opposing TPP because they believe free-trade agreements have caused an outflow of manufacturing jobs from the U.S. to other nations, and say competition from lower-wage countries produced by such agreements has contributed to stagnant wages and higher income inequality in the U.S.

Former Secretary of State Hillary Clinton also has declined to endorse the TPP, even though work on the agreement started while she was still in office. 

Mr. Obama declined to criticize Mrs. Clinton for failing to take a public stand in favor of the pact in recent days. “I think she said what she should be saying, which is that she is going to want to see a trade agreement that is strong on labor, strong on the environment, helps U.S. workers, helps the U.S. economy. That’s my standard as well, and I’m confident that standard can be met,” he said.

Mr. Obama has expressed annoyance and, on occasion, flashes of anger over the sometimes harsh criticism of the Asian trade agreement from his usual allies on his party’s left wing. In the interview, though, he said he understands their concerns.

“Over the course of 20, 25 years, what you saw was trade benefit the U.S. economy in the aggregate with cheaper prices, inflation low, the creation of a global supply chain that was good for U.S. companies,” he said. “But what is true—and the best economic data seems to show—is that there was some erosion of our manufacturing base at the time… Some people have been suspicious and feel burned from some of those experiences.”

But he said the TPP would be different because it would have more enforceable provisions on labor and environmental standards that are written into the pact, not contained in side agreements that critics say were difficult to enforce in the case of the North American Free Trade Agreement of the 1990s.

When that agreement was approved by Congress in 1993, 27 Democratic senators and 102 Democratic House members voted for it. Most analysts think the number of Democrats now in favor of TPP is much lower, though Mr. Obama said it is too early to forecast how many Democrats would back it. It is almost certain, though, that the president would have to rely more on Republican support than backing from fellow Democrats.

Mr. Obama did acknowledge that he is annoyed by the criticism of both him and the trade negotiations: “What I take personally is this notion somehow that after 6½ years of working to yank this economy out of a ditch, strengthening middle-class homeownership, making sure that their 401(k)s have recovered, making sure that we’ve got much better education systems and job-training systems, fighting for the minimum wage, fighting for a vibrant auto industry, that after all the work that I have done and we have done together to make sure that middle-class families have greater stability, that to believe some of the rhetoric that has been coming out of opponents—that I’m trying to just destroy the middle class or destroy our democracy—is a little unrealistic. And they know it.”

At several points in the conversation, Mr. Obama raised the danger of losing ground in economic competition with China if the trade deal isn’t completed: “We want China to be successful. We want China to continue to embark on its peaceful rise,” he said. “I think that’s good for the world.…We just want to make sure that the rules of the road allow us to compete and everybody else to compete. We don’t want China to use its size to muscle other countries in the region around rules that disadvantage us.”

Mr. Obama said U.S. and Japanese negotiators have come close to completing a bilateral agreement on trade issues—an agreement that would help pave the way for completion of the broader 12-nation TPP. 

But he said some sticking points remain. And while praising Mr. Abe for being “bold and aggressive” in reforming Japan’s economy, he added: “Negotiations are tough on both sides because he’s got his own politics and interests. Japanese farmers are tough, Japanese auto makers want certain things. I don’t expect that we will complete all negotiations” while he is in Washington this week. 

“I will say that the engagement has brought the parties much closer together,” Mr. Obama said.

On the broader U.S. economy, Mr. Obama acknowledged that the employment picture hasn’t improved as much in recent months as it had previously, but he attributed any current weaknesses to problems in Europe and China. 

Europe he said, “has been soft for a long time now, and China, which is trying to transition away from solely relying on exports to a more domestic consumption model—that’s actually a good thing over the long term, but short term it means that demand globally is a little bit soft.”

He also argued that the U.S. economy, while better than in other advanced economies, would benefit from congressional passage of “a strong infrastructure bill” to improve roads, ports, bridges and broadband lines. “If we did that, we’d put people back to work right now, it would be a huge boost to the economy, and it would pay off for decades to come,” he said. “That’s what we need to compete.” 

The president argued for such a measure earlier in this year, without success; his remarks Monday suggested he will renew his push.

 Write to Gerald F. Seib at jerry.seib@wsj.com