ANALYSIS:

Republicans looking to ease the friction among party stalwarts, tea party activists and Ron Paul supporters headed into this year’s election say they may have found a model of unity in Art Robinson, a scientist who is the GOP candidate for a congressional seat representing an Oregon district.

Mr. Robinson, a former CalTech chemist and home-schooling curriculum author who became a prominent skeptic of taxpayer-funded research based on his own experiences, has hit the kinds of notes that ring well with the limited-government tea party and with libertarian Paul forces.

“Art’s grass-roots campaign challenges the Washington, D.C., cabal of insider politics and is gaining momentum for a very good reason,” said Allen Alley, chairman of the Oregon Republican Party. “There’s a moral and political rudder, a core set of beliefs, that come through. The more you listen, the more his good sense comes through.”

But even more, Republicans hope Mr. Robinson can be a template for the broader national party. Many activists fret that Republicans cannot win the presidential election in November if they put up a candidate in a fractured party against a Democratic Party unified behind President Obama.

Tea partyers and Paul supporters claim enough voters in their camps to be kingmakers in the dozen or so swing states that will decide the election, and both camps have made a point to try to recruit like-minded candidates for lower-level offices.