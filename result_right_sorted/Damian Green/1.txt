Jamie Chung slides breathlessly into a banquette at the 101 Coffee Shop in Hollywood, fresh from her first wedding dress try-on session at Monique Lhuillier’s boutique. Has she found a winner? “No idea,” the actress says, exhaling and ordering a cup of soup. “I was super overwhelmed today — super overwhelmed.”

A bridal fitting was never something the stylish 32-year-old former “Real World” star ever imagined. Just a year and a half ago, she offered an anti-proposal to her boyfriend, actor Bryan Greenberg: “Let’s just be like Goldie Hawn and Kurt Russell,” she recalls. “Never get married, have beautiful children, live the dream and choose to be with each other.”

Greenberg’s response? “He freaked out,” Chung says with a laugh — because he’d had another proposal on his mind. One week later, on Dec. 23, 2013, the Nebraska-born Greenberg, 36, flew to Chung’s hometown of San Francisco and proposed with a round halo diamond engagement ring by Johnathon Arndt.

While Chung, the tomboy daughter of Korean immigrants, never spent time as a girl dreaming of her wedding (“Never — I was dreaming of recapping a game I’d played, or the glory I could have had if I caught the ball in the outfield”), she immediately said “yes.”

“I believe in the institution — my parents are still married and his parents are still married,” she explains. “I just didn’t believe that we needed to prove anything.”

Chung, it seems, is plenty busy proving herself on-screen — snagging memorable parts in movies like “Big Hero 6” (which won this year’s Academy Award for Best Animated Film), “The Hangover” (parts II and III) and the TV show “Once Upon a Time,” as well as online, via her popular whatthechung.com fashion blog.

This April, her half-hour comedy “Resident Advisors” will debut on Hulu. She’ll also soon join her fiancé in the films “A Year and Change” and, in a case of art imitating life, the romance “It’s Already Tomorrow in Hong Kong,” which Chung describes as “quite similar to the story of how we met.”

The couple first crossed paths in 2008 at a Young Hollywood party in the Chateau Marmont hotel. “But there was no flirting,” the petite, effervescent actress insists, noting they were both in relationships. “It was just like, ‘Oh my God, he’s great!’ and then ‘Darn, he’s taken!’ ”

Two years later they ran into each other again, but she was still unavailable. “It’s always been kind of back and forth,” Chung says. In 2011, they both ended up in New York filming separate projects, when Greenberg (then the star of HBO’s “How to Make It in America”) found himself at dinner with Chung’s lawyer.

“My lawyer knew that we had crushes on each other,” Jamie reveals, “so he was like, ‘You should come out with us. . .’ ” Couple formed; future wedding invite secured.

That wedding will finally take place this fall, somewhere along the California coast. “Maybe in Ojai, maybe in Santa Barbara or San Diego,” Chung muses. “Maybe Big Sur — I’m a bit biased, but I think California has one of the most beautiful coastlines in the world.”

They’ve hired wedding planner Lisa Vorce to help sort out some of the details for the rustic-chic outdoor ceremony. (“I’m the worst at making decisions!” admits Chung.) But they’re clear on a few things: Jewish ceremony? Yes. Garter belt? No. Bouquet toss? Yes. Viral-baiting choreographed couple dance? “Hell no,” she laughs. “Like we breakdance and then I rip open my skirt? I’m not that kind of girl.”

Chung is clearly close with her family (her older sister Jean will serve as her maid of honor) and in admiration of her folks (“My parents, in their 40s, moved to a different country, started a business, bought a house, didn’t speak the language, raised two kids — it’s kind of amazing”). But don’t expect a lot of sentimental speeches on her big day.

“Because the speeches aren’t really about you,” Chung says, “it’s about how that person knows you.” Plus, when she was asked to speak as a bridesmaid for a close friend’s wedding, she felt sick with nerves the entire day. “She asked me to say a little speech at the reception. I was a wreck. I was so stressed out because I didn’t want it to suck!”

One thing the street-style star clearly does not suck at is her ability to whip up a high-low fashion masterpiece. Today she’s in a long, white Rebecca Minkoff skirt, a striped top by Twenty, Sol Sana short black boots, and a faux-leather Missguided motorcycle jacket she says retails for less than $100. And it works.

It’s no wonder Chung has made so many best-dressed lists of late. In addition to her year-old style blog, she’s launching a line of baubles for Sachi Jewelry (“Sachi means ‘happiness’ in Japanese and ‘beautiful things’ in Korean,” she explains) and collaborating with a bridal site to design a capsule collection of jewelry for bridesmaids. She’s even designing, with the help of a friend who owns a clothing line, dresses for her own six or seven bridesmaids.

Chung describes her personal style as fun, feminine and fickle. “Sometimes I’m girly; sometimes I like to be a little edgy.” That explains her favorite item in her closet: a pair of flat, ankle-height Chloé motorcycle boots. “They go with jeans, and they would go with this outfit now,” she says, “but I’ve been wearing them for the last four days, so I figured I’d change it up.” She says her look has matured since she hit the big three-oh, because “in your 30s you are very aware of who you are and what you’re confident in.”

That newfound aplomb applies to her wedding gown search, where she’s considering “more of a mature look versus a flirty one.” She’s steering clear of empire-waist styles because she doesn’t want “anything too princess-y,” she says. “For this next stage, I think I want to look a bit more elegant and composed.”

As for Greenberg’s big-day style, she’s letting her future husband take the reins. “I’m the worst when it comes to men’s fashion,” she laughs. “I’m too busy with my own s – – -. I have no idea!” Plus, she trusts him. “He’s got really good taste, so I’m not too worried.”

The couple may or may not take a honeymoon after the wedding. The actors are both always on the hunt for their next gig, and work might have to come first. “He’s blocked off for those three days,” Chung says, but, “It’s pilot season. Whenever there’s a great opportunity, we’ll take it, you know? Life’s gotta come first.”

For now, their life is focused on settling into a new Brooklyn apartment they bought this spring. “I think what’s going to make it feel like our place is spending time there and decorating it together and starting to build memories,” she says. NYC isn’t new to them (they’ve long hopped between Manhattan and the West Coast), and they like to spend their evenings strolling through the city. “The best date we had was from our place downtown, we walked all the way up to Eataly, had rosé and cheese and meat, and sat down for a bowl of pasta. And then we walked back down.”

They’re also busy raising their new rescue puppy, Ewok, whom they adopted in early March. And as for kids, “I think we’re going to keep the goalie on the roster for the next three or four years,” she jokes.

Meanwhile, Chung is most excited for her first official dance with her husband. Greenberg, who’s also a singer/songwriter with talented musician friends, is in charge of the music. “And we always have impromptu dance parties with just the two of us,” she gushes.

She does admit to one big-day fear: “That it rains, because it’s an outdoor wedding. I just picture everything getting super muddy.” But even if it pours, she says she and her fiancé will tackle it as they would any problem in their future years of marriage.

“We’ll make the best of it,” Chung says. “We’ll hand out plastic bags with rubber bands and raincoats, and we’ll just dance in the rain.”

Photo shoot by Don Flood

Fashion Editor: Serena French Stylist: Anahita Moussavian Hair: Daniel Marrone Makeup: Kira Nasrat at One Represents using Chanel Rouge Coco Manicure: Naja Rickette at Opus Beauty using Essie Location: imagelocations.com

Fashion Credits for Top Photo: Needle & Thread dress, $475 at bhldn.com; Pumps, $245 at badgleymischka.com; Ring, Jamie Chung’s own

Fashion Credits for First Photo: (Left) Temperley Bridal dress, $3,395 at temperleylondon.com; Ring, Chung’s own (Right) Dress, $6,590 at Oscar de la Renta, 772 Madison Ave.; Pumps, $485 at sophiawebster.com; Diamond earrings, $30,000 at Miranda Castro, 310-860-0041; Ring, $105,935 at harrykotlar.com

Fashion Credits for Second Photo: Couture by Chagoury dress, $7,359 at Chagoury, 323-782-9008; Heels, $995 at moniquelhuillier.com; Headpiece, $500 at prestonandolivia.com (Right) Dress, $6,590 at Oscar de la Renta, 772 Madison Ave.; Diamond earrings, $30,000 at Miranda Castro, 310-860-0041; Ring, $105,935 at harrykotlar.com

Fashion Credits for Third Photo: (Left) Temperley Bridal dress, $3,395 at temperleylondon.com; Ring, Chung’s own (Right) Dress, $588 at thereformation.com; Pnina Tornai for Kleinfeld headpiece, $675 at kleinfeldbridal.com; Pumps, $485 at sophiawebster.com