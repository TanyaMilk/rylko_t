TRIPOLI, Libya (AP) — Mounting calls for an investigation  into whether Col. Moammar Gadhafi was executed in custody overshadowed plans  by Libya’s new rulers Sunday to declare liberation and a formal end to  the eight-month civil war that toppled the longtime dictator.

An  autopsy confirmed that Gadhafi died from a gunshot to the head, Libya’s  chief pathologist, Dr. Othman al-Zintani, said hours before the  liberation declaration was to start the clock on a transition to  democracy.

However, the pathologist said he would not disclose  further details or elaborate on Gadhafi’s final moments, saying he would  first deliver a full report to the attorney general. Libya’s acting  prime minister said he would not oppose an investigation, but he cited an  official reporting saying a wounded Gadhafi was killed in crossfire  following his capture.

U.S. Secretary of State Hillary Rodham  Clinton and Britain’s new defense secretary, Philip Hammond, said a full  investigation is necessary.

The Libyan revolutionaries’ image had  been “a little bit stained” by Gadhafi’s death, Mr. Hammond said Sunday,  adding that the new government “will want to get to the bottom of it in a  way that rebuilds and cleanses that reputation.”

“It’s certainly  not the way we do things,”  Mr. Hammond told BBC television. “We would have  liked to see Col. Gadhafi going on trial to answer for his misdeeds.”