World leaders meeting in London to discuss how best to fight the Islamic State said people shouldn’t get their hopes up — that it’s going to take quite a while to gain ground on the terror group.

“This isn’t going to be done in three months or six months,” said British Foreign Secretary Philip Hammond, USA Today reported.

“[Rather], it’s going to take a year, two years to push ISIL back out of Iraq,” said Mr. Hammond, who’s hosting the talks along with U.S. Secretary of State John Kerry and Iraqi Prime Minister Haider al-Abadi, USA Today reported.

“We are doing the things that need to be done in order to turn the tide,” he said.

Mr. Kerry, meanwhile, said the Paris terror attacks on Charlie Hebdo have only served to bring the world leader coalition in tighter union.

“They’re bringing us together with greater determination, with greater resolve to be able to get the job done,” Mr. Kerry said, USA Today reported.