Two central Ohio businessmen didn’t flinch on Wednesday when they were found guilty of
misappropriating millions of dollars that investors had poured into their sports-drink company.

After two days of deliberations, a federal jury found Thomas E. Jackson and Preston J. Harrison
guilty of conspiracy counts of wire fraud and money laundering, as well as 12 counts each of money
laundering in connection with their Westerville company, Imperial Integrative Health Research &
Development.

Imperial produced OXYwater, a new drink that reportedly was high in oxygen and healthy
minerals.

Jackson, of Murphys View Place in Powell, also was found guilty on eight of 10 wire-fraud
counts. The jury found Harrison, of Willow Springs Court in Lewis Center, not guilty on those
charges.

The company had collected $9.5 million in investments from 2010 to 2012 to produce OXYwater.

It began running out of money in 2012 and closed down in 2013, after investors began questioning
how the money had been spent and the 
FBI began an investigation.


The
Internal Revenue Service also joined the investigation.

The guilty verdicts brought no joy to Shaun Stonerook and Kenny Gregory, both professional
basketball players who played in Europe and who sat through the entire eight-day trial because they
invested in the company.

“Knowing they’re going to jail doesn’t get us our money back,” Stonerook said after the verdicts
were announced. “We would much rather the product be in stores and be sold. It was a great product.”


Gregory said Jackson and Harrison “ruined something that could have been potentially great” that
many people believed in. Neither, he said, showed any remorse.

U.S. District Judge Gregory L. Frost ordered U.S. marshals to immediately take Jackson, 40, and
Harrison, 43, into custody after the verdicts were announced. He turned down requests that they be
on electronic monitoring at home while they await sentencing.

A sentencing date has not been set.

The jury also found Harrison and his wife, Jovena E. Harrison, 42, guilty of conspiring to
defraud the Internal Revenue Service and filing a false income-tax return. Mrs. Harrison also was
found guilty of structuring financial deposits. She was not taken into custody.

The trial began last week and included four days of government witnesses. The defense called no
witnesses, saying in closing arguments that the government had not proved its case.

None of the defendants took the stand. They showed little emotion as evidence was presented of
expensive cars, jewelry and guns that Jackson and Mr. Harrison purchased after the company received
investor money.

“We paid for their lifestyle,” Stonerook said. Government attorneys said Jackson and Mr.
Harrison misappropriated about $2 million of the investors’ money and that all of that money was
gone.

Witnesses testified that company literature contained lies and misrepresentations about who its
employees were and which celebrities had agreed to promote the product.

R&B singer-songwriter Ne-Yo, as well as professional basketball players including Stonerook
and Gregory, testified that some of that literature helped persuade them to invest in the
start-up.

Assistant U.S. Attorney Jessica H. Kim said she was happy but not surprised by the verdicts.

“I think we had a strong case,” Kim said.

Harrison’s attorney, Kort W. Gatterdam, and Jackson’s attorney, Diane M. Menashe, said they were
not second-guessing their decision to not put witnesses or their clients on the stand.

“We’re confident with all the decisions we made,” Gatterdam said. He said he is planning an
appeal.

Stonerook and Gregory had contacted a lawyer about Imperial in 2012 after they received Imperial
tax forms that grossly underreported their investments. They learned that the FBI already had begun
investigating the three defendants, after a bank official noticed irregular withdrawals from one of
the Harrison’s bank accounts.

The maximum prison time for the charges against Jackson and Mr. Harrison ranges from 10 to 20
years on each count. Prison time for the tax counts against the Harrisons ranges from three to five
years in prison on each count.

Harrison played football at Ohio State University in the early 1990s and was drafted by the San
Diego Chargers in 1995, but never played.


kgray@dispatch.com



@reporterkathy