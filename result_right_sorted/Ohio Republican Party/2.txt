So, 37 percent of Ohio voters say U.S. Sen. Rob Portman deserves re-election next year, with 28
percent opposed.

And 40 percent approve of how the first-term Republican is performing his job, while 21 percent
disapprove.

Twenty-one months before the vote, is all that good news or bad for Portman?



Follow @OhioPoliticsNow on Twitter



First, let’s hear from the home team on the new Quinnipiac University Poll numbers:

“Sen. Portman is in a very strong position with twice as many Ohioans viewing him favorably than
do the opposite,” said Chris Schrimpf, spokesman for the Ohio Republican Party. 

Now, the challengers:

“Sen. Portman’s very weak poll numbers today are a result of his long record of promoting
policies that have hurt working Ohioans, like supporting the government shutdown and opposing the
auto rescue that saved Ohio’s economy,” said Meredith Tucker, spokeswoman for the Ohio Democratic
Party.

And the only announced opponent:

“With his failure to deliver for Ohio’s middle class and his out-of-touch approach to
representing our state in Washington, it’s no wonder Rob Portman is polling at such low approval
numbers. Anytime an incumbent elected leader has an approval rating under 50 percent, it’s a sign
of vulnerability,” said Ramsey Reid, campaign manager for Cincinnati Councilman P.G.
Sittenfeld.

Finally, the pollster:

“Portman is 10 points short of the 50 percent job-approval mark as he begins his quest for a
second term, not a place in which an incumbent likes to be,” said Peter Brown, the poll’s assistant
director, in a release.

Although Sittenfeld, 30, is so far the only declared opponent to Portman, 59, former Gov. Ted
Strickland, 73, is believed likely to enter the race soon. Also getting mentions are U.S. Rep. Tim
Ryan of Niles and Betty Sutton, a former Akron-area congresswoman.

It is Strickland who has drawn fire from Republicans both in Ohio and nationally over the past
several days. The state GOP rolled out a cartoon video yesterday criticizing the Democrat for Ohio’s
 job losses while he was governor.

Schrimpf said, “Not only is Sen. Portman in a strong position on his own, but the contrast
between how Ohioans feel now and what they’ll remember about the Strickland days of job loss is
enormous.”

Tucker said, “This 2016 U.S. Senate race will be extremely competitive, and ... we have a
strong, talented bench of Democrats who are well positioned to replace Sen. Portman and work
every day to fight for Ohio’s middle-class families.”

Meanwhile, the man who knocked Strickland out of office, Gov. John Kasich, enjoys a healthy
approval rating. The Republican’s job performance wins a thumbs up from 55 percent of Ohio voters,
while 30 percent disapprove.

“While Gov. John Kasich decides whether to take the presidential plunge, he has plenty of good
will left with Buckeye voters,” Browns said. “With a 55 percent job-approval rating, he has little
to worry about at home. Voters are optimistic about Ohio’s future under Gov. Kasich.”

Kasich’s numbers are bolstered by the 60 percent of Ohio voters who describe the state’s economy
as either “good” or “excellent.” And 49 percent say the Ohio economy is getting better, with only 9
percent seeing a worsening.

The Connecticut university’s telephone poll of 943 registered voters from Jan. 22 through Jan.
31 has a margin of sampling error of plus or minus 3.2 percentage points. The random-digital-dialed
calls resulted in responses from 32 percent independents, 28 percent Democrats, 26 percent
Republicans and 13 percent other or who didn’t answer.


drowland@dispatch.com



@OhioPoliticsNow