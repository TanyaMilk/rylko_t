“Jihadi John,” the key Islamic State terrorist behind the videotaped beheadings of at least two Americans and two Britons, is believed to have been injured in a U.S.-led airstrike in Iraq.

The Daily Mail first reported that a nurse in Syria claimed to have treated the terrorist, who also uses the name Jalman Al-Britani. The newspaper reported he was injured during a strike in the western portion of Iraq that occurred Saturday, and he then drove into Syria.

It’s not clear how badly he was injured, The Hill reported.


															
															
																SEE ALSO: Peter Kassig beheaded in Islamic State video; American aid worker latest murder of Westerner
															
															

The British Foreign Office said its agents were still investigating the report and trying to confirm if it were true.

“We are aware of reports that this individual has been injured, and we are looking into them,” the office said in a statement to the Daily Mail.

Prime Minister David Cameron, meanwhile, said he was anxiously awaiting results of the internal investigation.

“We should be in no doubt that I want ‘Jihadi John’ to face justice for the appalling acts that have been carried out in Syria,” Mr. Cameron said, Agence France-Presse reported. “But I wouldn’t make any comment on individual issues or strikes and the like — you wouldn’t expect me to.”