President Obama and other world leaders ramped up pressure Thursday on Syrian President Bashar Assad to step down, but the authoritarian regime responded by accusing the West of inciting more violence.

“It is strange that instead of offering [Damascus] a helping hand to implement its program of reforms, the West and Obama are seeking to stoke more violence in Syria,” Reem Haddad, the Syrian Information Ministry’s director of external relations, told Agence France-Presse.

After five months of crackdowns that human rights activists say have killed more than 2,200 in Syria, Mr. Obama finally called for Mr. Assad’s resignation Thursday and imposed new sanctions against the regime, including a freeze of all the country’s assets in the United States.

“For the sake of the Syrian people, the time has come for President Assad to step aside,” Mr. Obama said in a statement. “His calls for dialogue and reform have rung hollow while he is imprisoning, torturing and slaughtering his own people.”

In Europe, British Prime Minister David Cameron, French President Nicolas Sarkozy and German Chancellor Angela Merkel issued their own statement, saying Mr. Assad, who succeeded his father as president in July 2000, should quit and announcing they were imposing new sanctions as well on Damascus.

“Our three countries believe that President Assad, who is resorting to brutal military force against his own people and who is responsible for the situation, has lost all legitimacy and can no longer claim to lead the country,” they said. “We call on him to face the reality of the complete rejection of his regime by the Syrian people and to step aside in the best interests of Syria and the unity of its people. Violence in Syria must stop now.”