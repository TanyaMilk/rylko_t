Bears legend and NFL Hall of Fame coach Mike Ditka weighed in on the Rams players who took to the field with the “Hands up, don’t shoot!” gesture as a Ferguson protest before Sunday’s game, and he isn’t a fan.

“Da Coach” said it was “a shame” how events have unfolded since a grand jury in Ferguson, Missouri, decided not to indict Police Officer Darren Wilson for the Aug. 9 killing of Michael Brown.

“I understand what the Rams’  take on this was. I’m embarrassed for the  players more than anything.   They want to take a political stand on  this? Well, there are a lot of  other things that have happened in our  society that people have not  stood up and disagreed about,” Mr. Ditka told the Sun-Times Wednesday.

PHOTOS: Famous conservatives in professional sports

Mr. Ditka said that while he doesn’t know exactly what happened during Brown’s altercation with Mr. Wilson, stripping policemen of tools they need to enforce the law would lead to more problems.

“What do you do if someone pulls a gun on you or is robbing a store,  and  you stop them? I don’t want to hear about this hands-up crap. That’s   not what happened. I don’t know exactly what did happen, but I know   that’s not what happened. This policeman’s life is ruined.  Why? Because   we have to break somebody down.  Because we have to even out the game.  I  don’t know. I don’t get it. Maybe I’m just old-fashioned,” he told the Sun-Times.