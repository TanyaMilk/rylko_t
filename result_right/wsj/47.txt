Biometric identification systems, like fingerprints and eye scans, are old hat. Try this on –or in– for size: a PayPal executive who works with engineers and developers to find and test new technologies, now says that embeddable, injectable, and ingestible devices are the next wave in identification for mobile payments and other sensitive online interactions.

In a presentation called Kill all Passwords that he’s recently started giving at various tech conferences in the U.S. and Europe, PayPal’s global head of developer evangelism Jonathan Leblanc argued that technology has taken a huge leap forward to “true integration with the human body.”

Leblanc said that identification of people will shift from “antiquated” external body methods like fingerprints, toward internal body functions like heartbeat and vein recognition, where embedded and ingestible devices will allow “natural body identification.”

These devices include brain implants and attachable computers, which “put users in charge of their own security,” he said. Ingestible devices could be powered by stomach acid, which will run their batteries, he added.

In an interview with The Wall Street Journal, Mr. Leblanc said that passwords were broken, and that it was time to replace the concepts and methods of username and password verification.

“If there’s a weak password you need to harden that with something physical behind it,” he said.

While there are more advanced methods to increase login security,  like location verification, identifying people by their habits like the way they type in their passwords, fingerprints and other biometric identifiers, these can lead to false negative results, where valid users can’t log in to their online services, and false positives, where invalid users can log in.

Mr. Leblanc pointed to more accurate methods of identity verification, like thin silicon chips which can be embedded into the skin. The wireless chips can contain ECG sensors that monitor the heart’s unique electrical activity, and communicate the data via wireless antennae to “wearable computer tattoos.”

Ingestible capsules that can detect glucose levels and other unique internal features can use a person’s body as a way to identify them and beam that data out.

To protect against hacking, the data would be encrypted, he said.

Mr. Leblanc said PayPal was working with partners who are building vein recognition technologies as well as heartbeat recognition bands. PayPal is also working with developers, mostly through 24-hour hackathons, who are building prototypes of futuristic ID verification techniques, he said.

By talking about new biometric verification technologies, PayPal is not necessarily signaling that it’s thinking about adopting them, rather positioning itself as a thought leader, Mr. Leblanc said.

“I can’t speculate as to what PayPal will do in the future, but we’re looking at new techniques – we do have fingerprint scanning that is being worked on right now – so we’re definitely looking at the identity field,” he said.

Mr. Leblanc admits that there’s still a ways to go before cultural norms catch up with ingestible and injectable ID devices.

Still, he said that there’s a startup or company behind every idea or product he talks about.

“I ground a lot of my talks in reality, but toward the end of the presentation things get a little strange.”

 

 

______________________________________________________
For the latest news and analysis, follow @wsjd

Get breaking news and personal-tech reviews delivered right to your inbox. 

More from WSJ.D: And make sure to visit WSJ.D for all of our news, personal tech coverage, analysis and more, and add our XML feed to your favorite reader.