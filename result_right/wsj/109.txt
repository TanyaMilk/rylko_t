The times they are a-changing—at least a little and in a positive way. Ahead of International Women’s Day, March 8, a group of Afghan men last week showed their solidarity with women who want relief from imposed forms of Islamic dress. The men put on burqas, the garment worn in public by almost all Afghan women. The burqa covers the body from head to toe, allowing just the eyes to show (and then only through mesh). Last month in Turkey and Azerbaijan, men took to the streets in skirts to protest the murder of Ozgecan Aslan, a Turkish student who reportedly fought off a rape attempt by a bus driver – presumably because she was dressed provocatively — and was stabbed to death.

Such bold gestures by Muslim men in support of women struggling against patriarchal tradition are almost unprecedented. Perhaps this tongue-in-cheek approach will prove effective with those men who still think that women’s lives and choices should be dictated by them. One of the Afghan men even likened the burqa to a “prison.”

I have never worn the burqa, but on a number of occasions when I lived in Iran—for example, when visiting shrines—I did wear the chador, a garment that covers the whole of a woman’s body. Although not as forbidding in its coverage as the burqa, the chador is cumbersome. Women have to use one hand to keep the chador firmly in place so that it hides most of their face. Those wearing the chador see the world with only one eye, whether they are maneuvering through crowded streets, negotiating staircases, or climbing into or out of buses. I never had to wear the chador, but millions of women do. In the 21st century, millions of Islamic women fear the wrath of family members or the authorities if they do not observe a social- or government-mandated form of dress.

In Saudi Arabia the hijab is obligatory. In Iran, men have thrown acid at the faces of women not observing Islamic dress. Over the past 36 years the government-run morals police in Iran frequently punished women who violated the dress code by subjecting them to lashings. Islamic State, too, has a morals police, the al-Khansaa brigade, whose job is to ensure that women are properly dressed and behave appropriately. In ISIS-controlled areas, women have been executed for infractions of dress and behavior.

Of course, millions of women prefer to wear the veil. But there are millions of women in the Islamic world who don’t wish to cover themselves, women who find the veil inconvenient and/or view it as a symbol of subjugation by men. As a Middle Eastern woman, I feel it is an affront to my dignity to be told that the honor of my family rests on my modesty, which is symbolized by my wearing the veil. To veil or not to veil should be a matter of personal choice, a right that all women should be able freely to exercise. International Women’s Day is an occasion to reassert fundamental rights. It is also a time to hail those burqa-wearing men in Afghanistan and skirt-wearing men in Turkey for exercising their right to dress as they wish.

Haleh Esfandiari directs the Middle East program at the Woodrow Wilson International Center for Scholars. She was held in solitary confinement in Evin Prison in Tehran for 105 days in 2007. The views expressed here are her own.

ALSO IN THINK TANK:

In Iran, a Range of Reactions to Netanyahu’s Speech and Nuclear Talks

5 Reasons to Think a U.S.-Iran Deal May Be Near

Netanyahu’s 3 Objections to a U.S. Deal With Iran

From Khamenei, Conditions for a Deal on Iran’s Nuclear Program

The Significance of Hard-Liners’ Criticism of Iran’s Foreign Minister

______________________________________________________

Politics Alerts: Get email alerts on breaking news and big scoops. (NEW!)
Capital Journal Daybreak Newsletter: Sign up to get the latest on politics, policy and defense delivered to your inbox every morning.
For the latest Washington news, follow @wsjpolitics
For outside analysis, follow @wsjthinktank