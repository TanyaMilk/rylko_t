Americans’ feelings toward their own political leaders run hot and cold, but there is broad agreement that they like Pope Francis, according to the Wall Street Journal/NBC News poll.

The pope’s approval ratings are through the roof – 56% of those surveyed see him in a positive light, while only 6% have a negative view. The difference, 49 percentage points, is larger than anyone else the poll surveyed – 19 points more than former President Bill Clinton, who is the next most popular by this measure. By contrast, only 44% of Americans view Barack Obama in a positive light, compared to 43% who see him in a negative one.

Born in Argentina as Jorge Mario Bergoglio, the first pope from the Western Hemisphere has also improved Americans’ view of the Catholic Church, which in recent years has been ensnared in scandal. The poll found 43% of Americans have a more favorable view of the church because of the pope’s statements – which include his famous “Who am I to judge” position on gays – while only 4% see the church in a less favorable light.

Francis’s predecessor, Pope Benedict XVI, took far more conservative stances on the social issues facing the church. He was viewed less well in the U.S., with just 30% having a favorable view of him compared to 17% who did not, according to a Journal/NBC poll in February 2013. Pope John Paul II was seen positively by 65% of Americans in 1998, when only 7% had an unfavorable view of him.

American politicians know this, of course, and have rushed to share some of the pope’s aura. In March 2014, Mr. Obama met with the pope at the Vatican. His administration gave him credit for the diplomatic détente with Cuba last December. House Speaker John Boehner (R., Ohio) announced in February that the 78-year-old pope will address a joint session of Congress when he is in Washington in September. Mr. Obama also plans to host the pope at the White House.

The poll of 1,000 adults was conducted from March 1-5 and has a margin of error of plus or minus 3.1 percentage points.

______________________________________________________

Politics Alerts: Get email alerts on breaking news and big scoops. (NEW!)
Capital Journal Daybreak Newsletter: Sign up to get the latest on politics, policy and defense delivered to your inbox every morning.
For the latest Washington news, follow @wsjpolitics
For outside analysis, follow @wsjthinktank