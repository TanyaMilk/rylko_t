BARCELONA—It’s the kind of recipe that fails all the time in soccer: Stir together the most expensive attacking talent you can afford and wait for the magic to happen. Until, of course, your forwards get in each other’s way, bicker over the limelight, and grimace on the field when things go wrong.

It’s happened before at Real Madrid. And it’s happened before at Barcelona.

So when Barça spent $91 million last summer to add Luis Suárez to the mix of Lionel Messi and Neymar, fans could be forgiven their skepticism. The doubts grew when it took Suárez six games to score his first goal. But subtle tactical shifts, combined with a remarkable understanding within that trio, have led to $400 million worth of firepower finally clicking. Now Barcelona is chasing three trophies, including the UEFA Champions League.

“In the beginning you had doubts, because the goals took a while to come,” manager Luis Enrique said ahead of Tuesday’s quarterfinal second leg against Paris Saint-Germain. But Suárez, Neymar and Messi currently represent 43.6% of Barcelona’s scoring production. Almost half the club’s goals were either scored or assisted exclusively by those three.

Barcelona has been trying to figure out its center-forward puzzle since Pep Guardiola was in charge at the end of the 2000s. When its slow buildup and intricate passes ran into a wall, it needed a Plan B. 

The club didn’t find it last season when it tried to combine Neymar and Messi with forwards such as Pedro and Alexis Sánchez, who looked more at home on the wings. And it certainly didn’t work when Guardiola tried one of the most devastating pure strikers in Europe, Zlatan Ibrahimovic, who returns to Camp Nou with PSG.

Suárez gives the club an extra dimension.

Not quite a traditional No. 9 and too far inside to be considered a true winger, he ventures into spaces that few forwards exploit. It made sense on Monday when he cited the mobile Argentine striker Gabriel Batistuta as one of his strongest influences.

“He was a different kind of No. 9,” Suárez said. “He was never static.”

Part of this is down to Suárez’s phenomenal work rate, which was a hallmark of his game at his previous club, Liverpool. Only one striker still in the Champions League has covered more ground per game than Suárez’s 6.5 miles this season, PSG’s Edinson Cavani—and Cavani isn’t as incisive.

It also helps that the three personalities appear to have meshed. Suárez said on Monday that he felt shy when he signed for the club, because of “who is Leo, who is Ney.” Plus, he was still serving a four-month suspension from soccer for biting an opponent at last summer’s World Cup.

“But when I arrived here, they said ‘You stay the same as [you were] in Liverpool,’” Suárez said of his new partners in crime. “Outside the pitch, I’m so happy with them...a lot of joking.”

Judging from photos posted on Instagram of the chumminess between Neymar, Suárez and Messi, or their exuberant goal celebrations, there is a feeling around the team that this trio actually likes each other. (Compare that with Cristiano Ronaldo’s on-field vexations with Gareth Bale at Real Madrid.) The most compelling evidence is how happily Messi has adjusted his game to help Suárez’s.

A year ago when now-departed coach Gerardo Martino deployed Messi on the right in a Champions League game at Atlètico Madrid, Messi made his disgust apparent. Sulking along the sideline, he ran less than any of his teammates save the goalkeeper.

Now, it’s where he plays most weeks. Suárez said in a recent interview on Spanish radio that the switch, which put him in the middle of the three-man attack, was the result of a conversation he had with Messi, man to man. The implication was that Enrique had no say.

Asked about it this month, the manager shot back with sarcasm. “Yes,” he said. “They decide all the rotations, they pick the team, they decide how we attack, how we defend. If we win. If we lose, it’s me.”

The relationship between Enrique and his stars isn’t always easy. At various points this season, the Spanish sporting press has been full of stories about him falling out with Messi, or Neymar, or both. While the truth falls short of full-blown mutiny, there is no doubt who the club’s brass wants to keep happy above all.

“Messi is our star. He is the one star,” Barça President Josep Maria Bartomeu told The Wall Street Journal earlier this year.

For Messi’s orbit, to function, it has taken one of the lesser lights in the Barcelona galaxy to hold things together.

As the club’s midfield puppeteers get older—Xavi is 35 and Andrés Iniesta is 30—enter Croatian midfielder Ivan Rakitic, who last summer replaced Cesc Fabrègas, now of Chelsea. His work just behind the front three, combined with Messi’s growing inclination to drop into midfield to start the play, has sped up the side’s transition from defense to attack.

While the trend in the rest of the league was to slow down buildup play, perhaps on the Barcelona model, La Blaugrana has resisted. Every other team in La Liga went from averaging around 41 passes per shooting opportunity to 50, while Barca dipped slightly from 51, according to Opta Sports. (Still, Barcelona is nowhere near as trigger-happy as Real Madrid, which averages just 36.6 passes per shot.)

Rakitic’s impact was immediately obvious in Barcelona’s league game on Saturday against Valencia. The side had been penned in for much of the first half. But after coming off the bench for the second, Rakitic restored the fluidity to their midfield.

“We changed our shape and it made it easier to get a player free and get the ball to our attackers,” Enrique said.

Barça won the game 2-0. Messi to Suárez for the first. Neymar to Messi for the second. Which also happened to be Messi’s 400th goal for the club. Enrique called it “an impossible number for most players to even dream of. He’s a unique player, who can’t be replicated.” 

But as Neymar and Suárez are showing, he can be complemented.

 Write to Joshua Robinson at joshua.robinson@wsj.com