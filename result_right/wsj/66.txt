Video: How Rubio Hopes to Win the GOP Nomination

Sen. Marco Rubio is launching his presidential campaign on Monday, hoping to capitalize on his personal story and newcomer status to set himself apart in a crowded GOP primary. Here’s a look at the candidate.

* * *

Who’s running?

Marco Rubio

What’s his background?

Mr. Rubio was born May 28, 1971, in Miami, the son of Cuban immigrants, and raised in both Miami and Las Vegas. He attended Tarkio College in Missouri for one year on a football scholarship before returning to Florida and eventually graduating from the University of Florida. He earned a law degree from the University of Miami and started practicing law, while juggling his early political career as a city commissioner for West Miami. He won a seat in the state House in 2000, at the age of 28, before becoming the first-ever Cuban-American speaker of the Florida legislature.

What’s he doing now?

Mr. Rubio captured the national spotlight in 2010 by squaring off against then-Gov. Charlie Crist from the Republican nomination in a Senate race Rubio eventually won. Despite the tea-party label often applied to him, Rubio came up through the party infrastructure in Florida and has proved to be much less of a bomb-thrower than fellow Sens. Ted Cruz of Texas or Rand Paul of Kentucky. He has been considered a potential presidential candidate since he arrived in Washington, and one point occupied the frontrunner perch.

What positions is he best known for? 

Mr. Rubio is best-known for a policy position he probably wishes most conservatives would forget – his support for a sweeping immigration overhaul that would eventually create a pathway to citizenship for immigrants in the country illegally, a position that angers many conservative activists. Otherwise, Mr. Rubio tends to observe conservative orthodoxy on most other issues and has taken a hawkish stance on foreign policy.  He also criticized President Obama’s recent moves to normalize relations with Cuba and has opposed the Iranian nuclear talks. He has outlined a number of policy prescriptions that he says will boost lower- and middle-income Americans, including an expanded Child Tax Credit and reforms to make higher education more affordable.

What’s his path to victory?

Mr. Rubio’s path is less about geography than it is about the hopes and dreams of his party. He is presenting himself as the face and voice for a new generation of Republicans, leaning heavily on his resume as the son of middle-class Cuban immigrants and a message that stresses how conservative ideals can help people struggling to get by. He boasts broader appeal than most of the other Republicans in the race, but his challenge will be distinguishing himself as the top choice for primary voters in a crowded field that has something for everyone. He must also figure out how to emerge from the shadow cast for former Florida Gov. Jeb Bush, his close ally.

More

Rubio Has Enjoyed Success Running as Underdog

The Case for, and Against, Marco Rubio as Republican Presidential Candidate

Marco Rubio: The Immigration Reform Opportunity (Opinion)

First Three GOP Presidential Candidates Share Tea-Party Roots

Who’s Running: A Look at the GOP and Democratic Hopefuls

 

______________________________________________________

Politics Alerts: Get email alerts on breaking news and big scoops. (NEW!)
Capital Journal Daybreak Newsletter: Sign up to get the latest on politics, policy and defense delivered to your inbox every morning.
For the latest Washington news, follow @wsjpolitics
For outside analysis, follow @wsjthinktank