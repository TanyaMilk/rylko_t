When American Cynthia Woods found out that her husband, an executive at a multinational company, would be posted to Chengdu, China, in 2008, she embraced the adventure, conducting exhaustive research on the city, schools for the couple’s three boys, and life overall in China.

“I was just so happy,” says Ms. Woods.

But a few days into the move, she realized her marriage was over.

Within two weeks, the family moved back to the U.S., her husband’s assignment cut short, Ms. Woods says. “My oldest was 18 and had a girlfriend who was an expat and had moved back to Singapore. It was crushing. I still carry the guilt to this day that I took him away from her. But I knew for my own sanity I had to get us out of there. I thought, if I got us out, the family would heal.”

It’s virtually impossible, of course, to understand the causes of marital breakups, especially in the complicated world of expats. But a new study has found that expat life can exacerbate existing problems in a marriage, and bring in new tensions that can cause expat marriages to fray and break.

Expat scholar Yvonne McNulty, an associate faculty member at SIM University in Singapore, found in her study, “Till Stress Do Us Part: The Causes and Consequences of Expatriate Divorce,” to be published in June in the Journal of Global Mobility, that expat life brings its own form of marriage stressors. Dr. McNulty studied 38 expat divorces in 27 countries and found a range of issues: trailing spouses who may find themselves with a loss of identity after a move; a lack of a longtime community that might bolster a struggling couple; and long work hours mixed in with extensive travel that pulls couples apart.

Ironically, Dr. McNulty says, those stresses might not translate to a higher divorce rate for expats, since spouses far from home might be more inclined to stay longer in a bad situation. But when couples do break up the results are far more serious, she says, with international battles over child custody, confusion over which country has jurisdiction over the divorce, and huge relocation costs for companies that have sent entire families overseas.

One element that can intensify problems, she says, is the expat community itself, which can “become almost like a toxic influence on a marriage.”

“It’s like a groupthink attitude. If one or two individuals are engaging in extramarital affairs the men tend to say, that gives me permission to do it.” Some common expat destinations, particularly Asia and the Middle East, are “notorious for changing a marriage,” Dr. McNulty says.

“The way a lot of women described it was that their husbands were a big fish in a small pond,” says Dr. McNulty. “Western men are considered a prize catch, and women are ruthless in how they pursue them.”

READ MORE: After a Divorce, a Longterm Expat in Istanbul Falls in Love With the City

Read More: Expats in Love: Top Countries for Romance and Happiness

American John Lackey, who has lived abroad since 1999, says that one tradition in his former company in Singapore was to take the new hires – all male – to a place called Orchard Towers, which was known for its massage parlors, sex shops and discos.

For Mr. Lackey and his ex-wife, though, work stress and travel seem to have caused the breaking point. Mr. Lackey, an American who now holds a Singaporean passport, puts the blame on his constant travel for his divorce from his Malaysian-born wife.

For instance, during the first six months that the couple lived in Bangalore, Mr. Lackey says he was on the road 163 days. “I made the effort to take the family with me,” he says, “but I think it was rough on them.”

The couple had a very different sense of how to live their lives. “I would travel and get home on the weekend and be exhausted. She would say, ‘Let’s go to the market.’ I would say, ‘You have a driver, a maid, a cook, a gardener. Why not do it during the week?’ It just started to grind on us, take a toll,” he says. Reached by email for a comment, Mr. Lackey’s ex-wife declined to be interviewed.

Over a five-year period, the couple moved from Singapore to Hong Kong, to India, back to Singapore, and then to China, he says. “I think the strain and stress of all the moves and the travel just became too much.”

Another problem is social isolation. Most expat couples are far removed from any kind of setting – family, friends, communities – that might have bolstered a struggling marriage. Dr. McNulty says that in her research, “one of the strongest things that came out was the lack of role models or mentors” for troubled marriages. Many of the women she surveyed said to her, “If we had been home, my parents, his parents, his brother would have pulled him up by the scruff of his neck and said, ‘What are you doing?’”

In addition, the expat community is not always willing to get involved. “When you’ve got marital problems, the other expat wives think you have a disease, and they shun you,” Dr. McNulty says.

The situation becomes more complicated when one member of the couple is an expat and other is living in a home country.

Monika Fischer, a Czech who married a German man, says that living in Germany put stresses on the marriage. The Germans she encountered when the couple lived in northern Germany were not very warm or welcoming, and that made her far too dependent on her husband for her social contact. “I didn’t have anybody else but him,” says Ms. Fischer, 56, who now lives in Zurich. “For him, it became too much.” Ms. Fischer’s ex-husband did not respond to emails seeking a response.

When Ms. Fischer’s husband got a job offer in Singapore, they both leapt at the opportunity to leave Germany, despite their realization that the marriage was already struggling there. “To have a totally new experience in a totally different culture – maybe this will turn us around and change the situation,” she thought at the time.

Instead, her husband lost his job after the family had lived in Singapore for three years. With the high cost of housing in Singapore and no work, Ms. Fischer says they didn’t even have the money to fly the family of five 13,000 kilometers home.

The effects of expat divorce can be more powerful than other divorces too, says Dr. McNulty. Many countries don’t allow married couples to have a joint bank account, so when the marriage breaks up the spouse, often female, may be cut off from support with no access to money.

One English woman had moved with her husband, also British, to Ghana. There, she says, she learned that it was common for men to have mistresses. The woman says she loved living in Africa, which gave her a chance to travel and meet new friends. “I think it’s possible now that’s why I didn’t notice what was going on,” she says. Finally, her husband admitted he had been having an affair for two years and that he wanted her to leave. She was back in England within a week of learning the news.

“I didn’t have anybody there to stay for,” she says. “As much as I didn’t want to come home to my family, I kind of needed my family.”

Child custody issues are also far more complicated in an expat situation, says Dr. McNulty. While the Hague Convention of 1980 requires that children remain in the country where custody is disputed, many Middle Eastern countries are not members of the convention.  In many cases then, the father will automatically be awarded custody of the children.

Bente Sternberg, a family therapist living in Ho Chi Min City, Vietnam, agrees that custody issues are particularly difficult with expat couples who may be accustomed to moving to a new country every three years. “One partner may be moving and his next assignment might be from Vietnam to Africa. Meanwhile, the wife hasn’t lived in her home country for 12 years. It’s not this simple thing of ‘I’ll return home,’ wherever home may feel like.”

Dr. McNulty interviewed one woman in Singapore whose children have American passports. After she was divorced, she moved with them to the States, only to have to return to Singapore because her ex-husband invoked the Hague Convention. “She’s living every expat woman’s worse nightmare,” she says.

Overall, Dr. McNulty says, companies are not set up to support a trailing spouse in an expat assignment. “There’s the assumption that everything is going to go right, but there is no safety net.”

Monika Fischer says she now wonders whether she should have done things differently. “If you live abroad and your relationship breaks apart, you lose much more than just the partner. It’s everything – because you went that far for him.”

This article has been corrected to reflect that Yvonne McNulty’s study will be published in June, not April as originally reported.

Email us at expat@wsj.com. Join our Facebook group. Follow us follow @WSJExpat.