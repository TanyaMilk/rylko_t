If at first you don’t succeed, try and try again. At least that’s the approach of some Islamic parties in Indonesia that are campaigning to make it a crime to consume alcohol.

Two Islamic parties have put their support behind the drafting of a bill that will completely ban the production, distribution, sales and consumption of alcoholic drinks in the Muslim-majority country. Offenders would face steep fines and jail terms.

The draft bill was announced just days before a ban on the sale of alcohol in convenience stores takes effect, to opposition from retailers and areas that depend heavily on tourism.

The Islam-based United Development Party (PPP) has drafted an early version of the bill, which is supported by another more conservative Islamic party, the Prosperous Justice Party (PKS). The two parties combined have 79 seats in the 560-seat parliament.

That makes the likelihood of the bill passing slim for now. But PPP lawmaker Arwani Thomafi says he is certain it can get support from other lawmakers. Of the 10 parties represented in parliament, six are secular, accounting for 386 seats in total. The other two Islam-based parties have not spoken out about the bill.

Mr. Thomafi says the law is needed because alcohol consumption has “claimed lives” and contributes to high crime rates in some areas. He said a total ban would help protect the lives of young people.

Under the draft bill, anyone found to be distributing or producing alcoholic drinks containing more than one percent alcohol could face between two and 10 years in prison, or a fine of up to 1 billion rupiah ($77,059).

Those caught consuming alcohol could face jail time of between three months and two years or fines of up to 500 million rupiah, said Mr. Thomafi.

Mr. Thomafi said the sponsors have “no intention” of hurting tourism or investment, so some exemptions would be included in the bill. For example, alcohol consumption would be allowed during certain religious rituals and in tourism spots, such as the popular resort island of Bali.

Saan Mustopa, a lawmaker from the secular Democrat Party, said that the draft bill is open for discussion and will not necessarily be deliberated as a total alcohol ban law.

“I think it will be more on how to regulate alcohol [sales and distribution],” he said.

__________________________________________________

Other stories popular on Indonesia Real Time:

Minimarket Beers for Bali? Foreigners May Still be Buying

Mega’s Message to Jokowi: I’m the Boss

For the latest news and analysis, follow @WSJAsia