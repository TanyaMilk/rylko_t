Decades of rapid economic modernization has delivered prosperity to millions in China. It may also have sharpened the ideological divide between liberals and conservatives.

A new study by U.S.-based researchers finds that China has its own version of the U.S.’s red state-blue state divide between Republicans and Democrats. The more prosperous regions tend to be more ideologically liberal than those from impoverished rural provinces, while richer and better-educated individuals were also less likely to express conservative leanings, the study shows.

“Provinces with higher levels of economic development, trade openness, urbanization are more liberal than their poor, rural counterparts,” the researchers said in a paper published online by the Massachusetts Institute of Technology late last week. Residents in inland regions, on the other hand, are more likely to subscribe to conservative calls for a powerful state and collectivist values, they added.

These findings, based on data from an online survey of more than 170,000 people, suggest “that modernization, characterized by economic development, urbanization, increasing incomes, higher levels of education, as well as exposure to ideas from the Western democratic context, is tied to changes in ideological orientation,” wrote Jennifer Pan and Yiqing Xu, who are graduate students in political science from Harvard University and the Massachusetts Institute of Technology.

While the Communist Party enjoys a political monopoly and doesn’t do popular elections, the researchers say their findings have implications for policy-making. The study said that economic troubles, for example, could cause the leadership to adopt more nationalistic rhetoric, which could be more popular in poorer, rural areas.

“It may be an effective strategy for autocratic leaders to pander to a nationalistic base when facing economic challenges,” the paper said. “A liberal policy agenda is more likely welcomed by the rich and well-educated, but less by those who are poor, less educated, and culturally conservative.”

In mapping these divisions, the researchers found that Shanghai was the most liberal of the 29 provinces and municipalities analyzed, followed by the wealthy coastal provinces of Guangdong and Zhejiang. Beijing came in fourth, while Xinjiang—the relatively poor and restive Western border region—came last. Here’s a sortable list of of all provinces and regions ranked according to liberalness:

The study also suggests that, within the Chinese population, political orientation is strongly linked to ideological leanings on economic and social issues.

This means political conservatives—who support nationalism and a strong government—are also likely to back a return to socialism and a state-planned economy, as well as champion traditional, Confucian values.

“In contrast, political liberals, supportive of constitutional democracy and individual liberty, are also likely to be economic liberals who support market-oriented reform and social liberals who support modern science and values such as sexual freedom,” the researchers added.

It isn’t clear precisely why ideological orientation is strongly correlated with economic modernization, though the researchers cite as possibilities greater exposure to liberal ideas and to the benefits of market liberalization.

The study drew on 2014 survey data from the Chinese Political Compass, a website set up by individuals affiliated with Peking University. Responses from some 171,830 people – dominated by male college students – were analyzed, though the researchers acknowledged that this was neither a random nor representative sample of the Chinese population.

Roughly 89% of respondents were from mainland China, 1% were from the self-administered territory of Hong Kong, while the remainder were from abroad. Within China, however, respondents were distributed unevenly – Beijing accounted 23% of the total, followed by Guangdong with 9% and Shanghai with 8%, but nine provinces in western China and the southern island province of Hainan together contributed less than 4% of the survey sample.

– Chun Han Wong. Follow him on Twitter @ByChunHan.

 

Sign up for CRT’s daily newsletter to get the latest headlines by email.

For the latest news and analysis, follow @ChinaRealTime