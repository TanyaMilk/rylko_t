Microfinance was once heralded as a key weapon in the fight against global poverty. Yet some 2.5 billion impoverished people across the world still don’t have access to basic financial services, according to the latest World Bank data. 

Economist and author Jonathan Morduch has been thinking deeply about the conundrum for nearly three decades. A professor of public policy and economics at New York University and managing director of the Financial Access Initiative, he has co-authored two books on the topic: “Portfolios of the Poor: How the Poor Live on $2 a Day,” and “The Economics of Microfinance.” 

He recently spoke to The Wall Street Journal about the limitations of microfinance, the promise of new technologies, and the need for quality, not just cost effectiveness, when it comes to financial services for low-income groups. Edited excerpts: 

 There were big hopes for microfinance a decade ago. What’s your take on it? 

In many ways, microfinance has been a stunning success. In 1997, the first global count of microfinance customers netted about 13 million borrowers. Twenty years later, the tally found 200 million customers. If we assume that each borrower is part of a family of five, the tally implies that 1 billion people are touched by microfinance. The push to scale has been spectacular.

 Why didn’t this push solve the problems of financial exclusion and widespread poverty? 

The biggest constraint to further growth is that microfinance remains locked into a vision in which lending is restricted to finance for small-scale entrepreneurs. That box traps most microlenders.

The fact is that half the world’s adults—some 2.5 billion people—lack access to basic banking services. And most of these 2.5 billion are not small-scale entrepreneurs. They are construction workers, shop clerks, drivers, nannies, factory laborers and other kinds of wage earners. Many live in cities. They have the income to be reliable customers, and they desire loans to manage expenses, but the microfinance sector has little to offer them. Another share of the 2.5 billion comprises farmers, who microlenders have largely avoided for fear of the risks in agriculture. 

When it comes to poverty reduction, the evidence shows that access to finance alone is insufficient. There’s much to learn from microfinance, but the next steps require going beyond the strategies that have driven its success.

 People talk a lot today about the potential impact of new technologies on helping more people get access to banking services. Which technologies do you see as most likely to help and why? 

There are several ways that technologies are changing the picture. The first is radical improvements in digital-payment technologies. This is the backbone on which banking services can be built. In the U.S. and Europe, we hardly give a thought to what life would be like without credit cards and the like. But in the developing world, the new payment systems are disrupting traditional practices in positive ways. They allow government transfers to go directly to recipients, cutting corruption and leakage dramatically. In Kenya, they’re helping extended families cope with shocks by making it much easier to send money to relatives. Simply making it easier to move money is the fundamental step.

Second, technologies are making it easier to keep track of people and establish identities. India’s national ID card, for example, is now making it easier to provide banking services to millions of poor families.

The third technology is mobile banking. The biggest story so far has been the rise of M-Pesa in Kenya with over 12 million customers, a huge penetration rate given the size of Kenya’s population. Even bigger stories, though, will be told about the next innovators. GSMA, the industry group, counts 255 mobile money services in 89 countries. In about half of the countries, regulation allows both banks and nonbanks to provide mobile money services. The latest count found just under 300 million registered mobile money accounts in 2014, which is still a small share of the potential market.

 What’s the biggest technology game changer? 

It’s the possibility of using “big data” from the millions of records of mobile banking transactions to construct more accurate profiles of customers and sharpen assessments of credit risks. I’m more wary than most people here, though. Experience shows that big data models can end up including new customers—while too quickly excluding others.

 Are there any other policies that you favor that aren’t being tried? Or are there any initiatives out there not getting publicity that you think are on the right track? 

The microfinance movement got its start by turning away from traditional brick-and-mortar bank branches. The strategy slashed costs dramatically, and was a triumph. We’re at a point, though, when it’s time to go back to the idea of bank branches with a new mindset. KGFS in Tamil Nadu, south India, shows what the future holds. They created branches that deploy new technologies to serve low-income customers, not with stripped-down products but with a suite of suitable financial offerings. KGFS demonstrated that it is a mistake to think that the very cheapest option will always be most popular. The world’s unbanked seek quality too.

In that same way, consumer protection for microfinance customers remains a priority. The “Smart Campaign” has shown how to establish fair dispute resolution and improve the transparency of pricing and policies. So far, the strategy involves self-regulation, but it can’t stop there. We’ve seen in the U.S. that government-backed regulation is needed to create a mature market. There are risks in that, but there are greater risks in not providing consumers with basic protections.

—Tom Wright