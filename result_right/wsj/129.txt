It’s been a tough week for the news media. Bob Simon, a journalism war horse dating to Vietnam whose courageous coverage won him dozens of awards, including 27 Emmys, died in a fluke car crash Wednesday. Jon Stewart, impresario extraordinaire on “The Daily Show,” announced that he is abandoning us. And Brian Williams got suspended—and may have ended his career as NBC’s evening anchor—for lying.

The toughest news, however, is that media freedom is in “drastic decline” worldwide, according to a survey released Thursday by Reporters Without Borders. Press freedoms were diminished in two-thirds of the 180 countries surveyed, the report reveals.

The 2015 World Press Freedom Index, titled “Decline on All Fronts,” warns of a “worldwide deterioration in freedom of information. Beset by wars, the growing threat from non-state operatives, violence during demonstrations and the economic crisis, media freedom is in retreat on all five continents.”

In 2014, 66 journalists were killed. There has also been an “explosion” of journalists kidnapped in war zones, including Syria, Iraq, Libya, and Ukraine, says Delphine Hagland, the group’s U.S. director. Some 40 journalists were still in captivity at the end of last year.

Reasons vary widely: Extremist movements and criminal groups have silenced or slain reporters, photographers, and bloggers. States tightly control media, whether through censorship, Internet access, intimidation, or prosecution.

“Non-state groups follow no laws and disregard basic rights in pursuit of their own ends. From Boko Haram to Islamic State, Latin American drug traffickers and the Italian mafia, motives may vary but their modus operandi is the same–the use of fear and reprisals to silence journalists and bloggers who dare to investigate them or refuse to act as their mouthpieces,” the report concludes.

The index is also highly critical of the United States, which ranks 49th– down 13 from its ranking last year. Countries that scored better (some far better) include Ghana, Belize, Niger, Slovenia, and Botswana.

“Democracies often take liberties with their values in the name of national security. Faced with real or spurious threats, governments arm themselves routinely with an entire arsenal of laws aimed at muzzling independent voices. This phenomenon is common to both authoritarian governments and democracies,” the report warns.

Some U.S. allies fared poorly, too. France, home to Charlie Hebdo, came in at 38; Japan at 62; Israel at 101; Turkey at 149; and Egypt at 158. Italy fell 24 places, down to 73, because of mafia threats and “unjustified defamation suits,” the survey said.

Wars, the breakdown of nation states, and the proliferation of militias produce grave threats to journalists. The beheadings of Western journalists by ISIS underscore the dangers in Syria. Libya, where NATO airstrikes helped oust Moammar Gaddhafi, ranked 154; seven reporters were murdered and 37 kidnapped there last year.

The report describes some countries in North Africa as “black holes” because they are controlled by non-state groups.

Russia is among the worst places to work because of its draconian laws, blocking of Web sites and independent news “either brought under control or throttled out of existence,” the report says. It ranks 152.

Finland scored highest, followed by Norway and Denmark.

Perpetually near the bottom are North Korea, China, Turkmenistan, Syria, and, at the very end, Eritrea.

Robin Wright is a joint fellow at the U.S. Institute of Peace and the Woodrow Wilson International Center for Scholars. She is on Twitter: @wrightr

ALSO IN THINK TANK:

Kayla Mueller and the Sad History of Hostage-Taking

U.N. Report: Islamic State Has Buried Children Alive

Beyond ISIS Turmoil, Jordan Is Flush With Problems

4 Takeaways From the Fight Against ISIS After the Death of Jordanian Pilot

Why Even Our Friends in the Middle East Don’t Like Us

______________________________________________________

Politics Alerts: Get email alerts on breaking news and big scoops. (NEW!)
Capital Journal Daybreak Newsletter: Sign up to get the latest on politics, policy and defense delivered to your inbox every morning.
For the latest Washington news, follow @wsjpolitics
For outside analysis, follow @wsjthinktank