TACHE RESERVE, British Columbia—A proposed 730-mile pipeline to ship Canadian oil to a West Coast port brings with it the promise of 4,000 or more jobs along a route that would run through impoverished indigenous communities.

But Chief Justa Monk, who runs a reserve with an unemployment rate that hits 70%, wants none of them—and pledges to block the pipeline alongside the reserve’s territory.

It is another hurdle in Canada’s quest to become an energy superpower, even as political struggles in Washington continue to delay a different, better-known Canadian pipeline—TransCanada Corp.
  TRP
  
    
      -0.34
      %
    
    
  

’s Keystone XL, which would carry Canadian oil south across the U.S. to the Gulf Coast.

Chief Monk’s Tl’azt’en Nation, which claims land of 30,100 square miles in northern British Columbia, sits alongside the proposed path of Enbridge Inc.
  ENB
  
    
      -0.36
      %
    
    
  

’s Northern Gateway, a pipeline that Ottawa sees as a crucial piece of the energy infrastructure that will lessen Canada’s almost total dependence on U.S. demand.

Instead, the Tl’azt’en and other native bands have been emboldened by a recent Supreme Court of Canada decision on aboriginal land rights to fight a project they say risks damaging their environment and culture. First Nations’ territory covers much of British Columbia’s West Coast, so the builder of the US$6.3 billion pipeline must find a way to work with the groups to reach the port in Kitimat, where ocean tankers would be filled with oil bound for fast-growing Asian markets.

The clash comes as Ottawa has pledged to sell more oil outside the U.S., where surging supply means Canadian oil trades at a discount—even amid a six-year low in overall oil prices. And as a series of fiery oil-train derailments has brought scrutiny and tougher regulations on rail shipments, oil sands producers and policy makers have made finishing the Gateway pipeline a priority. The pipeline’s proposed capacity of 525,000 barrels of oil a day compares with Canada’s total exports of about three million barrels a day.

Along with the Tl’azt’en, First Nations including Gitxaala, Nak’azdli, Nadleh Whut’en and others have pledged to block the Enbridge pipeline, and aboriginal groups have filed seven separate lawsuits in the Federal Court of Appeal that aim to stop it. Around half the 50 First Nations along the route have yet to back the pipeline, according to Enbridge. First Nations are the largest category of aboriginal people in Canada, which also include Inuit and Métis.

Indeed, the majority of all British Columbia residents say they oppose or want to delay the project, according to public-opinion polls, raising questions on about 400 miles of the pipeline’s planned route.

The pipeline dispute is one of a number pitting companies trying to mine Canada’s oil and mineral riches against the country’s First Nations—its poorest and fastest-growing demographic—as they exercise a growing power to implement protections for the environment and to ensure economic benefits.

Native and environmental protesters have thrown into doubt an expansion of Kinder Morgan Inc.
  KMI
  
    
      0.42
      %
    
    
  

’s Trans Mountain Pipeline outside of Vancouver, and object to a proposed TransCanada pipeline that would ship crude from Alberta to Atlantic Canada.

Last fall, McEwen Mining Inc.
  MUX
  
    
      -2.80
      %
    
    
  

 pulled out of exploring for gold in northern Ontario after local First Nations demanded consultation and a confidential agreement on giving them a share of any find, Rob McEwen, the company’s founder and CEO said. And in December, the government of New Brunswick introduced a moratorium on hydraulic fracturing for oil and gas after months of protests from aboriginal groups.

In British Columbia, Chief Monk took a stand on Gateway at a meeting between First Nation chiefs and the provincial government in September. “I told them, 100% of our territory is owned by the Tl’azt’en Nation and there is no activity on it if we don’t give our consent,” he said in a recent interview.

As he talked during the interview, the chief, who speaks for the tribe’s leadership council, opened an email he had received from the pipeline’s operator, Enbridge, asking for a meeting.

“I don’t even talk to them. I am not interested,” he said.

By law, native groups must be consulted before land development to accommodate their concerns. But since only eight of British Columbia’s 198 First Nations have settled treaties over land claims, final decisions over land use are made by the provincial government. First Nations’ desires can be overturned if a court is persuaded that the parties involved fulfilled their duty to consult and accommodate them. The Tl’azt’en Nation doesn’t have a land treaty.

The national government issued a license for Enbridge last summer to proceed with Gateway as long as it fulfilled certain conditions, but the British Columbia provincial government has yet to sign off on the plan. It added its own conditions that must be met, including that aboriginal concerns are addressed.

Centuries of thorny legal issues surrounding aboriginal land rights became more complex in June, when a ruling from Canada’s top court gave natives new leverage over companies operating on their traditional lands.

The court granted the Tsilhqot’in, a First Nation in British Columbia, title over part of its land for the first time, giving the group the right to bar logging on its territory without approval. In March, the Nation said it was banning all mining and commercial logging on its land.

The judgment also outlined what Canadian native groups with unresolved land claims must demonstrate to either the government or the courts to gain title over their lands, and what rights come with title. Legal scholars and others say this opens the door for other aboriginal groups across the country with unresolved land claims to gain control of their territory.

“What this means is we have a new set of owners for many of the attractive resource areas in Canada,” said Harry Swain, former deputy minister at Canada’s aboriginal affairs department. “You must accommodate their interests and bring them into the deal.”

Scholars say claims could take years to be finalized through negotiations or the courts, and complications include the fact that the country’s provinces have jurisdiction over land use and natural resources, while the federal government controls aboriginal policy.

In the U.S., for comparison, companies generally need tribal permission to develop resources on reservations. Development that impacts treaty rights off-reservation, such as natives’ ability to fish and hunt on traditional lands, also typically needs to be cleared by the tribe, according to James Meggesto, an expert on Indian law at Holland & Knight in Washington, D.C.

To bring the Tl’azt’en and other First Nations along the pipeline’s proposed route on board, Enbridge says it will procure materials from native-owned suppliers and offer training opportunities and jobs, guaranteeing that aboriginals will make up 15% of total employment on the pipeline.

The company also has set aside a 10% equity stake that First Nations can buy in the project and says it will help them obtain the financing needed to afford the purchase. Enbridge says it has met with representatives of Tl’azt’en and incorporated their ideas over the past four years.

The company says the current drop in the oil price doesn’t affect its plans for the Gateway pipeline, which it expects to be in operation for decades.

But the people who live on the Tache Reserve are nearly universally against the project, and Chief Monk says the environmental risk is too great to even discuss it.

Among many Tl’azt’en, who see themselves as custodians of the land, the pipeline represents a threat to their way of life. Many blame the last major piece of infrastructure—a road built in the late 1960s that connects them to a local highway and ended centuries of relative isolation—for bringing drugs, alcohol and welfare, underscoring a long-rooted mistrust of what they see as “encroaching Canada.”

Darcy Isadore, even though he is unemployed, said he wants no jobs associated with the pipeline. He hunts moose and fishes salmon and said there is a risk of contamination if there were a spill from Gateway. Mr. Isadore, wearing thick rubber fishing boots, had a moose hanging in a nearby smoke house ready for a wake for a local funeral. “The pipeline will change all that, it will poison the land,” he said.

Mr. Isadore and others pointed to an Imperial Metals Corp.
  III
  
    
      0.46
      %
    
    
  

 copper mine in the province that spilled more than a billion gallons of waste material, including arsenic, into waterways in August. Imperial Metals says that there haven’t been negative effects on wildlife and that the company is cleaning up the spill.

Enbridge has promised a slew of protections for Gateway, including 24-hour monitoring at pump stations and control centers, aerial surveillance and 132 valves that can cut off oil flow to any leaks.

The National Energy Board, Canada’s main regulator, says pipelines are safe, rarely spill and that all the oil spilled between 2011 and 2013, an average of 883 barrels a year, was recovered.

Ottawa also says a bill now before the legislature will broaden companies’ responsibility for spills and give the NEB new powers to force operators to pay for cleanup costs associated with pipeline leaks.

A standoff on Gateway could put off potential overseas buyers of Canada’s crude, especially if the U.S. moves to allow more oil exports. Oil and mining executives from energy-hungry Asian nations, such as China, have expressed frustration with the process of dealing with Canada’s First Nations and the lengthy pipeline approval process as they look to invest. In September, the president of Enbridge, John Carruthers, said that efforts to win over British Columbia aboriginals have pushed the project back from its 2018 start date to an unspecified time.

The pipeline project could emerge as a thorny issue in Canada’s 2015 election, likely in the fall, as the three main parties compete for votes in British Columbia. The ruling Conservative Party has toned down its support amid negative public opinion. The opposition New Democratic and Liberal parties have vowed to revoke the license granted to Enbridge if elected.

Natives’ growing numbers in Canada are adding to their clout on a range of issues. They currently account for 4.3% of Canadians and will grow to 5.3% of the country by 2030; that compares to 1.7% and 2.3% respectively for Native Americans in the U.S., according to government statistics.

But by every measure of social welfare, from rates of infant mortality to literacy to suicide, the group remains on the margins of Canadian society. In 2011, an aboriginal was about three times as likely to be unemployed than the average Canadian and earned two-thirds the average income, according to the most recent government data. Aboriginals formed 23% of Canada’s federal prison population in 2014, and they are three times more likely to be assaulted than most Canadians.

Aboriginal groups say the dismal quality of life feeds distrust of Canada, even as the corrosive aspects of modern life have taken hold on many reserves.

On the Tache Reserve, close to 90% of residents over 14 abuse alcohol, drugs or prescription medications, Nation officials say. On a recent visit, many of the three-room homes had broken windows despite freezing temperatures and rotten floorboards, and packs of dogs wandered through unpaved streets.

Tl’azt’en officials say they are considering three other requests for resource development that they see as less risky to the environment: a gas pipeline and two mines. They say they are offering training and education for locals to take part in approved resource development, from forestry to some mines, and in other professions, such as law. But such programs are often not taken up, according to Olivia Roberts, the Nation’s Employment Services Coordinator.

Last summer, she says she booked two local men on six-month pipeline engineer training courses. Both quit within two weeks.

“Everybody is unemployed and it’s chaos here,” said Freddy Williams, motioning to his brother’s house, where a fistfight between two men had just erupted. Wearing a cap that said Native Pride, Mr. Williams said that though currently unemployed he gets work in the tourism industry and worries about the effects of potential pollution on that.

Carissa Duncan, 22, who was also against the Enbridge pipeline, describes a young generation that feels little confidence in its future and little connection to its past.

“Maybe I am scared of the future,” said Ms. Duncan, whose parents as children were both removed from the reserve and forced to attend boarding schools as part of a Canadian government policy that lasted into the 1980s meant to assimilate aboriginals into Canadian culture.

Ms. Duncan, who in the past has struggled with addiction and who failed to finish high school, recalls how in early childhood her now deceased grandparents took her out into the country to learn to hunt. “They tell us to go back to the old ways, but there is nobody to teach us the old ways,” she said.

—Chester Dawson contributed to this article.

 Write to Alistair MacDonald at alistair.macdonald@wsj.com and Paul Vieira at paul.vieira@wsj.com