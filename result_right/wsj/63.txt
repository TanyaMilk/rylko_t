India’s has one of the world’s most-skewed sex ratios and one of the worst rates of childhood stunting.

But does one contribute to the other? Can the Indian preference for sons be held responsible for restricting Indians’ growth?

A new working paper from academics at the Cambridge, Mass.-based National Bureau of Economic Research says yes.

Researchers analyzed data from over 174,000 children from India and 25 countries in Sub-Saharan Africa gathered in recent demographic and health surveys.

India outperforms Sub-Saharan Africa on most indicators, from maternal mortality and life expectancy to poverty rates and educational achievement. But, the study found, children born in India are, on average, shorter than those born in Sub-Saharan Africa, unless they are first-born sons.

Children born second or third in a family in India were more likely to be short for their age than their peers in the African countries studied. But among first-born sons, India had a height advantage.

“A son born at birth order 2 is taller in India than Africa if and only if he is the family’s eldest son,” the researchers added.

Indian girls though, on average, were shorter than their African counterparts regardless of birth order.

In other words, small stature was not just a result of malnutrition or genetics, the study said.

“Rather, we propose that a preference for eldest sons in India – encompassing both a desire to have at least one son and for the eldest son to be healthy – generates a starkly unequal allocation of resources within families in India,” the paper said.

India has 919 girls for every  1,000 boys in the age group 0-6, according to the latest Census figures from 2011. In urban areas, the sex ratio is even more skewed towards male children with 905 girls for every 1,000 boys compared to rural areas where there are 923 girls per 1,000 boys.

The authors, Seema Jayachandran and Rohini Pande, say it is the first time a study has “examined cross-country differences in birth-order effects or shown how birth-order preferences, at least in some societies, are entwined with and potentially derive from gender preferences.”

They drilled down further into the data to compare Indian Hindus with Muslims and found that what they call the “birth order gradient” – diminishing height of later born children – was only observed among Hindus. An exception was the majority-Hindu state of Kerala, a southern state with a strong matrilinieal tradition.

“Islam places less emphasis on the importance of having a son,” the study said, adding: “Eldest son preference can be traced to (at least) two aspects of Hindu” religio-cultural practices.

The first, they say, is that aging parents typically live with their eldest son to whom they leave property and second, traditions that state the male heir must conduct the funeral rituals.

Researchers ruled out differences in family income during the offspring’s childhood and other health and environment-oriental factors as having a significant effect on height differences among siblings. “Finally, other cultural factors, including greater communal care-giving in Africa and a historic preference for more family labor in Africa (related to greater land abundance) cannot explain the observed patterns,” the study’s authors said.

“The Indian height disadvantage materializes for second-born children and increases for third and higher-order births, at which point Indian children have a mean height-for-age lower than that of African children by 0.3 standard deviations of the worldwide distribution,” the study said.

For breaking news, features and analysis from India, click here and follow WSJ India on Facebook.