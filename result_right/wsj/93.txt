For centuries, ballet was all about presenting its glittering, performance-ready side. Then came Instagram. 

The photo-sharing app has become the go-to social-media platform for dancers of all ages. They post photos of bloody toes, mistakes in class, physical therapy and, of course, deliriously beautiful performances shot from the wings. 

As a virtual portal to the dance world, Instagram has also attracted an enthusiastic audience—and around that, a newly dance-centric marketing landscape has emerged.

While Instagram doesn’t track the number of dance-related accounts, according to a spokesman, dancers from most major American companies are actively participating in this blend of art and commerce.

New York City Ballet principal dancer Ashley Bouder, a frequent poster, exemplifies the balance of the two. “I’m looking to open up my life and the ballet world,” she said, adding that she has her own line of dancewear that she promotes. “I use Instagram all the time to do giveaways.” 

She takes “thousands and thousands of photos of ballets backstage”—as well as the occasional selfie with a bottle of Zico coconut water, sent to her by the company. “They asked me if I could post on Instagram.”

A spokesman for Zico said nothing was required in return for the gift.

Ms. Bouder, a principal since 2005, has about 11,400 followers. In a different stratosphere is American Ballet Theatre soloist Misty Copeland, with 428,000 followers. Among her performance and rehearsal shots or studio portraits, she will tout events for her memoir “Life in Motion” or Under Armour, the athletic brand for which she starred in a viral ad campaign in 2014.

While individual dancers have embraced the site, major ballet companies are more hands-off, preferring to focus on platforms that have easier ticket-buying access.

New York City Ballet currently has no official account, though it is in the process of a social-media audit. Facebook
  FB
  
    
      0.28
      %
    
    
  

 is its most effective platform in marketing tickets, said Karen Girty, senior director of marketing and media.

American Ballet Theatre allows its dancers to populate its Instagram feed, banking on dancers’ fandom to generate buzz—and ticket sales. “As they are building their individual brands, they share that spotlight with ABT,” said Ballet Theatre’s James Timm, director of marketing and brand management. 

The fledgling company BalletNext raised its profile by using Instagram. The six-dancer troupe was founded in 2011 and is led by former Ballet Theatre principal Michele Wiles. It had about 500 followers before Executive Director Elizabeth Johanningmeier came on board about a year ago.

With Ms. Johanningmeier’s regular posts, BalletNext now has more than 3,800 followers, and advance tickets to the company’s February shows sold earlier than last year. “People would say, ‘I’ve been watching you on Instagram, and I can’t wait to see what you’re doing,’” she said.

The dancewear brand Capezio also took note and expanded its support of the company, inviting BalletNext to appear in its new product brochures—a major step for a tiny company.

Capezio marketing consultant Julie DeLoca liked the contrast of tutus and sneakers in BalletNext’s collaboration with a dancer specializing in the street style Flex. “That’s where we want to be: technique and innovation,” she said.

For mass appeal, Capezio sponsors the 12-year-old competition dancer Sophia Lucia, who has 883,000 followers and appeared on Lifetime’s TV series “Dance Moms.”

Before a Capezio photo shoot, a picture of Sophia with a tutu and a cat went up on her feed, which is run by her mother. It generated more than 50,000 likes. 

“We’ve got these young brand ambassadors who are doing more than our traditional brand and marketing campaigns,” said Ms. DeLoca.

Mainstream fashion brands, too, have tapped in, mainly by partnering with the Ballerina Project, an Instagram feed and visual-art effort by photographer Dane Shitagi with more than 700,000 followers.

Mr. Shitagi shoots dancers, such as Ballet Theatre principal Isabella Boylston, in unusual settings—often outdoors—wearing contemporary clothing or dancewear. He rejects pink, frilly costumes for his dancer-models. “To put them in a practice tutu and tights…it’s horrendous. You’re dressing a beautiful woman as a 12-year-old,” he said.

When hired by a corporate brand, he will shoot as he normally would, except that the dancers will be wearing, say, bodysuits provided by Wolford, known for luxury legwear and lingerie. For the fashion retailer AG, his dancers wore its new line of stretch denim. 

In both cases, Mr. Shitagi then posted a series of those images exclusively several times a day for about a week, with a brand mention. Without an in-app shopping function, a direct-sales impact is difficult to gauge, said Johnathan Crocker, director of global communications for AG, but the objective was content that aligned with the art form. 

“Chic, sophisticated and classic are our brand tenets,” he said. “We felt ballet delivered on it.”

Not everyone is eager to have companies capitalize on dance. 

When Ballet Theatre principal Marcelo Gomes posts nondance photos, they are mainly supporting other art forms, such as Broadway shows or photography. “I try to be careful. If it is something that I support completely, that’s great,” said. “But if it is just for the profit of someone to use my name, I use my judgment.”

City Ballet soloist Sean Suozzi, who shoots portraits of colleagues, sees Instagram as an extension of his visual work on stage. 

“I feel like I can express myself,” he said of the site. “People with good Twitters…I could never do that.”

 Write to Pia Catton at pia.catton@wsj.com