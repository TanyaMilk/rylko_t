WASHINGTON — For them, it was a fight that began on a tarmac in Baltimore, when Cincinnati
resident Jim Obergefell said “I do” to the man he had loved for 20 years.

It’s a fight that will be decided by the U.S. Supreme Court, which on Tuesday will consider
whether states have the right to prohibit same-sex marriage.

“It hasn’t quite hit me that my personal life, John’s and my marriage, the fight we started at a
very personal level, is going to affect millions of people,” Obergefell said this month. “And the
fact that it’s going to have an effect coast to coast — that is slowly but surely sinking in.”

Two years ago, Obergefell and his spouse, John Arthur, filed a lawsuit challenging Ohio’s ban on
same-sex marriage, months before Arthur died of Lou Gehrig’s disease.

On Tuesday, Obergefell, still a grieving widower, will be the named plaintiff in a cluster of
six cases on same-sex marriage from Ohio, Kentucky, Michigan and Tennessee. At issue is whether the
Equal Protection Clause of the 14th Amendment requires states and territories to recognize same-sex
marriages performed in their own or other states and territories.

By scheduling three hours for arguments instead of the customary one hour, the justices are
signaling the importance of the cases.

Just two years ago in United States v. Windsor, the court invalidated a 1996 federal law
defining 
marriage as between a man and a woman. Although the justices have never ruled on whether
states could ban same-sex marriage, a growing number of legal experts say it would be difficult for
the justices to uphold Ohio’s ban after they struck down the federal law.

“There is a very powerful argument to be made that Windsor is the crossing of the Rubicon on
marriage recognition,” said Marc Spindelman, a law professor at Ohio State University.

Rarely has public opinion shifted faster on a major issue than on same-sex marriage. When
Democratic President Bill Clinton signed the federal law in 1996, his decision met with approval
from most voters.

In November 2004, 61.7 percent of Ohio voters approved a constitutional amendment prohibiting
same-sex marriage.

In the past decade, however, the views of Americans have changed dramatically. Today — either
through legislative action or court rulings — 37 states and the District of Columbia recognize
same-sex marriage.



>> Gay-Rights Timeline



In the wake of the Supreme Court’s 5-4 decision in 2013 striking down the federal law, four
federal courts of appeals across the country have invalidated a number of same-sex-marriage bans.
The only exception was a 2-1 ruling last year by the U.S. 6th Circuit Court of Appeals to uphold
the bans in Ohio, Kentucky, Tennessee and Michigan.

In the majority opinion, Judge Jeffrey Sutton, a former state solicitor of Ohio, acknowledged
the mood of the country by writing “it would now seem, the question is not whether American law
will allow gay couples to marry; it is when and how that will happen. That would not have seemed
likely as recently as a dozen years ago.”

But Sutton wrote that “what remains is a debate about whether to allow the democratic processes
begun in the states to continue” in Ohio, Kentucky, Michigan and Tennessee or have the courts end
the debate by extending “the definition of marriage to encompass gay couples.”

Sutton’s opinion likely will find favor with Chief Justice John Roberts and the other court
conservatives — Antonin Scalia, Clarence Thomas and Samuel Alito. The question is whether it will
meet the approval of Justice Anthony Kennedy, the key fifth vote and the author of the Windsor
opinion.

Kennedy concluded that the 1996 federal law violated the Fifth Amendment to the Constitution,
which guarantees that no one can be deprived of liberty without due process of law.

He wrote that the law “instructs all federal officials, and indeed all persons with whom
same-sex couples interact, including their own children, that their marriage is less worthy than
the marriages of others.”

Kennedy acknowledged that since the beginning of the republic, the states have had the
responsibility to devise marriage laws. But despite Kennedy’s nod toward states’ rights, Scalia and
the other court conservatives saw Kennedy’s opinion as a prelude to invalidating laws such as Ohio’s
 that ban same-sex marriage.

In his dissent in Windsor, Scalia warned that “by formally declaring anyone opposed to same-sex
marriage an enemy of human decency, the majority arms well every challenger to a state law
restricting marriage to its traditional definition.”

Conservatives such as J. Kenneth Blackwell, the 2006 Republican gubernatorial candidate in Ohio,
warn that if the justices strike down Ohio’s ban, they will “have started to unwind 2,500 years of
culture and tradition,” adding that “they will have established a right that is not in the
Constitution.”

Since filing the lawsuit, Obergefell has become widely known. While having dinner in a
Cincinnati restaurant, a waiter recognized him and said that he and his boyfriend “appreciated
everything I was doing,” Obergefell said

Long before they exchanged vows, Arthur and Obergefell considered themselves wed. But when the
two flew to Maryland to be married — Arthur was already very ill — everything changed.

“It was amazing that making it legal and having that license and saying those words in front of
witnesses and in front of our government — it made a difference.

“The three months I was able to be his husband were the three best months of my life.”


jwehrman@dispatch.com



@jessicawehrman



jtorry@dispatch.com



@jacktorry1