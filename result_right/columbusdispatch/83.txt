When state auditors made a surprise visit to a Youngstown charter school, they found staff members but no students. Not one.

The students, auditors were told, had been dismissed at 12:30 p.m. after taking a practice graduation test. The Academy for Urban Scholars Youngstown said it had 95 students.

A report released yesterday by Ohio Auditor Dave Yost found significantly lower attendance at half of the 30 charter schools where auditors conducted unannounced head counts this past fall.

The report raises questions about whether the schools receive more tax money than they are entitled because the state relies on student enrollment — reported by the schools — to calculate aid. The privately operated, publicly funded schools get nearly $6,000 per student each year.

“I’m really kind of speechless of everything that I found. It’s quite a morass,” Yost said during a Statehouse news conference.

Among those with the widest gap was Capital High School, 640 Harrisburg Pike, Columbus. The school reported 298 students; auditors counted 142, fewer than half.

School officials did not return a call from The Dispatch yesterday; however, they told auditors that their average daily attendance was 55 to 60 percent, fairly consistent with what investigators found.

Gateway Academy on Kimberly Parkway North in Columbus reported 100 students but auditors counted 52, with 20 students absent.

“They came during lunch, and we only had two classes” in session. Many students were at lunch, some outside the building, said Hydia Green, Gateway principal and superintendent. “To get a true count in my building, you need to come after lunch.”

Classes start at 7:30 a.m. at the school serving students in grades 7 to 12, but many arrive late. To accommodate them and others, the school offers blended learning, in which students can get their lessons online, Green said.

Ohio has about 300 charter schools. Of the 30 examined,

16 had enrollment discrepancies of more than 10 percent.

The 56-page report did not call on any school to return money to the state. Rather, Yost referred the schools and their sponsors to the Ohio Department of Education for further investigation.

“The numbers that are being used for funding decisions are not necessarily accurate,” Yost said.

The auditor’s report also critiqued some aspects of charter-school laws, arguing they lack segregation of duties and allow for conflicts of interest among charter-school boards, sponsors and management companies paid to run day-to-day operations.

The findings come as pressure mounts on state lawmakers to fix what even some charter-school supporters perceive to be a broken system, lacking sufficient accountability and oversight.

Sen. Peggy Lehner, a Kettering Republican leading a review of charter-school laws, said there are problems that “absolutely” need to be addressed.

“Obviously, this report raises some serious issues, and the legislature is not going to drop it,” she said. “We will continue working until we feel the taxpayers are getting value for their money and students are getting the education they deserve.

“The real problem with Ohio’s charter law is it’s 20 years old … it’s not cohesive,” Lehner said.

The auditor’s recommendations echo many of those made by Democrats and some pro-charter-school groups frustrated that Ohio’s system allows too many bad schools to continue operating with too little oversight.

“Auditor Yost is right. Ohio’s system of regulating public charter schools is broken,” said Senate Minority Leader Joe Schiavoni, D-Boardman. He intends to reintroduce legislation to clamp down on charter-school sponsors and operators.

“I hope we have reached a tipping point where both Republicans and Democrats can agree on the need for comprehensive charter-school reform,” he said. “There are just too many examples of students being cheated out of a good education and tax dollars being wasted for the General Assembly to ignore the problem any longer.”

The pro-charter-school StudentsFirst Ohio said the report exposes Ohio’s “glaring need” for more accountability.

“We need to ensure that all students have quality school choices,” said Greg Harris, state director of StudentsFirst Ohio. “Unfortunately, the victims of educational malpractice are largely Ohio’s most vulnerable citizens: impoverished schoolchildren.”

A spokesman for the Ohio Department of Education said the agency will review the findings. “Clearly, a student cannot receive a quality education if they are not in attendance,” said John Charlton, department spokesman.

He said some safeguards have been implemented. For example, new charter schools, starting this year, did not get their first state aid payment until after the beginning of the school year, instead of beginning July 1 based on estimated enrollment.

In addition, the state no longer relies on a single “count week” in October and requires both traditional public schools and charters to report enrollment three or more times during the year so that payments can be adjusted accordingly.

ccandisky@dispatch.com

jsiegel@dispatch.com