Fish. Macaroni and cheese. Maybe a grilled cheese sandwich.

If that sums up your meatless-cooking repertoire, some Columbus-area chefs can help elevate your
game.

With Lent beginning today, many Christians will be giving up meat at least one day a week during
the solemn period before Easter.

Others might be motivated by the Meatless Monday campaign or simply be seeking to improve their
health, save money or aid the environment.

Regardless, area chefs say, the easiest way to go meatless is to begin with a dish that’s
already a favorite.

Love pasta? Swap the meatballs and sausage for vegetables.

Enjoy a baked potato? Stuff it with high-protein beans, corn and veggies; and move it to the
center of the plate.

“One of the things that we talk about when we teach a wellness class is — when transitioning (to
meatless eating) — how it is they like to eat now and how some of those dishes can be made
vegetarian,” said Del Sroufe, executive chef at the Wellness Forum, the Worthington studio that
offers classes and counseling in nutrition, exercise and a healthful lifestyle.

The answer can be as simple as leaving out the meat — or, for those trying to go vegan, the
cheese.

Eating meatless doesn’t necessarily mean going full-on vegetarian or vegan (although tofu,
textured vegetable protein and other meat substitutes are available for those who want to try
them).

For a dish such as chili, Sroufe said, taking out the meat and replacing it with lentils or a
second type of bean will result in a meal that tastes largely the same without losing its inherent
heartiness.

If tacos are a family favorite, Sroufe suggested, fill them with beans and grilled veggies
instead of ground beef, and adding the same traditional toppings: lettuce, tomato, onions and
guacamole.

“Keep it simple,” he said, “and keep it to something that your family is going to eat.”

Beans and starchy vegetables, Sroufe said, should be the foundation of a diet for those seeking
to eliminate meat.

For those who miss the taste of meat, Justin Wotring, chef at the Crest Gastropub in the
Clintonville neighborhood, said it is easily mimicked by adding similar flavors.

Grilled or smoked vegetables, for example, give the essence of grilled or smoked meats, he
said.

A new veggie burger he is creating for the restaurant, Wotring said, consists of beans, rice,
quinoa and fire-roasted eggplant — which “adds a charred flavor to the burger.”

Julian Menaged, also a chef at the Crest, said mushrooms make a suitable meat substitute because
of their hearty, earthy flavor and similar texture.

Sliced thin and seared in a very hot skillet, mushrooms become crispy and smoky, and can
substitute for bacon bits, he said.

Going meatless, Wotring said, often means rethinking the notion that protein equals meat.

The grain quinoa, for example, has about 14 grams of protein per serving, making it an excellent
meat substitute to use with rice in a dish such as stuffed peppers, he said.

Meatless eating, Menaged added, means learning to enjoy the beauty and flavors of vegetables by
themselves.

The chefs agreed that potatoes, pasta and beans are easy go-to ingredients when planning
meatless meals.

They created the recipes below for dishes that are family-friendly and simple to prepare.


labraham@dispatch.com



@DispatchKitchen