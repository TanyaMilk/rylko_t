Journey to the frontier of food, and you’ll find a 3-D printer spewing out chocolate.

Although traditional cooking isn’t going anywhere, you can count on 3-D-printed foods eventually
finding a place in our world.

Researchers around the world are fiddling with ways to use 3-D printers to make food. Their
efforts could one day aid nutrition and sustainability. Most of the work so far is in printing
sugar and chocolate. But the growing momentum and early creations hint at something that will
change the way we eat.

“I don’t see this as a novelty. I see it as something that really will become a part of the
culinary fabric for years to come,” said Liz von Hasseln, creative director of the Sugar Lab at 3D
Systems.

Here are five ways that the precision of 3-D printers can be used to make foods:

For those who want their special day to be especially unique, 3-D printing is here to help. Why
have the same old plastic figurine of a bride and groom on your cake when you could have one 3-D
printed that is an exact replica of the couple?

Senior citizens with chewing or swallowing problems are often forced to eat foods in puree
form.

“Those blobs of puree that they get on a plate don’t look very appetizing, and, as a result,
these people who already have problems eating don’t eat enough because it doesn’t look very
attractive,” said Kjeld van Bommel, a research scientist at the Netherlands Organization for
Applied Scientific Research.

“They get malnourished in certain cases — which then leads to all sorts of medical
conditions."

Van Bommel and other researchers have begun to mash up peas, carrots and broccoli; and 3-D-print
them. They are softer but hold their shape thanks to a jelling agent. The 3-D-printed vegetables
are being served at retirement homes in Germany.

The precision 3-D printing allows could deliver exact dosages of vitamins or drugs.

“We can see a time when you might be wearing technology that would be sensing what your body
needs at any given time, whether you’re an athlete or whether you have a medical condition or
whether you’re elderly,” von Hasseln said. “And that could theoretically link up to your printer at
home. When you get home, a specialized meal could be waiting for you that provides exactly what
your body needs.”

Van Bommel is studying whether alternative protein sources from algae and insects could be
transformed into interesting foods with a texture people will like.

“If Western consumption levels of meat would apply to the whole world, we would have a huge
problem,” he said. “We would not be able to have so many cows. Where would you stick all these
cows, and what grass would they eat?”

It’s possible to 3-D-print a sugar lattice that a mixologist inserts into the glass. The
cocktail ingredients are chosen with respect to the effect of the sugar, which melts into the
drink.

“It adds to the kind of performance that mixologists are interested in — that pomp of serving a
custom cocktail,” von Hasseln said.

Her company will begin selling a 3-D printer for food this year. With a price tag of about
$20,000, it is expected to appeal to culinary professionals, not the typical consumer.

3-D Systems also plans to open a custom bakery in Los Angeles in the summer to serve as a
showroom and an event space to educate visitors about 3-D-printed food.

Von Hasseln expects that one day we’ll print other edibles, too.