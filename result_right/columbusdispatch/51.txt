The Grandview Yard development is adding a novelty to its urban mix of offices, restaurants and
apartments: single-family homes with garages, yards and sidewalks.

The Grandview Heights Planning Commission this week approved a plan to build 20 detached homes
and 25 townhouses on the western part of the development, west of Bobcat Avenue between Burr and
Williams avenues.

“We feel confident we can sell single-family homes quickly there,” said Michael Amicon, a
developer at Wagenbrenner Development, which plans to purchase the site from Grandview Yard’s
developer, Nationwide Realty Investors.

The detached homes will be two stories plusan unfinished attic space and will range from 2,300
square feet to about 3,500 square feet, including a finished basement, Amicon said. Most will have
three bedrooms, with options to finish the third floor for an additional bedroom.

The homes will include 10-foot ceilings on first floors, 9-foot ceilings in upper floors and
basements, open floor plans and walk-in closets.

Wagenbrenner Development President Mark Wagenbrenner said he expects prices to start above
$400,000, although prices have not been set.

The 20 detached homes will include detached garages and alleys behind the homes in keeping with
traditional Grandview Heights development, Amicon said.

He said the homes will be similar to ones Wagenbrenner built in Harrison Park, an urban infill
development in the Harrison West neighborhood.

Grandview Heights city administrator Patrik Bowman praised the plan for the homes.

“We like the idea of residential along the edge of the Grandview Yard community,” he said. “We’l
l come out of an existing residential neighborhood and go into new (residential), then get into the
more commercial aspects in Grandview Yard, so I think it transitions real well.”

Bowman said he believes the Grandview school district can accommodate whatever children the
homes attract.

The Grandview Heights City Council will take up the plan at its Monday meeting, Bowman said.

All the homes, including the detached ones, will be condominiums. Amicon and Wagenbrenner said
they expect the homes to appeal to a wide range of buyers, from young couples with families to
empty nesters.

“I think it’s going to be across the board,” Amicon said. “There are people in Grandview who
have lived there a long time who want the luxury of staying in Grandview in a new product and don’t
want to worry about shoveling snow and mowing a lawn.”

The development will offer a rare chance to buy a new home close to Downtown and in one of
central Ohio’s hottest real-estate markets.

“I think demand will be crazy, frankly,” said Sarah Eagleson, a Keller Williams Classic
Properties real-estate agent who lives and sells homes in Grandview Heights. “I definitely think
there’s an appetite for it.”

The median sales price of a Grandview Heights home rose 21 percent last year, four times the
pace of central Ohio overall, according to the Columbus Realtors trade association.

Amicon said construction is expected to begin in late spring or early summer. The first homes
should be ready for occupancy by the end of the year, he said.

“We are hoping to build more but we haven’t started conversations with NRI (Nationwide Realty
Investors),” he said.


jweiker@dispatch.com