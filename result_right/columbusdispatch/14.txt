GREENVILLE, S.C. – Here in the most conservative corner of a conservative state, Ohio Gov. John
Kasich was bound to get some pushback.

He is, after all, a Republican governor who chose to expand Medicaid. He doesn’t necessarily buy
all the criticism of Common Core. And he’s a little less likely to take a rhetorical swing at
President Barack Obama than some of his potential GOP presidential counterparts.

So it wasn’t too surprising Saturday that those attending the Greenville County Republican
Convention returned to their cars only to find a half-page yellow flier tucked under their
windshield wipers echoing some of the same complaints Ohio Tea Party members have tossed at
Kasich.

“Why is Governor John Kasich Pushing Obama’s Agenda?” the flier read.

“I was kind of delighted to see it,” said Kasich, who insists that he doesn’t “come to these
places to just serve red meat,” but rather, to offer solutions.

Kasich, who spoke at the convention just after former Pennsylvania senator and possible GOP
contender Rick Santorum Saturday, delivered essentially the same speech he gave elsewhere in the
state Friday, talking about his background, his time in Congress and his time as governor, but the
same Republicans who greeted him warmly the night before were just a tiny bit cooler Saturday.

That was, in part, because of the venue. The convention was a large gathering, and Kasich had no
opportunity to field questions from the crowd. Near the entrance, volunteers handed out literature
on Texas Gov. Rick Perry, Santorum and Sen. Ted Cruz of Texas, respectively.

But some admitted that he might have opinions that were a bit too moderate for their taste.

James Lee, a Reynoldsburg native who now lives in Greenville, S.C., acknowledged it might be “
difficult for him amongst conservatives,” and that both the Medicaid expansion and Kasich’s embrace
of “the old notion of the compassionate conservative” will likely hurt him. The latter, Lee said,
has become “a shortcut for someone who’s a moderate who wants to see more government and that sort
of thing.”

“People are listening for that 30 second sound bite, and if first few things they hear are, ‘I’m
going to grow the government and take the Medicaid funds,’ that’s almost a non-starter,” Lee
said.

But Matt Moore, chairman of the South Carolina Republican Party, said at this point, the primary
is “wide open.”

“I’m not sure most people here are familiar with the specifics of his record,” he said. “I think
people are mostly interested in looking a candidate in the eye, looking at his heart, are they
genuine. Certainly I think anyone who meets Gov. Kasich thinks he’s genuine. His message is not the
same as other candidates. It’s a little bit different stylistically and in tone, and maybe that
helps him here.”

The South Carolina stop was the first of the day; Kasich will be in New Hampshire this afternoon
for a gathering of possible GOP presidential hopefuls.

He’ll appear tomorrow on NBC’s “Meet the Press.”