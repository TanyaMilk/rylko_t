Spring is here, and greenery is emerging in some unexpected places — such as on children’s
menus.

More central Ohio restaurants are offering some surprisingly healthful choices for young
ones:

Broccoli, asparagus and salmon marketed to children? What in the name of fried chicken fingers
is going on?

“Believe it or not, a lot of kids eat grilled salmon,” said David Moeser, general manager of the
Cap City Diner near Grandview Heights, which offers the fish with buttermilk-chive mashed potatoes
and buttered corn. “We sell a decent amount of that.”

This isn’t a revolution, as the trend toward more healthful options for children has been
ongoing for perhaps a decade or more.

But it is definitely an evolution, as more restaurant owners see the wisdom of appealing to the
modern parent who has been raised with more awareness of nutrition and where their food comes
from.

“The generation that is having young children right now is thinking a lot more about what they’r
e feeding their kids,” said Acre owner Todd Mills, 34, who counts himself in that group, as he and
wife Genevieve are expecting their first child in June.

“We’re seeing the consequences of poor eating: obesity and early-onset diabetes. So the flag has
been raised.”

To that end, Mills’ children’s menu contains none of the old standbys such as macaroni and
cheese or pizza. Instead, cheese quesadillas and grilled chicken are found along with apple slices
and fruit smoothies.

Mills said he sought to strike a balance between offering more whole, unprocessed foods and to
have the choices represent “an approachable selection that wouldn’t turn a kid off.”

Acre calls itself a “farm-to-table” restaurant, so the healthful offerings might not be a
surprise. But other, more-traditional establishments are greening up their children’s menus — and
finding that the good stuff actually sells.

At Columbus Brewing Company, general manager Jennifer Kessel-White said their best-selling kids’
entree is grilled chicken.

“That’s funny, because (the most popular) used to be a cheeseburger,” Kessel-White said. “It has
definitely turned around. We get a lot of guests who thank us for (the healthful options). They’ll
say, ‘Oh, wow, they can get a salad.'"

Dr. Teresa Holt, the associate director of family medicine at Grant Medical Center, said more
parents are asking her for advice on healthful choices in their kids’ diets. She has noticed that
restaurant offerings are improving.

“Now you see more steak and fish — definitely better protein options in the sit-down restaurant
arena,” Holt said.

But even though parents can lead a kid to salmon, that doesn’t mean they can make him eat
it.

Moeser, the Cap City Diner manager, said that even though some of their healthful options sell
well, they still offer chicken tenders, hot dogs and hamburgers because “That’s what parents can
get their children to eat.

“At home, you can force a little more down them, but when they’re in public, they want less of a
scene and to enjoy their time out, so they say, ‘OK, let them have the hot dog if that means they’l
l be good.'"

Jessica Kittrell co-owns the 101 Beer Kitchens with husband Thad. They have four children, ages
1 to 8, and she recognizes the truth to what Moeser said.

“When you go out to eat, it’s a splurge — so if the kids want french fries, have the french
fries,” she said. “With our kids, if there are two choices for sides, we tell them they can pick
one. Then we pick one, like a fruit or vegetable, to keep it more balanced.”

Sharon Maerten-Moore understands that as well. The East Side resident and husband Karhlton Moore
also have four children, ages 3 to 12.

All of them have different tastes, she said.

“My 3-year-old, Cameron, is my french-fry kid,” she said. “That’s all he will eat. It doesn’t
matter what the options are, I don’t even have to get him a kids menu.”



kgordon@dispatch.com



@kgdispatch