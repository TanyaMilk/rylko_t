CLEVELAND — The Browns are the only NFL team without a logo on their helmets.

Now, they're also the only ones with their nickname written down the sides of their pants.

Cleveland unveiled its new uniforms, featuring nine color combinations, last night before
thousands of fans who seemed to like the team's new look following months of speculation and
secrecy.

After nearly an hour of buildup during a program honoring the team's past, nine current players
walked onto the stage wearing large orange sideline jackets to hide the new, Nike-designed
uniforms. When the orange, brown and white uniforms were finally revealed, Browns owners Jimmy and
Dee Haslam smiled and 3,000 fans applauded with approval.

The uniforms include “BROWNS” written on the pants, and “CLEVELAND” emblazoned across the front
of the jersey. Also, the words “Dawg Pound” — a nod to the team's rowdy fans — are stitched on the
inside of the jersey's collar.

“I like having Cleveland on the front because that means everything to the team,” Pro Bowl
tackle Joe Thomas said.

The Browns will be able to mix and match colors, allowing them to wear brown jerseys with brown
pants, brown jerseys and orange pants and a traditional white and white combination — Cleveland's
most recognized look — among others.

Thomas wasn't crazy about the all-orange ensemble.

“I don't think I look good as a traffic cone,” he said. “I think I look better as all brown or
all white or various combinations.”

Team president Alec Scheiner believes the Browns' new uniforms make the team unique.

“We could be like Oregon of the NFL,” Scheiner said before the uniform reveal, which included
the players posing for photographs and doing interviews on an orange carpet.

Scheiner said the team consulted with fans during the long process to come up with the Browns'
new look. He said it was important to be true to the team's rich history while also trying to look
toward the future.

“We listened to our fans, and with respect to the logo and the uniforms, we kind of got
permission from our fans on how far we can go,” he said. “It's a nice combination of a link to our
history and then kind of moving the team forward and matching the city's transformation.”

Scheiner knows not everyone will like the changes.

“The fans will tell us,” he said. “We know that. We've learned that for good or for bad. We're
hopeful. We're always hopeful. We work with good people and for the most part the fans dictated
what these uniforms would look like. It was important to them that we still have a link to our
tradition but matched the vibrancy of the city, and I think we did that.”

Browns linebacker Barkevious Mingo believes the new look honors the team's past.

“I went to LSU, it was traditional,” Mingo said. “I came to the Browns, traditional. But the
changes that they made here, I think you see the tradition in the jersey, but it's new and it's
different. I think different is sometimes good.”

The Browns spent two seasons on the redesign of their old uniforms, which some fans considered
bland while Browns traditionalists felt were a link to the franchise's glory days.

The team was hoping for a more positive response than they received when they updated their
orange helmet and logo. The Browns were panned for the helmet makeover, which was little more than
the team using a bolder shade of orange.

Newly signed wide receiver Dwayne Bowe, who previously played in Kansas City, said uniforms can
make a difference.

“I don't want to say it's a mind thing,” he said. “But in this age, when players look good, they
play good.”