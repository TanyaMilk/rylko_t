Revelers won’t need fancy hats and box seats at Churchill Downs to celebrate the pageantry and
tradition of the 141st running of the Kentucky Derby on Saturday.

A home Derby party featuring traditional race food and drink is easy to put together.

The social aspect of the Derby is the most significant part of the annual horse race, said Sara
Brown Meehan, director of lifestyle communication at Churchill Downs, the Louisville track that
hosts the Derby.

The race lasts about two minutes, she noted, but the celebration goes on all day.

“We encourage everybody to do the parties at home and watch it together if you can’t make it out
to the track,” Meehan said.

David Danielson, executive chef at Churchill Downs, has plenty of advice for home-party
planners.

“Unlike other sporting events where the game is the event, at the Derby the people are the event
— the participants, the food, the drink,” Danielson said. “It’s about getting together with good
friends and hopefully winning a little money.”

A Derby party, he said, should start with a 
mint julep — a
cocktail made with Kentucky bourbon, mint and simple syrup.

“A mint julep is an absolute must. We’ll sell 120,000 of them on Derby day.”

Danielson has ordered 900 pounds of fresh mint from a Louisville-area farm to garnish the
drinks.

“We have a whole team dedicated to making sure we have enough mint juleps for everybody,” he
said.

Kentucky bourbon also figures prominently in the menus he has designed to feed Derby-goers in
the suites and premier seating areas of the track. Buffet offerings will include bourbon-pickled
peaches with smoked duck breast and sweet potatoes with a maple-bourbon reduction.


>> 
See a recipe
for Derby Bourbon Shrimp


In less-exclusive seating areas, plenty of traditional Southern fare — including pulled pork,
fried chicken and shrimp — will be offered.

New Albany resident Alex Bilchak will be among the 150,000 fans heading to Louisville for the
race.

Bilchak, who works in communication for the Ohio Senate, will be making his ninth trip to the
Derby — and taking along about 250 friends. He organizes an annual bus trip from Columbus.

Barbecue, he said, is prominent at Churchill Downs and throughout the surrounding neighborhood,
where locals host parties and sometimes sell homemade sandwiches.

“There are all kinds of barbecue up and down the streets,” said Bilchak, 36.

The classic 
Kentucky Hot
Brown sandwich is another area favorite that he has enjoyed — an open-faced turkey sandwich
smothered in cheese sauce and topped with bacon and tomato slices.

Derby day, Danielson said, is the one time he doesn’t serve Hot Browns — instead offering more
elaborate food. But the dish is perfect for parties, he said, because it can be made in advance and
in large amounts.

The best choice for dessert is 
Derby-Pie, Danielson said.

Those who make it at home, though, won’t be making Derby-Pie — a trademarked name from Kern’s
Kitchen in Louisville for the company’s chocolate-walnut pie.

The pie was developed by George Kern in 1954, when he was manager of the Melrose Inn in
Prospect, Ky. The pie’s name and formula were granted a U.S. patent in 1968.

Derby-Pie from Kern’s can be ordered online (www.derbypie.com), but a similar version can be made using
pecans or walnuts, chocolate chips and — of course — Kentucky bourbon.


labraham@dispatch.com



@DispatchKitchen