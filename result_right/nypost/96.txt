There was a time when author and activist Ayaan Hirsi Ali believed it all: that, according to Islam, the infidel should die, that the Quran is infallible, that those who violated sharia law — thieves, gays, adulterers — deserved to be stoned to death or beheaded, as they were each Friday in a public gathering place she and her brother called “Chop-Chop Square.”

Today, she is that rare thing: a public intellectual who, despite death threats and charges of bigotry, calls for an end to Islam — not just as the faithful know it, but as we in the West think we know it.

“The assumption is that, in Islam, there are a few rotten apples, not the entire basket,” Ali tells The Post. “I’m saying it’s the entire basket.”

In her book, “Heretic,” Ali argues for a complete reformation of Islam, akin to the Protestant Reformation of the 16th century. Though her own education led her to reject Islam and declare herself an atheist, she believes that for the world’s 1.6 billion Muslims, there must be another way.

“If you are a child brought up to believe that Islam is a source of morality” — as she was, in Africa and Saudi Arabia — “the Muslim framework presents you with the Quran and the hijab. I don’t want to be cruel and say, ‘You grow up and you snap out of it.’ But maybe we who have snapped out of it have not done our best to appeal to those still in it,” she says.

In “Heretic,” Ali says there are three kinds of Muslims. There are the violent, the reformers, and what she believes is the largest group — those who want to practice as they see fit and live peaceably but do not challenge the Quran, the Muslim world’s treatment of women and the LGBT community, or terrorist attacks committed in the name of Islam.

Yet she refuses to label this group as moderate. She believes they have done nothing to deserve it. “I’ve never believed in the word,” Ali says. “It’s totally useless. I think we’re in a time now where we demand answers from Muslims and say, ‘Whose side are you on?’ ”

Ali argues for five amendments to the faith. “Only when these five things are recognized as inherently harmful and when they are repudiated and nullified,” she writes, “will a true Muslim reformation have been achieved.”

Those five notions are:

Rejecting these ideas, some of which date to the 7th century, is a shocking proposition to the faithful.

“The biggest obstacle to change within the Muslim world,” Ali writes, “is precisely its suppression of the sort of critical thinking I am attempting here.”

Ali has firsthand experience. In November 2004, after collaborating with the Dutch artist Theo van Gogh on the documentary “Submission” — which criticized the Muslim world’s abuse of women — van Gogh was shot to death by a Dutch-Moroccan Muslim. The assassin attempted to decapitate him and stabbed him in the chest, leaving a note affixed by the knife. It was a death threat against Ali.

She was forced into seclusion and given a 24-hour security detail. Today, she lives with her husband and young son in the United States yet remains a target.

“In no other modern religion,” Ali writes, “is dissent still a crime, punishable by death.”

She knows the greatest criticism she faces is that she is Islamophobic, that she is accusing all Muslims of adhering to jihad, to abuse, to the establishment of a caliphate.

In the book, Ali cites a 2013 report by the Pew Research Center on Muslims’ beliefs. It found that in Pakistan, 75 percent think those who leave Islam should be put to death. In Bangladesh, 43 percent think so. In Iraq, 41 percent.

Those who believe sharia is the infallible word of God: 81 percent in Pakistan, 65 percent in Bangladesh and 69 percent in Iraq.

She also cites a 2007 Pew study that found that among 18- to 29-year-old American Muslims, 7 percent had favorable opinions of al Qaeda, and they were twice as likely as older Muslims to believe suicide bombings in the name of their religion were warranted.

This is where Ali thinks the Obama administration has failed.

President Obama “has acknowledged Islamophobia, which is the worst thing you can do for Muslims who are trying to turn things around,” she says. Whether it’s ISIS or al Qaeda or the Taliban or so-called lone wolves — such as the Boston Marathon bombers or the Charlie Hebdo attackers or the suicide bomber who blew up 15 Christians in Pakistan last week or the ISIS suicide bombing that left 137 fellow Muslims dead — when these people say they are killing in the name of true Islam, Ali says, believe them.

She accepts that Obama’s administration is attempting a delicate balance — that to declare war on Islam is exactly what these fighters want — but says more can be done.

“Obama is saying, ‘Listen, Muslims, I’m on your side. I respect your beliefs, and I’d like you to help me fight these attacks committed in the name of your religion,’ ” Ali says. “He’s delivering, and they’re not.”

Western Europe, she says, is turning away from the threat of self-segregating Islamic immigrants at its grave peril. A 2009 study by the think tank Citivas found 85 operational sharia courts in Great Britain alone.

“I think with the Arab world, the West thinks we’re fighting an inferior enemy,” Ali says. “Look at the language we use: It’s jihad, it’s insurgency, it’s asymmetric.” Ali thinks the West, and the US especially, should look to the lessons of the Cold War and recognize we are waging a battle of ideas — that in 17 Muslim majority nations, the state religion is Islam.

“We did not say the Soviet system was morally equivalent to ours; nor did we proclaim that Soviet communism was an ideology of peace,” Ali writes. “In much the same way, we need to recognize that this is an ideological conflict that will not be won until the concept of jihad itself has been decommissioned.”

The greatest obstacle to an Islamic reformation is the diffuse nature of the religion itself. Unlike Catholicism, there is no leader, no papal equivalent to endorse or denounce jihad. In fact, there is no hierarchy of any kind, and any man who wishes can declare himself an imam.

Meanwhile, groups such as ISIS, al Qaeda and the Taliban are successful precisely because they have top-down leadership, codified warfare and an explicit, simple goal. “These groups are adapting to modern technology, to modern innovations in organization and management,” Ali says. “They know that without a hierarchy, human beings understand nothing.”

She is gratified by the stance taken by Sam Harris, a prominent American neuroscientist and author of “The End of Faith.”

“Sam realizes that among religions, Islam is unique in its atrocity, that everything we said about [violence in] Christianity and Judaism was hundreds of years ago. He calls Islam ‘the mother lode of bad ideas,’ which is extremely brave,” she says.

With “Heretic,” Ali is calling on those Muslims who reject jihad, acts of terror, and the subjugation of women and infidels to organize, to challenge, to speak out loudly and often against violence committed in the name of Allah — and she is calling on the West to actively demand it.

“This is a transformation of the West as we know it,” she says. “We’re at the beginning, and what we do right now is going to be consequential.”