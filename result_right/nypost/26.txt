Gun-loving conservatives willing to pony up $50,000 for a fund-raiser can fire machine guns and other weapons with the ex-Navy SEAL who says he took out Osama bin Laden.

Top contributors to the ForAmerica organization were invited to fire weapons and otherwise hang out this June with Robert O’Neill, who was credited with nailing the al Qaeda chief in his Abbottabad, Pakistan, hideout in May 2011, The Washington Post reported.

The event at the posh Amangani Resort Hotel in Jackson Hole, Wyo., is billed as “a historic weekend honoring Navy SEAL hero Robert O’Neill.”

“Our event will consist of clay, pistol and ‘machine gun fun’ competitions with Rob O’Neill,” according to invitations.

O’Neill left the Navy in 2012 after hundreds of combat missions, including the SEAL Team 6 raid that killed bin Laden, and recently founded a charity to help vets transition to civilian life.