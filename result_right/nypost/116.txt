Trapped in a marriage where the sex was routine, freelance journalist Robin Rinaldi, now 50, embarked on a 12-month experiment in which she lived apart from her husband during the week and took lovers. As she publishes her memoir, “The Wild Oats Project,” on Tuesday, she talks to The Post’s Jane Ridley about her erotic journey.


Pulling on his pants after our intimate encounter in my Las Vegas hotel room, the cute 23-year-old I’d just picked up holds out his cellphone, urging me to tap in my number.

“You really don’t have to take it,” I say.

Having sex with a stranger is thrilling, but I’m not that interested in a repeat performance.

Two minutes after he’s gone, I climb back into bed and text my husband, Scott, whom I’ve been with for 18 years. “Just saying good night,” I type. “Good night, dove,” writes back Scott from wherever he is.

Scenarios like these were typical during my year of living dangerously — the crazy 12 months in 2008 and 2009 I jokingly call my “Wild Oats Project,” when Scott and I had an open marriage.

Stuck in a rut — our once-a-week sex life was loving, but lacked spontaneity and passion — I was craving seduction and sexual abandon. I was having a midlife crisis and chasing this profound, deeply rooted experience of being female.

Before then, starting a family had felt like one route to this elusive state of feminine fulfillment. But Scott had made it absolutely clear he never wanted a baby, and even had a vasectomy.

Many people will find this hard to understand, but, as the door to motherhood closed, I found myself rushing towards this whole other outlet of heightened female experience — taking lovers.

I’d always been “the good girl,” and had slept with only three guys before getting involved with Scott at the age of 26. I was pretty conservative.

Sexually, I was experiencing what happens to a lot of women in their late 30s and early 40s. I was approaching my sexual peak and was relaxing into myself.

I broke the news to Scott that I wanted an open marriage in early 2008, a few months after his vasectomy. “I won’t go to my grave with no children and four lovers,” I told him repeatedly. “I refuse.”

Against the idea at first, he eventually relented. According to our deal, I’d rent a studio apartment during the week and come back to our home on weekends. Both of us could sleep with whomever we chose as long as we used protection. It was a case of “don’t ask, don’t tell.”

My first step was placing an ad on nerve.com, a kind of intellectual version of Craigslist’s Casual Encounters. Under the heading: “Good girl seeks experience,” it read: “I’m a 44-year-old professional, educated, attractive woman in an open marriage, seeking single men age 35-50 to help me explore my sexuality. You must be trustworthy, smart, and skilled at conversation as well as in bed.”

I added: “Our time together will be limited to three dates as I cannot become seriously involved.”

Within 24 hours, my inbox offered up 23 prospective suitors.

The first lover I met through nerve.com was a 40-something lawyer called Jonathan*. Slim, handsome with glasses and a stylish haircut, he suggested we kiss to test our sexual chemistry. “There’s a lot of heat there,” he said.

On our second date, the following week, he came to my studio after work with a cooler of snacks and some wine. We stumbled to the bed, where he turned me onto my hands and knees and took me from behind.

We had intercourse twice and, after he left, I felt satiated.

Around the same time, I took workshops at OneTaste, a sexual-education center, which has branches in New York and San Francisco, where I lived at the time. A sort of “sex-friendly” yoga retreat, it taught me something called orgasmic meditation, which is centered on the woman.

OneTaste was the place where I selected most of my lovers, although I picked up a couple of guys, like the 23-year-old in Vegas, on business trips. OneTaste was populated by cool, open-minded San Franciscans who wanted to expand their horizons.

They included an astrologer named Jude, 12 years my junior. The moment I saw him, I was irresistibly drawn in.

Slightly built and neo-hippy, he was spiritual, calm and centered. I was an Italian, meat-eating, busy magazine editor. But we had a real connection. I became infatuated with him, but the sex soon fizzled.

And then there was Alden, a writer, in his late 30s, who answered my nerve.com post.

“So your ad said only three dates,” he said, as we ate dinner in a crowded restaurant. “Yes,” I replied. Without missing a beat, he reached over and lightly took my fingertips in his. “Do you think we’ll be able to do that, to limit it?”

I loved our conversation, the fact he was a writer, the books he read. Things in the bedroom were mind-blowing and, before I knew it, I was hooked. But I’d made a pledge to my husband that I wouldn’t get involved with any of my lovers. I stuck to that.

And so the year went on. I had lots of “firsts,” including being intimate with women.

But the lessons I learned weren’t purely physical. They were about growing up, making mistakes, learning to live without so much fear, owning up to my dark side and, eventually, finding out the difference between being a “good girl” and a good person.

On weekends, I’d go back to Scott. It wasn’t as strange as you might imagine. I liked it. It was the perfect balance, living on my own during the week and then returning home.

We knew we were both sleeping with other people, but we kept to the rules and never spoke about it. We had sex as always and the open marriage spiced things up — at least at first.

But, by the end of the 12-month project, moving back home full time proved more difficult than I had thought. After you open up a marriage and experience a whole range of sexual variety and aspects of yourself you’ve never had before, it’s hard to put everything back in the box.

You’re changed.

I slept with a total of 12 people (including two women) during the Wild Oats Project.

Suddenly I found an updated version of myself. The person I was at 44 was so much different than the woman I’d been when I was last single at 26. She was less shy, more confident, wilder.

Meanwhile, it turned out that, for around six months, Scott had been exclusively sleeping with one woman, a lot younger than me. That bothered me, especially as they hadn’t been using condoms. But it wasn’t the catalyst for the end of the marriage, because he broke things off with her.

The turning point was hearing from Alden. He sent me an email, out of the blue, several months after the project had come to an end.

Before long, we were having sex again. Being with him was exquisite. After reconnecting with Alden and falling deeply in love with him, there was no going back.

Five years on, Alden and I are happily living together. It’s a regular, monogamous relationship. I’m grateful I experienced my marriage to Scott (who has since found a new partner) but now, for this part of my life, I believe being with someone who is the most temperamentally like me is where I can learn more.

As for not having children, I’m at peace with that, too.

First I channeled the creativity I would have used to become a mom into my sexuality, and then I channeled it into writing my memoir. As my story shows, there are many different ways in life to find passion and fulfillment.

* All of Robin’s lovers’ names have been changed.