If there’s anyone coming out on top right now in Manhattan’s luxury real estate market, it’s the guy in the middle. Specifically, the lower-priced buyer subset that some refer to as “mid-market luxury,” though others use the oxymoronic “affordable luxury” classification.

No matter the term, this type of property is now emerging across prime Manhattan neighborhoods.

In simple terms, “affordable luxury” refers to top-tier property beginning at around $1,400 or $1,500 per square foot and going to $2,500 per square foot, experts say. This figure marks the entry level of Manhattan’s high-end market — a pricing arena that was previously underserved, if not overlooked, by developers. Now, however, the segment is flourishing.

Want proof? Corcoran Group’s Tamir Shemesh next week launches sales of the 22-unit, Peter Poon-designed Three Thirty Seven East Sixty Second ground-up condominium on East 62nd Street, a project boasting floor-to-ceiling windows and outdoor spaces, with one-bedrooms starting at $695,000 and going up to $3.15 million for a roomy duplex “townhome.”

There’s also HFZ Capital Group’s Fifty Third and Eighth — a 252-unit conversion at 301 W. 53rd St. in Hell’s Kitchen that launched sales in January — with prices from $1.13 million for a 667-square-foot one-bedroom to $1.87 million for a 1,040-square-foot three-bedroom. Apartments have scratch-resistant marble floors in the bathrooms, and quartz counters in the kitchen.

While these prices are certainly high for many buyers, they actually represent an evolution in the luxury arena — according to the latest Douglas Elliman data, the average luxury market sales price is a whopping $7.4 million.

Traditionally, this top end of the market has served only the wealthiest buyers. But developers are finally responding to consumers’ demand for sleek pads at better prices. And they primarily overcome land cost issues these days by converting existing properties; ultimately “that’s what makes [units] more affordable,” says Miller Samuel President and CEO Jonathan Miller.

“Because we didn’t have to buy the land and we didn’t have to build from the ground up, we’re able to offer prices that don’t compare to other ground-up developments in the neighborhood,” says Sherry Tobak of Related Sales, who’s overseeing deals at Carnegie Park, a 325-unit conversion at 200 E. 94th St. Prices here go from $695,000 for a 510-square-foot studio to $2.79 million for a 1,795-square-foot three-bedroom. Robert A.M. Stern Architects, of 15 CPW fame, redesigned the lobby and amenity spaces.

But lower cost does not necessarily mean lower quality.

“Everybody thinks of luxury as something they cannot afford — it’s beyond their reach,” says Shemesh. “The idea is the product is built like any top-price luxury building, but the prices can be accessed by a wider market.”

Apartments in this segment of the market aren’t exactly available for a song, but developers says they’ve found a welcome buyer base frustrated with a lack of appropriate opportunities.

“We don’t want to lose them to another city,” says developer Aby Rosen of buyers in the mid-market range. “You need to create some places for them … where the luxury is different.”

Like many of his fellow developers, “different” luxury doesn’t mean lower quality for Rosen.

His firm, RFR Holding, is handling the conversion at 300 E. 64th St., a 103-unit property which last month debuted Stonehill Taylor-designed amenity spaces including a landscaped roof terrace, media room, library and gym.

Units here — which hit the market last spring — span $725,000 for a nearly 535-square-foot studio to $2.85 million for a 1,431-square-foot three-bedroom.

They come with floor-to-ceiling windows, marble bathroom details and Bosch appliances.

The main difference here — and this is generally the case with the mid-luxury market — is that units are smaller, and this helps make them less expensive.

Compare them to the uber-luxury One57, where a $4.95 million one-bedroom measures 1,021 square feet, or a two-bedroom at one of the city’s priciest new buildings, 432 Park, asking $16.95 million for 3,575 square feet.

“If we don’t have bigger apartments, we use the amenity spaces similar to those of a hotel,” says Rosen of the robust selection of offerings at 300 E. 64th St. that give residents more space for activity.

“I’m paying less, but I can hang out.”

It’s similar at Tessler Developments’ ground-up 172 Madison in NoMad, whose amenity package is also generous, even though most apartments will inhabit the mid-price range. There will be a 67-foot pool, Jacuzzi with waterfall, gym and an outdoor plaza.

Units, which start at roughly 900 square feet, will also get the luxe treatment: Miele appliances, marbled walls in the master bathrooms, 11-foot ceilings and floor-to-ceiling windows. Pricing will start at $1.2 million, or $1,300 per square foot, for a one-bedroom here. Occupancy here is expected in late 2016.

And let’s not forget views! Take, for instance, the 125-unit 325 LEX, which looks out to the Empire State and Chrysler Buildings, where units currently range from $835,000 for a studio to $8.5 million for a three-bedroom penthouse.

“When you look at ‘affordable luxury,’ sometimes you don’t think you’re going to get those premium views in a building,” says Jodi Stasse of Corcoran Sunshine, which is leading sales and marketing at 325 LEX.

The luxe finishes and feel — and reasonable prices — brought 29-year-old Lauren Walsh to sign an $835,000 contract last month for a one-bedroom at Carnegie Park. Walsh, a first-time buyer who works at an investment management firm, will move into her 705-square-foot digs by the end of the year. The unit has hardwood floors, marble counters in the bathroom and all-new appliances, she says.

For Walsh, the apartment was a wise investment primarily for its Upper East Side location, where she sees property values increasing.

“It’s really good for first-time buyers [because] we’re in the early stages of building our wealth,” she says of Carnegie Park.

Another prominent conversion is the CetraRuddy-designed 135 W. 52nd St. — which is bringing lower-priced luxury to the heart of Midtown Manhattan’s new “billionaires’ row.” Here, asking prices average $2,148 per square foot, according to StreetEasy — significantly below the $2,747 luxury market average tallied by Douglas Elliman.

“We’ve seen One57, 432 Park, the Baccarat — we see a lot of buildings like those at a much higher price-per-square-foot,” says Douglas Elliman’s Stacy Spielman, who’s leading sales and marketing here.

“There is an excess of those … [135 W. 52nd St.] speaks to someone who wants to be in Midtown and understands that the area is transforming into a residential area that’s attainable.”