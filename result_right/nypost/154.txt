His name says it all: Sumluv Robert Deniro Johnson was bred to be a star.

His drama-queen mother is Susan Lucci. His high-jumping dad: Magic Johnson. Deniro himself is a grand champion at the art and sport of being a Chinese shar-pei.

Why Deniro?

“He’s a ham,” owner Maria Johnson of Newark, New Jersey, said Monday as Deniro stared down Westminster Kennel Club dog show visitors as though auditioning them for a movie of his own: “Pet This.”

With mash-up monikers and more initials than a doctor’s office diploma wall, it’s no wonder show dogs leave some spectators asking just what’s in a name.

But the fanciful-sounding handles aren’t just for show. They reflect dogs’ credentials, pedigrees, and characteristics, plus breeders’ systems for distinguishing one litter of puppies from another.

Sometimes, they’re just plain pun.

“Naming the puppies is one of the most enjoyable parts of having a litter. We have a lot of fun with it,” says Dalmatian breeder Mary-Lynn Jensen of Windermere, Florida. Her latest pups are getting names related to the HBO drama “Game of Thrones.” Previous litters were named after Champagne appellations, beers, Disney characters, novelist Janet Evanovich’s Stephanie Plum mystery series and phrases that include “ain’t” or “mind.”

Perhaps the most irresistible: Woodwynd’s Eternal Sunshine of the Spotted Mind, aka Shiner.

A primer: Show dogs and many other purebreds have formal “registered names” that go in breed records. Whether chosen by breeders or owners, registered names generally begin with the breeder’s kennel, such as Sumluv in Deniro’s case or Woodwynd in Shiner’s.

Names might include other kennels or dogs from the pup’s pedigree. Registries may add a number or an owner’s name if needed to differentiate one Rover from another. Abbreviations before and after dogs’ names denote their accomplishments, such as “GCH” for grand champion in traditional breed judging, “MACH” for agility master champion or “OM” for obedience master.

Then the dogs have everyday “call names.” They’re generally chosen by owners and may or may not relate to registered names. Adding another layer, breeders might use temporary “puppy names” before the dogs go to their owners’ homes.

Names can function as a quick-reference code for recalling a dog’s lineage or litter. Many breeders name litters in alphabetical order — all “A” names one year, “B” the next, and so on.

Others like to get a little more creative.

Dawin Hearts on Fire, the standard poodle called Flame that won the non-sporting group Monday night, was named partly for mom Dawin Spitfire and partly for being born on Valentine’s Day 2011, handler Sarah Perchick said.

Toy group winner Hallmark Jolei Rocket Power — Rocket to his friends — got the “Power” from dad Hallmark Jolei Austin Powers and the “Rocket” from his own abundant energy, said Diane Erhicht, wife of handler Luke Erhicht.

Generations of Linda Albert’s chow chows have “man” in their names, including 2010 Westminster chow chow breed winner E-Lin Traveling Man (his owners both worked for a travel agency), E-Lin Music Man (for a musical toy he liked), and E-Lin Magic Man (because after his mother’s rocky pregnancy, “he appeared like magic,” says Albert, of Hauppauge, New York).

Former fashion buyer Justine Romano names all her cotons de Tulear for designers. One of them, Mi-Toi’s Burberry At Justincredible, won the breed as cotons debuted Monday at Westminster, besting a 14-dog field that also included Romano’s Just Incredible Chanel.

Now a breeder, Romano even tries to wear a dog’s namesake designer when showing it. “They deserve it,” says Romano, of Montville, New Jersey.

Of course, breeders aren’t the only people who put thought into naming lots of dogs. American Society for the Prevention of Cruelty to Animals staffers consider an animal’s former home and behavior and even consult baby-name books to find something relatable for the roughly 3,800 dogs and cats that come into organization’s New York shelter each year, Senior Vice President Gail Buchwald said.

“We want to make sure prospective adopters can really envision the pet as part of their family” to help forge a lasting match, she said.

For the family pet, experts recommend keeping names to one or two syllables and avoiding words that resemble a household member’s name or a canine command (sorry, “Fletch!”).

Think of a name as just “a sound which catches attention … Really, dogs read body language, mostly,” says Martin Deeley, a dog trainer and the executive director of the International Association of Canine Professionals.

Plenty of owners do keep it simple. The name most often registered with the American Kennel Club, spokeswoman Gina DiNardo says, is Spot.