A Wisconsin church is trying to save its bacon.

St. Patrick Catholic Parish in Stephensville, Wis., is canceling its annual parishioners vs. pigs mud-wrestling fundraiser after critics launched an online petition campaign against the event that garnered more than 81,000 signatures.

According to Christian Today, the 44-year tradition — dubbed “Original Pig Rassle” — is being scrapped in exchange for a human mud foosball tournament.

The rassle was part of the church’s annual fundraiser, called the Roundup. This year, the Roundup is planned for Aug. 8 and 9 and will include bands, a parish dinner, a parade and the human football event.

“We are simply doing something different this year … we are simply moving in a different direction,” Deacon Ken Bilgrien told the website.

He refused to discuss the pig wrestling controversy.

The church posted a message on its website:

“After much prayer and many hours of discussion, we realize that what we had for 44 years in the Original Pig Rassle was memorable, legal and great family fun. We also realize that our parish and diocesan talents could be better spent in areas that are less controversial. It is with great regret that we have discontinued the Original Pig Rassle. We are, however, very excited to begin this new tradition at St. Patrick Parish.”

Last year, teams wrestled 37 pigs that were slaughtered after the event, the website reported, citing the Global Conservation Group, an animal advocacy organization based in Wisconsin.

Some pigs were “punched in the face, kicked, body-slammed, jumped on, yelled at and thrown into a bucket,” the group said.

The group accused the church of breaking a state law that says humans cannot fight animals.

This article originally appeared on Fox News.