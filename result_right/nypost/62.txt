The University of Michigan has cancelled an upcoming screening of the Iraq war drama “American Sniper” after hundreds of students blasted the movie’s subject matter — claiming it promotes “negative and misleading stereotypes” against Muslims and “sympathizes with a mass killer.”

The college’s Center for Campus Involvement officially slammed the brakes on the April 10 showing of the film on Tuesday after a sophomore named Lamees Mekkaoui started a petition earlier that morning when she first heard about the scheduled event.

Students will now instead be forced to watch “Paddington” — a PG-rated movie that centers around the misadventures of a stuffed bear.

Identifying herself as Arab and Middle Eastern, Mekkaoui reached out to others across the Ann Arbor campus and asked them to join her cause and ultimately help stop the screening of a film that “condones a lot of anti-Middle Eastern and North African propaganda.”

Clint Eastwood’s “American Sniper” was an Academy Award nominee for Best Picture and took home the title for top grossing film of 2014. The movie is based on the autobiography of the late Chris Kyle — a US Navy Seal who served in Iraq and became the deadliest sniper in US history.

“Chris Kyle was a racist who took a disturbing stance on murdering Iraqi civilians,” Mekkaoui’s petition charged. “Middle Eastern characters in the film are not lent an ounce of humanity and watching this movie is provocative and unsafe to MENA [Middle Eastern and North Africa] and Muslim students who are too often reminded of how little the media and world values their lives. What we instead should offer is compassion and respect towards others.”

Mekkaoui’s document garnered about 300 signatures from students and quickly got the attention of the university — who apologized in a statement posted to Twitter explaining they had cancelled the screening.

“We deeply regret causing harm to members of our community, and appreciate the thoughtful feedback provided to us by students,” the release said. “Student reactions have clearly articulated that this is neither the venue nor the time to show this movie.”

One of the biggest reasons officials most likely axed Friday’s showing of “American Sniper” may have been the concerns Mekkaoui raised about the possibility of attacks against Muslim students.

“Anti-Muslim and anti-MENA hate crimes are growing increasingly common,” her petition noted. “These incidents create an unsafe space that does not allow for positive dialogue and triggers U of M students. Examples like the recent Chapel Hill shooting, which took the lives of three Arab American Muslim students…contribute to this lack of safety and space for Muslim and/or MENA students.”

“Although we respect the right to freedom of speech, we believe that with this right comes responsibility: responsibility of action, intention, and outcome.”

But not everyone had bad things to say about the controversial blockbuster being shown on campus.

University of Michigan students involved in the local chapter of the conservative organization, Young Americans for Freedom, started their own petition urging the CCI to overturn their decision and go ahead with the scheduled showing.

“The movie ‘American Sniper’ is not about a racist mass murderer or a criminal,” the document states. “It is about a decorated American war hero who served his country valiantly. If the University prevents a movie like this from being shown, it promotes intolerance and stifles dialogue and debate on the subject…As adults at a public university, we should have the option to view this movie if we so choose and have the opportunity to engage on the topics it presents to come to our own conclusions on the subjects.”

With Post Wires