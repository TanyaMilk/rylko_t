The mother of Jihadi John knew instantly in August that he was the ISIS butcher in the James Foley execution video — but kept her mouth shut as he went on to slaughter at least five more Western hostages, a report said Monday.

“The mother recognized the voice and she screamed ‘that is my son’ while he was talking before beheading the first American hostage,” a source told the Telegraph of Ghania Emwazi, who only spoke to investigators after her son was publicly identified last week.

“When they played the video again, the father was sure it was his son.”

Since he was unmasked last week, details have continued to emerge about Mohammed Emwazi’s childhood and personal life as authorities try to piece together how he was radicalized.

On Sunday, the 21-year-old’s father, mother and eldest brother were all taken in for questioning by Kuwaiti authorities, the Guardian reports.

Emwazi’s parents live in the Kuwaiti city of Jahra. They had lived in Kuwait for some time before moving to the UK in the early 1990s. A new image taken during his time there in early 2010 shows Emwazi wearing a thick black beard and a traditional red and white Arabic headscarf. The photo is the most recent picture of him to date.

The ISIS fiend was described Monday as a “hard-working and aspirational young man” who grew up to become a top-notch salesman at an IT company in Kuwait, reports said.

“He was the best employee we ever had,” a former boss explained to the Guardian. “He was very good with people. Calm and decent. He came to our door and gave us his CV. How could someone as calm and quiet as him become like the man who we saw on the news? It’s just not logical that he could be this guy.”

The Kuwait-born college grad traveled to Britain as a small child with his family. He attended state schools in London, including Quintin Kynaston school, before moving on to study computer science at the University of Westminster. Emwazi left for Syria in 2013.

Speaking to BBC Radio, Jo Shuter, a former principal at Quintin Kynaston, described him as a stellar student who unfortunately had “adolescent issues.”

In spite of being bullied by his classmates, Emwazi showed no signs of radicalization or anything else that would lead teachers to believe he would become the man behind the mask seen in gruesome ISIS beheading videos, Shuter said.

“I can’t stress enough, he wasn’t a huge concern to us,” she told BBC. “He had some issues with being bullied, which we dealt with, and by the time he got into the sixth form, he was to all intents and purposes a hard-working, aspirational young man who went on to the university he wanted to go to.”

“I can’t even begin to say the shock and the horror that I feel,” Shuter added. “Even now when I’m listening to the news and I hear his name, I feel the skin on the back of my neck stand up because it is just so far from what I knew of him and it is so shocking and so horrendous the things that he has done.”

Once in Kuwait, Emwazi was granted a three-month probation period, earning about $737 per month plus around $122 in expenses, according to the Guardian.

He was even promised a 5 percent commission in April 2010 on business he brought in — but after the three months was up, Emwazi disappeared completely, his former boss said.

When he arrived at the IT company, the staff was shocked that a young man from London would want to find work in Kuwait, especially since he could have been earning a lot more money in the UK or Europe, the employer said.

“Muslim and Arabic people travel from here to London or the US, and they stay two years looking for a job or even a place to stay,” he told the Guardian. “It always puzzled me. Why would he come here?”

“But it seemed as though he faced some problems, maybe family, social or psychological,” the former boss added. “I didn’t really ask. He wanted a good job (in London) and he wanted to get married, but he couldn’t and it made a problem for him.”

During Emwazi’s brief stint at the IT company, he requested emergency family leave to return to the UK on two separate occasions, his boss said.

On both trips he traveled to London — but he was never seen by the staff again after leaving the second time on April 25, 2010. Soon after Emwazi returned to the UK, counterterrorism officials detained him and told him he was no longer allowed to travel to Kuwait.