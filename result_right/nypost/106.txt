These teenagers aren’t bored, they’re on the board.

Leila Eliot was just appointed by Manhattan Borough President Gale Brewer to the Lower East Side’s Community Board 3, and she’s only 16.

The ambitious high-school junior is the first to use a new state law that allows 16- and 17-year-olds to serve on the advisory, but powerful, panels that weigh in on issues such as landmarks, land use and liquor licensing.

Traditionally, those over the age of 40 dominate CBs, but the law change brought a flood of applications from politically minded youngsters, including 35 in Manhattan, 12 in The Bronx and 10 in Brooklyn.

Early this month, a nervous Eliot attended her first full CB meeting as a member, where she stressed over the inflection of her voting voice.

“I knew I was going to say ‘abstain’ no matter what, but do I say ‘abstain’ or ‘abstain’? There’s so many different ways to say one word, it’s crazy,” laughed Eliot.

But once that was out of the way, Eliot found the guts to speak out publicly in favor of term limits to help free up space for younger members.

Brewer rebuts complaints that teens are just trying to polish their college applications. “They’re looking to change the world,” she said.

A former intern for City Council member Rosie Mendez, Eliot is also co-president of the school yearbook at Manhattan’s Bard College HS, writes for the school paper and is teaching herself violin. She acknowledges she’s no expert in liquor-licensing or land-use issues but notes “there are lots of adults who don’t know much about it ­either.”

Hunter College HS senior Quentin Dupouy applied to the Upper West Side’s CB 7 last year but was rejected due to his age. His résumé is beefier than those of many adults: He is vice chair of the organization of state high-school

Democrats, was an Obama campaign worker and interned for Rep. Charles Rangel.

“Community boards are not the sexiest thing to say you’re a part of,” said Dupouy, 18, who applied to colleges within two hours of home so he can return for monthly meetings if his application is successful.

The city’s 59 boards each have 50 members — half appointed by the local City Council member and half by the borough president.

Staten Island Beep James Oddo said one interested St. Joseph Hill HS student would be “strongly considered” when a vacancy arose.