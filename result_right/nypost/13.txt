It was a beautiful day for baseball in Baltimore — too bad no fans were there to enjoy it.

The Orioles took the field at Camden Yards to play the Chicago White Sox under a cone of silence Wednesday, in Major League Baseball’s first closed-door game.

The no-fans game — won 8-2 by the hometown O’s — followed consecutive postponements due to riots in Baltimore, touched off by the death of Freddie Gray in police custody.

The game was played in a lightning -fast 2 hours and 3 minutes, as if players were trying get out of Dodge before dark.

A public address announcer told players the national anthem — an old-school orchestral recording — was about to be played. They stood at attention, and then the PA was used sparingly the rest of the day.

The saddest ball girl in the whole world. http://t.co/YiEEAvqULP— Logan Rhoades (@LoganRhoades) April 29, 2015

The public address system delivered the most unsurprising news of day, announcing that: “Today’s official paid attendance is zero.”

Former O’s great and current TV commentator Jim Palmer said he’s happy baseball is back even if no fans were allowed inside to enjoy the grand game and pitch-perfect 73-degree weather.

The game was televised in Chicago and Baltimore and streamed to baseball fans outside those markets.

“It’s a tough time, hopefully this will make a difference,” Palmer told fans tuned into the Mid-Atlantic Sports Network, televising the game in and around Baltimore.

City, Orioles and MLB officials agreed to play Wednesday’s game — but believed that having fans in the stands would divert too many police resources.

With no fans in the stands, every crack of the bat or scream of “I got it” echoed around Camden Yards, which would have been impossible to hear with 30,000-plus fans in their seats.

Orioles first baseman Chris Davis crushed a three-run homer to center field in the first inning to give his team a 4-0 lead.

The ball rattled harmless around a walkway and came to a stop outside a restroom without one person jumping on the valuable homer ball.

Later in the game, Davis caught a ball and stepped on the first base bag of the inning final out.

He strolled off the field and casually tossed the ball into the stands – a familiar scene played out at MLB parks around the nation. But this time, instead of touching off a wild scramble for the souvenir, the ball just rattled around empty seats.

Davis probably got the idea when he struck out to end the second inning when White Sox catcher Geovany Soto tossed the ball into the stands toward imaginary fans.

In the middle of the seventh, Orioles rise for the traditional stretch and tap their toes to John Denver’s “Thank God I’m a Country Boy.”

While there wasn’t a single fan in Camden Yards, the PA still blared the Denver classic to the enjoyment of 50 Orioles and White Sox players.

A handful of fans gathered outside the park’s iron gates on Camden Street and struggled to enjoy obstructed views of the action.

Their chants of “Let’s go O’s” could be faintly heard as Baltimore scored six runs in the first inning.

Some fans chanted: “Let us in, let us in!”

But Orioles fan Larry Marsh, 21, said he was OK with the move and was just happy to be there.

“I’m just going to stand here and watch as long as I can,” Marsh said.

Marsh and his brother, 28-year-old Les Bowen, came to their native Baltimore from their current home in Chincoteague, Va., in hopes of seeing a bit of the city they so fondly remember.

“We come here because sports brings different people together,” Bowen told USA Today. “It’s sad that we can’t have that today.”

Despite this day of no fans, Marsh said he’s confident Orioles supports will be back in force with the birds come back to town.

“I think people will come,” Marsh said. “It’s another way to show the city is strong.”

A few dozen guests of the Baltimore Hilton, well beyond the center field wall, took to their balconies to grab a far-away look at the game.

Despite the dour nature of Wednesday’s game, some players — most notably Orioles catcher Caleb Joseph — tried to have fun with it.

He mimicked signing autographs for fans in the front row. Then he sprinted into the outfield and threw his hands in the air to accept wild cheers, heard only in his head.

Even the TV crew got into the weird act of broadcasting a game with no fans.

O’s play-by-play man Gary Thorne hilariously went into a hushed “Masters voice” and called a play as it were golf.

Baltimore’s Adam Jones smacked a booming double to center field that the barely audible Thorne said: “Jones will whack the son-of-a-gun to center field. That’s very deep, it’s deep and it’s off the base of the wall. Adam Jones has a double, and that green jacket is well within reach, [broadcast partner] Jim [Palmer].”

The Orioles were supposed to host the Tampa Bay Rays at home this weekend and those three games were moved to St. Petersburg. The O’s will operate as the home team despite playing on the Rays’ turf.

The Rays announced that all tickets will be general admission. Fans can have the best seats in the house for $15 on Friday and $18 on Saturday and Sunday.

Additional reporting by Chris Perez