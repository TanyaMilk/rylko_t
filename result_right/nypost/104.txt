The war over my column on out-of-touch restaurant critics ended in a rout. My rivals have raised the white flag. One even said he’d take my words to heart. The Village Voice’s Zachary Feldman tweeted:

Not even The New York Times seemed mad.

What next — no more frenzy over bone broth and squid-ink bagels?

Problem is, I must raise the white flag too — because I’m only slightly less transfixed by what’s trendy and weird than my competitors.

When I wrote two weeks ago that food journos (self included) are wildly out of step with mainstream taste, even sophisticated mainstream taste, none raised a peep in his or her own publication or Web site.

But, wow, they sure let off steam on the neutral ground of Twitter. The 140-character debate coughed up an outpouring of mostly sympathetic, nuanced views, not the trash-talk I expected.

I wrote on March 8 that in chasing an “obscure repertory” of barely edible oddities like Sushi Nakazawa’s barred knifejaw, while often ignoring popular, traditional restaurants, elitist food journos “inhabit a different planet than the one where normal people eat.”

Popular blog The Braiser hit the nail on the head:

Sunita’s Masala ‏(@masalasocial), a Brooklyn Heights vegetarian Indian cooking school, was on board:

So was Joe Dobias, chef of Joe & MissesDoe on East 1st Street, who seconded my conclusion that for critics to care about a ham sandwich, it would have to be made from rare Andalusian pork, artisanal bread and Brooklyn-made ramps mustard.

Elegant Upper East Side Italian restaurant Sistina thought the crisis in media focus was so acute, it offered to host a summit meeting:

We’ve yet to take up their back-room offer.

The Times’ Pete Wells tweeted into the fray:

Or an inedible one like a squid ink bagel?

Wells took mild issue with my statement that we cater to an elitist crowd by skipping mainstream steakhouses for esoteric and/or gimmicky ones: “ . . . the [unreviewed] steakhouses you name, at those prices, also cater to the elite, if not the foodie elite.”

But Wells seemed to target more the latter crowd in his very next review — not of plain-vanilla beeferies Charlie Palmer Steak, Mastro’s, NYY Steak or Angus Club Steak.

It was of celeb-magnet Bowery Meat Company which, he noted, was trying not to be a steakhouse with dishes like Chinese barbecue-style pork belly.

Why hasn’t Wells checked out the others? For the same reason I haven’t: we’re bored by another USDA-prime cut devoid of stunts.

The new Hunt & Fish Club, which despite over-the-top decor does have a traditional menu, drew just one review. Bloomberg’s Tejal Rao trashed its “corpse-cold” porterhouse. I suggested she chose it for laugh value, just as I’d done myself when I ridiculed rodizio-style chain joint Texas de Brazil.

I mused on March 8: “Would the dining millions be better served had Rao and I written about steak joints worth their time and money?”

Rao responded:

And yet, we do choose — places either trendy or easy to howl over.

I’ll spot a point to Eater.com critic Ryan Sutton who gently chided my disdain for when reviewers over-emphasize looney experimentation:

That not surprisingly drew a torrent of “favoriting” by envelope-stretching, media-darling chefs such as Empellon Cocina’s Alex Stupak.

But it also prompted counter-tweets to Sutton’s, among them my own:

Sutton tweeted back: “Well said! The proportion of risky vs safe items is a tough balance! . . . hard to tell in advance what’s the next pork belly bun or just another frog skin taco. failure & risk are key.”

Publicist Steven Hall laughed off the funky stunts:

Not this one. But it sure gobbled up space on Eater, GrubStreet, Food & Wine and Time Out NY.

Now, I’m hardly adventure-averse. I raced to chef Dan Barber’s pop-up wastED menu at Blue Hill, featuring dishes made from “waste that occurs at every link in the food chain” — like “cured cuts of waste-fed pigs and reject carrot mustard” (which tasted lots better than it sounded).

But certain writers must think I subsist on boiled beef and potatoes. Eater.com features editor Helen Rosner (@hels) lectured, “relevance, not newness,” was cause to ignore generic restaurants “even if popular.”

And, “Food media covers weird-ass foods for the same reason fashion covers couture. Trends & art & culture rarely begin w mass appeal.”

Aha! It’s about culture. Or is it just about music? TV’s “Bizarre Foods” host/author Andrew Zimmern said my stance was “like saying discussing Sonic Youth decades back was pointless . . . Steve, if you don’t get Sonic Youth, we are done.”

To prove that “Innovation begets populism,” Zimmern proclaimed:

I responded, “And how excellent is supermarket/gas station sushi?”

Rout complete!