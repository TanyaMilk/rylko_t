President Obama brought down the house Saturday night at the annual White House Correspondents’ Dinner, aiming zingers at targets from Hillary Clinton to the Koch Brothers.

“I have a friend who used to make millions of dollars a year,’’ he said of the presumed Democratic presidential nominee. “Now she’s living out of a van in Iowa.’’

On Charles and David Koch, who are poised to raise up to a billion dollars for a GOP candidate, the president quipped: “I raised a lot of money too, but my middle name is Hussein. What’s their excuse?’’

Obama said he had a plan to keep drones from landing on the White House lawn. Then a Photoshopped picture of the vice president standing on the lawn with a baseball bat, ready to swat one out of the sky, flashed on the screen.

And Obama joked that he and Biden have “gotten so close that some places in Indiana won’t serve us pizza anymore.”

The president also took a shot at himself. Noting that Vermont Sen. Bernie Sanders is thinking of running for president, he said, “A pot-smoking socialist as president? We might get a third Obama term after all.’’

And, in keeping with tradition, he took a couple of shots at the press.

Praising keynote speaker Cecily Strong’s impersonation of CNN anchor Brook Baldwin, the president said, “Usually the only people impersonating journalists on CNN are journalists on CNN.’’

Speaking of the cold winter just passed, Obama said, “The polar vortex caused so many record lows, they renamed it MSNBC.”

Keegan-Michael Key of Comedy Central, appearing as the president’s “anger translator,’’ said, “We can count on Fox News to terrify old white people with some nonsense.’’

The president ended on a somber note, with a toast to “journalists we lost in the past year, journalists like Stephen Sotloff and James Foley, murdered for nothing more than trying to shine a light into some of the world’s darkest corners.”