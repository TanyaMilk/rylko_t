House Majority Leader Eric Cantor endorsed Mitt Romney and predicted   the former Massachusetts governor would win all of Virginia’s  Republican  delegates on Tuesday.

“I am endorsing Mitt Romney  in his candidacy for president of the United  States,” Mr. Cantor,   Virginia Republican, said on NBC’s “Meet the  Press.”

Mr.  Cantor said Mr. Romney is the only Republican presidential candidate   who has put forward a viable plan to create jobs and move the sluggish   economy forward.

“If you look at Mitt Romney’s economic plan,  what it does is it lowers  taxes for everyone who pays income taxes; it  will result in reduced red  tape for small businesses; it will help us  get back on track. That’s why  I think that Mitt Romney is the man for  this year,” he said.

Mr. Cantor downplayed Mr.  Romney’s struggle to win the support  of the GOP’s conservative base,  saying, “This hard-fought primary has  been full of all kinds of issues.”

“But one thing that can bring people together in this country is the   economy and jobs,” Mr. Cantor said. “This plan, this pro-growth plan   that Mitt Romney has put forward is something that I think the more   people look into the details and see that plan, the more they will rally   behind Mitt Romney.”