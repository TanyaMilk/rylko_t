The Islamic State terror group is operating a camp in the northern Mexican state of Chihuahua, just eight miles from the U.S. border, Judicial Watch reported Tuesday.

Citing sources that include a “Mexican Army field grade officer and a Mexican Federal Police Inspector,” the conservative watchdog group reported that the Islamic State, also known as ISIS or ISIL, is organizing only a few miles from El Paso, Texas, in the Anapra neighborhood of Juárez and in Puerto Palomas.

Judicial Watch sources said that “coyotes” working for the notorious Juarez Cartel are smuggling Islamic State terrorists across the U.S. border between the New Mexico cities of Santa Teresa and Sunland Park, as well as “through the porous border between Acala and Fort Hancock, Texas.”


															
															
																SEE ALSO: Texas officials say no credible information to confirm Judicial Watch report on Islamic State
															
															

“These specific areas were targeted for exploitation by ISIS because of their understaffed municipal and county police forces, and the relative safe-havens the areas provide for the unchecked large-scale drug smuggling that was already ongoing,” Judicial Watch reported.

Mexican intelligence sources say the Islamic State intends to exploit the railways and airport facilities in the vicinity of Santa Teresa, New Mexico.

“The sources also say that ISIS has ‘spotters’ located in the East Potrillo Mountains of New Mexico (largely managed by the Bureau of Land Management) to assist with terrorist border crossing operations,” Judicial Watch reported. “ISIS is conducting reconnaissance of regional universities; the White Sands Missile Range; government facilities in Alamogordo, NM; Ft. Bliss; and the electrical power facilities near Anapra and Chaparral, NM.”