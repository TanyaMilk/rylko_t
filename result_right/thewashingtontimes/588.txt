WASHINGTON (AP) — Americans are staying put more than at any time since World War II, as the housing bust and unemployment keep young adults at home and thwart older Americans’ plans for a beachfront or lakeside retirement.

New information from the Census Bureau is the latest indicator of economic trouble, after earlier signs that mobility was back on the upswing. It’s also a shift from America’s long-standing cultural image of ever-changing frontiers, dating to the westward migration of the 1800s and more recently in the spreading out of whites, blacks and Hispanics in the Sun Belt’s housing boom.

Rather than housing magnets such as Arizona, Florida and Nevada, it is now more traditional, densely populated states — California, Illinois, Massachusetts, New York and New Jersey — that are showing some of the biggest population gains in the recent economic slump, according to the data released Thursday.

Residents have been largely locked in place; families are stuck in devalued homes and young adults are living with parents or staying put in the towns where they went to college.

“The fact that mobility is crashing is something that I think is quite devastating,” said Richard Florida, an urban theorist and professor at the University of Toronto’s Rotman School of Management. He described America’s residential movement as a key element of its economic resilience and history, from development of the nation’s farmland in the Midwest to its coastal ports and homesteading in the West.

“The latest decline shows we are in a long-run economic reset and that we never really recovered — we’ve just been stagnating along,” Florida said.