Mitt Romney urged the country’s news editors on Wednesday to delve more deeply into what he said were President Obama’s secret second-term plans, telling them they have a duty to “do the seeking” to expose what the White House has in store.

He made the remarks to the American Society of News Editors, meeting in Washington, a day after Mr. Obama met with them and told them not to buy into an equivalence between the two parties, saying Democrats have remained mainstream but the Republicans have tilted far to the right of where they were a couple of decades ago.

The exchange marked the first time Mr. Obama and Mr. Romney, the likely Republican presidential nominee, have gone tit-for-tat in a dueling format like that. It gave both men a chance to work the referees, trying to sway the reporters and editors whose coverage will help determine which man is victorious in November.

“I find myself missing the presence of editors to exercise quality control. I miss the days of two or more sources for a story — when at least one source was actually named,” Mr. Romney told the editors, huddled for a convention in downtown Washington.

Mr. Romney even noted a decline in standards since his last run for the presidency, four years ago: “In 2008, the coverage was about what I said in my speech. These days, it’s about what brand of jeans I am wearing and what I ate for lunch.”

Mr. Obama told the editors that Mr. Romney and the Republican Party he seeks to lead have moved to the right and said it’s not fair to blame both sides equally for political gridlock in Washington.