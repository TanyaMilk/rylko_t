The U.S. Department of Justice has sent a member of its Community Relations Service team to investigate a Nebraska parade float that criticized President Obama.

A Fourth of July parade float featured at the annual Independence Day parade in Norfolk sparked criticism when it depicted a zombie-like figure resembling Mr. Obama standing outside an outhouse, which was labeled the “Obama Presidential Library.”

The Nebraska Democratic Party called the float one of the “worst shows of racism and disrespect for the office of the presidency that Nebraska has ever seen.”

PHOTOS: Eye-popping excuses in American political scandals

The Omaha World-Herald reported Friday that the Department of Justice sent a CRS member who handles discrimination disputes to a Thursday meeting about the issue.

Also at the meeting were the NAACP, Norfolk mayor Sue Fuchtman and the Norfolk Odd Fellows, which coordinated the parade.

The float’s creator, Dale Remmich, has said the mannequin depicted himself, not President Obama. He said he is upset with the president’s handling of the Veterans Affairs Department, the World-Herald reported.

PHOTOS: See Obama's biggest White House fails

“Looking at the float, that message absolutely did not come through,” said NAACP chapter president Betty C. Andrews.