The Islamic State is increasingly struggling to justify its sanctioning of slavery, extortion, smuggling and murder against scholars and religious leaders who say the extremist group’s Mafia-style tactics can’t be reconciled with Muslim law.

In the time since the Sunni Muslim group split from al Qaeda in Iraq in February, it has distinguished itself among terrorist organizations by embracing criminal activities as its main source of funding. Kidnapping and ransom, seizure and sale of black market oil and enslavement and sex trafficking of women and children have all netted profits for the organization, analysts say.

“Just about any criminal enterprise in that part of the world, they are into it,” said Bill Roggio, a senior fellow with the Foundation for Defense of Democracies. “They seem to be taking all the traditional routes of the Mafia organizations except the drugs and alcohol. Although if they really needed the money, I’m sure they’d justify it.”


															
															
																SEE ALSO: ‘Inherent Resolve’: Pentagon officially names mission against Islamic State
															
															

But to defend its actions and to lure recruits, the group — which has proved savvy in the management of its image and its message — has engaged in heavy-handed attempts to portray its cause as both authentically Muslim and sanctioned by Islamic law. The latest effort came this week in the form of an article in its slickly produced online magazine, Dabiq, that affirmed the group enslaved Yazidi women and children after its siege this summer of Mount Sinjar in Iraq — and then went on to defend the practice.

“One should remember that enslaving the families of the kuffar and taking their women as concubines is a firmly established aspect of the Shariah that if one were to deny or mock, he would be denying or mocking the verses of the Qur’an and the narrations of the Prophet,” the article states.

But scholars and religious leaders say the group, also known as ISIS or ISIL, misinterprets the Koran or uses passages outside the proper context to prop up its actions.