David Brooks, a columnist for The New York Times, said during a nationally televised interview on “Meet the Press” that whites need to bend over backwards to understand blacks when it comes to police and racism.

Why?

Because police and blacks have quite a history, some of which has pitted white-dominated law enforcement agencies against minorities, he said, Mediaite reported.

“We all have to have a new social compact on this,” Mr. Brooks said, Mediaite reported. “Whites especially have to acknowledge the legacy of racism and have to go the extra yard to show respect and understand how differently whites and blacks see police issues.”

Mr. Brooks said whites can’t simply discount blacks who see racism at play with police.

“So whites can’t just say, ‘Does this look right to me,’ but ‘Does this look trustworthy to the black community,’ ” he said, Mediaite reported. “That has to be the standard.”