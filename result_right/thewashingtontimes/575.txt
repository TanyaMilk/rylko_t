A bipartisan group of senators introduced a bill Wednesday aimed at  giving states more power to collect billions of dollars in sales taxes  on out-of-state Internet and catalog purchases.

While traditional brick-and-mortar retailers collect state, city and  other local sales taxes from customers who make purchases in their  stores and then pass the taxes on to the respective governments,  divergent state laws have let many online and catalog retailers avoid  collecting and passing on those same taxes.

The proposal, sponsored by five Democrats and five Republicans, is  designed to streamline the nation’s more than 7,500 sales-tax  jurisdictions and make it easier for states to collect sales taxes from  online and catalog purchases.

“This legislation would give states the ability to close the online  sales-tax loophole, created when out-of-state sellers don’t collect, and  purchasers don’t pay, the state sales tax - even though they still owe  it,” said Sen. Lamar Alexander, Tennessee Republican who co-sponsored  the bill. “This loophole subsidizes out-of-state businesses at the  expense of Tennessee businesses and subsidizes some taxpayers at the  expense of others.”

Online retail giant Amazon.com, which for years has fought measures at  the state level to collect sales taxes, recently agreed to begin  collecting the taxes and supports the Senate proposal.

Amazon “will work with Congress, retailers and the states to get this  bipartisan legislation passed,” said Paul Misener, Amazon vice president  for global public policy. “It’s a win-win resolution.”