The Boy Scouts of America will accept openly gay youths starting on   New Year’s Day, a historic change that has prompted the BSA to ponder a   host of potential complications — ranging from policies on tentmates  and  showers to whether Scouts can march in gay pride parades.

Yet  despite their be-prepared approach, BSA leaders are rooting for  the  change to be a non-event, comparable to another New Year’s Day in  2000  when widespread fears of digital-clock chaos to start the new  millennium  proved unfounded.

“My hope is there will be the same effect this  Jan. 1 as the Y2K  scare,” said Brad Haddock, a BSA national executive  board member who  chairs the policy implementation committee. “It’s  business as usual,  nothing happens and we move forward.”

Some  churches are dropping their sponsorship of Scout units because  of the  new policy and some families are switching to a new conservative   alternative called Trail Life USA. But massive defections haven’t   materialized and most major sponsors, including the Roman Catholic and   Mormon churches, are maintaining ties.

“There hasn’t been a whole  lot of fallout,” said Haddock, a lawyer  from Wichita, Kan. “If a church  said they wouldn’t work with us, we’d  have a church right down the  street say, ‘We’ll take the troop.’”

The new policy was approved  in May, with support from 60 percent of  the 1,400 voting members of the  BSA’s National Council. The vote  followed bitter nationwide debate, and  was accompanied by an  announcement that the BSA would continue to  exclude openly gay adults  from leadership positions.