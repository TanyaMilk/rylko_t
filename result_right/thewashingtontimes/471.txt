Former Pennsylvania Sen. Arlen Specter said Friday that ex-Senate colleague Rick Santorum is “so far to the right” that it’s not realistic for him to win the presidency.

“I do not think it is realistic for Rick Santorum to represent America,” Mr. Specter said on MSNBC. He said Mr. Santorum’s views are “so far to the right, with his attitude on women in the workplace, gays” and other issues.

The two men served together as Pennsylvania’s Republican senators until 2006, when Mr. Santorum lost a bid for reelection. Mr. Santorum is taking heat from conservatives in this year’s GOP presidential primary for supporting the reelection bid of the moderate Mr. Specter in 2004 over conservative Pat Toomey, who narrowly lost that primary. Mr. Toomey went on to win the Senate seat in 2010.

Mr. Santorum’s chief rival for the GOP nomination, former Massachusetts Gov. Mitt Romney, criticized Mr. Santorum in a debate in Arizona Wednesday for supporting Mr. Specter eight years ago. Mr. Santorum defended his allegiance to Mr. Specter, saying he had a “conversation” at the time with Mr. Specter about supporting the judicial nominees of Republican President George W. Bush, implying it was a quid pro quo for his endorsement. Mr. Specter was in line to become chairman of the Senate Judiciary Committee.

But Mr. Specter says the discussion never took place.

“That is flatly not true,” he told MSNBC’s Chuck Todd. “We never had any such conversation.” Mr. Specter said it would have been improper for him to give such assurances about judicial nominees.