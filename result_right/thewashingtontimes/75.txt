Under pressure from a large faction of rank-and-file members, Senate Republican leaders are warming to the idea of keeping the rule change known as the “nuclear option” that Democrats imposed to end filibusters of nominations.

Senate Minority Leader Mitch McConnell, a Kentucky Republican who will become majority leader in January, is leaving the decision to the will of his members, and at least half of them currently oppose reversing course, said GOP aides close to the discussions.

The leadership team this week began signaling that the new rule, which reduced the number of votes needed to cut off confirmation filibusters from 60 to 51, is likely here to stay.

“My view at the time [Democrats] did it was, if this rule changes, it’s likely never to revert back to where it was,” said Missouri Sen. Roy Blunt, vice chairman of the Republican Conference.

The rule change dramatically shifted power in the Senate, ending the institution’s 200-year tradition of granting substantial power to the minority party. The new rule eliminated the minority’s sway over nominations and all but assured that a president whose party holds the Senate majority would get nominees easily confirmed.

Sen. Orrin G. Hatch, Utah Republican, has emerged as the most outspoken proponent of using the new rule to the GOP’s advantage.