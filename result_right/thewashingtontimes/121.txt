As a new school year dawns, the U.S. Department of Agriculture is reminding of its new rules against Gatorade and caffeine-filled drinks in school cafeterias — and it’s a matter of national security, they say.

“Obesity is not just a health issue,” Kevin Concannon, the department’s undersecretary for food, nutrition and consumer services, said in a statement reported by National Review earlier this month. “It is an economic and national security issue.”

The department’s new rules come in the form of Smart Snacks in School nutrition standards, part of the fallout from the first lady Michelle Obama-fueled Healthy, Hunger-Free Kids Act of 2010. Under the rules, middle schools can’t provide kids with Gatorade, some tea drinks, lemonades, fruit punches or caffeinated beverages.

Some schools are taking those standards of healthy eating to even higher levels.

National Review reported that Elyria city schools in Ohio have banned the sale of signature pink cookies, ending a 40-year-old tradition. South Carolina’s Socastee High School students won’t be able to buy Chick-fil-A sandwiches any longer at school. And Banks County schools in Georgia will shutter their vending machines during breakfast and lunch hours.

Meanwhile, other healthy eating supporters have been pushing for even more regulatory controls on school fare: A Minnesota individual wanted to ban chocolate milk. A Massachusetts mom wanted to ban peanuts, the National Review reported.