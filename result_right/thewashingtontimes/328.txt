JANESVILLE, Wis. — Presumptive GOP nominee Mitt Romney campaigned alongside this state’s top Republicans on Monday, declaring that he’ll win the Badger State in the general election — a prediction that, if it turns out to be true, would be a bad omen for President Obama’s re-election plans.

Speaking to a crowd gathered inside a sweltering textile business, the former Massachusetts governor said that Mr. Obama is seriously mistaken if he thinks he can count on Wisconsin’s support come Election Day.

“I think President Obama has put this in his column, he just assumed from the very beginning that Wisconsin was gong to be his. But you know what? We are going to win Wisconsin and we are going to get the White House,” Mr. Romney said.

The remarks came during the the fourth day of Mr. Romney’s five-day, six-state bus tour through small cities and towns in the Rust Belt and the Midwest, where he is trying to gin up excitement among the rural, conservative voters that he will likely need to capture the White House.

Mr. Romney campaigned here with three of the state’s top elected Republicans — Rep. Paul Ryan, Sen. Ron Johnson, and Gov. Scott Walker — and Reince Priebus, another Wisconsin native who now is chairman of the Republican National Committee. All four men shared Mr. Romney’s optimistic tone.

“It is my honor to be on stage with the man I hope is the 45th president of these United States,” Mr. Walker said.