KABUL, Afghanistan (AP) — France’s plans to withdraw its combat  troops from Afghanistan a year early drew harsh words Saturday in the  Afghan capital, with critics accusing French President Nicolas Sarkozy  of putting domestic politics ahead of Afghans’ safety.

A wider  proposal by Sarkozy for NATO to hand over all security to Afghans by the  end of next year also came under fire, with one Afghan lawmaker saying  it would be “a big mistake” that would leave security forces unprepared  to fight the Taliban insurgency and threaten a new descent into violence  in the 10-year-old war.

Sarkozy’s decision, which came a week  after four French troops were shot dead by an Afghan army trainee  suspected of being a Taliban infiltrator, raises new questions about the  unity of the U.S.-led military coalition.

It also reopens the  debate over whether setting a deadline for troop withdrawals will allow  the Taliban to run out the clock and seize more territory once foreign  forces are gone.

“Afghan forces are not self-sufficient yet. They  still need more training, more equipment and they need to be stronger,”  said military analyst Abdul Hadi Khalid, Afghanistan’s former interior  minister.

Khalid said the decision by Sarkozy was clearly  political. Sarkozy’s conservative party faces a tough election this  year, and the French public’s already deep discontent with the Afghan  war only intensified when unarmed French troops were gunned down by an  Afghan trainee Jan. 20 at a joint base in the eastern province of  Kapisa.