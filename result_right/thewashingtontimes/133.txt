An overwhelming majority of Americans say they have deep worries that illegal immigration will erode the country’s culture and economy, revealed a new poll.

A Reuters/Ipsos poll showed that 70 percent of Americans — including 86 percent of Republicans — say illegal immigrants threaten traditional U.S. believes and customs, as well as jeopardize the economy.

The widespread fear emerged just as President Obama mulled executive action to circumvent Congress and ease immigration policy, likely granting legal status to 5 million undocumented residents and further limiting deportations.

Hispanic and liberal voters would welcome the executive action, but the poll suggested that Mr. Obama and his Democrats could suffer political backlash from much of the rest of the country.

About 63 percent of voters said the illegal immigrants place a burden on the country’s sluggish economy, which is already a hot-button issue in this year’s midterm elections, according to the poll.

The immigration debate, likely spurred by the crisis of more than 57,000 unaccompanied alien children inundating the U.S. so far this year, is resonating far from the southern border.

In New England, more voters oppose illegal immigration compared to the rest of the country. Nearly 80 percent of New Englanders said illegal immigrants threaten U.S. beliefs and customs, according to the poll.