Osama bin Laden is dead, but that won’t stop the 9/11 mastermind from playing a role in the 2012 presidential election.

Nearly a year after U.S. forces killed bin Laden in Pakistan, President Obama’s re-election team has rolled out a strategy to paint the Democratic incumbent as the stronger candidate on foreign policy issues, particularly when it comes to dealing with Islamic extremists.

His latest campaign ad features former President Bill Clinton praising Mr. Obama for making the difficult decision to order Navy SEALs into bin Laden’s compound on May 2, 2011. It then asks whether Republican presidential hopeful Mitt Romney would have made the same choice.

Vice President Joseph R. Biden has taken that same line of attack to the campaign trail, seeking to cut into the GOP’s traditional edge among voters on homeland security and defense matters.

“Thanks to President Obama, bin Laden is dead and General Motors is alive,” the vice president said in a Thursday stump speech. “You have to ask yourself, had Gov. Romney been president, could he have used the same slogan in reverse?”

Republicans are beating back those talking points.