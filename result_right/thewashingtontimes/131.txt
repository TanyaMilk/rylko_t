Conservative radio host Rush Limbaugh argued that Robin Williams committed suicide because of his “leftist world view.”

“What is the left’s world view in general?” Mr. Limbaugh asked listeners. “If you had to attach, not a philosophy, but an attitude to a leftist world view. It’s one of pessimism, and darkness, sadness. They’re never happy, are they?”

Mr. Williams’ body was found in the seated position after he hanged himself using a belt at his California home Monday. Marin County police said the actor died from “asphyxia due to hanging.”

PHOTOS: Celebrity deaths in 2014

Mr. Limbaugh pointed to a Fox News report that said Mr. Williams had struggled with financial issues and embarrassment from having to take TV roles after a sterling movie career.

“[Liberals] are animated in large part by the false promises of America, because the promises of America are not for everyone,” he said. “He had it all, but he had nothing. Made everybody else laugh, but was miserable inside. It fits a certain picture or a certain image that the left has. [I’m] talking about low expectations and general unhappiness.”

Fox News reported Mr. Williams suffered from “survivor’s guilt” after the deaths of Christopher Reeve, Andy Kaufman and John Belushi.