LONDON — When it comes to curry, some like it hot, with India’s signature dish as popular in London as it is in New Delhi.

But now the heat is on the British government for creating a curry crisis because of a shortage of Asian chefs, thanks to an immigration clampdown last year.

The government capped the number of foreigners migrating to Britain and set new minimum-wage rules for non-European residents that forced the closures of some Indian, Bangladeshi, Chinese and Thai restaurants.

“We’re facing an unprecedented crisis,” said Enam Ali, founder of the British Curry Awards.

Newspapers have published editorials of outrage, as Britons worry about the future of their favorite takeout.

Eric Pickles, a Conservative parliamentarian, said Britain has some of the finest Asian food in the world, which is “part of the very fabric of our national life.”