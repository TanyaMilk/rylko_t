BRUNSWICK, Ohio — Mitt Romney is trying to do something that Sen. John McCain, the Republican Party’s 2008 presidential nominee, could not — shore up enough support among the rural, blue-collar voters in such Rust Belt states as Pennsylvania and Ohio to win the White House.

The former Massachusetts governor chased after those voters over the Father’s Day weekend as part of a six-state bus tour in which he bought a meatball hoagie at a gas station outside Philadelphia. He also tested the axiom that the way to a person’s heart is through his stomach by serving pancakes to hungry voters in Cleveland suburbs.

Along with Florida, Ohio and Pennsylvania are the electoral battlegrounds that analysts say have been the key to the past three elections. If Republicans can win two of the three, they should find their path to the White House.

Ohio traditionally has been the easier win for the GOP among those two Northern states, but new polling putting Mr. Romney within striking distance in Pennsylvania has Republicans thinking they might be able to sweep all three.

Christopher P. Borrick, a political science professor at Muhlenberg College in Allentown, Pa., said Republicans dominated the 2010 elections in Pennsylvania and the party has been able to reverse some of the losses in party registration it experienced at the end of the past decade.

“I think that Romney has some potential to make some headway among suburban Philadelphia voters who are fiscally conservative and not very enamored with the president’s performance on the economy and budget matters,” Mr. Borrick said.