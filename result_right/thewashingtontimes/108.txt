Paula Deen sat down with Matt Lauer on Tuesday, more than one year since her tearful interview with the “Today” host amid a racism scandal that brought down her multimillion-dollar culinary empire.

“Words are so powerful. They can hurt. They can make people happy. Well, my words hurt people,” she said when asked about the lessons she’s learned over the past months. “They disappointed people and frankly, I disappointed myself. And for that, I’m so sorry, I’m so sorry for the hurt that I caused people because it went deep.”

The celebrity chef lost her Food Network show in 2013 after a leaked deposition revealed she admitted using the N-word three decades ago. A federal judge eventually threw out the lawsuit, but a number of businesses cut their ties with her. Mrs. Deen said she was disappointed that the final ruling “got almost no attention,” Today reported.

The TV chef is stepping back into the limelight, starting a new subscription-based online channel called the “The Paula Deen Network,” that will air her old cooking shows, which she bought from The Food Network.

Still, Mrs. Deen said the scandal has left her feeling more guarded.

“I’ve always been very, very naïve,” she said. “Unless you do something that I can see you’re trying to hurt me, in my heart, you’re good. That’s childlike and it’s naïve, and I’m trying to be more guarded. I don’t ever want to get to the point where I’m cynical because I believe there are more good people than bad.”