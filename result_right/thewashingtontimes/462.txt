BEIRUT (AP) — Syria blocked a Red Cross convoy Friday from delivering  badly needed food, medical supplies and blankets to a rebellious  neighborhood of Homs cut off by a monthlong siege, and activists accused  regime troops who overran the shattered district of execution-style  killings and a scorched-earth campaign.

Humanitarian conditions in the former rebel stronghold of Baba Amr have  been described as catastrophic, with extended power outages, shortages  of food and water, and no medical care for the sick and wounded.

British Prime Minister David Cameron called Homs “a scene of medieval barbarity.”

Syrian state TV showed burned-out and destroyed buildings in Baba Amr, a  western neighborhood of Homs, which was covered with a blanket of fresh  snow.

Syrian government forces took control of Baba Amr on Thursday after  rebels fled the district under constant bombardment that activists said  killed hundreds of people since early February. The Syrian regime has  said it was fighting “armed gangs” in Baba Amr, and had vowed to  “cleanse” the neighborhood.

“It is unacceptable that people who have been in need of emergency  assistance for weeks have still not received any help,” said Jakob  Kellenberger, president of the International Committee of the Red Cross.