Tony Blankley, a noted conservative commentator, Ronald Reagan speechwriter and former editorial page editor of The Washington Times, died late Saturday, leaving a legacy of significant analysis that bridged politics and culture with finesse, optimism and a sense of history.

He was 63 and had been battling stomach cancer.

At the time of his death, Mr. Blankley was an executive vice president of the Edelman public relations firm in Washington, a visiting senior fellow at the Heritage Foundation think tank, a syndicated newspaper columnist and an on-air political commentator for CNN, NBC and NPR.

He was also a regular weekly guest on “The McLaughlin Group.”

From 1990 to 1997, he served as press secretary and general adviser to House Speaker Newt Gingrich, ultimately earning a reputation among political friends and foes as one of Washington’s most genial, quick-witted and effective operatives.

Mr. Gingrich, campaigning in New Hampshire on Sunday for the Republican presidential nomination, called his former press secretary a “very dear friend” and a key part of the team behind the 1994 Contract With America.