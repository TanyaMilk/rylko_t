The conservative filmmaker who is producing a film about abortionist and convicted murderer Kermit Gosnell will be unveiling a play in Los Angeles this month based on the events surrounding the Ferguson police shooting of 18-year-old Michael Brown.

Phelim McAleer, an Irish journalist-turned-filmmaker, said he was inspired to create the play after learning the story that unfolded in Ferguson was much more complicated than what was presented to him by the U.K. media.

“If I’d been in the United States, I might have seen the details come out one by one,” he told Bloomberg. “In the U.K. the story was presented in a storybook way. From the moment I saw it, I thought: That’s a great, great story. Therefore, it’s too good to be true.”

“Ferguson,” which will run from April 26-29 at Los Angeles’s Odyssey Theater, adapts the grand jury materials from the case into an interactive drama, Bloomberg reported.

“It’s going to be a dramatized stage reading,” Mr. McAleer said. “Witnesses are going to describe what happened, just as they described it in the grand jury room. We’re going to have cast at least 13 people, playing 20 different characters. Very interestingly, the one cast member who won’t be there is Michael Brown, because he didn’t give evidence before the grand jury. He couldn’t. So he’ll be a spectral, ghostlike presence. He won’t be there, but his picture won’t be there. The documents accompany the testimony will be on a large screen.”

Like “Gosnell,” Ms. McAleer is raising funds for “Ferguson” via a crowd-sourcing campaign. In three days, he has raised more than $8,000.