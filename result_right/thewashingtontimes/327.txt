The State Department on Monday sharpened its criticism of Egypt’s ruling military council after it granted itself broad new powers as Egyptians voted in their first free presidential election since the ouster of authoritarian leader Hosni Mubarak last year.

“We’re particularly concerned by decisions that appear to prolong the military’s hold on power,” State Department spokeswoman Victoria Nuland said. “This is a critical moment in Egypt, and the world is watching closely.”

While votes in the presidential election were being counted Monday, Muslim Brotherhood candidate Mohammed Morsi claimed victory.

A series of developments in recent days, however, has cast doubt over the Brotherhood’s rise in Egypt, along with the nation’s overall transition to democracy.

The nation’s Supreme Council of the Armed Forces — a group of senior military officers who have filled the power vacuum in Cairo since Mr. Mubarak was overthrown 16 months ago — began granting itself new powers last week after a panel of Mubarak-appointed judges dissolved the nation’s elected parliament.

The military council went a step further Sunday night by issuing a new interim constitution that appears designed to cement the group’s grip on power.