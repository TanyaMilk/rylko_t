A  group of protesters in Ferguson, Mo., confronted civil  rights activist  Jesse Jackson in a McDonald’s parking lot, saying they  believed he was  in the town for his own gain and not to support  protesters.

“When you going to stop selling us out Jesse?” a  protester shouts at  Mr. Jackson in a video captured of the incident and  first posted by  conservative news site Western Journalism on Friday.

“We don’t want you here in St. Louis! When you gonna stop selling us out, Jesse?” the person continues.


															
															
																SEE ALSO: On day of national protest, evidence suggest Ferguson cop beaten badly before shooting
															
															

Mr.  Jackson largely remains silent during the confrontation, stating  only  that he is there to support the people of Ferguson and has met  with the  parents of Michael Brown, the unarmed black teenager who was  killed.

The  video makes it difficult to tell exactly how many people  confronted  Mr. Jackson, but one of them asks him “Are you marching today  with us,  or are you just going to sit in the car?”

At another point, a protester tells him, “You not a leader. If you not a leader we don’t want you here, brother.”