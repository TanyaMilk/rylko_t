DES MOINES, Iowa (AP) — Primary day at hand, fast-climbing Newt  Gingrich told South Carolinians on Saturday that he was “the only  practical conservative vote” able to stop front-runner Mitt Romney in  the GOP presidential race. Romney acknowledged the first-in-the-South  contest “could be real close” and prepared for an extended fight by  agreeing to two more debates in Florida, next on the election calendar.

Former Pennsylvania Sen. Rick Santorum braced for a setback and looked  ahead to the Jan. 31 contest after getting the most votes in Iowa and  besting Gingrich in New Hampshire. Texas Rep. Ron Paul made plans to  focus on states where his libertarian, Internet-driven message might  find more of a reception with voters; his campaign said it had purchased  a substantial ad buy in Nevada and Minnesota, which hold caucuses next  month.

The first contest without Texas Gov. Rick Perry, who dropped out this  past week and endorsed Gingrich, was seen as Romney’s to lose just days  ago. Instead, the gap closed quickly between the Massachusetts governor  who portrays himself as the Republicans best positioned to defeat  President Barack Obama and Gingrich, the confrontational former House  speaker from Georgia.

Romney avoided a run-in with Gingrich at Tommy’s Country Ham House,  where both had scheduled campaign events for the same time. Romney  stopped by the breakfast restaurant 45 minutes ahead of schedule. When  Gingrich arrived, just minutes after Romney’s bus left the parking lot,  he said: “Where’s Mitt?”

Earlier, Gingrich had a message for voters during a stop at The  Grapevine restaurant in Boiling Springs not long after the polls opened:  Come out and vote for me if you want to help deny Romney nomination.

He told diners who were enjoying plates of eggs and grits that he was  the “the only practical conservative vote” to the rival he called a  Massachusetts moderate. “Polls are good, votes are better,” he said.