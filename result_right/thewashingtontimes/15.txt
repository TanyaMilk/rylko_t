NASCAR has joined the widespread backlash against Indiana’s Religious Freedom Restoration Act, announcing on Tuesday that it condemns discrimination and “will continue to welcome all competitors and fans” at racing events in Indiana and elsewhere.

The auto-racing association, which is set to hold a major race in Indianapolis in July, said it is “disappointed” by the state’s recent legislation.

“We will not embrace nor participate in exclusion or intolerance,” NASCAR said, CNN reported. “We are committed to diversity and inclusion within our sport and therefore will continue to welcome all competitors and fans at our events in the state of Indiana and anywhere else we race.”

The statement came as Gov. Mike Pence said in a press conference that he would “fix” the law to clarify that it does not condone discrimination against gay people. 

Conservative Twitter users nascar-weighs-in-on-indiana-rfra-law-with-puzzling-statement/?utm_source=autotweet&utm_medium=twitter&utm_campaign=twitter” target=”_blank”>slammed NASCAR’s statement as unnecessary and oversimplistic. The act, which takes effect on July 1, prohibits laws that “substantially burden” a person’s freedom of religion unless the government can prove a compelling interest in imposing that burden.

“So NASCAR was thinking Indiana brownshirts were going to stand outside the track and prevent gay people from entering?” asked Nathan Wurtzel. “I’m trying to fathom what they possibly thought was going to happen as a result of the law.”