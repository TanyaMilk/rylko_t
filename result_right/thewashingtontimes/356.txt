To hear the Sage of Omaha tell it, newspapers may not be dead after all.

Billionaire superinvestor Warren Buffett on Thursday placed another bet on the industry as his company, Berkshire Hathaway, announced the purchase of 63 newspapers from Richmond-based Media General for $142 million. The company will add daily and weekly newspapers in Alabama, Virginia, North Carolina and South Carolina to its existing papers at the Omaha World-Herald Co.

Among the larger newspapers included in the transfer are the Richmond Times-Dispatch and North Carolina’s Winston-Salem Journal. Small-town community papers in the package include the Goochland Gazette in Virginia.

In a separate deal, Mr. Buffett’s company will provide about $445 million in loans and a credit line to Media General, getting in return warrants that could give Berkshire Hathaway a 20 percent interest in the company. Media General’s stock jumped by a third in trading Thursday, closing up $1.04 to $4.18.

Traditional newspapers have been hammered in the new online-dominated media market, and Mr. Buffett has warned in the past about the industry’s woes. A huge challenge has been the migration of advertising dollars - from classifieds to car ads - once claimed by local newspapers to online rivals.

But the Omaha-based investor also has made his fortune in large part on a number of contrarian bets in undervalued industries and has spoken often of his abiding affection for newsprint. Among Berkshire Hathaway’s current media holdings is a minority stake in The Washington Post Co., where Mr. Buffett once served on the board of directors.