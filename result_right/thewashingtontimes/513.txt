NORTH CHARLESTON, S.C. — Texas Gov. Rick Perry ended his bid for the  Republican presidential nomination Thursday morning and endorsed rival  Newt Gingrich, further crystallizing the race for the conservative  alternative to front-runner Mitt Romney.

Saying he saw no viable path to the party’s nomination, Mr. Perry  dropped out two days before the South Carolina primary, leaving Mr.  Gingrich and former Pennsylvania Sen. Rick Santorum to fight over the  mantle of the mainstream conservative alternative to Mr. Romney.

“I know when it’s time to make a strategic retreat,” Mr. Perry said.

The three-term Texas governor said Mr. Gingrich has “the heart of a  conservative reformer,” and he tried to reassure social conservatives that  the former House speaker, who has had turbulent personal history, can  lead the GOP.

“Newt is not perfect, but who among us is?” he said. “The fact is, there  is forgiveness for those who seek God, and I believe in the power of  redemption.”

Mr. Gingrich said he was “humbled and honored to have the support of my  friend Rick Perry,” and said it further clarifies the choice for South  Carolina voters this weekend.