Addressing protests over minorities killed by police officers, President Obama said racism is “deeply rooted” in the U.S. and that activists should keep pressing steadily in their demands for reform.

“This is something that is deeply rooted in our society, it’s deeply rooted in our history,” Mr. Obama said in an interview with BET, a portion of which was released Sunday. “When you’re dealing with something as deeply rooted as racism or bias … you’ve got to have vigilance but you have to recognize that it’s going to take some time, and you just have to be steady so you don’t give up when we don’t get all the way there.”

Mr. Obama held meetings at the White House last week with young civil-rights activists who are protesting the shooting death of an unarmed black teenager by a white police officer last August in Ferguson, Missouri.

PHOTOS: See Obama's biggest White House fails

And on Thursday, after a grand jury in New York refused to indict a white police officer for the choke-hold death of a black man, the president he wants to ensure that law-enforcement officials “are serving everybody equally.”

Demonstrators protested across the nation again Saturday night, in locations including New York City; Seattle; Portland, Oregon; Davidson, North Carolina; and Tampa.

“This isn’t going to be solved overnight,” Mr. Obama said in the BET interview, adding that America has made progress on civil rights over the past 50 years and that he believes the nation will eventually solve its problems with racism.