The apparent safety woes of the much-touted, all-electric Chevrolet  Volt touched off a firestorm on Capitol Hill on Wednesday morning, as  House Republicans charged that the Obama administration conspired with  General Motors Co. to conceal those risks from consumers while pushing  the vehicle as part of the “green” future.

“We have now created a question of trust. I think we’re protecting the  administration more than the American public,” said Rep. Mike Kelly,  Pennsylvania Republican and member of the House Oversight and Government  Reform subcommittee on regulatory affairs, which heard testimony  Wednesday from GM CEO Daniel Akerson and administration officials.

Mr. Kelly and others suspect that the National Highway Traffic Safety  Administration intentionally withheld concerns for several months that  the electric Volt’s lithium-ion battery could explode during a crash.

The car is a centerpiece of the planned market rebound for GM, which the  federal government rescued from the verge of bankruptcy in 2008 and  2009. Uncle Sam still owns about a quarter of the company’s stock.

In June, a Volt burst into flames while being stored in a warehouse  after safety tests. The NHTSA launched an investigation into the  incident, but it kept that probe under wraps until November and went  public only after news reports of the fire began to leak.

The administration completed its investigation last week and declared  that the Volt and other all-electric cars such as the Nissan Leaf do not  pose any greater risk than traditional gasoline-powered vehicles.