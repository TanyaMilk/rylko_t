Wrapping up a three-day marathon of oral arguments about President Obama’s health care overhaul, the Supreme Court heard arguments Wednesday on whether the rest of the law can stand on its own if the justices were to strike down the individual mandate requiring Americans to purchase insurance.

The nine justices seemed to agree that carving out the mandate would dramatically alter much of the law but disagreed on which route to take, with Republican-appointed judges appearing to lean toward scrapping the whole thing while Democrat-appointed judges said that’s a decision for Congress to make.

“It’s a choice between a wrecking operation, which is what you are requesting, or a salvage job,” Justice Ruth Bader Ginsburg told Paul Clement, an attorney for the states seeking to have the Affordable Care Act tossed out in its entirety. The more conservative approach, Justice Ginsburg said, would be to salvage, “rather than throwing out everything.”

The hearings on Mr. Obama’s signature domestic achievement kicked off Monday with the justices considering whether a 19th century tax law blocks them from deciding on the mandate until later, while on Tuesday they heard arguments on whether the mandate itself is constitutional.

On Wednesday, they heard 26 states’ challenge to the law’s massive expansion of Medicaid, as well as arguments about whether the individual mandate is so crucial that the rest of the law cannot survive without it.Both the administration and the challengers agree at least two parts of the law need the mandate in order to work, but they disagreed on whether to throw the rest of the law out, too.

The two dependent parts require insurance companies to cover anyone regardless of their age or how healthy they are. As insurers add sicker and older Americans onto their rolls, they are likely to face dramatic losses if they don’t also have healthy folks paying in, both sides say.