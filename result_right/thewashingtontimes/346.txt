After clinching the Republican presidential nomination Tuesday in Texas, Mitt Romney has emerged from a bloody primary slog running neck-and-neck with President Obama — putting him in the exact same position as the last two Republicans to lead their party into the general election.

“I am humbled to have won enough delegates to become the Republican Party’s 2012 presidential nominee,” Mr. Romney said after easily taking the Texas primary. “Whatever challenges lie ahead, we will settle for nothing less than getting America back on the path to full employment and prosperity.”

With the Texas win, the campaign that had struggled to catch fire is suddenly on a hot streak.

Five months before Election Day, the former Massachusetts governor is nipping at Mr. Obama’s heels in national polls and is generally viewed as the more trusted candidate when it comes to strengthening the nation’s economy, which has been the consuming issue for voters this campaign season.

On the flip side, Mr. Obama is better funded and holds a higher favorability rating, and more of his supporters say they’re fired up about his candidacy. To top it off, Mr. Romney is playing catch-up in most swing states.

“The Republican Party is firmly behind him, and he doesn’t need to worry anymore about the base, but he has been damaged a little bit with swing voters who are going to end up deciding the election,” said Tom Jensen, director of Democrat-leaning survey firm Public Policy Polling. “Romney’s biggest obstacle is that he has very little margin for error in the Electoral College.”