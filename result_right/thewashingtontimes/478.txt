Pay no attention to those projected delegate counts you’ve seen.

Though tallies show that former Massachusetts Gov. Mitt Romney is ahead of his GOP rivals in the race to capture the 1,144 delegates needed to sew up the party’s presidential nomination, they are misleading.

They don’t account for the pool of 180 delegates that have yet to be doled out by Iowa, Minnesota, Missouri, Colorado and Maine, all of which have voted — opening the door for a reshuffling of the leader board in the coming months.

“I believe all those delegates are in play,” said Saul Anuzis, former head of the Michigan Republican Party and candidate for Republican National Committee chairman. “So, it’s anyone’s ballgame in those states.”

In other words, the old mantra “a win’s a win” is hard to apply to the nomination race because a “win” carries different meanings among states — thanks to a complex web of rules governing the allocation of delegates to the national convention in Tampa, Fla., where the party will formally nominate its candidate in August.

Former Sen. Rick Santorum of Pennsylvania, for instance, has won four contests but officially has just three delegates committed to vote for him in Tampa. Those three came from his last-place finish in Nevada.