When it comes to Congress, the only number higher than voters’ disapproval of its members is the rate at which they re-elect those same lawmakers. With sitting members of Congress backed by cash from Washington-based trade groups and party leaders, running against an incumbent traditionally has seemed like an exercise in futility.

But activists are using a new tool to disrupt the status quo: super PACs that could spend enough money across the country to knock off low-performing members of Congress in low-turnout, little-discussed primaries.

With the vast majority of districts a lock for one party or the other, the party primaries — 10 states hold elections or caucuses Tuesday — are where the face of Congress really takes shape.

“Almost 90 percent of districts are one-party dominated. The only chance for a competitive election is in the primaries, but 9 out of 10 eligible voters don’t participate,” said Curtis Ellis, a spokesman for the Campaign for Primary Accountability, a super PAC that has been the most active independent group in Tuesday’s primaries, according to federal records.

Those races also are the most ignored, except by those with an interest in maintaining the status quo.

“County committeemen, people who depend on patronage from the party, government jobs. Those are the only people who participate, and they’re going to pull the lever faithfully for who the party leaders tell them to vote for — and that will always be the incumbent,” Mr. Ellis said.