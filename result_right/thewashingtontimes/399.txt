BERLIN — The eurozone’s conservative leaders are bracing themselves as France, an economic giant, prepares to elect a Socialist amid their efforts to impose austerity measures to stanch Europe’s fiscal crisis.

Socialist candidate Francois Hollande’s election would “create a strong shock and a lot of turmoil in Brussels and in Berlin,” said Philippe Moreau-Defarges, a political scientist at the French Institute of International Relations in Paris.

President Nicolas Sarkozy holds a slight lead over Mr. Hollande in polls for the first round of voting Sunday, but the Socialist has commanded a significant lead over the conservative incumbent for the second round on May 6.

Mr. Hollande has promised to create public-sector jobs, increase taxes on the rich and “dominate” the financial sector.

That message has enthused French voters and caused concern beyond the borders.

“What many people [outside France] think but won’t say is that the French people are crazy,” Mr. Moreau-Defarges said, adding that the prevailing mood in Europe is against high public spending.