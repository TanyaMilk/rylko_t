CANNES, France (AP) — Leaders of the world’s 20 most powerful  economies failed to agree on how to increase the firepower of the  International Monetary Fund, so that it can help stem the European debt  crisis, though they acknowledged its resources should be boosted.

The leaders struggled to reach concrete resolutions at their summit in  the French resort of Cannes that has focused on Greece’s political  turmoil and worries about Italy that are threatening the world economy.

“It’s important that the IMF sees its resources reinforced,” European Commission President Jose Manuel Barroso told reporters.

He said that Italy had also asked the IMF for help monitoring its  budgetary and structural reforms. Doubts have grown over whether Italian  Prime Minister Silvio Berlusconi will have the political strength to  implement promised reform measures meant to revive the country’s  lackluster economy and bring down its massive debt.

Leaders of the world’s biggest economies were focusing on strengthening  the IMF as they scrambled for ways to help Europe contain its raging  debt crisis without worsening their own money troubles. European and  non-European countries disagree about how to better use the IMF — the  institution that was set up as the lender of last resort for struggling  governments after World War II — to help.

With their own finances already stretched from bailing out Greece,  Ireland and Portugal — and traditional allies like the United States  wrestling with their own problems — eurozone countries are looking to  the IMF to use its resources and rescue experience to help prevent the  debt crisis from spreading to large economies like Italy and Spain.