Nelson Mandela, who spent 27 years in prison for his armed militancy against South Africa’s apartheid regime only to emerge as a global icon for peaceful resistance and become his nation’s first black president, died Thursday in Johannesburg after a long illness. He was 95.

South African President Jacob Zuma made the announcement at a news conference late Thursday, saying “we’ve lost our greatest son” and calling the Nobel Peace laureate “Mandiba,” the traditional clan name of Mr. Mandela.

At the White House, President Obama said Mr. Mandela “achieved more than could be expected of any man. Today, he has gone home. And we have lost one of the most influential, courageous, and profoundly good human beings that any of us will share time with on this earth.


															
															
																SEE ALSO: Nelson Mandela now ‘belongs to the ages’
															
															

PHOTOS: Nelson Mandela, anti-apartheid icon and statesman

“He no longer belongs to us. He belongs to the ages,” Mr. Obama said.

Mr. Mandela was surrounded by his family as a gaggle of reporters and TV cameras crowded near his house. He was hospitalized in June with a lung infection and released in September, but he had struggled with ill health for years.

Mr. Mandela won the Nobel Peace Prize in 1993 and was elected president a year later. He was widely regarded in South Africa as the “Father of the Nation,” but critics denounced him as a terrorist and communist sympathizer.