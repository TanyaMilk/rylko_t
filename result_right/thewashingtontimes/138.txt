Retired brain surgeon and noted conservative Ben Carson, who has resisted calls for him to make a run for the presidency in 2016, said that he’s now softened his stance on the issue.

“I certainly didn’t give it much thought early on,” he told Fox News. “[But] there’re just hundreds of people [who are] so enthusiastic” about the idea.

“So I am starting to think about it,” Mr. Carson said. “But it certainly wasn’t on my bucket list when I retired.”

Mr. Carson made national headlines at the National Prayer Breakfast in 2013, when he bluntly criticized government policies while President Obama sat just feet away.

He’s since become a leader among the conservative crowd, and many have started a campaign to press him to run in 2016. He’s previously said that he would only run if felt compelled by God — a viewpoint he usually expressed in a tone that suggested he wasn’t interested in the presidency.

Just a few days ago, Mr. Carson won a straw poll for president at the Western Conservative Summit in Colorado.