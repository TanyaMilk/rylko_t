DETROIT — He chairs one of Capitol Hill’s most powerful committees, won his 2010 race with 62 percent of the vote and even boasts a niece who graced Sports Illustrated’s swimsuit-edition cover. But all that hasn’t saved Rep. Fred Upton of Michigan from a strong Republican primary challenge fueled by restive party conservatives who say he is too moderate.

Mr. Upton, chairman of the House Energy and Commerce Committee, was first elected to Congress in 1986 and has won re-election easily 12 times, usually pulling in 60 percent or more of the vote from the Kalamazoo-area 6th Congressional District.

But his GOP challenger from 2010, former state lawmaker Jack Hoogendyk, is taking on the veteran congressman again in the Aug. 7 primary. This year, the tea party-backed favorite has garnered increasing support from staunchly conservative groups, including the powerful Club for Growth, the Eagle Forum and the Madison Project political action committee.

Mr. Upton’s energy policy is “anti-free market,” and his record is “marred by fiscal and social liberal votes that are too numerous to count,” the Madison Project said in endorsing Mr. Hoogendyk.

“At a time when we are lambasting [President] Obama for his ‘Solyndra-style’ energy policy, this is not the sort of Republican we need to represent us,” said Madison Project political director Drew Ryun, referring to the failed solar-panel company.

Eagle Forum head Phyllis Schlafly said: “Now more than any other time in our history, we need solid conservatives, not just ‘go along to get along’ politicians in Congress.”