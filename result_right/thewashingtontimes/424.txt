PYONGYANG, NORTH KOREA (AP) - Five teenagers in school uniforms hold accordions. On the wall is a giant painting of the secret mountain hideout of their nation’s founder, Kim Il Sung. Small red stickers on their instruments mark them as gifts from Kim Jong Il.

Yes, this is North Korea. But as they grind their accordions into song, what comes out is no somber ode to either of the late leaders. Instead, as more than 1.5 million YouTube viewers already know, it’s one of the poppiest of 1980s pop songs, A-ha’s “Take on Me.”

The three young men and two women perform with gusto, swaying to the music, tapping their accordions and clapping their hands overhead. Their catchy cover, recorded in December, became a sensation as it challenged the world’s preconceptions about North Koreans.

After taking their arrangement to Norway to perform at an Arctic arts festival, lead player Choe Hyang Hwa and fellow band members gave The Associated Press a peek into their lives at the Kumsong school in Pyongyang.

Outside, a gaggle of students marched across the school yard in twos, arms thrown around one another and lunch pails swinging from their hands. They walked past a huge mosaic depicting North Korea’s founder, Kim Il Sung, and his son and successor, Kim Jong Il, with students working at computer terminals.

Inside, one young woman received a private lesson on the kayagum, a traditional Korean stringed instrument, while her teacher played a traditional drum called a janggo.