Republican presidential candidate Sen. Rand Paul offered sympathy to Baltimore police Tuesday, blaming the community’s lack of fathers and morals for its violent unrest.

“The police have to do what they have to do, and I am very sympathetic to the plight of the police in this,” the Kentucky senator said in an interview with conservative radio host Laura Ingraham, Talking Points Memo reported.

At least 20 police officers have been wounded since Monday in riots stemming from the April 19 death of Freddie Gray, a 25-year-old black man who died after suffering a spinal injury while in police custody.

Mr. Paul explained what he believes has caused the city to erupt into chaos.

“There are so many things we can talk about,” he said, “the breakdown of the family structure, the lack of fathers, the lack of a moral code in our society.”

He added that “this isn’t just a racial thing.”