LE MARS, Iowa (AP) — The volatile Republican presidential race  in Iowa will come down to which way an enormous chunk of undecided  voters breaks in the coming days.

With the first-in-the-nation  voting of the 2012 race for the White House looming Tuesday, Mitt Romney  is contending for victory in a state that eluded him four years ago,  while Rick Santorum — a hero among social conservatives — surges and  libertarian-leaning Ron Paul slides in a contest that remains incredibly  fluid.

With many factors at play, the dynamics can shift rapidly.

Yet,  two things were clear on the final weekend before the caucuses: The  yearlong effort to establish a consensus challenger to Romney had so far  come up short, and Romney’s carefully laid plan to survive Iowa may  succeed because conservative voters have so far failed to unite behind  one candidate.

“It may be Romney’s to lose at this point,” said  John Stineman, an Iowa GOP campaign strategist. “And it’s a battle among  the rest.”

Underscoring the unpredictability of the race, a new  poll by The Des Moines Register showed that a remarkable 41 percent of  likely caucus-goers say they were undecided or still might change their  minds.