President Obama’s latest jobs initiative — a pledge to accelerate expansion plans for five ports along the Eastern seaboard — is getting rave reviews from an unexpected corner: a handful of Republicans usually sharply critical of the president but who have also fought hard in recent years for federal dollars to help ready their ports for the flood of shipping and commerce expected by the Panama Canal’s expansion.

At times, Republicans representing some of the East Coast ports in question have faced resistance from within their own party for requesting federal help.

But GOP lawmakers such as Sens. Lindsey Graham and Jim DeMint of South Carolina support the accelerated government approval schedule for the port projects, which they say will be a boon for their state’s struggling economy. Mr. DeMint, one of the chamber’s most conservative members, backs the president’s plan despite working in the past to kill attempts to earmark money for the Port of Charleston.

“This is very good news and a recognition the rest of the country now understands what we’ve known all along — Charleston Harbor deepening is a critical project for our state, region and national economy,” Mr. Graham said. “It is a vital economic engine which must be deepened so it can handle 24/7 the new, larger post-Panamax ships coming online.”

Chris Crawford, a spokesman for Rep. Jack Kingston, a Republican who represents Savannah, Ga., the home to another port-expansion project, also applauded the administration effort.

“We’re definitely happy to get the attention and hope this will help speed along any regulatory bumps we might run into along the way,” he said.