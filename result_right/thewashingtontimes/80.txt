Ben Stein argued Monday that the “real problem” with race in America is the “pathetic, self-defeating black underclass” that’s plagued by drugs and gangs.

“You would think, if you read the liberal mainstream media, that the main problem with race in America was poor innocent black people being set upon and mistreated by the police. That’s just nonsense,” the conservative author said on Newsmax TV, Mediaite reported.

“I mean, the real problem with race in America is a very, very beaten-down, pathetic, self-defeating black underclass that just can’t seem to get its way going in the way that blacks were able to before the scourge of drugs and the scourge of gangs,” Mr. Stein said.

His comments came one week after he accused President Obama of being the most racist president in American history.

“I mean, it’s an amazing thing — blacks were on their way in this country, even after the horrors of slavery, and then drugs came in, the destruction of families came in, and the crisis in the black community is just absolutely unbelievable,” Mr. Stein told Newsmax. “And that, it seems to me, is something that Mr. Obama could have addressed, and he has ignored it completely.”