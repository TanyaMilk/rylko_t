COLUMBUS, Ohio — Mitt Romney and Rick Santorum wrapped up their final full day of campaigning here before Tuesday’s pivotal Republican presidential primary here, delivering their respective closing arguments as Mr. Santorum’s stock dipped in the polls and Mr. Romney brushed off new questions about his support for a federal health care mandate.

The former Massachusetts governor kicked off his day at a steel manufacturer in Canton, where he argued that Republicans should be focused on strengthening the economy. His 25 years of experience in the business world, he said, give him the tools needed to tackle the nation’s economic and employment woes.

“I look at this campaign right now, and I see a lot of folks talking about lots of things, but what we need to talk about to defeat Barack Obama is getting good jobs and scaling back the size of government — and that’s what I do,” Mr. Romney said in an apparent swipe at Mr. Santorum, a former senator from Pennsylvania who has often touted his socially conservative views on abortion, gay marriage and religion on the campaign trail.

“I keep bringing it back to more jobs, less debt and smaller government. That is what my campaign is about — and that is why I think we are doing well in this state,” Mr. Romney said.

Mr. Santorum, though, showed no signs of backing off on social issues, arguing at campaign stops that the nation cannot afford to ignore problems created by the breakdown of American families. In a conference call with reporters and on the campaign stump, Mr. Santorum made the case that Mr. Romney was for a federal health care mandate before he was against it — making him an unreliable conservative.

“What you have with Gov. Romney is someone who is simply not the genuine article. He’s not someone you can trust on the issue of big government,” Mr. Santorum said in the conference call.