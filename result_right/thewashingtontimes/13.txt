Muslim bakers and florists have flown under the media radar during the recent uproar over Christian-owned businesses and gay rights, but a hidden-camera video may have changed that.

The video showing Muslim bakers in Michigan reluctant to bake a cake for a gay wedding went viral last weekend, snaring more than 2.2 million views in three days and igniting debate over whether Christian business owners are being singled out for lawsuits, complaints and media focus.

That, of course, was the point of the video. Steven Crowder, conservative comedian and host of the podcast Louder with Crowder, said he believes that the Muslim bakers are well within their rights to refuse the cake-baking job — and so are Christian bakers.

“I’m not even saying these Muslim bakeries shouldn’t have a right to do whatever they did — they absolutely should — and many more of them would than Christian bakeries,” said Mr. Crowder in comments on the video.

This month’s fierce national debate over religious-freedom bills, particularly in Arkansas and Indiana, has increased the attention on Christian-owned businesses, which suddenly find themselves the subject of intense and potentially ruinous media focus.

“There is a witch-hunt now for Christian business owners,” said conservative radio talk-show host Dana Loesch on her Thursday program.