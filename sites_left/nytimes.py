#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('nytimes', ('bit.ly', 'nyti.ms', 'nytimes.com', 'www.nytimes.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.find_class('story-body')
    if len(div) == 0:
        return ''
    
    div = div[0]
    data = ''

    for p in div.iterfind('p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
