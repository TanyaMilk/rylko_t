#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('newsday', ('bit.ly', 'nwsdy.li', 'newsday.com', 'www.newsday.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.find_class('p402_premium')
    
    if len(div) == 0:
        div = parser.find_class('gatedView')

        if len(div) == 0:
            return ''

    data = ''
    div = div[0]

    for p in div.iterfind('p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
