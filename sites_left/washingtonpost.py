#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('washingtonpost', ('bit.ly', 'wapo.st', 'washingtonpost.com', 'www.washingtonpost.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    article = parser.cssselect('article')
    if len(article) == 0:
        return ''

    article = article[0]
    data = ''

    for p in article.iterfind('p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
