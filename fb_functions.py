#!/usr/bin/python
# coding: utf-8

import re
import urlparse
import requests
import facebook

from requests.exceptions import *

FACEBOOK_ACCESS_TOKEN = '1596855083929386|R1uoWWnIPPm_xGOpvjBkqig-R6k'


def facebook_get_links(page_name, hostnames):
    graph = facebook.GraphAPI(FACEBOOK_ACCESS_TOKEN)
    page = graph.get_object(page_name)

    while True:
        try:
            posts = graph.get_connections(page['id'], 'posts')
            break
        except facebook.GraphAPIError:
            print "GraphAPI error, trying again"
            pass


    flag = True
    link = ''
    while flag:
        # Perform some action on each post in the collection we receive from
        # Facebook.
        if 'data' not in posts.keys():
            break

        for post in posts['data']:
            links = []

            if 'link' in post.keys():
                links.append(post['link'])

            if 'description' in post.keys():
                link_regexp = r'http:\/\/[A-Za-z0-9/._-]+'
                links += re.findall(link_regexp, post['description'])

            for link in links:
                if urlparse.urlparse(link).netloc in hostnames:
                    yield link

        # Attempt to make a request to the next page of data, if it exists.
        while True:
            try:
                print "Getting next page"
                link = posts['paging']['next']
                posts = requests.get(link).json()
                break
            except KeyError:
                flag = False
                break
            except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout):
                print ">>> Error while fetching next page, trying again"
                pass
