#!/usr/bin/python
# coding: utf-8


import os
import codecs
import urllib2
import urlparse
import re
import requests
import lxml

from requests.exceptions import *
from functions import get_keywords_list, found_kw

from sites_left import chroncom, dallasmorningnews, denverpost, nydailynews, newsday, latimes, sanfranciscochronicle, detroitfreepress
from sites_right import nationalreview, outsidethebeltway, weeklystandard, americanthinker, dailycaller, voiceofamerica, cityjournal
from sites_neutral import pittsburghpostgazette, thechicagosuntimes, christiansciencemonitor, theoregonian



class FoundEnough(Exception): pass


def get_content(link):
    try:
        req = requests.get(link)
        text = req.text
        req.close()

        return text
    except (RequestException, ConnectionError, HTTPError, URLRequired, TooManyRedirects, Timeout):
        return None


def count_tokens(article):
    return len(re.findall(r"[a-zA-Z']+", article))


def parse_site(name, links_function, parse_function):
    global result_dir, current, max_count
    articles_count = 0

    for link in links_function():

        if articles_count == max_count:
            print "%s статей найдено." % max_count
            return

        print u"Разбираем статью %s" % link,

        try:
            article_html = get_content(link)
            if article_html is None:
                print
                continue
        except requests.exceptions.ConnectionError:
            print u"HTTP error"
            continue

        try:
            article = parse_function(article_html)
            print u"(длина статьи - %s символов)" % len(article)
        except (lxml.etree.ParserError, ValueError, lxml.etree.XMLSyntaxError):
            print u"parse error"
            continue
        except ValueError:
            print u"value error"
            continue

        if len(article) == 0:
            continue

        for kw in keywords:
            if found_kw(kw, article):
                print u"    В статье %s найдена фраза \"%s\"" % (link, kw)
                filename = "{}-{}-{}.txt".format(current, name, articles_count+1)

                f = codecs.open(os.path.join(result_dir, filename), 'w', 'utf-8')
                f.write(article)
                f.close()
                articles_count += 1
                break


result_dir = 'test_material'
max_count = 6

keywords = get_keywords_list('left')
current = 'left'

parse_site('chroncom', chroncom.links, chroncom.parse)
parse_site('dallasmorningnews', dallasmorningnews.links, dallasmorningnews.parse)
parse_site('nydailynews', nydailynews.links, nydailynews.parse)
parse_site('newsday', newsday.links, newsday.parse)
parse_site('latimes', latimes.links, latimes.parse)
parse_site('sanfranciscochronicle', sanfranciscochronicle.links, sanfranciscochronicle.parse)
parse_site('detroitfreepress', detroitfreepress.links, detroitfreepress.parse)

keywords = get_keywords_list('right')
current = 'right'

parse_site('nationalreview', nationalreview.links, nationalreview.parse)
parse_site('outsidethebeltway', outsidethebeltway.links, outsidethebeltway.parse)
parse_site('weeklystandard', weeklystandard.links, weeklystandard.parse)
parse_site('americanthinker', americanthinker.links, americanthinker.parse)
parse_site('dailycaller', dailycaller.links, dailycaller.parse)
parse_site('voiceofamerica', voiceofamerica.links, voiceofamerica.parse)
parse_site('cityjournal', cityjournal.links, cityjournal.parse)

keywords = get_keywords_list('left') + get_keywords_list('right')
current = 'neutral'
max_count = 10

parse_site('pittsburghpostgazette', pittsburghpostgazette.links, pittsburghpostgazette.parse)
parse_site('thechicagosuntimes', thechicagosuntimes.links, thechicagosuntimes.parse)
parse_site('christiansciencemonitor', christiansciencemonitor.links, christiansciencemonitor.parse)
parse_site('theoregonian', theoregonian.links, theoregonian.parse)
