# coding: utf-8

def found_kw(kw, article):
    return article.lower().find(kw.lower().strip()) != -1


def found_kw_tuple(kw, article):
    if kw[1] == 'keyword':
        return found_kw(kw[0], article)
    else: 
        return found_kw(kw[0], article) or found_kw(make_short_name(kw[0]), article)


def get_keywords(typ):
    infile = open('keywords-%s.txt' % typ)
    raw_text = infile.read()
    infile.close()
    keywords = raw_text.decode("utf-8").strip().split("\n")

    return keywords


def get_names(typ):
    infile = open('names-%s.txt' % typ)
    raw_text = infile.read()
    infile.close()
    names = raw_text.decode("utf-8").strip().split("\n")
    return names


def make_short_name(name):
    name_short = ''
    for name_part in name.split()[:-1]:
        name_short += name_part[0] + '. '
    return name_short + name.split()[-1]


def get_keywords_list(typ):
    keywords = get_keywords(typ)
    names = get_names(typ)
    short_names = [make_short_name(x) for x in names]

    return keywords + names + short_names
