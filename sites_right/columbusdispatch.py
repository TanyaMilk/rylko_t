#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('columbusdispatch', ('bit.ly', 'dispatch.com', 'www.dispatch.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.get_element_by_id('story-body', None)

    if div is None:
        return ''

    data = ''
    for p in div.iterfind('p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
