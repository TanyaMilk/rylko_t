#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('outsidethebeltway', ('bit.ly', 'nblo.gs', 'outsidethebeltway.com', 'www.outsidethebeltway.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.find_class('entry-content')
    if len(div) > 0:
        div = div[0]
    else:
        return ''

    data = ''
    for p in div.xpath('p|blockquote/p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
