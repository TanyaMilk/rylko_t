#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('wsj', ('bit.ly', 'wsj.com', 'www.wsj.com', 'blogs.wsj.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.get_element_by_id('wsj-article-wrap', None)

    if div is None:
        div = parser.find_class('post-content')
        if len(div) > 0: # blogs.wsj.com
            div = div[0]
        else:
            return ''

    data = ''
    for p in div.iterfind('p'):
        if p.text_content():
            data += p.text_content() + '\n\n'

    return data.strip()
