#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('AmericanThinker', ('bit.ly', 'americanthinker.com', 'www.americanthinker.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    divs = parser.find_class('article_body')
    if len(divs) == 0:
        return ''

    for div in divs:
        data = ''
        for p in div.iterfind('p'):
            if p.text_content():
                data += p.text_content() + '\n\n'

    return data.strip()
