#!/usr/bin/python
# coding: utf-8

import lxml.html
from fb_functions import facebook_get_links


def links():
    for link in facebook_get_links('newsmax', ('bit.ly', 'newsmax.com', 'www.newsmax.com')):
        yield link


def parse(article_html):
    parser = lxml.html.document_fromstring(article_html)
    div = parser.get_element_by_id('mainArticleDiv', None)

    if div is None:
        return ''

    data = div.text_content()
    if data.find('Related Stories:') != -1:
        data = data.split('Related Stories:')[0]

    return data.strip()
