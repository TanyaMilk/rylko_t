# coding: utf-8

import os
import codecs
import functions

from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

def get_directory_by_kw(typ, keyword, material):
    rpl = {'anti-war movements': 'civil rights movement',
           'environmental movements': 'civil rights movement',
           'hippie movement': 'civil rights movement',
           'anti-globalization movement': 'civil rights movement',
           'anti-capital punishment movement': 'civil rights movement',
           'green politics': 'Green Party',
           'green anarchism': 'Green Party',
           'stem-cell research': 'contraceptives',
           'social progressivism': 'egalitarianism',
           'eco-socialism': 'egalitarianism',
           'islamic socialism': 'egalitarianism',
           'buddhist socialism': 'egalitarianism',
           'american abolitionist': 'egalitarianism',
           'marxian economics': 'Keynesian economics',
           'right to choose abortion': 'LGBT rights',
           'anti-clerical': 'separation of church and state',
           'secularization': 'separation of church and state',
           'abolition of slavery': 'social equality',
           'cognitive liberty': 'social equality',
           'alter-globalization': 'welfare state',

           'against adultery': 'against homosexuality',
           'against euthanasia': 'against homosexuality',
           'against abortion': 'against homosexuality',
           'anti-euthanasia': 'anti-abortion', 
           'anti-homosexuality': 'anti-abortion', 
           'anti-adultery': 'anti-abortion',
           'paleoconservative': 'conservative',
           'heritage of a culture': 'cultural norms', 
           'cultural pride': 'cultural norms',
           'social welfare policies': 'economic freedom',
           'the strong state': 'economic freedom',
           'the market economy': 'economic freedom',
           'limited welfare state': 'economic freedom',
           'heritage of a nation': 'family values', 
           'heritage of a culture': 'family values', 
           'national values': 'family values',
           'traditional families': 'family values',
           'the market economy': 'free trade',
           'modernity': 'hierarchy',
           'clericalism': 'individualism', 
           'anti-democracy': 'individualism', 
           'laissez-faire capitalism': 'individualism', 
           'corporatism': 'individualism', 
           'traditionalism': 'individualism',
           'national strength': 'nationalism',
           'reactionaries': 'nationalists', 
           'christian democrats': 'nationalists', 
           'classical liberals': 'nationalists', 
           'paleoconservatives': 'nationalists',
           'social catholic right': 'property rights', 
           'privatisation': 'property rights',
           'natural law': 'rule of law',
           'social stratification': 'social inequality',
           'social conformity': 'social inequality', 
           'social progress': 'social inequality',
           'political legitimacy': 'tradition',
           'heritage of a nation': 'tradition',
           'heritage of a culture': 'tradition',
           'traditionalism': 'tradition',
           'national values': 'tradition',
    }

    pth = os.path.join('result_%s_sorted'%typ, keyword)
    material = material.lower()
    if os.path.exists(pth):
        return [keyword,], material


    if (keyword.lower().find('party') != -1) or (keyword.lower().find('committee') != -1):
        exists = os.listdir('result_%s_sorted'%typ)
        parties = [x for x in exists if (x.lower().find('party') != -1) or (x.lower().find('committee') != -1)]
        result = parties

    elif keyword in functions.get_names(typ):
        exists = os.listdir('result_%s_sorted'%typ)
        names = [x for x in functions.get_names(typ) if x in exists]
        result = names

    elif keyword.lower() in rpl.keys():
        result = rpl[keyword.lower()]
        material = material.replace(keyword.lower(), result.lower())
    else:
        print keyword
        print type(keyword)
        print sorted(functions.get_names(typ))
        raise ValueError

    if isinstance(result, str):
        result = [result,]
    return result, material


material_dir = 'test_material'

keywords  = [(x, 'keyword', 'left') for x in functions.get_keywords('left')] 
keywords += [(x, 'name', 'left') for x in functions.get_names('left')]
keywords += [(x, 'keyword', 'right') for x in functions.get_keywords('right')] 
keywords += [(x, 'name', 'right') for x in functions.get_names('right')]

for f in sorted(os.listdir(material_dir)):
    fobj = codecs.open(os.path.join(material_dir, f), 'r', 'utf-8')
    material = fobj.read()
    fobj.close()

    flag = False
    result = 0
    result_typ = ''

    for kw in keywords:
        keyword = kw[0]
        typ = kw[2]
        result_dir_sorted = 'result_%s_sorted'%typ

        if functions.found_kw_tuple(kw, material):
            flag = True

            existing_keywords, material = get_directory_by_kw(typ, keyword, material)

            articles_filenames = []
            for existing_keyword in existing_keywords:
                kw_directory = os.path.join('result_%s_sorted'%typ, existing_keyword)
                articles_filenames += [os.path.join(kw_directory, x) for x in os.listdir(kw_directory)]

            articles = []
            for filename in articles_filenames:
                fobj = codecs.open(filename, 'r', 'utf-8')
                articles.append(fobj.read())
                fobj.close()

            if len(articles) > 1:
                vectorizer = TfidfVectorizer(analyzer='word')
                articles_features = vectorizer.fit_transform(articles)
                material_feature = vectorizer.transform([material,])

                cls = KMeans(n_clusters=min(8, len(articles)))
                clusters_indexes = cls.fit_predict(articles_features)
                closest_index = cls.predict(material_feature)[0]

                clustered_group = []
                clustered_group_features = []
                for i, index in enumerate(clusters_indexes):
                    if index == closest_index:
                        clustered_group.append(articles[i])
                        clustered_group_features.append(articles_features[i])

                metrics = [cosine_similarity(material_feature, x) for x in clustered_group_features]
                metrics.sort()
                metrics.reverse()
            else:
                clustered_group = [articles[0],]
                clustered_group_features = [articles_features[0],]
                metrics = [cosine_similarity(material_feature, articles_features[0]),]

            if metrics[0][0][0] > result:
                result = metrics[0][0][0]
                if typ == 'left':
                    result_typ = 'левая'
                else:
                    result_typ = 'правая'
    
    if flag:
        if result < 0.54:
            comment = 'пристрастность отсутствует'
        elif result < 0.6:
            comment = 'небольшой процент пристрастности'
        elif result < 0.7:
            comment = 'средний процент пристрастности'
        else:
            comment = 'большой процент пристрастности'
        print "Для файла %s результат: %s, %s (%s)" % (f, result, comment, result_typ)
    else:
        print "Статья %s не обсуждает темы, которые могли бы иметь пропагандистский характер" % f
